import smartpy as sp

class EuroTz(sp.Contract):
    '''
    Constructor: called in deployment process
    '''
    def __init__(self, tokenName, symbol, decimals, admin, eventSinkContractAddress, chainID):
        sp.default_verify_message=sp.string("No entry")
        self.init(  name = tokenName, 
                    decimals = decimals, 
                    symbol = symbol, 
                    administrator = admin, 
                    paused = False, 
                    balances = sp.big_map(tkey = sp.TAddress), 
                    totalSupply = 0, 
                    eventSinkContractAddress = eventSinkContractAddress,
                    transferAgents = sp.set(t = sp.TAddress),
                    superTransferAgents = sp.set(t = sp.TAddress),
                    nonces = sp.big_map(tkey = sp.TAddress, tvalue = sp.TInt),
                    chainID = chainID
                 )
                 
    '''
    euroTz SC Modifiers
    '''             
    # self explanatory             
    def onlyAdmin(self):
        # message = "Only admin allowed"  
        sp.verify((sp.sender == self.data.administrator), message = "01")   
    
    # contracts management modifier 
    def onlyAdminOrSuperTransferAgent(self):
        # message = "Only admin or Super transfer agent are allowed"
        sp.verify(( (sp.sender == self.data.administrator) | (self.data.superTransferAgents.contains(sp.sender))), message = "02")
    
    # transfer/transferFeeless modifier
    def onlyAdminOrTransferAgent(self):
        # message = "Only admin or Transfer agent are allowed"
        sp.verify(((sp.sender == self.data.administrator) | self.data.transferAgents.contains(sp.sender)), message = "03")
            
    # approvals function modifier
    def onlySelfOrWithRole(self, params):
        
        sp.if ((sp.sender == self.data.administrator) | (~self.data.paused & (params.f == sp.sender))):
            pass
        sp.else:
            sp.if self.data.superTransferAgents.contains(sp.sender):
                pass
            sp.else:
                sp.if self.data.transferAgents.contains(sp.sender):
                    sp.if (sp.sender == params.t):
                        pass
                    sp.else:
                        # message = "TransferAgent is only allowed to approve to his address"
                        sp.failwith("25")
                sp.else:
                     # message = "Not allowed to approve from this account"
                     sp.failwith("04")
                    
    # resetAllAllowances/resetAllowance modifier
    def onlySelfOrAdminOrSueprTransferAgent(self, params):
        # message = "Only account Owner, Admin or SuperTransferAgent are allowed"
        sp.verify(((sp.sender == self.data.administrator) | (sp.sender == params.address) | self.data.superTransferAgents.contains(sp.sender)), message = "05")
    
    # only account owner or approved account can call the transfer entrypoint
    def onlySelfOrApproved(self, params):
        # message = "Contract is paused."
        sp.verify(~self.data.paused, message = "06")
        
        sp.if (params.f == sp.sender):
            pass
        sp.else:
            # message = "Caller not approved"
            sp.verify((self.data.balances[params.f].approvals.contains(sp.sender) | self.data.balances[params.f].approvals.contains(sp.source)), message = "07")
            
            # message = "Amount allowed smaller than amount to transfer."
            sp.verify(((self.data.balances[params.f].approvals[sp.sender] >= params.amount) | (self.data.balances[params.f].approvals[sp.source] >= params.amount)), message = "08")
    '''
    Views entrypoints
    '''
            
    def _getTotalSupply(self, params):
        
        c = sp.contract(
                        t = sp.TInt, 
                        address = params.viewTotalSupplyContractAddress, 
                        ).open_some()
        sp.transfer(self.data.totalSupply, sp.mutez(0), c)
    
    
    def _getBalance(self, params):
        
        # message = user not found.
        sp.verify(self.data.balances.contains(params.address), message = "32")
        
        userBalance = self.data.balances[params.address].balance
        
        c = sp.contract(
                        t = sp.TInt, 
                        address = params.viewBalanceContractAddress, 
                        ).open_some()
        sp.transfer(userBalance, sp.mutez(0), c)
    
    def _getAllowance(self, params):
        
        # message = user not found.
        sp.verify(self.data.balances.contains(params.owner), message = "32")
        
        # message = spender not found in the owner's approvals map.
        sp.verify(self.data.balances[params.owner].approvals.contains(params.spender), message = "33")
        
        allowance = self.data.balances[params.address].approvals[params.spender]
        
        c = sp.contract(
                        t = sp.TInt, 
                        address = params.viewAllowanceContractAddress, 
                        ).open_some()
        sp.transfer(allowance, sp.mutez(0), c)            
        
    '''
    Internal functions
    '''
    
    def addAddressIfNecessary(self, address):
        sp.if ~ self.data.balances.contains(address):
            self.data.balances[address] = sp.record(balance = 0, approvals = {})
    
    def addInNoncesIfNecessary(self, address):
        sp.if ~ self.data.nonces.contains(address):
            self.data.nonces[address] = sp.int(0)  
    
    def initialiseAllowance(self, params):
        sp.if ~self.data.balances[params.f].approvals.contains(params.t):
            self.data.balances[params.f].approvals[params.t] = 0
                        
    def callEventSinkContractTransfer(self, params):
        entryPointName = "transferEvent"
        transferParamsRecord = sp.record(amount = params.amount, fromAddress = params.fromAddress, toAddress = params.toAddress)
        c = sp.contract(
                        t = sp.TRecord(amount = sp.TIntOrNat, fromAddress = sp.TOption(sp.TAddress), toAddress = sp.TOption(sp.TAddress)), 
                        address = self.data.eventSinkContractAddress, 
                        entry_point = entryPointName
                        ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
        
        
    def callEventSinkContractApprove(self, params):
        approvalParamsRecord = sp.record(amount = params.amount, owner = params.owner, spender = params.spender)
        entryPointName = "approvalEvent"
        c = sp.contract(
                        t = sp.TRecord(amount = sp.TIntOrNat, owner = sp.TAddress, spender = sp.TAddress), 
                        address = self.data.eventSinkContractAddress, 
                        entry_point = entryPointName
                        ).open_some()
        sp.transfer(approvalParamsRecord, sp.mutez(0), c)
        
    def checkSignature(self, params):
        # Check for signature
        #  message= "Wrong signature"
        sp.verify(sp.check_signature(params.k, params.s, params.b), message= "20")
    
    def checkNonce(self, f, nonce):
        # Add only if the sender uses the Feeless for the first time
        self.addInNoncesIfNecessary(f)
        
        # Nonce verification
        # message = "Invalid nonce"
        sp.verify((self.data.nonces[f] == nonce), message = "19")
    
    def checkChainMatch(self):
        sp.verify_equal(self.data.chainID, sp.chain_id, message = "27")    
    
    def unpackOperationData(self, operationbytes):
        # Unpack data in order to get: from, to, amount, nonce
        dataRecordType = sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TPair(sp.TAddress, sp.TPair(sp.TAddress, sp.TAddress))))
        
        dataRecord = sp.unpack(
            operationbytes, 
            t = dataRecordType
            ).open_some()
        
        amount = sp.fst(dataRecord)
        nonce = sp.fst(sp.snd(dataRecord))
        from_ = sp.fst(sp.snd(sp.snd(dataRecord)))
        to_ = sp.fst(sp.snd(sp.snd(sp.snd(dataRecord))))
        contractAddress = sp.snd(sp.snd(sp.snd(sp.snd(dataRecord))))
        
        operationData = sp.record(amount = amount, nonce = nonce, from_ = from_, to_ = to_, contractAddress = contractAddress)
        
        return operationData
    
    def checkSameContract(self, contractAddress):
        thisAddress = sp.to_address(sp.self)
        
        sp.verify((thisAddress == contractAddress), message = "35")
    
    
    '''
    Internal main entrypoints
    '''
        
        
    def _mint(self, params):
        self.addAddressIfNecessary(params.address)
        self.data.balances[params.address].balance += params.amount
        self.data.totalSupply += params.amount
        self.callEventSinkContractTransfer(sp.record(amount = params.amount, fromAddress = sp.none, toAddress = sp.some(params.address)))
        
    def _burn(self, params):
        # message = "amount to burn must be smaller than user balance"
        sp.verify(self.data.balances[params.address].balance >= params.amount, message = "09")
        self.data.balances[params.address].balance -= params.amount
        self.data.totalSupply -= params.amount
        self.callEventSinkContractTransfer(sp.record(amount = params.amount, fromAddress = sp.some(params.address), toAddress = sp.none))
        
    def _transfer(self, params):
        self.addAddressIfNecessary(params.t)
        
        # message = "Amount to transfer must be smaller than user balance."
        sp.verify(self.data.balances[params.f].balance >= params.amount, message = "10")
        
        self.data.balances[params.f].balance -= params.amount
        self.data.balances[params.t].balance += params.amount
        
        sp.if (params.f != sp.sender):
            self.data.balances[params.f].approvals[sp.sender] -= params.amount 
        
        self.callEventSinkContractTransfer(sp.record(amount = params.amount, fromAddress = sp.some(params.f), toAddress = sp.some(params.t)))
        
    def _approve(self, params):
        # message =  "amount to approve must be greater than zero"
        sp.verify(params.amount > 0, message =  "11")
        
        '''
        We'll use this approach one SmartPy fix a bug.
        alreadyApproved = self.data.balances[params.f].approvals.get(params.t,defaultValue =  0)
        sp.verify(alreadyApproved == 0, message =  "29" )
        '''
        
        sp.if self.data.balances[params.f].approvals.contains(params.t):
            sp.if self.data.balances[params.f].approvals[params.t] > 0:
                # message = account already allowed to spend > 0 amount.
                sp.failwith("29")

        self.data.balances[params.f].approvals[params.t] = params.amount
        
        self.callEventSinkContractApprove(sp.record(amount = params.amount, owner = params.f, spender = params.t))
        
    def _increaseAllowance(self, params):
        # message = "amount to increase must be greater than zero"
        sp.verify(params.amount > 0, message = "12")
        
        # message = "impossible to increase allowance of unexistant account"
        sp.verify(self.data.balances[params.f].approvals.contains(params.t), message = "13")
        
        # increase allowance
        self.data.balances[params.f].approvals[params.t] += params.amount
        
        self.callEventSinkContractApprove(sp.record(amount = params.amount, owner = params.f, spender = params.t)) 
    
    def _decreaseAllowance(self, params):
        #  message =  "amount to decrease must be greater than zero"
        sp.verify(params.amount > 0, message =  "14")
        
        # message = "impossible to decrease allowance of unexistant account"
        sp.verify(self.data.balances[params.f].approvals.contains(params.t), message = "15")
        
        # message = "amount to decrease must be smaller than the one already approved"
        sp.verify((self.data.balances[params.f].approvals[params.t] > params.amount), message = "16")
        
        # decrease allowance
        self.data.balances[params.f].approvals[params.t] -= params.amount
        
        self.callEventSinkContractApprove(sp.record(amount = params.amount, owner = params.f, spender = params.t))
    
    def _resetAllAllowances(self, params):
        #sp.for approvedAddress in self.data.balances[params.address].approvals.keys():
            #(del self.data.balances[params.address].approvals[approvedAddress]
        sp.if (sp.len(self.data.balances[params.address].approvals.keys()) > 0):
            self.data.balances[params.address].approvals = {}  
        sp.else:
            # message = "Approvals map is already empty"
            sp.failwith("26")
    
    
    def _resetAllowance(self, params): 
        # message = "Not approved or already reseted"
        sp.verify((self.data.balances[params.address].approvals.contains(params.f) & (self.data.balances[params.address].approvals[params.f] > 0)), message = "17")
        del self.data.balances[params.address].approvals[params.f]      
    
    '''
    Internal Feeless entrypoints
    '''
    
    def _transferFeeless(self, params):

        # verify that the id of the network currently evaluating the transaction is the same where the SC was deployed.
        self.checkChainMatch()
        
        # Check for signature
        self.checkSignature(params)
                
        operationData = self.unpackOperationData(params.b)
        
        self.checkSameContract(operationData.contractAddress)
                
        # Check nonce
        self.checkNonce(operationData.from_, operationData.nonce)
        
        # Add only new receiver to contract's storage in Balances bigMap    
        self.addAddressIfNecessary(operationData.to_)

        # Verify balance sufficiency
        # message = "Insufficient balance"
        sp.verify((self.data.balances[operationData.from_].balance >= operationData.amount), message = "18")
        
        # Increment user's nonce
        self.data.nonces[operationData.from_] += 1
        
        # Update balances
        self.data.balances[operationData.from_].balance -= operationData.amount
        self.data.balances[operationData.to_].balance += operationData.amount
        
        # Call eventSink contract
        self.callEventSinkContractTransfer(sp.record(amount = operationData.amount, fromAddress = sp.some(operationData.from_), toAddress = sp.some(operationData.to_)))
        
    def _approveFeeless(self, params):
        
        # verify that the id of the network currently evaluating the transaction is the same where the SC was deployed.
        self.checkChainMatch()
        
        # Check for signature
        self.checkSignature(params)
        
        operationData = self.unpackOperationData(params.b)
        
        self.checkSameContract(operationData.contractAddress)
        
        # Check nonce
        self.checkNonce(operationData.from_, operationData.nonce)
            
        # message =  "amount to approve must be greater than zero"
        sp.verify(operationData.amount > 0, message =  "11")
        
        '''
        alreadyApproved = self.data.balances[params.f].approvals.get(params.t,defaultValue =  0)
        sp.verify(alreadyApproved == 0, message =  "29" )
        '''
        
        sp.if self.data.balances[operationData.from_].approvals.contains(operationData.to_):
            sp.if self.data.balances[operationData.from_].approvals[operationData.to_] > 0:
                # message = account already allowed to spend > 0 amount.
                sp.failwith("29")
            
        # Increment user's nonce
        self.data.nonces[operationData.from_] += 1    
            
        # overwrite allowance
        self.data.balances[operationData.from_].approvals[operationData.to_] = operationData.amount
        
        # Call eventSink contract
        self.callEventSinkContractApprove(sp.record(amount = operationData.amount, owner = operationData.from_, spender = operationData.to_))
    

    def _increaseAllowanceFeeless(self, params):
        
        # verify that the id of the network currently evaluating the transaction is the same where the SC was deployed.
        self.checkChainMatch()
        
        # Check for signature
        self.checkSignature(params)
        
        operationData = self.unpackOperationData(params.b)
        
        self.checkSameContract(operationData.contractAddress)
        
        # Check nonce
        self.checkNonce(operationData.from_, operationData.nonce)
        
        # message =  "amount to increase must be greater than zero"
        sp.verify(operationData.amount > 0, message =  "12")
        
        # message = "impossible to increase allowance of unexistant account"
        sp.verify(self.data.balances[operationData.from_].approvals.contains(operationData.to_), message = "13")
            
        # Increment user's nonce
        self.data.nonces[operationData.from_] += 1    
            
        # increase allowance
        self.data.balances[operationData.from_].approvals[operationData.to_] += operationData.amount
        
        # Call eventSink contract
        self.callEventSinkContractApprove(sp.record(amount = operationData.amount, owner = operationData.from_, spender = operationData.to_))
    
    def _decreaseAllowanceFeeless(self, params):
        
        # verify that the id of the network currently evaluating the transaction is the same where the SC was deployed.
        self.checkChainMatch()
        
        # Check for signature
        self.checkSignature(params)
        
        operationData = self.unpackOperationData(params.b)
        
        self.checkSameContract(operationData.contractAddress)
        
        # Check nonce
        self.checkNonce(operationData.from_, operationData.nonce) 
        
        # message =  "amount to decrease must be greater than zero"
        sp.verify(operationData.amount > 0, message =  "14")
        
        # message = "impossible to decrease allowance of unexistant account"
        sp.verify(self.data.balances[operationData.from_].approvals.contains(operationData.to_), message = "15")
            
        # message = "amount to decrease must be smaller than the one already approved"
        sp.verify((self.data.balances[operationData.from_].approvals[operationData.to_] > operationData.amount), message = "16")
            
        # Increment user's nonce
        self.data.nonces[operationData.from_] += 1    
            
        # increase allowance
        self.data.balances[operationData.from_].approvals[operationData.to_] -= operationData.amount
        
        # Call eventSink contract
        self.callEventSinkContractApprove(sp.record(amount = operationData.amount, owner = operationData.from_, spender = operationData.to_))    
      
    
    '''
    Entrypoints / External functions
    '''
    
    '''
    External contract's management entrypoints
    '''  
    
    @sp.entry_point
    def changeEventSinkContractAddress(self, newEventSinkContractAddress):
        self.onlyAdmin()
        self.data.eventSinkContractAddress = newEventSinkContractAddress     
    
    @sp.entry_point
    def setPause(self, params):
        self.onlyAdmin()
        self.data.paused = params

    @sp.entry_point
    def setAdministrator(self, params):
        self.onlyAdmin()
        self.data.administrator = params    
            
    @sp.entry_point
    def addTransferAgent(self, params):
        self.onlyAdminOrSuperTransferAgent()
        # message = "Transfer agent exists already"
        sp.verify(~ self.data.transferAgents.contains(params.address), message = "21") 
        self.data.transferAgents.add(params.address)
        
    @sp.entry_point
    def deleteTransferAgent(self, params):
        self.onlyAdminOrSuperTransferAgent()
        # message = "Transfer agent doesn't exist"
        sp.verify(self.data.transferAgents.contains(params.address), message = "22")
        self.data.transferAgents.remove(params.address)
    
    @sp.entry_point
    def addSuperTransferAgent(self, params):
        self.onlyAdmin()
        # message = "Super transfer agent exists already"
        sp.verify(~ self.data.superTransferAgents.contains(params.address), message = "23")
        self.data.superTransferAgents.add(params.address)
        
    @sp.entry_point
    def deleteSuperTransferAgent(self, params):
        self.onlyAdmin()
        # message = "Super transfer agent doesn't exist"
        sp.verify(self.data.superTransferAgents.contains(params.address), message = "24")
        self.data.superTransferAgents.remove(params.address)
        
    '''
    External main entrypoints
    ''' 
    
    @sp.entry_point
    def mint(self, params):
        
        # verify that entrypoint's caller is the admin
        self.onlyAdmin()
        
        self._mint(params)

    @sp.entry_point
    def burn(self, params):
        
        # verify that entrypoint's caller is the admin
        self.onlyAdmin()
        
        self._burn(params)
        
    
    @sp.entry_point
    def transfer(self, params):
        self.onlySelfOrApproved(params)
        
        self._transfer(params)
        
    @sp.entry_point
    def approve(self, params):
        self.onlySelfOrWithRole(params)

        self._approve(params)
    
   
    @sp.entry_point
    def increaseAllowance(self, params):
        self.onlySelfOrWithRole(params)

        self._increaseAllowance(params)
    
    @sp.entry_point
    def decreaseAllowance(self, params):
        self.onlySelfOrWithRole(params)

        self._decreaseAllowance(params) 
    

    @sp.entry_point
    def resetAllAllowances(self, params):
        self.onlySelfOrAdminOrSueprTransferAgent(params)

        self._resetAllAllowances(params)
    
    @sp.entry_point
    def resetAllowance(self, params):
        self.onlySelfOrAdminOrSueprTransferAgent(params)

        self._resetAllowance(params)       
    
        
    '''
    Feeless entrypoints
    
    '''
    
    @sp.entry_point
    def transferFeeless(self, params):
    
        # verify that entrypoint's caller is the admin or a transferAgent
        self.onlyAdminOrTransferAgent()
        
        self._transferFeeless(params)
    

    @sp.entry_point
    def approveFeeless(self, params): 
        
        # verify that entrypoint's caller is the admin or a transferAgent
        self.onlyAdminOrTransferAgent()
        
        self._approveFeeless(params)
        
    
    @sp.entry_point
    def increaseAllowanceFeeless(self, params):
        
        # verify that entrypoint's caller is the admin or a transferAgent
        self.onlyAdminOrTransferAgent()
        
        self._increaseAllowanceFeeless(params)
    
    @sp.entry_point
    def decreaseAllowanceFeeless(self, params):
        
        # verify that entrypoint's caller is the admin or a transferAgent
        self.onlyAdminOrTransferAgent()
        
        self._decreaseAllowanceFeeless(params)
        
    
    @sp.entry_point
    def getTotalSupply(self, params): 
        self._getTotalSupply(params)
    
    @sp.entry_point
    def getBalance(self, params):   
        self._getBalance(params)
    
    @sp.entry_point
    def getAllowance(self, params):  
        self._getAllowance(params)
        

if "templates" not in __name__:
    @sp.add_test(name = "euroTz")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("euroTz Contract")

        admin = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")
        alice = sp.address("tz1-alice-address-43287")
        bob = sp.address("tz1-bob-address-43287")
        fred = sp.address("tz1-fred-address-43287")
        daly = sp.test_account("Daly")
        thib = sp.address("tz1-thib-address-1234")
        
        transferAgent = sp.address("tz1-transferAgent-address-4321")
        transferAgentBis = sp.address("tz1-transferAgentBis-address-4321")
        
        superTransferAgent = sp.address("tz1-superTransferAgent-address-4321")
        superTransferAgentBis = sp.address("tz1-superTransferAgentBis-address-4321")
        
        otherEventSinkContractAddress = sp.address("KT1-otherEventSinkContractAddress-1234")
        
        eventSinkContractAddress = sp.address("KT1RzokejsxyURUBeaGoB9DMeqeWz89YJBjy")
        tokenName = sp.string("TezosTkNext")
        symbol = sp.string("euroTz")
        decimals = sp.nat(2)
        
        # carthagenet ID: NetXjD3HPJJjmcd => 0x9caecab9
        chainID = sp.chain_id_cst("0x9caecab9")

        c1 = EuroTz(tokenName, symbol, decimals, admin, eventSinkContractAddress, chainID)

        scenario += c1

        
        
        scenario.h2("** Mint EntryPoint tests **")
        scenario.h3("Admin mints 70 euroTz to Alice")
        scenario += c1.mint(address = alice, amount = 70).run(sender = admin)
        scenario.verify(c1.data.balances[alice].balance == 70)
        scenario.h3("Admin mints 2 euroTz to Bob")
        scenario += c1.mint(address = bob, amount = 2).run(sender = admin)
        scenario.verify(c1.data.balances[bob].balance == 2)
        scenario.h3("Admin mints 28 euroTz to Daly")
        scenario += c1.mint(address = daly.address, amount = 28).run(sender = admin)
        scenario.verify(c1.data.balances[daly.address].balance == 28)
        scenario.h3("Non Admin tries to mint 10 euroTz to Bob")
        scenario += c1.mint(address = bob, amount = 12).run(sender = alice, valid = False)
        scenario.verify(c1.data.totalSupply == 100)
        scenario.h2("** End Mint tests **")
        
        scenario.h2("** AddSuperTransferAgent EntryPoint Test **")
        scenario.h3("Admin adds two superTransferAgents")
        scenario += c1.addSuperTransferAgent(address = superTransferAgent).run(sender = admin)
        scenario += c1.addSuperTransferAgent(address = superTransferAgentBis).run(sender = admin)
        scenario.h3("Non Admin tries to add a superTransferAgent")
        scenario += c1.addSuperTransferAgent(address = fred).run(sender = alice, valid = False)
        scenario.h2("** End AddSuperTransferAgent EntryPoint Test **")
        
        scenario.h2("** AddTransferAgent EntryPoint Test **")
        scenario.h3("Admin adds two transferAgents")
        scenario += c1.addTransferAgent(address = transferAgent).run(sender = admin)
        scenario += c1.addTransferAgent(address = transferAgentBis).run(sender = superTransferAgent)
        scenario.h3("Non Admin tries to add a transferAgent")
        scenario += c1.addTransferAgent(address = fred).run(sender = alice, valid = False)
        scenario.h2("** End AddTransferAgent EntryPoint Test **")
        
        
        
        scenario.h2("** DeleteSuperTransferAgent EntryPoint Test **")
        scenario.h3("Admin deletes transferAgentBis")
        scenario += c1.deleteSuperTransferAgent(address = superTransferAgentBis).run(sender = admin)
        scenario.h3("Non Admin tries to delete a superTransferAgent")
        scenario += c1.deleteSuperTransferAgent(address = superTransferAgent).run(sender = alice, valid = False)
        scenario.h2("** End DeleteSuperTransferAgent EntryPoint Test **")
        
        scenario.h2("** DeleteTransferAgent EntryPoint Test **")
        scenario.h3("Admin deletes transferAgentBis")
        scenario += c1.deleteTransferAgent(address = transferAgentBis).run(sender = admin)
        scenario.h3("Non Admin tries to delete a transferAgent")
        scenario += c1.deleteTransferAgent(address = transferAgent).run(sender = alice, valid = False)
        scenario.h2("** End DeleteTransferAgent EntryPoint Test **")
        
        
        scenario.h2("** Transfer/Approve EntryPoint tests **")
        
        scenario.h3("Admin tries to transfer 8 euroTz from Alice to Bob without approve himSelf to send")
        scenario += c1.transfer(f = alice, amount = 8, t = bob).run(sender = admin, valid = False)
        
        scenario.h3("Admin approves his self to transfer 8 euroTz from Alice account")
        scenario += c1.approve(f = alice, amount = 8, t = admin).run(sender = admin)
        scenario.h3("Admin transfers 8 euroTz from Alice to Bob")
        scenario += c1.transfer(f = alice, amount = 8, t = bob).run(sender = admin)
        scenario.verify(c1.data.balances[alice].balance == 62)
        scenario.verify(c1.data.balances[bob].balance == 10)
        
        scenario.h3("Alice transfers 4 euroTz to Bob")
        scenario += c1.transfer(f = alice, t = bob, amount = 4).run(sender = alice)
        scenario.verify(c1.data.balances[alice].balance == 58)
        scenario.verify(c1.data.balances[bob].balance == 14)
        
        scenario.h3("Transfer Agent tries to transfer 6 euroTz from Alice to Bob without approve himSelf to send")
        scenario += c1.transfer(f = alice, t = bob, amount = 6).run(sender = transferAgent, valid = False)
        
        scenario.h3("Transfer Agent tries to approve someone to send from Alice account 6 euroTz")
        scenario += c1.approve(f = alice, t = bob, amount = 6).run(sender = transferAgent, valid = False)
        
        scenario.h3("Transfer Agent approves himself to send 6 euroTz from Alice account")
        scenario += c1.approve(f = alice, t = transferAgent, amount = 6).run(sender = transferAgent)
        
        scenario.h3("Transfer Agent transfers 6 euroTz from Alice to Bob")
        scenario += c1.transfer(f = alice, t = bob, amount = 6).run(sender = transferAgent)
        
        scenario.verify(c1.data.balances[alice].balance == 52)
        scenario.verify(c1.data.balances[bob].balance == 20)
        
        
        scenario.h3("Bob tries to transfer from Alice but he doesn't have her approval")
        scenario += c1.transfer(f = alice, t = bob, amount = 4).run(sender = bob, valid = False)
        scenario.verify(c1.data.balances[alice].balance == 52)
        scenario.verify(c1.data.balances[bob].balance == 20)
        
        
        scenario.h3("Alice approves Bob and Bob transfers")
        scenario += c1.approve(f = alice, t = bob, amount = 5).run(sender = alice)
        scenario += c1.transfer(f = alice, t = bob, amount = 4).run(sender = bob)
        scenario.verify(c1.data.balances[alice].balance == 48)
        scenario.verify(c1.data.balances[bob].balance == 24)
        
        scenario.h3("Alice tries to approve an other time Bob while he was already approved")
        
        scenario += c1.approve(f = alice, t = bob, amount = 5).run(sender = alice, valid = False)
        
        
        scenario.h3("SuperTransferAgent approves Bob and Bob transfers")
        scenario += c1.increaseAllowance(f = alice, t = bob, amount = 3).run(sender = superTransferAgent)
        scenario += c1.transfer(f = alice, t = bob, amount = 3).run(sender = bob)
        scenario.verify(c1.data.balances[alice].balance == 45)
        scenario.verify(c1.data.balances[bob].balance == 27)
        
        
        scenario.h3("Admin approves Bob and Bob transfers")
        scenario += c1.increaseAllowance(f = alice, t = bob, amount = 9).run(sender = admin)
        scenario += c1.transfer(f = alice, t = bob, amount = 7).run(sender = bob)
        scenario.verify(c1.data.balances[alice].balance == 38)
        scenario.verify(c1.data.balances[bob].balance == 34)
        
        
        scenario.h3("Bob tries to over-transfer from Alice")
        scenario += c1.transfer(f = alice, t = bob, amount = 5).run(sender = bob, valid = False)
        scenario.verify(c1.data.balances[alice].balance == 38)
        scenario.verify(c1.data.balances[bob].balance == 34)
        
        
        scenario.h3("Call approve will overwrite approval")
        scenario += c1.resetAllowance(address = alice, f = bob).run(sender = admin)
        scenario += c1.approve(f = alice, t = bob, amount = 18).run(sender = admin)
        scenario.verify(c1.data.balances[alice].approvals[bob] == 18)
        
        scenario.h3("Increase approval")
        scenario += c1.increaseAllowance(f = alice, t = bob, amount = 18).run(sender = admin)
        scenario.verify(c1.data.balances[alice].approvals[bob] == 36)
        
        scenario.h3("Decrease approval")
        scenario += c1.decreaseAllowance(f = alice, t = bob, amount = 10).run(sender = superTransferAgent)
        scenario.verify(c1.data.balances[alice].approvals[bob] == 26)
        
        scenario.h3("Try to Decrease approval with a greater value than already approved")
        scenario += c1.decreaseAllowance(f = alice, t = bob, amount = 28).run(sender = admin, valid = False)
        scenario.verify(c1.data.balances[alice].approvals[bob] == 26)
        
        scenario.h2("** End Transfer/Approve EntryPoints tests **")
                
        scenario.h2("** setPause EntryPoint tests **")
        
        scenario.h3("Admin pauses the contract and Alice cannot transfer anymore")
        
        scenario.h3("Admin transfers while on pause")
        scenario += c1.approve(f = alice, t = admin, amount = 1).run(sender = admin)
        scenario.verify(c1.data.balances[alice].approvals[admin] == 1)
        
        scenario += c1.transfer(f = alice, t = bob, amount = 1).run(sender = admin)
        scenario.verify(c1.data.balances[alice].balance == 37)
        scenario.verify(c1.data.balances[bob].balance == 35)
        
        
        scenario.h3("transferAgent transfers while on pause")
        scenario += c1.approve(f = alice, t = transferAgent, amount = 5).run(sender = transferAgent)
        scenario += c1.transfer(f = alice, t = bob, amount = 5).run(sender = transferAgent)
        scenario.verify(c1.data.balances[alice].balance == 32)
        scenario.verify(c1.data.balances[bob].balance == 40)
        
        scenario.h2("** End setPause EntryPoint tests **")
        
        
        scenario.h2("** burn EntryPoint tests **")
        
        scenario.h3("Admin burns 9 euroTz from Bob's account")
        scenario += c1.burn(address = bob, amount = 9).run(sender = admin)
        scenario.verify(c1.data.balances[bob].balance == 31)
        scenario.verify(c1.data.totalSupply == 91)
        scenario.h3("Alice tries to burn 10 euroTz from Bob's account")
        scenario += c1.burn(address = bob, amount = 10).run(sender = alice, valid = False)
        scenario.verify(c1.data.balances[bob].balance == 31)
        scenario.verify(c1.data.totalSupply == 91)
        
        scenario.h2("** End burn EntryPoint tests **")
        
        
        
        scenario.h2("** setAdministrator EntryPoint tests **")
        
        scenario.h3("Alice tries to set Bob as admin")
        scenario += c1.setAdministrator(bob).run(sender = alice, valid = False)
        
        scenario.h3("Admin set Fred as admin")
        scenario += c1.setAdministrator(fred).run(sender = admin)
        
        scenario.h2("** End setAdministrator EntryPoint tests **")
        
        scenario.h2("** changeEventSinkContractAddress EntryPoint tests **")
        
        scenario.h3("Alice tries to change eventSinkContractAddress")
        scenario += c1.changeEventSinkContractAddress(otherEventSinkContractAddress).run(sender = alice, valid = False)
        scenario.h3("Fred (the new admin) changes eventSinkContractAddress")
        scenario += c1.changeEventSinkContractAddress(otherEventSinkContractAddress).run(sender = fred)
        
        scenario.h2("** End changeEventSinkContractAddress EntryPoint tests **")

        scenario.simulation(c1)
        
