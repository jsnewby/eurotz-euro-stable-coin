export default [
  {
    prim: "storage",
    args: [
      {
        prim: "pair",
        args: [
          {
            prim: "pair",
            args: [
              { prim: "address", annots: ["%administrator"] },
              {
                prim: "pair",
                args: [
                  {
                    prim: "big_map",
                    args: [{ prim: "bytes" }, { prim: "bytes" }],
                    annots: ["%bigMapData"],
                  },
                  {
                    prim: "option",
                    args: [{ prim: "address" }],
                    annots: ["%eventSinkContractAddress"],
                  },
                ],
              },
            ],
          },
          {
            prim: "pair",
            args: [
              {
                prim: "pair",
                args: [
                  {
                    prim: "option",
                    args: [{ prim: "bool" }],
                    annots: ["%paused"],
                  },
                  {
                    prim: "option",
                    args: [{ prim: "set", args: [{ prim: "address" }] }],
                    annots: ["%superTransferAgents"],
                  },
                ],
              },
              {
                prim: "pair",
                args: [
                  {
                    prim: "option",
                    args: [
                      {
                        prim: "pair",
                        args: [
                          { prim: "int", annots: ["%decimals"] },
                          {
                            prim: "pair",
                            args: [
                              { prim: "string", annots: ["%name"] },
                              { prim: "string", annots: ["%symbol"] },
                            ],
                          },
                        ],
                      },
                    ],
                    annots: ["%tokenMetadata"],
                  },
                  {
                    prim: "option",
                    args: [{ prim: "set", args: [{ prim: "address" }] }],
                    annots: ["%transferAgents"],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "parameter",
    args: [
      {
        prim: "or",
        args: [
          {
            prim: "or",
            args: [
              { prim: "string", annots: ["%PauseOrUnpause"] },
              {
                prim: "or",
                args: [
                  {
                    prim: "map",
                    args: [{ prim: "bytes" }, { prim: "bytes" }],
                    annots: ["%receiveFeelessEntrypoints"],
                  },
                  {
                    prim: "map",
                    args: [{ prim: "bytes" }, { prim: "bytes" }],
                    annots: ["%receiveManagementEntrypoints"],
                  },
                ],
              },
            ],
          },
          {
            prim: "or",
            args: [
              {
                prim: "or",
                args: [
                  {
                    prim: "map",
                    args: [{ prim: "bytes" }, { prim: "bytes" }],
                    annots: ["%receiveStrandardEntrypoints"],
                  },
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%address"] },
                      { prim: "string", annots: ["%entryPointName"] },
                    ],
                    annots: ["%run"],
                  },
                ],
              },
              {
                prim: "or",
                args: [
                  { prim: "address", annots: ["%upgrade"] },
                  { prim: "address", annots: ["%upgradeFeeless"] },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "code",
    args: [
      [
        { prim: "DUP" },
        { prim: "CDR" },
        { prim: "SWAP" },
        { prim: "CAR" },
        {
          prim: "IF_LEFT",
          args: [
            [
              [
                {
                  prim: "IF_LEFT",
                  args: [
                    [
                      [
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "CDR" },
                        { prim: "SWAP" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "PACK" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:117" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "lambda",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    {
                                      prim: "big_map",
                                      args: [
                                        { prim: "bytes" },
                                        { prim: "bytes" },
                                      ],
                                      annots: ["%data"],
                                    },
                                    { prim: "address", annots: ["%sender"] },
                                  ],
                                },
                                {
                                  prim: "big_map",
                                  args: [{ prim: "bytes" }, { prim: "bytes" }],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "SENDER" },
                        { prim: "DIG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "PAIR", annots: ["%data", "%sender"] },
                        { prim: "EXEC" },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "PAIR" },
                      ],
                    ],
                    [
                      {
                        prim: "IF_LEFT",
                        args: [
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "bool" },
                                        { prim: "True" },
                                      ],
                                    },
                                  ],
                                  [
                                    [
                                      { prim: "SWAP" },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "2" }] },
                                      { prim: "CAR" },
                                      { prim: "CAR" },
                                      { prim: "SOURCE" },
                                      { prim: "COMPARE" },
                                      { prim: "EQ" },
                                    ],
                                  ],
                                ],
                              },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "not allowed." },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "DUP" },
                              {
                                prim: "ITER",
                                args: [
                                  [
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "4" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "5" }] },
                                    { prim: "CDR" },
                                    { prim: "SOME" },
                                    { prim: "DIG", args: [{ int: "5" }] },
                                    { prim: "CAR" },
                                    { prim: "UPDATE" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                  ],
                                ],
                              },
                              { prim: "DROP" },
                            ],
                          ],
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "bool" },
                                        { prim: "True" },
                                      ],
                                    },
                                  ],
                                  [
                                    [
                                      { prim: "SWAP" },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "2" }] },
                                      { prim: "CAR" },
                                      { prim: "CAR" },
                                      { prim: "SOURCE" },
                                      { prim: "COMPARE" },
                                      { prim: "EQ" },
                                    ],
                                  ],
                                ],
                              },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "not allowed." },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "DUP" },
                              {
                                prim: "ITER",
                                args: [
                                  [
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "4" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "5" }] },
                                    { prim: "CDR" },
                                    { prim: "SOME" },
                                    { prim: "DIG", args: [{ int: "5" }] },
                                    { prim: "CAR" },
                                    { prim: "UPDATE" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                  ],
                                ],
                              },
                              { prim: "DROP" },
                              { prim: "DUP" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "bytes" },
                                  {
                                    bytes:
                                      "05010000000d746f6b656e4d65746164617461",
                                  },
                                ],
                              },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:22" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "int", annots: ["%decimals"] },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "string", annots: ["%name"] },
                                          {
                                            prim: "string",
                                            annots: ["%symbol"],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "DUP" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "bytes" },
                                  { bytes: "050100000006706175736564" },
                                ],
                              },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:18" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "UNPACK", args: [{ prim: "bool" }] },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "DUP" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "bytes" },
                                  {
                                    bytes:
                                      "0501000000186576656e7453696e6b436f6e747261637441646472657373",
                                  },
                                ],
                              },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:21" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "UNPACK", args: [{ prim: "address" }] },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "DUP" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "bytes" },
                                  {
                                    bytes:
                                      "05010000000e7472616e736665724167656e7473",
                                  },
                                ],
                              },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:20" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              {
                                prim: "UNPACK",
                                args: [
                                  { prim: "set", args: [{ prim: "address" }] },
                                ],
                              },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "DUP" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "bytes" },
                                  {
                                    bytes:
                                      "05010000001373757065725472616e736665724167656e7473",
                                  },
                                ],
                              },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:19" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              {
                                prim: "UNPACK",
                                args: [
                                  { prim: "set", args: [{ prim: "address" }] },
                                ],
                              },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                            ],
                          ],
                        ],
                      },
                    ],
                  ],
                },
                { prim: "NIL", args: [{ prim: "operation" }] },
              ],
            ],
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    [
                      {
                        prim: "IF_LEFT",
                        args: [
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "bool" },
                                        { prim: "True" },
                                      ],
                                    },
                                  ],
                                  [
                                    [
                                      { prim: "SWAP" },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "2" }] },
                                      { prim: "CAR" },
                                      { prim: "CAR" },
                                      { prim: "SOURCE" },
                                      { prim: "COMPARE" },
                                      { prim: "EQ" },
                                    ],
                                  ],
                                ],
                              },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "not allowed." },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "DUP" },
                              {
                                prim: "ITER",
                                args: [
                                  [
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "4" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "5" }] },
                                    { prim: "CDR" },
                                    { prim: "SOME" },
                                    { prim: "DIG", args: [{ int: "5" }] },
                                    { prim: "CAR" },
                                    { prim: "UPDATE" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                  ],
                                ],
                              },
                              { prim: "DROP" },
                            ],
                          ],
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "4" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "5" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "4" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "5" }] },
                              { prim: "CDR" },
                              { prim: "PACK" },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:92" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "lambda",
                                    args: [
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "address",
                                            annots: ["%address"],
                                          },
                                          {
                                            prim: "pair",
                                            args: [
                                              {
                                                prim: "big_map",
                                                args: [
                                                  { prim: "bytes" },
                                                  { prim: "bytes" },
                                                ],
                                                annots: ["%data"],
                                              },
                                              {
                                                prim: "address",
                                                annots: ["%sender"],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                      {
                                        prim: "big_map",
                                        args: [
                                          { prim: "bytes" },
                                          { prim: "bytes" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "SENDER" },
                              { prim: "DIG", args: [{ int: "6" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "PAIR", annots: ["%data", "%sender"] },
                              { prim: "DIG", args: [{ int: "5" }] },
                              { prim: "CAR" },
                              { prim: "PAIR", annots: ["%address"] },
                              { prim: "EXEC" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                            ],
                          ],
                        ],
                      },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                    ],
                  ],
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            {
                              prim: "CONTRACT",
                              args: [
                                {
                                  prim: "map",
                                  args: [{ prim: "bytes" }, { prim: "bytes" }],
                                },
                              ],
                              annots: ["%upgrade"],
                            },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "SWAP" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            {
                              prim: "PUSH",
                              args: [
                                {
                                  prim: "map",
                                  args: [{ prim: "bytes" }, { prim: "bytes" }],
                                },
                                [],
                              ],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001563616c6c4465637265617365416c6c6f77616e6365",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:8" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001563616c6c4465637265617365416c6c6f77616e6365",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000863616c6c4275726e" },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:4" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000863616c6c4275726e" },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001563616c6c496e637265617365416c6c6f77616e6365",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:7" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001563616c6c496e637265617365416c6c6f77616e6365",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001373757065725472616e736665724167656e7473",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:19" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001373757065725472616e736665724167656e7473",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001263616c6c5265736574416c6c6f77616e6365",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:10" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001263616c6c5265736574416c6c6f77616e6365",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001663616c6c5265736574416c6c416c6c6f77616e636573",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:9" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001663616c6c5265736574416c6c416c6c6f77616e636573",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "0501000000186576656e7453696e6b436f6e747261637441646472657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:21" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "0501000000186576656e7453696e6b436f6e747261637441646472657373",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000000d61646d696e6973747261746f72",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:23" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000000d61646d696e6973747261746f72",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000863616c6c4d696e74" },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:3" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000863616c6c4d696e74" },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes: "05010000000c63616c6c5472616e73666572",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:5" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes: "05010000000c63616c6c5472616e73666572",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000b63616c6c417070726f7665" },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:6" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000b63616c6c417070726f7665" },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "050100000006706175736564" },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:18" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "050100000006706175736564" },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000000d746f6b656e4d65746164617461",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:22" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000000d746f6b656e4d65746164617461",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000000e7472616e736665724167656e7473",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:20" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000000e7472616e736665724167656e7473",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                          ],
                        ],
                        [
                          [
                            {
                              prim: "CONTRACT",
                              args: [
                                {
                                  prim: "map",
                                  args: [{ prim: "bytes" }, { prim: "bytes" }],
                                },
                              ],
                              annots: ["%upgrade"],
                            },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "SWAP" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            {
                              prim: "PUSH",
                              args: [
                                {
                                  prim: "map",
                                  args: [{ prim: "bytes" }, { prim: "bytes" }],
                                },
                                [],
                              ],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001c63616c6c496e637265617365416c6c6f77616e63654665656c657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:15" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001c63616c6c496e637265617365416c6c6f77616e63654665656c657373",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001263616c6c417070726f76654665656c657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:13" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001263616c6c417070726f76654665656c657373",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001363616c6c5472616e736665724665656c657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:11" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001363616c6c5472616e736665724665656c657373",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001c63616c6c4465637265617365416c6c6f77616e63654665656c657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:16" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "SOME" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001c63616c6c4465637265617365416c6c6f77616e63654665656c657373",
                                },
                              ],
                            },
                            { prim: "UPDATE" },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                          ],
                        ],
                      ],
                    },
                  ],
                ],
              },
            ],
          ],
        },
        { prim: "PAIR" },
      ],
    ],
  },
];
