import conf from "../../../conf/conf";

export default {
  prim: "Pair",
  args: [
    {
      prim: "Pair",
      args: [
        { string: conf.adminAddress },
        { prim: "Pair", args: [[], { prim: "None" }] },
      ],
    },
    {
      prim: "Pair",
      args: [
        { prim: "Pair", args: [{ prim: "None" }, { prim: "None" }] },
        { prim: "Pair", args: [{ prim: "None" }, { prim: "None" }] },
      ],
    },
  ],
};
