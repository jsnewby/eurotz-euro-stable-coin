import smartpy as sp

callMintInBytes = sp.bytes("0x05010000000863616c6c4d696e74")
callBurnInBytes = sp.bytes("0x05010000000863616c6c4275726e")
callTransferInBytes = sp.bytes("0x05010000000c63616c6c5472616e73666572")
callApproveInBytes = sp.bytes("0x05010000000b63616c6c417070726f7665")
callIncreaseAllowanceInBytes = sp.bytes("0x05010000001563616c6c496e637265617365416c6c6f77616e6365")
callDecreaseAllowanceInBytes = sp.bytes("0x05010000001563616c6c4465637265617365416c6c6f77616e6365")
callResetAllAllowancesInBytes = sp.bytes("0x05010000001663616c6c5265736574416c6c416c6c6f77616e636573")
callResetAllowanceInBytes = sp.bytes("0x05010000001263616c6c5265736574416c6c6f77616e6365")
callTransferFeelessInBytes = sp.bytes("0x05010000001363616c6c5472616e736665724665656c657373")
callApproveFeelessInBytes = sp.bytes("0x05010000001263616c6c417070726f76654665656c657373")
callIncreaseAllowanceFeelessInBytes = sp.bytes("0x05010000001c63616c6c496e637265617365416c6c6f77616e63654665656c657373")
callDecreaseAllowanceFeelessInBytes = sp.bytes("0x05010000001c63616c6c4465637265617365416c6c6f77616e63654665656c657373")
pausedInBytes = sp.bytes("0x050100000006706175736564")
superTransferAgentsInBytes = sp.bytes("0x05010000001373757065725472616e736665724167656e7473")
transferAgentsInBytes = sp.bytes("0x05010000000e7472616e736665724167656e7473")
eventSinkContractAddressInBytes = sp.bytes("0x0501000000186576656e7453696e6b436f6e747261637441646472657373")
tokenMetadataInBytes = sp.bytes("0x05010000000d746f6b656e4d65746164617461")
administratorInBytes = sp.bytes("0x05010000000d61646d696e6973747261746f72")
totalSupplyInBytes = sp.bytes("0x05010000000b746f74616c537570706c79")
callUnpackOperationDataInBytes = sp.bytes("0x05010000001763616c6c556e7061636b4f7065726174696f6e44617461")


class ProxyContract(sp.Contract):
    def __init__(self, admin):
        self.init(
            bigMapData = sp.big_map(
                tkey = sp.TBytes, 
                tvalue = sp.TBytes
                ),
            administrator = admin,
            tokenMetadata = sp.none,
            paused = sp.none, 
            eventSinkContractAddress = sp.none,
            transferAgents = sp.none,
            superTransferAgents = sp.none
            )
        
    @sp.entry_point
    def receiveManagementEntrypoints(self, params):
        sp.set_type(self.data.tokenMetadata, sp.TOption(sp.TRecord(name = sp.TString, symbol = sp.TString, decimals = sp.TInt)))
        sp.set_type(self.data.paused, sp.TOption(sp.TBool))
        sp.set_type(self.data.eventSinkContractAddress, sp.TOption(sp.TAddress))
        sp.set_type(self.data.transferAgents, sp.TOption(sp.TSet(sp.TAddress)))
        sp.set_type(self.data.superTransferAgents, sp.TOption(sp.TSet(sp.TAddress)))
        
        sp.verify(((sp.sender == self.data.administrator) | (sp.source == self.data.administrator)), message = "not allowed.")
        
        sp.for x in params.items():
            self.data.bigMapData[x.key] = x.value
            
        
        self.data.tokenMetadata = sp.unpack(self.data.bigMapData[tokenMetadataInBytes], t = sp.TRecord(name = sp.TString, symbol = sp.TString, decimals = sp.TInt))
        
        self.data.paused = sp.unpack(self.data.bigMapData[pausedInBytes], t = sp.TBool)
        
        self.data.eventSinkContractAddress = sp.unpack(self.data.bigMapData[eventSinkContractAddressInBytes], t = sp.TAddress)
        
        self.data.transferAgents = sp.unpack(self.data.bigMapData[transferAgentsInBytes], t = sp.TSet(sp.TAddress))
        
        self.data.superTransferAgents = sp.unpack(self.data.bigMapData[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress))
        
    
    @sp.entry_point
    def receiveStrandardEntrypoints(self, params):
        
        sp.verify(((sp.sender == self.data.administrator) | (sp.source == self.data.administrator)), message = "not allowed.")
        
        sp.for x in params.items():
            self.data.bigMapData[x.key] = x.value
            
            
        
    @sp.entry_point
    def receiveFeelessEntrypoints(self, params):
        
        sp.verify(((sp.sender == self.data.administrator) | (sp.source == self.data.administrator)), message = "not allowed.")
        
        sp.for x in params.items():
            self.data.bigMapData[x.key] = x.value
    
    @sp.entry_point
    def run(self, params):
        sp.set_type(params.entryPointName, sp.TString)
        sp.set_type(params.address, sp.TAddress)

        epBytesScript = self.data.bigMapData[sp.pack(params.entryPointName)]
        
        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        sender = sp.TAddress, 
                        address = sp.TAddress, 
                        ),
                    sp.TBigMap(sp.TBytes, sp.TBytes)
                ))

        epParams = sp.record(
            data = self.data.bigMapData, 
            sender = sp.sender, 
            address = params.address
            )

        self.data.bigMapData = epScript.open_some()(epParams)
    
    @sp.entry_point
    def PauseOrUnpause(self, params):
        sp.set_type(params.entryPointName, sp.TString)

        epBytesScript = self.data.bigMapData[sp.pack(params.entryPointName)]

        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        sender = sp.TAddress
                        ),
                    sp.TBigMap(sp.TBytes, sp.TBytes)
                ))

        epParams = sp.record(
            data = self.data.bigMapData, 
            sender = sp.sender
            )
     
        self.data.bigMapData = epScript.open_some()(epParams)    
    
    '''
    We've separated the upgrade process into two parts because putting all in one operation will exceed the max operation size.
    '''

    @sp.entry_point
    def upgradeStandard(self, params):
        
        mapRecord = {
            callMintInBytes: self.data.bigMapData[callMintInBytes],
            callBurnInBytes: self.data.bigMapData[callBurnInBytes],
            callTransferInBytes: self.data.bigMapData[callTransferInBytes],
            callApproveInBytes: self.data.bigMapData[callApproveInBytes],
            callIncreaseAllowanceInBytes: self.data.bigMapData[callIncreaseAllowanceInBytes],
            callDecreaseAllowanceInBytes: self.data.bigMapData[callDecreaseAllowanceInBytes],
            callResetAllowanceInBytes: self.data.bigMapData[callResetAllowanceInBytes],
            callResetAllAllowancesInBytes: self.data.bigMapData[callResetAllAllowancesInBytes]
        }    
        
        EPType = sp.TMap(sp.TBytes, sp.TBytes)
        
        sp.transfer(
            mapRecord, 
            sp.tez(0), 
            sp.contract(
                t = EPType,
                address = params.target,
                entry_point = "upgrade"
                ).open_some()
            )
    
    @sp.entry_point
    def upgradeManagement(self, params):
        
        mapRecord = {
            administratorInBytes: self.data.bigMapData[administratorInBytes],
            pausedInBytes: self.data.bigMapData[pausedInBytes],
            tokenMetadataInBytes: self.data.bigMapData[tokenMetadataInBytes],
            eventSinkContractAddressInBytes: self.data.bigMapData[eventSinkContractAddressInBytes],
            superTransferAgentsInBytes: self.data.bigMapData[superTransferAgentsInBytes],
            transferAgentsInBytes: self.data.bigMapData[transferAgentsInBytes],

        }    
        
        EPType = sp.TMap(sp.TBytes, sp.TBytes)
        
        sp.transfer(
            mapRecord, 
            sp.tez(0), 
            sp.contract(
                t = EPType,
                address = params.target,
                entry_point = "upgrade"
                ).open_some()
            )        
    
    
    @sp.entry_point
    def upgradeFeeless(self, params):
        
        mapRecord = {
            callTransferFeelessInBytes: self.data.bigMapData[callTransferFeelessInBytes],
            
            callApproveFeelessInBytes: self.data.bigMapData[callApproveFeelessInBytes],
            
            callIncreaseAllowanceFeelessInBytes: self.data.bigMapData[callIncreaseAllowanceFeelessInBytes],
            
            callDecreaseAllowanceFeelessInBytes: self.data.bigMapData[callDecreaseAllowanceFeelessInBytes],
        }
        
        EPType = sp.TMap(sp.TBytes, sp.TBytes)
        
        sp.transfer(
            mapRecord, 
            sp.tez(0), 
            sp.contract(
                t = EPType,
                address = params.target,
                entry_point = "upgrade"
                ).open_some()
            )        
            
        
      

@sp.add_test(name = "ProxyContract")
def test():
    scenario = sp.test_scenario()
    scenario.h1("ProxyContract ")

    admin = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")

    c1 = ProxyContract(admin)

    scenario += c1
    
    