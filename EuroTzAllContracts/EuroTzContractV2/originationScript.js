import { Tezos, TezosOperationError } from "@taquito/taquito";

import conf from "../../conf/conf";

import euroTzCode from "./EuroTzContract/code";
import euroTzStorage from "./EuroTzContract/storage";

import proxyCode from "./ProxyContract/code";
import proxyStorage from "./ProxyContract/storage";

import feelessBuilderCode from "./FeelessBuilderContract/code";
import feelessBuilderStorage from "./FeelessBuilderContract/storage";

import standardBuilderCode from "./StandardBuilderContract/code";
import standardBuilderStorage from "./StandardBuilderContract/storage";

import managementBuilderCode from "./ManagementBuilderSC/code";
import managementBuilderStorage from "./ManagementBuilderSC/storage";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

Tezos.importKey(conf.adminSecretKey);

export async function originateEuroTz() {
  try {
    console.log("Begin origination operation...");

    const originationOp = await Tezos.contract.originate({
      code: euroTzCode,
      init: euroTzStorage,
    });

    const contractOriginationOp = await originationOp.contract();

    console.log("EuroTz contractAddress :", contractOriginationOp.address);

    console.log("End origination operation...");
    return contractOriginationOp.address;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log(e.errors);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

export async function originateProxy() {
  try {
    console.log("Begin origination operation...");

    const originationOp = await Tezos.contract.originate({
      code: proxyCode,
      init: proxyStorage,
    });

    const contractOriginationOp = await originationOp.contract();

    console.log("Proxy contractAddress :", contractOriginationOp.address);

    console.log("End origination operation...");
    return contractOriginationOp.address;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log(e.errors);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

export async function originateStandardBuilder() {
  try {
    console.log("Begin origination operation...");

    const originationOp = await Tezos.contract.originate({
      code: standardBuilderCode,
      init: standardBuilderStorage,
    });

    const contractOriginationOp = await originationOp.contract();

    console.log(
      "StandardBuilder contractAddress :",
      contractOriginationOp.address
    );

    console.log("End origination operation...");
    return contractOriginationOp.address;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log(e.errors);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

export async function originateFeelessBuilder() {
  try {
    console.log("Begin origination operation...");

    const originationOp = await Tezos.contract.originate({
      code: feelessBuilderCode,
      init: feelessBuilderStorage,
    });

    const contractOriginationOp = await originationOp.contract();

    console.log(
      "FeelessBuilder contractAddress :",
      contractOriginationOp.address
    );

    console.log("End origination operation...");
    return contractOriginationOp.address;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log(e.errors);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

export async function originateManagementBuilder() {
  try {
    console.log("Begin origination operation...");

    const originationOp = await Tezos.contract.originate({
      code: managementBuilderCode,
      init: managementBuilderStorage,
    });

    const contractOriginationOp = await originationOp.contract();

    console.log(
      "ManagementBuilder contractAddress :",
      contractOriginationOp.address
    );
    console.log("End origination operation...");
    return contractOriginationOp.address;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log(e.errors);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

async function originateContracts() {
  try {
    console.log("Begin origination operations...");
    console.log("=======================================");

    const euroTzAddress = await originateEuroTz();
    const proxyAddress = await originateProxy();
    const standardAddress = await originateStandardBuilder();
    const feelessAddress = await originateFeelessBuilder();
    const managementAddress = await originateManagementBuilder();

    console.log("=======================================");
    console.log("End origination operations...");

    const allContractsAddresses = {
      euroTzAddress,
      proxyAddress,
      standardAddress,
      feelessAddress,
      managementAddress,
    };

    console.log(allContractsAddresses);
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log(e.errors);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

originateContracts();
