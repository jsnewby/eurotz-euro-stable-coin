export default [
  {
    prim: "storage",
    args: [
      {
        prim: "pair",
        args: [
          { prim: "address", annots: ["%administrator"] },
          {
            prim: "big_map",
            args: [{ prim: "bytes" }, { prim: "bytes" }],
            annots: ["%entrypointsBytes"],
          },
        ],
      },
    ],
  },
  {
    prim: "parameter",
    args: [
      {
        prim: "or",
        args: [
          { prim: "unit", annots: ["%buildFeelessEntrypoints"] },
          { prim: "address", annots: ["%sendEntrypoints"] },
        ],
      },
    ],
  },
  {
    prim: "code",
    args: [
      [
        { prim: "DUP" },
        { prim: "CDR" },
        { prim: "SWAP" },
        { prim: "CAR" },
        {
          prim: "IF_LEFT",
          args: [
            [
              [
                { prim: "SWAP" },
                { prim: "DUP" },
                { prim: "DUG", args: [{ int: "2" }] },
                { prim: "DUP" },
                { prim: "CAR" },
                { prim: "SWAP" },
                { prim: "CDR" },
                {
                  prim: "LAMBDA",
                  args: [
                    {
                      prim: "pair",
                      args: [
                        {
                          prim: "pair",
                          args: [
                            { prim: "bytes", annots: ["%b"] },
                            {
                              prim: "pair",
                              args: [
                                {
                                  prim: "big_map",
                                  args: [
                                    { prim: "address" },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "map",
                                          args: [
                                            { prim: "address" },
                                            { prim: "int" },
                                          ],
                                          annots: ["%approvals"],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "int",
                                              annots: ["%balance"],
                                            },
                                            { prim: "int", annots: ["%nonce"] },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                  annots: ["%balances"],
                                },
                                {
                                  prim: "big_map",
                                  args: [{ prim: "bytes" }, { prim: "bytes" }],
                                  annots: ["%data"],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "pair",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "key", annots: ["%k"] },
                                { prim: "signature", annots: ["%s"] },
                              ],
                            },
                            {
                              prim: "pair",
                              args: [
                                { prim: "address", annots: ["%sender"] },
                                { prim: "address", annots: ["%thisAddress"] },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                    {
                      prim: "big_map",
                      args: [
                        { prim: "address" },
                        {
                          prim: "pair",
                          args: [
                            {
                              prim: "map",
                              args: [{ prim: "address" }, { prim: "int" }],
                              annots: ["%approvals"],
                            },
                            {
                              prim: "pair",
                              args: [
                                { prim: "int", annots: ["%balance"] },
                                { prim: "int", annots: ["%nonce"] },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                    [
                      [
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        {
                          prim: "PUSH",
                          args: [
                            { prim: "bytes" },
                            { bytes: "05010000000d61646d696e6973747261746f72" },
                          ],
                        },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:3" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "UNPACK", args: [{ prim: "address" }] },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "bool" }, { prim: "True" }],
                              },
                            ],
                            [
                              [
                                { prim: "SWAP" },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "2" }] },
                                { prim: "CAR" },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "bytes" },
                                    {
                                      bytes:
                                        "05010000000e7472616e736665724167656e7473",
                                    },
                                  ],
                                },
                                { prim: "GET" },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "string" },
                                            { string: "Get-item:11" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [],
                                  ],
                                },
                                {
                                  prim: "UNPACK",
                                  args: [
                                    {
                                      prim: "set",
                                      args: [{ prim: "address" }],
                                    },
                                  ],
                                },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "unit" },
                                            { prim: "Unit" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [[]],
                                  ],
                                },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "3" }] },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                { prim: "CAR" },
                                { prim: "MEM" },
                              ],
                            ],
                          ],
                        },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "03" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        { prim: "CHECK_SIGNATURE" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "20" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "35" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "19" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "COMPARE" },
                        { prim: "GE" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "18" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "MEM" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    {
                                      prim: "option",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "map",
                                              args: [
                                                { prim: "address" },
                                                { prim: "int" },
                                              ],
                                              annots: ["%approvals"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "int",
                                                  annots: ["%balance"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%nonce"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "Some",
                                      args: [
                                        {
                                          prim: "Pair",
                                          args: [
                                            [],
                                            {
                                              prim: "Pair",
                                              args: [
                                                { int: "0" },
                                                { int: "0" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "3" }] },
                                { prim: "CAR" },
                                { prim: "CAR" },
                                {
                                  prim: "UNPACK",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "int" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "int" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    { prim: "address" },
                                                    { prim: "address" },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "unit" },
                                            { prim: "Unit" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [[]],
                                  ],
                                },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                { prim: "CAR" },
                                { prim: "UPDATE" },
                              ],
                            ],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "set_in_top-any" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "PUSH", args: [{ prim: "int" }, { int: "1" }] },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "7" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "ADD" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SOME" },
                        { prim: "SWAP" },
                        { prim: "UPDATE" },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "set_in_top-any" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "7" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "SUB" },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SOME" },
                        { prim: "SWAP" },
                        { prim: "UPDATE" },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "set_in_top-any" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:73" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "ADD" },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SOME" },
                        { prim: "SWAP" },
                        { prim: "UPDATE" },
                      ],
                    ],
                  ],
                },
                { prim: "DIG", args: [{ int: "4" }] },
                { prim: "DROP" },
                { prim: "SOME" },
                {
                  prim: "IF_NONE",
                  args: [
                    [
                      [
                        {
                          prim: "PUSH",
                          args: [{ prim: "unit" }, { prim: "Unit" }],
                        },
                        { prim: "FAILWITH" },
                      ],
                    ],
                    [[]],
                  ],
                },
                { prim: "PACK" },
                { prim: "SOME" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes:
                        "05010000001363616c6c5472616e736665724665656c657373",
                    },
                  ],
                },
                { prim: "UPDATE" },
                { prim: "SWAP" },
                { prim: "PAIR" },
                { prim: "SWAP" },
                { prim: "SWAP" },
                { prim: "DUP" },
                { prim: "DUG", args: [{ int: "2" }] },
                { prim: "DUP" },
                { prim: "CAR" },
                { prim: "SWAP" },
                { prim: "CDR" },
                {
                  prim: "LAMBDA",
                  args: [
                    {
                      prim: "pair",
                      args: [
                        {
                          prim: "pair",
                          args: [
                            { prim: "bytes", annots: ["%b"] },
                            {
                              prim: "pair",
                              args: [
                                {
                                  prim: "big_map",
                                  args: [
                                    { prim: "address" },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "map",
                                          args: [
                                            { prim: "address" },
                                            { prim: "int" },
                                          ],
                                          annots: ["%approvals"],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "int",
                                              annots: ["%balance"],
                                            },
                                            { prim: "int", annots: ["%nonce"] },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                  annots: ["%balances"],
                                },
                                {
                                  prim: "big_map",
                                  args: [{ prim: "bytes" }, { prim: "bytes" }],
                                  annots: ["%data"],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "pair",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "key", annots: ["%k"] },
                                { prim: "signature", annots: ["%s"] },
                              ],
                            },
                            {
                              prim: "pair",
                              args: [
                                { prim: "address", annots: ["%sender"] },
                                { prim: "address", annots: ["%thisAddress"] },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                    {
                      prim: "big_map",
                      args: [
                        { prim: "address" },
                        {
                          prim: "pair",
                          args: [
                            {
                              prim: "map",
                              args: [{ prim: "address" }, { prim: "int" }],
                              annots: ["%approvals"],
                            },
                            {
                              prim: "pair",
                              args: [
                                { prim: "int", annots: ["%balance"] },
                                { prim: "int", annots: ["%nonce"] },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                    [
                      [
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        {
                          prim: "PUSH",
                          args: [
                            { prim: "bytes" },
                            { bytes: "05010000000d61646d696e6973747261746f72" },
                          ],
                        },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:3" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "UNPACK", args: [{ prim: "address" }] },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "bool" }, { prim: "True" }],
                              },
                            ],
                            [
                              [
                                { prim: "SWAP" },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "2" }] },
                                { prim: "CAR" },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "bytes" },
                                    {
                                      bytes:
                                        "05010000000e7472616e736665724167656e7473",
                                    },
                                  ],
                                },
                                { prim: "GET" },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "string" },
                                            { string: "Get-item:11" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [],
                                  ],
                                },
                                {
                                  prim: "UNPACK",
                                  args: [
                                    {
                                      prim: "set",
                                      args: [{ prim: "address" }],
                                    },
                                  ],
                                },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "unit" },
                                            { prim: "Unit" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [[]],
                                  ],
                                },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "3" }] },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                { prim: "CAR" },
                                { prim: "MEM" },
                              ],
                            ],
                          ],
                        },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "03" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        { prim: "CHECK_SIGNATURE" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "20" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "35" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "19" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "PUSH", args: [{ prim: "int" }, { int: "0" }] },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "COMPARE" },
                        { prim: "GT" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "11" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "MEM" },
                        {
                          prim: "IF",
                          args: [
                            [
                              [
                                { prim: "DUP" },
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "int" }, { int: "0" }],
                                },
                                { prim: "SWAP" },
                                { prim: "DIG", args: [{ int: "3" }] },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "4" }] },
                                { prim: "CAR" },
                                { prim: "CAR" },
                                {
                                  prim: "UNPACK",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "int" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "int" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    { prim: "address" },
                                                    { prim: "address" },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "unit" },
                                            { prim: "Unit" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [[]],
                                  ],
                                },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                { prim: "CAR" },
                                { prim: "GET" },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "string" },
                                            { string: "Get-item:72" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [],
                                  ],
                                },
                                { prim: "CAR" },
                                { prim: "DIG", args: [{ int: "3" }] },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "4" }] },
                                { prim: "CAR" },
                                { prim: "CAR" },
                                {
                                  prim: "UNPACK",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "int" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "int" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    { prim: "address" },
                                                    { prim: "address" },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "unit" },
                                            { prim: "Unit" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [[]],
                                  ],
                                },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                { prim: "CAR" },
                                { prim: "GET" },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "string" },
                                            { string: "Get-item:73" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [],
                                  ],
                                },
                                { prim: "COMPARE" },
                                { prim: "GT" },
                                {
                                  prim: "IF",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "string" },
                                            { string: "29" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [[]],
                                  ],
                                },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "set_in_top-any" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "PUSH", args: [{ prim: "int" }, { int: "1" }] },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "7" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "ADD" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SOME" },
                        { prim: "SWAP" },
                        { prim: "UPDATE" },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "set_in_top-any" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "CDR" },
                        { prim: "SWAP" },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "SOME" },
                        { prim: "DIG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "UPDATE" },
                        { prim: "PAIR" },
                        { prim: "SOME" },
                        { prim: "SWAP" },
                        { prim: "UPDATE" },
                      ],
                    ],
                  ],
                },
                { prim: "DIG", args: [{ int: "4" }] },
                { prim: "DROP" },
                { prim: "SOME" },
                {
                  prim: "IF_NONE",
                  args: [
                    [
                      [
                        {
                          prim: "PUSH",
                          args: [{ prim: "unit" }, { prim: "Unit" }],
                        },
                        { prim: "FAILWITH" },
                      ],
                    ],
                    [[]],
                  ],
                },
                { prim: "PACK" },
                { prim: "SOME" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes: "05010000001263616c6c417070726f76654665656c657373",
                    },
                  ],
                },
                { prim: "UPDATE" },
                { prim: "SWAP" },
                { prim: "PAIR" },
                { prim: "SWAP" },
                { prim: "SWAP" },
                { prim: "DUP" },
                { prim: "DUG", args: [{ int: "2" }] },
                { prim: "DUP" },
                { prim: "CAR" },
                { prim: "SWAP" },
                { prim: "CDR" },
                {
                  prim: "LAMBDA",
                  args: [
                    {
                      prim: "pair",
                      args: [
                        {
                          prim: "pair",
                          args: [
                            { prim: "bytes", annots: ["%b"] },
                            {
                              prim: "pair",
                              args: [
                                {
                                  prim: "big_map",
                                  args: [
                                    { prim: "address" },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "map",
                                          args: [
                                            { prim: "address" },
                                            { prim: "int" },
                                          ],
                                          annots: ["%approvals"],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "int",
                                              annots: ["%balance"],
                                            },
                                            { prim: "int", annots: ["%nonce"] },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                  annots: ["%balances"],
                                },
                                {
                                  prim: "big_map",
                                  args: [{ prim: "bytes" }, { prim: "bytes" }],
                                  annots: ["%data"],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "pair",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "key", annots: ["%k"] },
                                { prim: "signature", annots: ["%s"] },
                              ],
                            },
                            {
                              prim: "pair",
                              args: [
                                { prim: "address", annots: ["%sender"] },
                                { prim: "address", annots: ["%thisAddress"] },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                    {
                      prim: "big_map",
                      args: [
                        { prim: "address" },
                        {
                          prim: "pair",
                          args: [
                            {
                              prim: "map",
                              args: [{ prim: "address" }, { prim: "int" }],
                              annots: ["%approvals"],
                            },
                            {
                              prim: "pair",
                              args: [
                                { prim: "int", annots: ["%balance"] },
                                { prim: "int", annots: ["%nonce"] },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                    [
                      [
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        {
                          prim: "PUSH",
                          args: [
                            { prim: "bytes" },
                            { bytes: "05010000000d61646d696e6973747261746f72" },
                          ],
                        },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:3" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "UNPACK", args: [{ prim: "address" }] },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "bool" }, { prim: "True" }],
                              },
                            ],
                            [
                              [
                                { prim: "SWAP" },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "2" }] },
                                { prim: "CAR" },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "bytes" },
                                    {
                                      bytes:
                                        "05010000000e7472616e736665724167656e7473",
                                    },
                                  ],
                                },
                                { prim: "GET" },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "string" },
                                            { string: "Get-item:11" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [],
                                  ],
                                },
                                {
                                  prim: "UNPACK",
                                  args: [
                                    {
                                      prim: "set",
                                      args: [{ prim: "address" }],
                                    },
                                  ],
                                },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "unit" },
                                            { prim: "Unit" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [[]],
                                  ],
                                },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "3" }] },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                { prim: "CAR" },
                                { prim: "MEM" },
                              ],
                            ],
                          ],
                        },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "03" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        { prim: "CHECK_SIGNATURE" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "20" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "35" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "19" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "PUSH", args: [{ prim: "int" }, { int: "0" }] },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "COMPARE" },
                        { prim: "GT" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "11" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "MEM" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "13" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "set_in_top-any" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "PUSH", args: [{ prim: "int" }, { int: "1" }] },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "7" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "ADD" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SOME" },
                        { prim: "SWAP" },
                        { prim: "UPDATE" },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "set_in_top-any" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "CDR" },
                        { prim: "SWAP" },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "7" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "7" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:73" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "ADD" },
                        { prim: "SOME" },
                        { prim: "DIG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "UPDATE" },
                        { prim: "PAIR" },
                        { prim: "SOME" },
                        { prim: "SWAP" },
                        { prim: "UPDATE" },
                      ],
                    ],
                  ],
                },
                { prim: "DIG", args: [{ int: "4" }] },
                { prim: "DROP" },
                { prim: "SOME" },
                {
                  prim: "IF_NONE",
                  args: [
                    [
                      [
                        {
                          prim: "PUSH",
                          args: [{ prim: "unit" }, { prim: "Unit" }],
                        },
                        { prim: "FAILWITH" },
                      ],
                    ],
                    [[]],
                  ],
                },
                { prim: "PACK" },
                { prim: "SOME" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes:
                        "05010000001c63616c6c496e637265617365416c6c6f77616e63654665656c657373",
                    },
                  ],
                },
                { prim: "UPDATE" },
                { prim: "SWAP" },
                { prim: "PAIR" },
                { prim: "SWAP" },
                { prim: "SWAP" },
                { prim: "DUP" },
                { prim: "DUG", args: [{ int: "2" }] },
                { prim: "DUP" },
                { prim: "CAR" },
                { prim: "SWAP" },
                { prim: "CDR" },
                {
                  prim: "LAMBDA",
                  args: [
                    {
                      prim: "pair",
                      args: [
                        {
                          prim: "pair",
                          args: [
                            { prim: "bytes", annots: ["%b"] },
                            {
                              prim: "pair",
                              args: [
                                {
                                  prim: "big_map",
                                  args: [
                                    { prim: "address" },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "map",
                                          args: [
                                            { prim: "address" },
                                            { prim: "int" },
                                          ],
                                          annots: ["%approvals"],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "int",
                                              annots: ["%balance"],
                                            },
                                            { prim: "int", annots: ["%nonce"] },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                  annots: ["%balances"],
                                },
                                {
                                  prim: "big_map",
                                  args: [{ prim: "bytes" }, { prim: "bytes" }],
                                  annots: ["%data"],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "pair",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "key", annots: ["%k"] },
                                { prim: "signature", annots: ["%s"] },
                              ],
                            },
                            {
                              prim: "pair",
                              args: [
                                { prim: "address", annots: ["%sender"] },
                                { prim: "address", annots: ["%thisAddress"] },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                    {
                      prim: "big_map",
                      args: [
                        { prim: "address" },
                        {
                          prim: "pair",
                          args: [
                            {
                              prim: "map",
                              args: [{ prim: "address" }, { prim: "int" }],
                              annots: ["%approvals"],
                            },
                            {
                              prim: "pair",
                              args: [
                                { prim: "int", annots: ["%balance"] },
                                { prim: "int", annots: ["%nonce"] },
                              ],
                            },
                          ],
                        },
                      ],
                    },
                    [
                      [
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        {
                          prim: "PUSH",
                          args: [
                            { prim: "bytes" },
                            { bytes: "05010000000d61646d696e6973747261746f72" },
                          ],
                        },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:3" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "UNPACK", args: [{ prim: "address" }] },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "bool" }, { prim: "True" }],
                              },
                            ],
                            [
                              [
                                { prim: "SWAP" },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "2" }] },
                                { prim: "CAR" },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "bytes" },
                                    {
                                      bytes:
                                        "05010000000e7472616e736665724167656e7473",
                                    },
                                  ],
                                },
                                { prim: "GET" },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "string" },
                                            { string: "Get-item:11" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [],
                                  ],
                                },
                                {
                                  prim: "UNPACK",
                                  args: [
                                    {
                                      prim: "set",
                                      args: [{ prim: "address" }],
                                    },
                                  ],
                                },
                                {
                                  prim: "IF_NONE",
                                  args: [
                                    [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "unit" },
                                            { prim: "Unit" },
                                          ],
                                        },
                                        { prim: "FAILWITH" },
                                      ],
                                    ],
                                    [[]],
                                  ],
                                },
                                { prim: "DIG", args: [{ int: "2" }] },
                                { prim: "DUP" },
                                { prim: "DUG", args: [{ int: "3" }] },
                                { prim: "CDR" },
                                { prim: "CDR" },
                                { prim: "CAR" },
                                { prim: "MEM" },
                              ],
                            ],
                          ],
                        },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "03" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        { prim: "CHECK_SIGNATURE" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "20" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "35" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "19" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "PUSH", args: [{ prim: "int" }, { int: "0" }] },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "COMPARE" },
                        { prim: "GT" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "11" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "MEM" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "15" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "4" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:73" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "COMPARE" },
                        { prim: "GT" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "string" }, { string: "16" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "set_in_top-any" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "PUSH", args: [{ prim: "int" }, { int: "1" }] },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "7" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "ADD" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SOME" },
                        { prim: "SWAP" },
                        { prim: "UPDATE" },
                        { prim: "DUP" },
                        { prim: "DIG", args: [{ int: "2" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "3" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "set_in_top-any" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "DUP" },
                        { prim: "CDR" },
                        { prim: "SWAP" },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "4" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "5" }] },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "7" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:72" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "6" }] },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "7" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "GET" },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    { string: "Get-item:73" },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [],
                          ],
                        },
                        { prim: "SUB" },
                        { prim: "SOME" },
                        { prim: "DIG", args: [{ int: "5" }] },
                        { prim: "CAR" },
                        { prim: "CAR" },
                        {
                          prim: "UNPACK",
                          args: [
                            {
                              prim: "pair",
                              args: [
                                { prim: "int" },
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            { prim: "address" },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                          ],
                        },
                        {
                          prim: "IF_NONE",
                          args: [
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [{ prim: "unit" }, { prim: "Unit" }],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                            [[]],
                          ],
                        },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "UPDATE" },
                        { prim: "PAIR" },
                        { prim: "SOME" },
                        { prim: "SWAP" },
                        { prim: "UPDATE" },
                      ],
                    ],
                  ],
                },
                { prim: "DIG", args: [{ int: "4" }] },
                { prim: "DROP" },
                { prim: "DIG", args: [{ int: "3" }] },
                { prim: "DROP" },
                { prim: "SOME" },
                {
                  prim: "IF_NONE",
                  args: [
                    [
                      [
                        {
                          prim: "PUSH",
                          args: [{ prim: "unit" }, { prim: "Unit" }],
                        },
                        { prim: "FAILWITH" },
                      ],
                    ],
                    [[]],
                  ],
                },
                { prim: "PACK" },
                { prim: "SOME" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes:
                        "05010000001c63616c6c4465637265617365416c6c6f77616e63654665656c657373",
                    },
                  ],
                },
                { prim: "UPDATE" },
                { prim: "SWAP" },
                { prim: "PAIR" },
                { prim: "NIL", args: [{ prim: "operation" }] },
              ],
            ],
            [
              [
                {
                  prim: "CONTRACT",
                  args: [
                    {
                      prim: "map",
                      args: [{ prim: "bytes" }, { prim: "bytes" }],
                    },
                  ],
                  annots: ["%receiveFeelessEntrypoints"],
                },
                { prim: "NIL", args: [{ prim: "operation" }] },
                { prim: "SWAP" },
                {
                  prim: "IF_NONE",
                  args: [
                    [
                      [
                        {
                          prim: "PUSH",
                          args: [{ prim: "unit" }, { prim: "Unit" }],
                        },
                        { prim: "FAILWITH" },
                      ],
                    ],
                    [[]],
                  ],
                },
                { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                {
                  prim: "PUSH",
                  args: [
                    {
                      prim: "map",
                      args: [{ prim: "bytes" }, { prim: "bytes" }],
                    },
                    [],
                  ],
                },
                { prim: "DIG", args: [{ int: "4" }] },
                { prim: "DUP" },
                { prim: "DUG", args: [{ int: "5" }] },
                { prim: "CDR" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes:
                        "05010000001363616c6c5472616e736665724665656c657373",
                    },
                  ],
                },
                { prim: "GET" },
                {
                  prim: "IF_NONE",
                  args: [
                    [
                      [
                        {
                          prim: "PUSH",
                          args: [{ prim: "string" }, { string: "Get-item:5" }],
                        },
                        { prim: "FAILWITH" },
                      ],
                    ],
                    [],
                  ],
                },
                { prim: "SOME" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes:
                        "05010000001363616c6c5472616e736665724665656c657373",
                    },
                  ],
                },
                { prim: "UPDATE" },
                { prim: "DIG", args: [{ int: "4" }] },
                { prim: "DUP" },
                { prim: "DUG", args: [{ int: "5" }] },
                { prim: "CDR" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes: "05010000001263616c6c417070726f76654665656c657373",
                    },
                  ],
                },
                { prim: "GET" },
                {
                  prim: "IF_NONE",
                  args: [
                    [
                      [
                        {
                          prim: "PUSH",
                          args: [{ prim: "string" }, { string: "Get-item:6" }],
                        },
                        { prim: "FAILWITH" },
                      ],
                    ],
                    [],
                  ],
                },
                { prim: "SOME" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes: "05010000001263616c6c417070726f76654665656c657373",
                    },
                  ],
                },
                { prim: "UPDATE" },
                { prim: "DIG", args: [{ int: "4" }] },
                { prim: "DUP" },
                { prim: "DUG", args: [{ int: "5" }] },
                { prim: "CDR" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes:
                        "05010000001c63616c6c496e637265617365416c6c6f77616e63654665656c657373",
                    },
                  ],
                },
                { prim: "GET" },
                {
                  prim: "IF_NONE",
                  args: [
                    [
                      [
                        {
                          prim: "PUSH",
                          args: [{ prim: "string" }, { string: "Get-item:8" }],
                        },
                        { prim: "FAILWITH" },
                      ],
                    ],
                    [],
                  ],
                },
                { prim: "SOME" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes:
                        "05010000001c63616c6c496e637265617365416c6c6f77616e63654665656c657373",
                    },
                  ],
                },
                { prim: "UPDATE" },
                { prim: "DIG", args: [{ int: "4" }] },
                { prim: "DUP" },
                { prim: "DUG", args: [{ int: "5" }] },
                { prim: "CDR" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes:
                        "05010000001c63616c6c4465637265617365416c6c6f77616e63654665656c657373",
                    },
                  ],
                },
                { prim: "GET" },
                {
                  prim: "IF_NONE",
                  args: [
                    [
                      [
                        {
                          prim: "PUSH",
                          args: [{ prim: "string" }, { string: "Get-item:9" }],
                        },
                        { prim: "FAILWITH" },
                      ],
                    ],
                    [],
                  ],
                },
                { prim: "SOME" },
                {
                  prim: "PUSH",
                  args: [
                    { prim: "bytes" },
                    {
                      bytes:
                        "05010000001c63616c6c4465637265617365416c6c6f77616e63654665656c657373",
                    },
                  ],
                },
                { prim: "UPDATE" },
                { prim: "TRANSFER_TOKENS" },
                { prim: "CONS" },
              ],
            ],
          ],
        },
        { prim: "PAIR" },
      ],
    ],
  },
];
