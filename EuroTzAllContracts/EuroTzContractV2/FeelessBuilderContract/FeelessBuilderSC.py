import smartpy as sp

##################

administratorInBytes = sp.bytes("0x05010000000d61646d696e6973747261746f72")
callTransferFeelessInBytes = sp.bytes("0x05010000001363616c6c5472616e736665724665656c657373")
callApproveFeelessInBytes = sp.bytes("0x05010000001263616c6c417070726f76654665656c657373")
callIncreaseAllowanceFeelessInBytes = sp.bytes("0x05010000001c63616c6c496e637265617365416c6c6f77616e63654665656c657373")
callDecreaseAllowanceFeelessInBytes = sp.bytes("0x05010000001c63616c6c4465637265617365416c6c6f77616e63654665656c657373")
transferAgentsInBytes = sp.bytes("0x05010000000e7472616e736665724167656e7473")
eventSinkContractAddressInBytes = sp.bytes("0x0501000000186576656e7453696e6b436f6e747261637441646472657373")
callUnpackOperationDataInBytes = sp.bytes("0x05010000001763616c6c556e7061636b4f7065726174696f6e44617461")

##################

class FeelessBuilder(sp.Contract):
    def __init__(self, admin):
        self.init(
            entrypointsBytes = sp.big_map(
                tkey = sp.TBytes, 
                tvalue = sp.TBytes
            ),
            administrator = admin
            )
    '''
    Modiifiers
    '''
    
    # transfer/transferFeeless modifier
    def onlyAdminOrTransferAgent(self, params):
        # message = "Only admin or Transfer agent are allowed"
        sp.verify(((params.sender == params.administrator) | params.transferAgents.contains(params.sender)), message = "03") 
    
    '''
    Feeless Checks
    '''
    def checkSignature(self, params):
        # Check for signature
        #  message= "Wrong signature"
        sp.verify(sp.check_signature(params.k, params.s, params.b), message= "20")
    
    def checkNonce(self, params):
        
        # Nonce verification
        # message = "Invalid nonce"
        sp.verify((params.storedNonce == params.nonce), message = "19")   
    
    def unpackOperationData(self, operationbytes):
        # Unpack data in order to get: from, to, amount, nonce
        dataRecordType = sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TPair(sp.TAddress, sp.TPair(sp.TAddress, sp.TAddress))))
        
        dataRecord = sp.unpack(
            operationbytes, 
            t = dataRecordType
            ).open_some()
        
        amount = sp.fst(dataRecord)
        nonce = sp.fst(sp.snd(dataRecord))
        from_ = sp.fst(sp.snd(sp.snd(dataRecord)))
        to_ = sp.fst(sp.snd(sp.snd(sp.snd(dataRecord))))
        contractAddress = sp.snd(sp.snd(sp.snd(sp.snd(dataRecord))))
        
        operationData = sp.record(amount = amount, nonce = nonce, from_ = from_, to_ = to_, contractAddress = contractAddress)
        
        return operationData
    
    def checkSameContract(self, contractAddress, thisAddress):
        # thisAddress = sp.to_address(sp.self)
        
        sp.verify((thisAddress == contractAddress), message = "35")    
        
    '''
    Entrypoints
    '''
            
    
    def transferFeeless(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.s, sp.TSignature)
        sp.set_type(params.k, sp.TKey)
        sp.set_type(params.b, sp.TBytes)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        sp.set_type(params.thisAddress, sp.TAddress)    
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)
        
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        transferAgents = sp.unpack(params.data[transferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        checkRecord = sp.record(transferAgents = transferAgents, administrator = administrator, sender = params.sender)
        
        self.onlyAdminOrTransferAgent(checkRecord)
        
        checkSignatureRecord = sp.record(b = params.b, s = params.s, k = params.k)
        
        self.checkSignature(checkSignatureRecord)
        
        #####
        
        operationData = self.unpackOperationData(params.b)
        
        self.checkSameContract(operationData.contractAddress, params.thisAddress)
        
        #####
        
        checkNonceRecord = sp.record(storedNonce = balances.value[operationData.from_].nonce, nonce = operationData.nonce)
        
        self.checkNonce(checkNonceRecord)

        # message = "Insufficient balance"
        sp.verify((balances.value[operationData.from_].balance >= operationData.amount), message = "18")
        
        # add user in entrypointsBytes if he doesn't exist
        sp.if ~ balances.value.contains(operationData.to_):
            balances.value[operationData.to_] = sp.record(balance = sp.int(0), approvals = sp.map(tkey = sp.TAddress, tvalue = sp.TInt), nonce = sp.int(0))       
        
        # Increment user's nonce
        balances.value[operationData.from_].nonce += 1
        
        # update user's balance
        balances.value[operationData.from_].balance -=  operationData.amount
        balances.value[operationData.to_].balance += operationData.amount
        
        return balances.value 
    
    
    def approveFeeless(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.s, sp.TSignature)
        sp.set_type(params.k, sp.TKey)
        sp.set_type(params.b, sp.TBytes)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        sp.set_type(params.thisAddress, sp.TAddress)
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)
        
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        transferAgents = sp.unpack(params.data[transferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        checkRecord = sp.record(transferAgents = transferAgents, administrator = administrator, sender = params.sender)
        
        self.onlyAdminOrTransferAgent(checkRecord)
        
        checkSignatureRecord = sp.record(b = params.b, s = params.s, k = params.k)
        
        self.checkSignature(checkSignatureRecord)
        
        #####
        
        operationData = self.unpackOperationData(params.b)
        
        self.checkSameContract(operationData.contractAddress, params.thisAddress)
        
        #####
        
        checkNonceRecord = sp.record(storedNonce = balances.value[operationData.from_].nonce, nonce = operationData.nonce)
        
        self.checkNonce(checkNonceRecord)
        
        # message =  "amount to approve must be greater than zero"
        sp.verify(operationData.amount > 0, message =  "11")
        
        sp.if balances.value[operationData.from_].approvals.contains(operationData.to_):
            sp.if balances.value[operationData.from_].approvals[operationData.to_] > 0:
                # message = account already allowed to spend > 0 amount.
                sp.failwith("29")

        # Increment user's nonce
        balances.value[operationData.from_].nonce += 1
        
        # update allowance
        balances.value[operationData.from_].approvals[operationData.to_] = operationData.amount

        return balances.value
        
    
    
    def increaseAllowanceFeeless(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.s, sp.TSignature)
        sp.set_type(params.k, sp.TKey)
        sp.set_type(params.b, sp.TBytes)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        sp.set_type(params.thisAddress, sp.TAddress)
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)
        
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        transferAgents = sp.unpack(params.data[transferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        checkRecord = sp.record(transferAgents = transferAgents, administrator = administrator, sender = params.sender)
        
        self.onlyAdminOrTransferAgent(checkRecord)
        
        checkSignatureRecord = sp.record(b = params.b, s = params.s, k = params.k)
        
        self.checkSignature(checkSignatureRecord)
        
        #####
        
        operationData = self.unpackOperationData(params.b)
        
        self.checkSameContract(operationData.contractAddress, params.thisAddress)
        
        #####
        
        checkNonceRecord = sp.record(storedNonce = balances.value[operationData.from_].nonce, nonce = operationData.nonce)
        
        self.checkNonce(checkNonceRecord)
        
        # message =  "amount to approve must be greater than zero"
        sp.verify(operationData.amount > 0, message =  "11")
        
        # message = "impossible to increase allowance of unexistant account"
        sp.verify(balances.value[operationData.from_].approvals.contains(operationData.to_), message = "13")

        # Increment user's nonce
        balances.value[operationData.from_].nonce += 1
        
        # update allowance
        balances.value[operationData.from_].approvals[operationData.to_] += operationData.amount

        return balances.value
    
    def decreaseAllowanceFeeless(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.s, sp.TSignature)
        sp.set_type(params.k, sp.TKey)
        sp.set_type(params.b, sp.TBytes)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)
        
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        transferAgents = sp.unpack(params.data[transferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        checkRecord = sp.record(transferAgents = transferAgents, administrator = administrator, sender = params.sender)
        
        self.onlyAdminOrTransferAgent(checkRecord)
        
        checkSignatureRecord = sp.record(b = params.b, s = params.s, k = params.k)
        
        self.checkSignature(checkSignatureRecord)
        
        #####
        
        operationData = self.unpackOperationData(params.b)
        
        self.checkSameContract(operationData.contractAddress, params.thisAddress)
        
        #####
        
        checkNonceRecord = sp.record(storedNonce = balances.value[operationData.from_].nonce, nonce = operationData.nonce)
        
        self.checkNonce(checkNonceRecord)
        
        # message =  "amount to approve must be greater than zero"
        sp.verify(operationData.amount > 0, message =  "11")
        
        # message = "impossible to decrease allowance of unexistant account"
        sp.verify(balances.value[operationData.from_].approvals.contains(operationData.to_), message = "15")
            
        # message = "amount to decrease must be smaller than the one already approved"
        sp.verify((balances.value[operationData.from_].approvals[operationData.to_] > operationData.amount), message = "16")

        # Increment user's nonce
        balances.value[operationData.from_].nonce += 1
        
        # update allowance
        balances.value[operationData.from_].approvals[operationData.to_] -= operationData.amount

        return balances.value
        
        
    @sp.entry_point
    def buildFeelessEntrypoints(self):

        callTransferFeeless = sp.some(sp.build_lambda(self.transferFeeless))
        self.data.entrypointsBytes[callTransferFeelessInBytes] = sp.pack(callTransferFeeless.open_some())
        
        callApproveFeeless = sp.some(sp.build_lambda(self.approveFeeless))
        self.data.entrypointsBytes[callApproveFeelessInBytes] = sp.pack(callApproveFeeless.open_some())
        
        callIncreaseAllowanceFeeless = sp.some(sp.build_lambda(self.increaseAllowanceFeeless))
        self.data.entrypointsBytes[callIncreaseAllowanceFeelessInBytes] = sp.pack(callIncreaseAllowanceFeeless.open_some())
        
        callDecreaseAllowanceFeeless = sp.some(sp.build_lambda(self.decreaseAllowanceFeeless))
        self.data.entrypointsBytes[callDecreaseAllowanceFeelessInBytes] = sp.pack(callDecreaseAllowanceFeeless.open_some())
        
        
    @sp.entry_point
    def sendEntrypoints(self, params):
        
        mapRecord = {

            callTransferFeelessInBytes: self.data.entrypointsBytes[callTransferFeelessInBytes],
            
            callApproveFeelessInBytes: self.data.entrypointsBytes[callApproveFeelessInBytes],
            
            callIncreaseAllowanceFeelessInBytes: self.data.entrypointsBytes[callIncreaseAllowanceFeelessInBytes],
            
            callDecreaseAllowanceFeelessInBytes: self.data.entrypointsBytes[callDecreaseAllowanceFeelessInBytes],
            
        } 
        
        EPType = sp.TMap(sp.TBytes, sp.TBytes)
        
        sp.transfer(
            mapRecord, 
            sp.tez(0), 
            sp.contract(
                t = EPType,
                address = params.target,
                entry_point = "receiveFeelessEntrypoints"
                ).open_some()
            )  
             
       
@sp.add_test(name = "FeelessBuilder")
def test():
    scenario = sp.test_scenario()
    scenario.h1("FeelessBuilder Contract")

    admin = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")


    c1 = FeelessBuilder(admin)

    scenario += c1
    
    