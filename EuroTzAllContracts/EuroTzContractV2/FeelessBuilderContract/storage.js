import conf from "../../../conf/conf";

export default {
  prim: "Pair",
  args: [{ string: conf.adminAddress }, []],
};
