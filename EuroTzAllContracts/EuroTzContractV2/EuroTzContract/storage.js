import conf from "../../../conf/conf";

export default {
  prim: "Pair",
  args: [
    {
      prim: "Pair",
      args: [{ string: conf.adminAddress }, []],
    },
    { prim: "Pair", args: [[], { int: "0" }] },
  ],
};
