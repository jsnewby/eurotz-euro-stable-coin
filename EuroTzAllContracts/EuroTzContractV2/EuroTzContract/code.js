export default [
  {
    prim: "storage",
    args: [
      {
        prim: "pair",
        args: [
          {
            prim: "pair",
            args: [
              { prim: "address", annots: ["%administrator"] },
              {
                prim: "big_map",
                args: [
                  { prim: "address" },
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "map",
                        args: [{ prim: "address" }, { prim: "int" }],
                        annots: ["%approvals"],
                      },
                      {
                        prim: "pair",
                        args: [
                          { prim: "int", annots: ["%balance"] },
                          { prim: "int", annots: ["%nonce"] },
                        ],
                      },
                    ],
                  },
                ],
                annots: ["%balances"],
              },
            ],
          },
          {
            prim: "pair",
            args: [
              {
                prim: "big_map",
                args: [{ prim: "bytes" }, { prim: "bytes" }],
                annots: ["%bigMapData"],
              },
              { prim: "int", annots: ["%totalSupply"] },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "parameter",
    args: [
      {
        prim: "or",
        args: [
          {
            prim: "or",
            args: [
              {
                prim: "pair",
                args: [
                  { prim: "int", annots: ["%amount"] },
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%f"] },
                      { prim: "address", annots: ["%t"] },
                    ],
                  },
                ],
                annots: ["%approve"],
              },
              {
                prim: "or",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "bytes", annots: ["%b"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "key", annots: ["%k"] },
                          { prim: "signature", annots: ["%s"] },
                        ],
                      },
                    ],
                    annots: ["%approveFeeless"],
                  },
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%address"] },
                      { prim: "int", annots: ["%amount"] },
                    ],
                    annots: ["%burn"],
                  },
                ],
              },
            ],
          },
          {
            prim: "or",
            args: [
              {
                prim: "or",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%address"] },
                      { prim: "int", annots: ["%amount"] },
                    ],
                    annots: ["%mint"],
                  },
                  {
                    prim: "pair",
                    args: [
                      { prim: "int", annots: ["%amount"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "address", annots: ["%f"] },
                          { prim: "address", annots: ["%t"] },
                        ],
                      },
                    ],
                    annots: ["%transfer"],
                  },
                ],
              },
              {
                prim: "or",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "bytes", annots: ["%b"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "key", annots: ["%k"] },
                          { prim: "signature", annots: ["%s"] },
                        ],
                      },
                    ],
                    annots: ["%transferFeeless"],
                  },
                  {
                    prim: "map",
                    args: [{ prim: "bytes" }, { prim: "bytes" }],
                    annots: ["%upgrade"],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "code",
    args: [
      [
        { prim: "DUP" },
        { prim: "CDR" },
        { prim: "SWAP" },
        { prim: "CAR" },
        {
          prim: "IF_LEFT",
          args: [
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    [
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      {
                        prim: "PUSH",
                        args: [
                          { prim: "bytes" },
                          { bytes: "05010000000b63616c6c417070726f7665" },
                        ],
                      },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:6" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "lambda",
                            args: [
                              {
                                prim: "pair",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "int", annots: ["%amount"] },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "big_map",
                                            args: [
                                              { prim: "address" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  {
                                                    prim: "map",
                                                    args: [
                                                      { prim: "address" },
                                                      { prim: "int" },
                                                    ],
                                                    annots: ["%approvals"],
                                                  },
                                                  {
                                                    prim: "pair",
                                                    args: [
                                                      {
                                                        prim: "int",
                                                        annots: ["%balance"],
                                                      },
                                                      {
                                                        prim: "int",
                                                        annots: ["%nonce"],
                                                      },
                                                    ],
                                                  },
                                                ],
                                              },
                                            ],
                                            annots: ["%balances"],
                                          },
                                          {
                                            prim: "big_map",
                                            args: [
                                              { prim: "bytes" },
                                              { prim: "bytes" },
                                            ],
                                            annots: ["%data"],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address", annots: ["%f"] },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "address",
                                            annots: ["%sender"],
                                          },
                                          { prim: "address", annots: ["%t"] },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "big_map",
                                args: [
                                  { prim: "address" },
                                  {
                                    prim: "pair",
                                    args: [
                                      {
                                        prim: "map",
                                        args: [
                                          { prim: "address" },
                                          { prim: "int" },
                                        ],
                                        annots: ["%approvals"],
                                      },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "int", annots: ["%balance"] },
                                          { prim: "int", annots: ["%nonce"] },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "SENDER" },
                      { prim: "PAIR", annots: ["%sender", "%t"] },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%f"] },
                      { prim: "DIG", args: [{ int: "5" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "6" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "6" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "PAIR", annots: ["%balances", "%data"] },
                      { prim: "DIG", args: [{ int: "5" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "6" }] },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%amount"] },
                      { prim: "PAIR" },
                      { prim: "EXEC" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      {
                        prim: "PUSH",
                        args: [
                          { prim: "bytes" },
                          {
                            bytes:
                              "0501000000186576656e7453696e6b436f6e747261637441646472657373",
                          },
                        ],
                      },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:20" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "UNPACK", args: [{ prim: "address" }] },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      {
                        prim: "CONTRACT",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "int", annots: ["%amount"] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address", annots: ["%owner"] },
                                  { prim: "address", annots: ["%spender"] },
                                ],
                              },
                            ],
                          },
                        ],
                        annots: ["%approvalEvent"],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%owner", "%spender"] },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%amount"] },
                      { prim: "TRANSFER_TOKENS" },
                      { prim: "CONS" },
                    ],
                  ],
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001263616c6c417070726f76654665656c657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:12" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "lambda",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "bytes", annots: ["%b"] },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "map",
                                                          args: [
                                                            { prim: "address" },
                                                            { prim: "int" },
                                                          ],
                                                          annots: [
                                                            "%approvals",
                                                          ],
                                                        },
                                                        {
                                                          prim: "pair",
                                                          args: [
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%balance",
                                                              ],
                                                            },
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%nonce",
                                                              ],
                                                            },
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                  annots: ["%balances"],
                                                },
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "bytes" },
                                                    { prim: "bytes" },
                                                  ],
                                                  annots: ["%data"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "key", annots: ["%k"] },
                                                {
                                                  prim: "signature",
                                                  annots: ["%s"],
                                                },
                                              ],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "address",
                                                  annots: ["%sender"],
                                                },
                                                {
                                                  prim: "address",
                                                  annots: ["%thisAddress"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "big_map",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "map",
                                              args: [
                                                { prim: "address" },
                                                { prim: "int" },
                                              ],
                                              annots: ["%approvals"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "int",
                                                  annots: ["%balance"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%nonce"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "SELF" },
                            { prim: "ADDRESS" },
                            { prim: "SENDER" },
                            {
                              prim: "PAIR",
                              annots: ["%sender", "%thisAddress"],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%k", "%s"] },
                            { prim: "PAIR" },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "PAIR", annots: ["%balances", "%data"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%b"] },
                            { prim: "PAIR" },
                            { prim: "EXEC" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "0501000000186576656e7453696e6b436f6e747261637441646472657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:20" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "UNPACK", args: [{ prim: "address" }] },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "CONTRACT",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int", annots: ["%amount"] },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address", annots: ["%owner"] },
                                        {
                                          prim: "address",
                                          annots: ["%spender"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                              annots: ["%approvalEvent"],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "int" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "int" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%owner", "%spender"] },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "int" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%amount"] },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                          ],
                        ],
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000863616c6c4275726e" },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:4" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "lambda",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "address",
                                              annots: ["%address"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "int",
                                                  annots: ["%amount"],
                                                },
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "map",
                                                          args: [
                                                            { prim: "address" },
                                                            { prim: "int" },
                                                          ],
                                                          annots: [
                                                            "%approvals",
                                                          ],
                                                        },
                                                        {
                                                          prim: "pair",
                                                          args: [
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%balance",
                                                              ],
                                                            },
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%nonce",
                                                              ],
                                                            },
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                  annots: ["%balances"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "big_map",
                                              args: [
                                                { prim: "bytes" },
                                                { prim: "bytes" },
                                              ],
                                              annots: ["%data"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "address",
                                                  annots: ["%sender"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%totalSupply"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "big_map",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "map",
                                                  args: [
                                                    { prim: "address" },
                                                    { prim: "int" },
                                                  ],
                                                  annots: ["%approvals"],
                                                },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "int",
                                                      annots: ["%balance"],
                                                    },
                                                    {
                                                      prim: "int",
                                                      annots: ["%nonce"],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                          annots: ["%balances"],
                                        },
                                        {
                                          prim: "int",
                                          annots: ["%totalSupply"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "SENDER" },
                            {
                              prim: "PAIR",
                              annots: ["%sender", "%totalSupply"],
                            },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%data"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "PAIR", annots: ["%amount", "%balances"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%address"] },
                            { prim: "PAIR" },
                            { prim: "EXEC" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000863616c6c4275726e" },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:4" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "lambda",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "address",
                                              annots: ["%address"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "int",
                                                  annots: ["%amount"],
                                                },
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "map",
                                                          args: [
                                                            { prim: "address" },
                                                            { prim: "int" },
                                                          ],
                                                          annots: [
                                                            "%approvals",
                                                          ],
                                                        },
                                                        {
                                                          prim: "pair",
                                                          args: [
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%balance",
                                                              ],
                                                            },
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%nonce",
                                                              ],
                                                            },
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                  annots: ["%balances"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "big_map",
                                              args: [
                                                { prim: "bytes" },
                                                { prim: "bytes" },
                                              ],
                                              annots: ["%data"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "address",
                                                  annots: ["%sender"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%totalSupply"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "big_map",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "map",
                                                  args: [
                                                    { prim: "address" },
                                                    { prim: "int" },
                                                  ],
                                                  annots: ["%approvals"],
                                                },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "int",
                                                      annots: ["%balance"],
                                                    },
                                                    {
                                                      prim: "int",
                                                      annots: ["%nonce"],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                          annots: ["%balances"],
                                        },
                                        {
                                          prim: "int",
                                          annots: ["%totalSupply"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "SENDER" },
                            {
                              prim: "PAIR",
                              annots: ["%sender", "%totalSupply"],
                            },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%data"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "PAIR", annots: ["%amount", "%balances"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%address"] },
                            { prim: "PAIR" },
                            { prim: "EXEC" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "0501000000186576656e7453696e6b436f6e747261637441646472657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:20" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "UNPACK", args: [{ prim: "address" }] },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "CONTRACT",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int", annots: ["%amount"] },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%fromAddress"],
                                        },
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%toAddress"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                              annots: ["%transferEvent"],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "NONE", args: [{ prim: "address" }] },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "SOME" },
                            {
                              prim: "PAIR",
                              annots: ["%fromAddress", "%toAddress"],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "PAIR", annots: ["%amount"] },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                          ],
                        ],
                      ],
                    },
                  ],
                ],
              },
            ],
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000863616c6c4d696e74" },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:3" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "lambda",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "address",
                                              annots: ["%address"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "int",
                                                  annots: ["%amount"],
                                                },
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "map",
                                                          args: [
                                                            { prim: "address" },
                                                            { prim: "int" },
                                                          ],
                                                          annots: [
                                                            "%approvals",
                                                          ],
                                                        },
                                                        {
                                                          prim: "pair",
                                                          args: [
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%balance",
                                                              ],
                                                            },
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%nonce",
                                                              ],
                                                            },
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                  annots: ["%balances"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "big_map",
                                              args: [
                                                { prim: "bytes" },
                                                { prim: "bytes" },
                                              ],
                                              annots: ["%data"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "address",
                                                  annots: ["%sender"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%totalSupply"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "big_map",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "map",
                                                  args: [
                                                    { prim: "address" },
                                                    { prim: "int" },
                                                  ],
                                                  annots: ["%approvals"],
                                                },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "int",
                                                      annots: ["%balance"],
                                                    },
                                                    {
                                                      prim: "int",
                                                      annots: ["%nonce"],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                          annots: ["%balances"],
                                        },
                                        {
                                          prim: "int",
                                          annots: ["%totalSupply"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "SENDER" },
                            {
                              prim: "PAIR",
                              annots: ["%sender", "%totalSupply"],
                            },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%data"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "PAIR", annots: ["%amount", "%balances"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%address"] },
                            { prim: "PAIR" },
                            { prim: "EXEC" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                { bytes: "05010000000863616c6c4d696e74" },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:3" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "lambda",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "address",
                                              annots: ["%address"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "int",
                                                  annots: ["%amount"],
                                                },
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "map",
                                                          args: [
                                                            { prim: "address" },
                                                            { prim: "int" },
                                                          ],
                                                          annots: [
                                                            "%approvals",
                                                          ],
                                                        },
                                                        {
                                                          prim: "pair",
                                                          args: [
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%balance",
                                                              ],
                                                            },
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%nonce",
                                                              ],
                                                            },
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                  annots: ["%balances"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "big_map",
                                              args: [
                                                { prim: "bytes" },
                                                { prim: "bytes" },
                                              ],
                                              annots: ["%data"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "address",
                                                  annots: ["%sender"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%totalSupply"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "big_map",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "map",
                                                  args: [
                                                    { prim: "address" },
                                                    { prim: "int" },
                                                  ],
                                                  annots: ["%approvals"],
                                                },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "int",
                                                      annots: ["%balance"],
                                                    },
                                                    {
                                                      prim: "int",
                                                      annots: ["%nonce"],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                          annots: ["%balances"],
                                        },
                                        {
                                          prim: "int",
                                          annots: ["%totalSupply"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "SENDER" },
                            {
                              prim: "PAIR",
                              annots: ["%sender", "%totalSupply"],
                            },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%data"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "PAIR", annots: ["%amount", "%balances"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%address"] },
                            { prim: "PAIR" },
                            { prim: "EXEC" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "0501000000186576656e7453696e6b436f6e747261637441646472657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:20" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "UNPACK", args: [{ prim: "address" }] },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "CONTRACT",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int", annots: ["%amount"] },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%fromAddress"],
                                        },
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%toAddress"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                              annots: ["%transferEvent"],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CAR" },
                            { prim: "SOME" },
                            { prim: "NONE", args: [{ prim: "address" }] },
                            {
                              prim: "PAIR",
                              annots: ["%fromAddress", "%toAddress"],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "PAIR", annots: ["%amount"] },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                          ],
                        ],
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes: "05010000000c63616c6c5472616e73666572",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:5" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "lambda",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "int",
                                              annots: ["%amount"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "map",
                                                          args: [
                                                            { prim: "address" },
                                                            { prim: "int" },
                                                          ],
                                                          annots: [
                                                            "%approvals",
                                                          ],
                                                        },
                                                        {
                                                          prim: "pair",
                                                          args: [
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%balance",
                                                              ],
                                                            },
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%nonce",
                                                              ],
                                                            },
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                  annots: ["%balances"],
                                                },
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "bytes" },
                                                    { prim: "bytes" },
                                                  ],
                                                  annots: ["%data"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address", annots: ["%f"] },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "address",
                                                  annots: ["%sender"],
                                                },
                                                {
                                                  prim: "address",
                                                  annots: ["%t"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "big_map",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "map",
                                              args: [
                                                { prim: "address" },
                                                { prim: "int" },
                                              ],
                                              annots: ["%approvals"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "int",
                                                  annots: ["%balance"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%nonce"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "SENDER" },
                            { prim: "PAIR", annots: ["%sender", "%t"] },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%f"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "PAIR", annots: ["%balances", "%data"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%amount"] },
                            { prim: "PAIR" },
                            { prim: "EXEC" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "0501000000186576656e7453696e6b436f6e747261637441646472657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:20" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "UNPACK", args: [{ prim: "address" }] },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "CONTRACT",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int", annots: ["%amount"] },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%fromAddress"],
                                        },
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%toAddress"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                              annots: ["%transferEvent"],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "SOME" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "SOME" },
                            {
                              prim: "PAIR",
                              annots: ["%fromAddress", "%toAddress"],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%amount"] },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                          ],
                        ],
                      ],
                    },
                  ],
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "05010000001363616c6c5472616e736665724665656c657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:11" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "lambda",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "bytes", annots: ["%b"] },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "map",
                                                          args: [
                                                            { prim: "address" },
                                                            { prim: "int" },
                                                          ],
                                                          annots: [
                                                            "%approvals",
                                                          ],
                                                        },
                                                        {
                                                          prim: "pair",
                                                          args: [
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%balance",
                                                              ],
                                                            },
                                                            {
                                                              prim: "int",
                                                              annots: [
                                                                "%nonce",
                                                              ],
                                                            },
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                  annots: ["%balances"],
                                                },
                                                {
                                                  prim: "big_map",
                                                  args: [
                                                    { prim: "bytes" },
                                                    { prim: "bytes" },
                                                  ],
                                                  annots: ["%data"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "key", annots: ["%k"] },
                                                {
                                                  prim: "signature",
                                                  annots: ["%s"],
                                                },
                                              ],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "address",
                                                  annots: ["%sender"],
                                                },
                                                {
                                                  prim: "address",
                                                  annots: ["%thisAddress"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "big_map",
                                      args: [
                                        { prim: "address" },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "map",
                                              args: [
                                                { prim: "address" },
                                                { prim: "int" },
                                              ],
                                              annots: ["%approvals"],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "int",
                                                  annots: ["%balance"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%nonce"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "SELF" },
                            { prim: "ADDRESS" },
                            { prim: "SENDER" },
                            {
                              prim: "PAIR",
                              annots: ["%sender", "%thisAddress"],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%k", "%s"] },
                            { prim: "PAIR" },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "PAIR", annots: ["%balances", "%data"] },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%b"] },
                            { prim: "PAIR" },
                            { prim: "EXEC" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "PUSH",
                              args: [
                                { prim: "bytes" },
                                {
                                  bytes:
                                    "0501000000186576656e7453696e6b436f6e747261637441646472657373",
                                },
                              ],
                            },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:20" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "UNPACK", args: [{ prim: "address" }] },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "CONTRACT",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int", annots: ["%amount"] },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%fromAddress"],
                                        },
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%toAddress"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                              annots: ["%transferEvent"],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "int" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "SOME" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "int" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "SOME" },
                            {
                              prim: "PAIR",
                              annots: ["%fromAddress", "%toAddress"],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "int" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "PAIR", annots: ["%amount"] },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                          ],
                        ],
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "SENDER" },
                            { prim: "COMPARE" },
                            { prim: "EQ" },
                            {
                              prim: "IF",
                              args: [
                                [
                                  {
                                    prim: "PUSH",
                                    args: [{ prim: "bool" }, { prim: "True" }],
                                  },
                                ],
                                [
                                  [
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CAR" },
                                    { prim: "CAR" },
                                    { prim: "SOURCE" },
                                    { prim: "COMPARE" },
                                    { prim: "EQ" },
                                  ],
                                ],
                              ],
                            },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "01" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "DUP" },
                            {
                              prim: "ITER",
                              args: [
                                [
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "CDR" },
                                  { prim: "SWAP" },
                                  { prim: "CAR" },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CDR" },
                                  { prim: "SOME" },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "CAR" },
                                  { prim: "UPDATE" },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                ],
                              ],
                            },
                            { prim: "DROP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                          ],
                        ],
                      ],
                    },
                  ],
                ],
              },
            ],
          ],
        },
        { prim: "PAIR" },
      ],
    ],
  },
];
