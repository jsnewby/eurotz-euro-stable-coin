import smartpy as sp

##################

callMintInBytes = sp.bytes("0x05010000000863616c6c4d696e74")
callBurnInBytes = sp.bytes("0x05010000000863616c6c4275726e")
callTransferInBytes = sp.bytes("0x05010000000c63616c6c5472616e73666572")
callApproveInBytes = sp.bytes("0x05010000000b63616c6c417070726f7665")
callIncreaseAllowanceInBytes = sp.bytes("0x05010000001563616c6c496e637265617365416c6c6f77616e6365")
callDecreaseAllowanceInBytes = sp.bytes("0x05010000001563616c6c4465637265617365416c6c6f77616e6365")
callResetAllAllowancesInBytes = sp.bytes("0x05010000001663616c6c5265736574416c6c416c6c6f77616e636573")
callResetAllowanceInBytes = sp.bytes("0x05010000001263616c6c5265736574416c6c6f77616e6365")
callTransferFeelessInBytes = sp.bytes("0x05010000001363616c6c5472616e736665724665656c657373")
callApproveFeelessInBytes = sp.bytes("0x05010000001263616c6c417070726f76654665656c657373")
callIncreaseAllowanceFeelessInBytes = sp.bytes("0x05010000001c63616c6c496e637265617365416c6c6f77616e63654665656c657373")
callDecreaseAllowanceFeelessInBytes = sp.bytes("0x05010000001c63616c6c4465637265617365416c6c6f77616e63654665656c657373")
pausedInBytes = sp.bytes("0x050100000006706175736564")
superTransferAgentsInBytes = sp.bytes("0x05010000001373757065725472616e736665724167656e7473")
transferAgentsInBytes = sp.bytes("0x05010000000e7472616e736665724167656e7473")
eventSinkContractAddressInBytes = sp.bytes("0x0501000000186576656e7453696e6b436f6e747261637441646472657373")
tokenMetadataInBytes = sp.bytes("0x05010000000d746f6b656e4d65746164617461")
administratorInBytes = sp.bytes("0x05010000000d61646d696e6973747261746f72")
totalSupplyInBytes = sp.bytes("0x05010000000b746f74616c537570706c79")
callUnpackOperationDataInBytes = sp.bytes("0x05010000001763616c6c556e7061636b4f7065726174696f6e44617461")

##################

class EuroTz(sp.Contract):
    def __init__(self, admin):
        self.init(
            bigMapData = sp.big_map(
                tkey = sp.TBytes, 
                tvalue = sp.TBytes
                ),
            administrator = admin,
            balances = sp.big_map(tkey = sp.TAddress, tvalue = sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
            totalSupply = sp.int(0)
            )
    '''
    Views entrypoints
    '''
            
    def _getTotalSupply(self, params):
        
        c = sp.contract(
                        t = sp.TInt, 
                        address = params.viewTotalSupplyContractAddress, 
                        ).open_some()
        sp.transfer(self.data.totalSupply, sp.mutez(0), c)
    
    
    def _getBalance(self, params):
        
        # message = user not found.
        sp.verify(self.data.balances.contains(params.address), message = "32")
        
        userBalance = self.data.balances[params.address].balance
        
        c = sp.contract(
                        t = sp.TInt, 
                        address = params.viewBalanceContractAddress, 
                        ).open_some()
        sp.transfer(userBalance, sp.mutez(0), c)
    
    def _getAllowance(self, params):
        
        # message = user not found.
        sp.verify(self.data.balances.contains(params.owner), message = "32")
        
        # message = spender not found in the owner's approvals map.
        sp.verify(self.data.balances[params.owner].approvals.contains(params.spender), message = "33")
        
        allowance = self.data.balances[params.address].approvals[params.spender]
        
        c = sp.contract(
                        t = sp.TInt, 
                        address = params.viewAllowanceContractAddress, 
                        ).open_some()
        sp.transfer(allowance, sp.mutez(0), c)    
    
    '''
    EventSink Utils
    '''
    
    def callEventSinkData(self, operationbytes):
        # Unpack data in order to get: from, to, amount
        dataRecordType = sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TPair(sp.TAddress, sp.TPair(sp.TAddress, sp.TAddress))))
        
        dataRecord = sp.unpack(
            operationbytes, 
            t = dataRecordType
            ).open_some()
        
        amount = sp.fst(dataRecord)
        from_ = sp.fst(sp.snd(sp.snd(dataRecord)))
        to_ = sp.fst(sp.snd(sp.snd(sp.snd(dataRecord))))

        operationData = sp.record(amount = amount, from_ = from_, to_ = to_)
        
        return operationData

    def callEventSinkContractTransfer(self, params):
        entryPointName = "transferEvent"
        transferParamsRecord = sp.record(amount = params.amount, fromAddress = params.fromAddress, toAddress = params.toAddress)
        c = sp.contract(
                        t = sp.TRecord(amount = sp.TIntOrNat, fromAddress = sp.TOption(sp.TAddress), toAddress = sp.TOption(sp.TAddress)), 
                        address = params.eventSinkContractAddress, 
                        entry_point = entryPointName
                        ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
        
        
    def callEventSinkContractApprove(self, params):
        approvalParamsRecord = sp.record(amount = params.amount, owner = params.owner, spender = params.spender)
        entryPointName = "approvalEvent"
        c = sp.contract(
                        t = sp.TRecord(amount = sp.TIntOrNat, owner = sp.TAddress, spender = sp.TAddress), 
                        address = params.eventSinkContractAddress, 
                        entry_point = entryPointName
                        ).open_some()
        sp.transfer(approvalParamsRecord, sp.mutez(0), c)
 
    '''
    Set contract metadata & entrypoints code 
    '''
        
    @sp.entry_point
    def upgrade(self, params):
        
        sp.verify(((sp.sender == self.data.administrator) | (sp.source == self.data.administrator)), message = "01")
        
        sp.for x in params.items():
            self.data.bigMapData[x.key] = x.value
    
    '''
    Main entrypoints
    '''

    @sp.entry_point
    def mint(self, params):
        
        # get the entrypoint script in bytes format
        epBytesScript = self.data.bigMapData[callMintInBytes]
        
        # unpack entrypoint bytes to get the lambda
        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        amount = sp.TInt, 
                        address = sp.TAddress, 
                        sender = sp.TAddress,
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        totalSupply = sp.TInt
                        ),
                    sp.TRecord(
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        totalSupply = sp.TInt
                        )    
                ))
               
        epParams = sp.record(
            data = self.data.bigMapData, 
            address = params.address, 
            amount = params.amount, 
            sender = sp.sender,
            balances = self.data.balances,
            totalSupply = self.data.totalSupply
            )
        
        # execute the lambda    
        execMint = epScript.open_some()(epParams)       
        
        # update the contract's storage after executoion
        self.data.balances = execMint.balances
        self.data.totalSupply = execMint.totalSupply
        
        # get the eventSinkContractAddress
        eventSinkContractAddress = sp.unpack(self.data.bigMapData[eventSinkContractAddressInBytes], t = sp.TAddress).open_some()
        
        # call EventSink contract
        self.callEventSinkContractTransfer(
            sp.record(
                amount = params.amount, 
                fromAddress = sp.none, 
                toAddress = sp.some(params.address),
                eventSinkContractAddress = eventSinkContractAddress
                )
            )

    
    @sp.entry_point
    def burn(self, params):
        
        epBytesScript = self.data.bigMapData[callBurnInBytes]
        
        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        amount = sp.TInt, 
                        address = sp.TAddress, 
                        sender = sp.TAddress,
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        totalSupply = sp.TInt
                        ),
                    sp.TRecord(
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        totalSupply = sp.TInt
                        )    
                ))
               
        epParams = sp.record(
            data = self.data.bigMapData, 
            address = params.address, 
            amount = params.amount, 
            sender = sp.sender,
            balances = self.data.balances,
            totalSupply = self.data.totalSupply
            )
            
        execBurn = epScript.open_some()(epParams)       

        self.data.balances = execBurn.balances
        self.data.totalSupply = execBurn.totalSupply
        
        eventSinkContractAddress = sp.unpack(self.data.bigMapData[eventSinkContractAddressInBytes], t = sp.TAddress).open_some()
        
        self.callEventSinkContractTransfer(
            sp.record(
                amount = params.amount, 
                toAddress = sp.none, 
                fromAddress = sp.some(params.address),
                eventSinkContractAddress = eventSinkContractAddress
                )
            )
        
    @sp.entry_point
    def transfer(self, params):

        epBytesScript = self.data.bigMapData[callTransferInBytes]
        
        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        amount = sp.TInt, 
                        f = sp.TAddress,
                        t = sp.TAddress,
                        sender = sp.TAddress,
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        ),
                        sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt))
                ))
               
        epParams = sp.record(
            data = self.data.bigMapData, 
            f = params.f,
            t = params.t,
            amount = params.amount, 
            sender = sp.sender,
            balances = self.data.balances
            )
            

        self.data.balances = epScript.open_some()(epParams) 
        
        eventSinkContractAddress = sp.unpack(self.data.bigMapData[eventSinkContractAddressInBytes], t = sp.TAddress).open_some()
        
        self.callEventSinkContractTransfer(
            sp.record(
                amount = params.amount, 
                toAddress = sp.some(params.t),
                fromAddress = sp.some(params.f),
                eventSinkContractAddress = eventSinkContractAddress
                )
            )

        
    @sp.entry_point
    def approve(self, params):
        
        epBytesScript = self.data.bigMapData[callApproveInBytes]
        
        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        amount = sp.TInt, 
                        f = sp.TAddress, 
                        sender = sp.TAddress, 
                        t = sp.TAddress,
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        ),
                    sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt))
                ))
       
        epParams = sp.record(
            data = self.data.bigMapData, 
            f = params.f, 
            amount = params.amount, 
            sender = sp.sender,
            t = params.t,
            balances = self.data.balances
            )
        
        self.data.balances = epScript.open_some()(epParams)
        
        eventSinkContractAddress = sp.unpack(self.data.bigMapData[eventSinkContractAddressInBytes], t = sp.TAddress).open_some()
        
        self.callEventSinkContractApprove(
            sp.record(
                amount = params.amount, 
                spender = params.t,
                owner = params.f,
                eventSinkContractAddress = eventSinkContractAddress
                )
            )
    


    @sp.entry_point
    def changeAllowance(self, params):
        sp.set_type(params.entryPointName, sp.TString)

        epBytesScript = self.data.bigMapData[sp.pack(params.entryPointName)]

        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        amount = sp.TInt, 
                        f = sp.TAddress, 
                        sender = sp.TAddress, 
                        t = sp.TAddress,
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        ),
                    sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt))
                ))
       
        epParams = sp.record(
            data = self.data.bigMapData, 
            f = params.f, 
            amount = params.amount, 
            sender = sp.sender,
            t = params.t,
            balances = self.data.balances
            )
     
        self.data.balances = epScript.open_some()(epParams) 
        
        eventSinkContractAddress = sp.unpack(self.data.bigMapData[eventSinkContractAddressInBytes], t = sp.TAddress).open_some()
        
        self.callEventSinkContractApprove(
            sp.record(
                amount = params.amount, 
                spender = params.t,
                owner = params.f,
                eventSinkContractAddress = eventSinkContractAddress
                )
            )        
    
    @sp.entry_point
    def resetAllAllowances(self, params):

        epBytesScript = self.data.bigMapData[callResetAllAllowancesInBytes]
        

        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        address = sp.TAddress, 
                        sender = sp.TAddress, 
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        ),
                    sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt))
                ))
       
        epParams = sp.record(
            data = self.data.bigMapData, 
            address = params.address, 
            sender = sp.sender,
            balances = self.data.balances
            )
     
        self.data.balances = epScript.open_some()(epParams)
        
    @sp.entry_point
    def resetAllowance(self, params):
        
        epBytesScript = self.data.bigMapData[callResetAllowanceInBytes]
        

        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        address = sp.TAddress,
                        f = sp.TAddress,
                        sender = sp.TAddress, 
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        ),
                    sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt))
                ))
       
        epParams = sp.record(
            data = self.data.bigMapData, 
            address = params.address, 
            f = params.f,
            sender = sp.sender,
            balances = self.data.balances
            )
     
        self.data.balances = epScript.open_some()(epParams)         
    

    
    @sp.entry_point
    def transferFeeless(self, params):
 
        epBytesScript = self.data.bigMapData[callTransferFeelessInBytes]
        
        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        sender = sp.TAddress, 
                        s = sp.TSignature,
                        b = sp.TBytes,
                        k = sp.TKey,
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        thisAddress = sp.TAddress
                        ),
                    sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt))

                ))

        epParams = sp.record(
            data = self.data.bigMapData, 
            sender = sp.sender, 
            s = params.s,
            b = params.b,
            k = params.k,
            balances = self.data.balances,
            thisAddress = sp.to_address(sp.self)
            )
        
        self.data.balances = epScript.open_some()(epParams)
        
        operationData = self.callEventSinkData(params.b) 

        eventSinkContractAddress = sp.unpack(self.data.bigMapData[eventSinkContractAddressInBytes], t = sp.TAddress).open_some()
        
        self.callEventSinkContractTransfer(
            sp.record(
                amount = operationData.amount, 
                toAddress = sp.some(operationData.to_),
                fromAddress = sp.some(operationData.from_),
                eventSinkContractAddress = eventSinkContractAddress
                )
            )
    
    @sp.entry_point
    def approveFeeless(self, params):
        
        epBytesScript = self.data.bigMapData[callApproveFeelessInBytes]


        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        sender = sp.TAddress, 
                        s = sp.TSignature,
                        b = sp.TBytes,
                        k = sp.TKey,
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        thisAddress = sp.TAddress
                        ),
                    sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt))
                ))

        epParams = sp.record(
            data = self.data.bigMapData, 
            sender = sp.sender, 
            s = params.s,
            b = params.b,
            k = params.k,
            balances = self.data.balances,
            thisAddress = sp.to_address(sp.self)
            )
     
        self.data.balances = epScript.open_some()(epParams) 
        
        operationData = self.callEventSinkData(params.b) 

        eventSinkContractAddress = sp.unpack(self.data.bigMapData[eventSinkContractAddressInBytes], t = sp.TAddress).open_some()
        
        self.callEventSinkContractApprove(
            sp.record(
                amount = operationData.amount, 
                spender = operationData.to_,
                owner = operationData.from_,
                eventSinkContractAddress = eventSinkContractAddress
                )
            )
            
   
    @sp.entry_point
    def changeAllowanceFeeless(self, params):
        sp.set_type(params.entryPointName, sp.TString)
        
        epBytesScript = self.data.bigMapData[sp.pack(params.entryPointName)]

        epScript = sp.unpack(
            epBytesScript, 
            t = sp.TLambda(
                    sp.TRecord(
                        data = sp.TBigMap(sp.TBytes, sp.TBytes), 
                        sender = sp.TAddress, 
                        s = sp.TSignature,
                        b = sp.TBytes,
                        k = sp.TKey,
                        balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)),
                        thisAddress = sp.TAddress
                        ),
                    sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt))
                ))

        epParams = sp.record(
            data = self.data.bigMapData, 
            sender = sp.sender, 
            s = params.s,
            b = params.b,
            k = params.k,
            balances = self.data.balances,
            thisAddress = sp.to_address(sp.self)
            )
        
        self.data.balances = epScript.open_some()(epParams)
        
        operationData = self.callEventSinkData(params.b) 

        
        eventSinkContractAddress = sp.unpack(self.data.bigMapData[eventSinkContractAddressInBytes], t = sp.TAddress).open_some()
        
        self.callEventSinkContractApprove(
            sp.record(
                amount = operationData.amount, 
                spender = operationData.to_,
                owner = operationData.from_,
                eventSinkContractAddress = eventSinkContractAddress
                )
            ) 
            
 
    '''
    View entrypoints: A contract which implements approvable ledger must have these 
    entrypoints.
    
    We can add an sp.verify if we want to control who can call the views entrypoins to 
    avoid that users invoke operations on an arbitrary callback contract on behalf of 
    this contract.
    '''
    

    @sp.entry_point
    def getTotalSupply(self, params): 
        self._getTotalSupply(params)
    
    @sp.entry_point
    def getBalance(self, params):   
        self._getBalance(params)
    
    @sp.entry_point
    def getAllowance(self, params):  
        self._getAllowance(params)
      
    
    '''
    The followning view entrypoints will be implemented in the proxy contract to avoid 
    adding extra entrypoints in the euroTz contract.
    '''
    
    '''
    @sp.entry_point
    def getAdministrator(self, params): 
        
    @sp.entry_point
    def getTransferAgents(self, params):    
    
    @sp.entry_point
    def getSuperTransferAgents(self, params):
        
    @sp.entry_point
    def getTokenMetaData(self, params):
        
    @sp.entry_point
    def getPauseState(self, params):
        
    @sp.entry_point
    def getEventSinkContractAddress(self, params):    
    '''
    
    
@sp.add_test(name = "EuroTz")
def test():
    scenario = sp.test_scenario()
    scenario.h1("EuroTz Contract")

    admin = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")

    c1 = EuroTz(admin)

    scenario += c1
    
    