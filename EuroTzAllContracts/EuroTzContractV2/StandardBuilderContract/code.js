export default [
  {
    "prim": "storage",
    "args": [
      {
        "prim": "pair",
        "args": [
          { "prim": "address", "annots": [ "%administrator" ] },
          { "prim": "big_map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ], "annots": [ "%entrypointsBytes" ] }
        ]
      }
    ]
  },
  {
    "prim": "parameter",
    "args": [ { "prim": "or", "args": [ { "prim": "unit", "annots": [ "%buildStandardEntrypoints" ] }, { "prim": "address", "annots": [ "%sendEntrypoints" ] } ] } ]
  },
  {
    "prim": "code",
    "args": [
      [
        { "prim": "DUP" },
        { "prim": "CDR" },
        { "prim": "SWAP" },
        { "prim": "CAR" },
        {
          "prim": "IF_LEFT",
          "args": [
            [
              [
                { "prim": "SWAP" },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "2" } ] },
                { "prim": "DUP" },
                { "prim": "CAR" },
                { "prim": "SWAP" },
                { "prim": "CDR" },
                {
                  "prim": "LAMBDA",
                  "args": [
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "address", "annots": [ "%address" ] },
                            {
                              "prim": "pair",
                              "args": [
                                { "prim": "int", "annots": [ "%amount" ] },
                                {
                                  "prim": "big_map",
                                  "args": [
                                    { "prim": "address" },
                                    {
                                      "prim": "pair",
                                      "args": [
                                        { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                        { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                      ]
                                    }
                                  ],
                                  "annots": [ "%balances" ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "big_map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ], "annots": [ "%data" ] },
                            { "prim": "pair", "args": [ { "prim": "address", "annots": [ "%sender" ] }, { "prim": "int", "annots": [ "%totalSupply" ] } ] }
                          ]
                        }
                      ]
                    },
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "big_map",
                          "args": [
                            { "prim": "address" },
                            {
                              "prim": "pair",
                              "args": [
                                { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                              ]
                            }
                          ],
                          "annots": [ "%balances" ]
                        },
                        { "prim": "int", "annots": [ "%totalSupply" ] }
                      ]
                    },
                    [
                      [
                        { "prim": "DUP" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000d61646d696e6973747261746f72" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:12" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "UNPACK", "args": [ { "prim": "address" } ] },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "COMPARE" },
                        { "prim": "EQ" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "01" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "MEM" },
                        {
                          "prim": "IF",
                          "args": [
                            [ [] ],
                            [
                              [
                                {
                                  "prim": "PUSH",
                                  "args": [
                                    {
                                      "prim": "option",
                                      "args": [
                                        {
                                          "prim": "pair",
                                          "args": [
                                            { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                            { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                          ]
                                        }
                                      ]
                                    },
                                    { "prim": "Some", "args": [ { "prim": "Pair", "args": [ [], { "prim": "Pair", "args": [ { "int": "0" }, { "int": "0" } ] } ] } ] }
                                  ]
                                },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CAR" },
                                { "prim": "CAR" },
                                { "prim": "UPDATE" }
                              ]
                            ]
                          ]
                        },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "ADD" },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "3" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "4" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "SWAP" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "DIG", "args": [ { "int": "5" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "6" } ] },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "DIG", "args": [ { "int": "7" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:103" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "ADD" },
                        { "prim": "PAIR" },
                        { "prim": "SWAP" },
                        { "prim": "PAIR" },
                        { "prim": "SOME" },
                        { "prim": "SWAP" },
                        { "prim": "UPDATE" },
                        { "prim": "SWAP" },
                        { "prim": "SWAP" },
                        { "prim": "PAIR", "annots": [ "%balances", "%totalSupply" ] }
                      ]
                    ]
                  ]
                },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DROP" },
                { "prim": "SOME" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                { "prim": "PACK" },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000863616c6c4d696e74" } ] },
                { "prim": "UPDATE" },
                { "prim": "SWAP" },
                { "prim": "PAIR" },
                { "prim": "SWAP" },
                { "prim": "SWAP" },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "2" } ] },
                { "prim": "DUP" },
                { "prim": "CAR" },
                { "prim": "SWAP" },
                { "prim": "CDR" },
                {
                  "prim": "LAMBDA",
                  "args": [
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "address", "annots": [ "%address" ] },
                            {
                              "prim": "pair",
                              "args": [
                                { "prim": "int", "annots": [ "%amount" ] },
                                {
                                  "prim": "big_map",
                                  "args": [
                                    { "prim": "address" },
                                    {
                                      "prim": "pair",
                                      "args": [
                                        { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                        { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                      ]
                                    }
                                  ],
                                  "annots": [ "%balances" ]
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "big_map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ], "annots": [ "%data" ] },
                            { "prim": "pair", "args": [ { "prim": "address", "annots": [ "%sender" ] }, { "prim": "int", "annots": [ "%totalSupply" ] } ] }
                          ]
                        }
                      ]
                    },
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "big_map",
                          "args": [
                            { "prim": "address" },
                            {
                              "prim": "pair",
                              "args": [
                                { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                              ]
                            }
                          ],
                          "annots": [ "%balances" ]
                        },
                        { "prim": "int", "annots": [ "%totalSupply" ] }
                      ]
                    },
                    [
                      [
                        { "prim": "DUP" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000d61646d696e6973747261746f72" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:12" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "UNPACK", "args": [ { "prim": "address" } ] },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "COMPARE" },
                        { "prim": "EQ" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "01" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "DIG", "args": [ { "int": "3" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "4" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:140" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "COMPARE" },
                        { "prim": "GE" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "09" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "SWAP" },
                        { "prim": "SUB" },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "3" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "4" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "SWAP" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "DIG", "args": [ { "int": "5" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "6" } ] },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "DIG", "args": [ { "int": "7" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:140" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "SUB" },
                        { "prim": "PAIR" },
                        { "prim": "SWAP" },
                        { "prim": "PAIR" },
                        { "prim": "SOME" },
                        { "prim": "SWAP" },
                        { "prim": "UPDATE" },
                        { "prim": "SWAP" },
                        { "prim": "SWAP" },
                        { "prim": "PAIR", "annots": [ "%balances", "%totalSupply" ] }
                      ]
                    ]
                  ]
                },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DROP" },
                { "prim": "SOME" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                { "prim": "PACK" },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000863616c6c4275726e" } ] },
                { "prim": "UPDATE" },
                { "prim": "SWAP" },
                { "prim": "PAIR" },
                { "prim": "SWAP" },
                { "prim": "SWAP" },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "2" } ] },
                { "prim": "DUP" },
                { "prim": "CAR" },
                { "prim": "SWAP" },
                { "prim": "CDR" },
                {
                  "prim": "LAMBDA",
                  "args": [
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "int", "annots": [ "%amount" ] },
                            {
                              "prim": "pair",
                              "args": [
                                {
                                  "prim": "big_map",
                                  "args": [
                                    { "prim": "address" },
                                    {
                                      "prim": "pair",
                                      "args": [
                                        { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                        { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                      ]
                                    }
                                  ],
                                  "annots": [ "%balances" ]
                                },
                                { "prim": "big_map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ], "annots": [ "%data" ] }
                              ]
                            }
                          ]
                        },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "address", "annots": [ "%f" ] },
                            { "prim": "pair", "args": [ { "prim": "address", "annots": [ "%sender" ] }, { "prim": "address", "annots": [ "%t" ] } ] }
                          ]
                        }
                      ]
                    },
                    {
                      "prim": "big_map",
                      "args": [
                        { "prim": "address" },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                            { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                          ]
                        }
                      ]
                    },
                    [
                      [
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "050100000006706175736564" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:13" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "UNPACK", "args": [ { "prim": "bool" } ] },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "IF", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "06" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000d61646d696e6973747261746f72" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:12" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "UNPACK", "args": [ { "prim": "address" } ] },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "COMPARE" },
                        { "prim": "EQ" },
                        {
                          "prim": "IF",
                          "args": [
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "True" } ] } ],
                            [
                              [
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "COMPARE" },
                                { "prim": "EQ" }
                              ]
                            ]
                          ]
                        },
                        {
                          "prim": "IF",
                          "args": [
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "True" } ] } ],
                            [
                              [
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CAR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001373757065725472616e736665724167656e7473" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:15" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "MEM" }
                              ]
                            ]
                          ]
                        },
                        {
                          "prim": "IF",
                          "args": [
                            [ [] ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:176" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "CAR" },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "MEM" },
                                { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "07" } ] }, { "prim": "FAILWITH" } ] ] ] },
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CAR" },
                                { "prim": "CAR" },
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "DIG", "args": [ { "int": "3" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "4" } ] },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:176" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "CAR" },
                                { "prim": "DIG", "args": [ { "int": "3" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "4" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:174" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "COMPARE" },
                                { "prim": "GE" },
                                { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "08" } ] }, { "prim": "FAILWITH" } ] ] ] }
                              ]
                            ]
                          ]
                        },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "DIG", "args": [ { "int": "3" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "4" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:176" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "COMPARE" },
                        { "prim": "GE" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "10" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "MEM" },
                        {
                          "prim": "IF",
                          "args": [
                            [ [] ],
                            [
                              [
                                {
                                  "prim": "PUSH",
                                  "args": [
                                    {
                                      "prim": "option",
                                      "args": [
                                        {
                                          "prim": "pair",
                                          "args": [
                                            { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                            { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                          ]
                                        }
                                      ]
                                    },
                                    { "prim": "Some", "args": [ { "prim": "Pair", "args": [ [], { "prim": "Pair", "args": [ { "int": "0" }, { "int": "0" } ] } ] } ] }
                                  ]
                                },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "UPDATE" }
                              ]
                            ]
                          ]
                        },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "SWAP" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "DIG", "args": [ { "int": "6" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "7" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:176" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "SUB" },
                        { "prim": "PAIR" },
                        { "prim": "SWAP" },
                        { "prim": "PAIR" },
                        { "prim": "SOME" },
                        { "prim": "SWAP" },
                        { "prim": "UPDATE" },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "SWAP" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "DIG", "args": [ { "int": "6" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "7" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:175" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "ADD" },
                        { "prim": "PAIR" },
                        { "prim": "SWAP" },
                        { "prim": "PAIR" },
                        { "prim": "SOME" },
                        { "prim": "SWAP" },
                        { "prim": "UPDATE" },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "COMPARE" },
                        { "prim": "NEQ" },
                        {
                          "prim": "IF",
                          "args": [
                            [
                              [
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CAR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001373757065725472616e736665724167656e7473" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:15" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "MEM" },
                                { "prim": "NOT" }
                              ]
                            ],
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "False" } ] } ]
                          ]
                        },
                        {
                          "prim": "IF",
                          "args": [
                            [
                              [
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CAR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000d61646d696e6973747261746f72" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:12" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "address" } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                { "prim": "COMPARE" },
                                { "prim": "NEQ" }
                              ]
                            ],
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "False" } ] } ]
                          ]
                        },
                        {
                          "prim": "IF",
                          "args": [
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "DUP" },
                                { "prim": "CDR" },
                                { "prim": "SWAP" },
                                { "prim": "CAR" },
                                { "prim": "DIG", "args": [ { "int": "4" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "5" } ] },
                                { "prim": "CAR" },
                                { "prim": "CAR" },
                                { "prim": "DIG", "args": [ { "int": "4" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "5" } ] },
                                { "prim": "DIG", "args": [ { "int": "6" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "7" } ] },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:176" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "CAR" },
                                { "prim": "DIG", "args": [ { "int": "6" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "7" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:174" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "SUB" },
                                { "prim": "SOME" },
                                { "prim": "DIG", "args": [ { "int": "5" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "6" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "UPDATE" },
                                { "prim": "PAIR" },
                                { "prim": "SOME" },
                                { "prim": "SWAP" },
                                { "prim": "UPDATE" }
                              ]
                            ],
                            [ [] ]
                          ]
                        },
                        { "prim": "SWAP" },
                        { "prim": "DROP" }
                      ]
                    ]
                  ]
                },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DROP" },
                { "prim": "SOME" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                { "prim": "PACK" },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000c63616c6c5472616e73666572" } ] },
                { "prim": "UPDATE" },
                { "prim": "SWAP" },
                { "prim": "PAIR" },
                { "prim": "SWAP" },
                { "prim": "SWAP" },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "2" } ] },
                { "prim": "DUP" },
                { "prim": "CAR" },
                { "prim": "SWAP" },
                { "prim": "CDR" },
                {
                  "prim": "LAMBDA",
                  "args": [
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "int", "annots": [ "%amount" ] },
                            {
                              "prim": "pair",
                              "args": [
                                {
                                  "prim": "big_map",
                                  "args": [
                                    { "prim": "address" },
                                    {
                                      "prim": "pair",
                                      "args": [
                                        { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                        { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                      ]
                                    }
                                  ],
                                  "annots": [ "%balances" ]
                                },
                                { "prim": "big_map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ], "annots": [ "%data" ] }
                              ]
                            }
                          ]
                        },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "address", "annots": [ "%f" ] },
                            { "prim": "pair", "args": [ { "prim": "address", "annots": [ "%sender" ] }, { "prim": "address", "annots": [ "%t" ] } ] }
                          ]
                        }
                      ]
                    },
                    {
                      "prim": "big_map",
                      "args": [
                        { "prim": "address" },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                            { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                          ]
                        }
                      ]
                    },
                    [
                      [
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000d61646d696e6973747261746f72" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:12" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "UNPACK", "args": [ { "prim": "address" } ] },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "COMPARE" },
                        { "prim": "EQ" },
                        {
                          "prim": "IF",
                          "args": [
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "True" } ] } ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CAR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "050100000006706175736564" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:13" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "bool" } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                {
                                  "prim": "IF",
                                  "args": [
                                    [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "False" } ] } ],
                                    [
                                      [
                                        { "prim": "DUP" },
                                        { "prim": "CDR" },
                                        { "prim": "CDR" },
                                        { "prim": "CAR" },
                                        { "prim": "SWAP" },
                                        { "prim": "DUP" },
                                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                                        { "prim": "CDR" },
                                        { "prim": "CAR" },
                                        { "prim": "COMPARE" },
                                        { "prim": "EQ" }
                                      ]
                                    ]
                                  ]
                                }
                              ]
                            ]
                          ]
                        },
                        {
                          "prim": "IF",
                          "args": [
                            [ [] ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CAR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001373757065725472616e736665724167656e7473" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:15" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "MEM" },
                                {
                                  "prim": "IF",
                                  "args": [
                                    [ [] ],
                                    [
                                      [
                                        { "prim": "DUP" },
                                        { "prim": "CAR" },
                                        { "prim": "CDR" },
                                        { "prim": "CDR" },
                                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000e7472616e736665724167656e7473" } ] },
                                        { "prim": "GET" },
                                        {
                                          "prim": "IF_NONE",
                                          "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:17" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                        },
                                        { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                        {
                                          "prim": "IF_NONE",
                                          "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ]
                                        },
                                        { "prim": "SWAP" },
                                        { "prim": "DUP" },
                                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                                        { "prim": "CDR" },
                                        { "prim": "CDR" },
                                        { "prim": "CAR" },
                                        { "prim": "MEM" },
                                        {
                                          "prim": "IF",
                                          "args": [
                                            [
                                              [
                                                { "prim": "DUP" },
                                                { "prim": "CDR" },
                                                { "prim": "CDR" },
                                                { "prim": "CDR" },
                                                { "prim": "SWAP" },
                                                { "prim": "DUP" },
                                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                                { "prim": "CDR" },
                                                { "prim": "CDR" },
                                                { "prim": "CAR" },
                                                { "prim": "COMPARE" },
                                                { "prim": "EQ" },
                                                {
                                                  "prim": "IF",
                                                  "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "25" } ] }, { "prim": "FAILWITH" } ] ] ]
                                                }
                                              ]
                                            ],
                                            [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "04" } ] }, { "prim": "FAILWITH" } ] ]
                                          ]
                                        }
                                      ]
                                    ]
                                  ]
                                }
                              ]
                            ]
                          ]
                        },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "0" } ] },
                        { "prim": "COMPARE" },
                        { "prim": "LT" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "11" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:222" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "MEM" },
                        {
                          "prim": "IF",
                          "args": [
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "0" } ] },
                                { "prim": "SWAP" },
                                { "prim": "DIG", "args": [ { "int": "3" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "4" } ] },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:222" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "CAR" },
                                { "prim": "DIG", "args": [ { "int": "3" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "4" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:221" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "COMPARE" },
                                { "prim": "GT" },
                                { "prim": "IF", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "29" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] }
                              ]
                            ],
                            [ [] ]
                          ]
                        },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "DUP" },
                        { "prim": "CDR" },
                        { "prim": "SWAP" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "SOME" },
                        { "prim": "DIG", "args": [ { "int": "5" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "UPDATE" },
                        { "prim": "PAIR" },
                        { "prim": "SOME" },
                        { "prim": "SWAP" },
                        { "prim": "UPDATE" }
                      ]
                    ]
                  ]
                },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DROP" },
                { "prim": "SOME" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                { "prim": "PACK" },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000b63616c6c417070726f7665" } ] },
                { "prim": "UPDATE" },
                { "prim": "SWAP" },
                { "prim": "PAIR" },
                { "prim": "SWAP" },
                { "prim": "SWAP" },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "2" } ] },
                { "prim": "DUP" },
                { "prim": "CAR" },
                { "prim": "SWAP" },
                { "prim": "CDR" },
                {
                  "prim": "LAMBDA",
                  "args": [
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "int", "annots": [ "%amount" ] },
                            {
                              "prim": "pair",
                              "args": [
                                {
                                  "prim": "big_map",
                                  "args": [
                                    { "prim": "address" },
                                    {
                                      "prim": "pair",
                                      "args": [
                                        { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                        { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                      ]
                                    }
                                  ],
                                  "annots": [ "%balances" ]
                                },
                                { "prim": "big_map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ], "annots": [ "%data" ] }
                              ]
                            }
                          ]
                        },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "address", "annots": [ "%f" ] },
                            { "prim": "pair", "args": [ { "prim": "address", "annots": [ "%sender" ] }, { "prim": "address", "annots": [ "%t" ] } ] }
                          ]
                        }
                      ]
                    },
                    {
                      "prim": "big_map",
                      "args": [
                        { "prim": "address" },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                            { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                          ]
                        }
                      ]
                    },
                    [
                      [
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000d61646d696e6973747261746f72" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:12" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "UNPACK", "args": [ { "prim": "address" } ] },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "COMPARE" },
                        { "prim": "EQ" },
                        {
                          "prim": "IF",
                          "args": [
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "True" } ] } ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CAR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "050100000006706175736564" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:13" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "bool" } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                {
                                  "prim": "IF",
                                  "args": [
                                    [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "False" } ] } ],
                                    [
                                      [
                                        { "prim": "DUP" },
                                        { "prim": "CDR" },
                                        { "prim": "CDR" },
                                        { "prim": "CAR" },
                                        { "prim": "SWAP" },
                                        { "prim": "DUP" },
                                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                                        { "prim": "CDR" },
                                        { "prim": "CAR" },
                                        { "prim": "COMPARE" },
                                        { "prim": "EQ" }
                                      ]
                                    ]
                                  ]
                                }
                              ]
                            ]
                          ]
                        },
                        {
                          "prim": "IF",
                          "args": [
                            [ [] ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CAR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001373757065725472616e736665724167656e7473" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:15" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "MEM" },
                                {
                                  "prim": "IF",
                                  "args": [
                                    [ [] ],
                                    [
                                      [
                                        { "prim": "DUP" },
                                        { "prim": "CAR" },
                                        { "prim": "CDR" },
                                        { "prim": "CDR" },
                                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000e7472616e736665724167656e7473" } ] },
                                        { "prim": "GET" },
                                        {
                                          "prim": "IF_NONE",
                                          "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:17" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                        },
                                        { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                        {
                                          "prim": "IF_NONE",
                                          "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ]
                                        },
                                        { "prim": "SWAP" },
                                        { "prim": "DUP" },
                                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                                        { "prim": "CDR" },
                                        { "prim": "CDR" },
                                        { "prim": "CAR" },
                                        { "prim": "MEM" },
                                        {
                                          "prim": "IF",
                                          "args": [
                                            [
                                              [
                                                { "prim": "DUP" },
                                                { "prim": "CDR" },
                                                { "prim": "CDR" },
                                                { "prim": "CDR" },
                                                { "prim": "SWAP" },
                                                { "prim": "DUP" },
                                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                                { "prim": "CDR" },
                                                { "prim": "CDR" },
                                                { "prim": "CAR" },
                                                { "prim": "COMPARE" },
                                                { "prim": "EQ" },
                                                {
                                                  "prim": "IF",
                                                  "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "25" } ] }, { "prim": "FAILWITH" } ] ] ]
                                                }
                                              ]
                                            ],
                                            [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "04" } ] }, { "prim": "FAILWITH" } ] ]
                                          ]
                                        }
                                      ]
                                    ]
                                  ]
                                }
                              ]
                            ]
                          ]
                        },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "0" } ] },
                        { "prim": "COMPARE" },
                        { "prim": "LT" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "11" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:270" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "MEM" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "13" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "DUP" },
                        { "prim": "CDR" },
                        { "prim": "SWAP" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "DIG", "args": [ { "int": "6" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "7" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:270" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "6" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "7" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:269" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "ADD" },
                        { "prim": "SOME" },
                        { "prim": "DIG", "args": [ { "int": "5" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "UPDATE" },
                        { "prim": "PAIR" },
                        { "prim": "SOME" },
                        { "prim": "SWAP" },
                        { "prim": "UPDATE" }
                      ]
                    ]
                  ]
                },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DROP" },
                { "prim": "SOME" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                { "prim": "PACK" },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001563616c6c496e637265617365416c6c6f77616e6365" } ] },
                { "prim": "UPDATE" },
                { "prim": "SWAP" },
                { "prim": "PAIR" },
                { "prim": "SWAP" },
                { "prim": "SWAP" },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "2" } ] },
                { "prim": "DUP" },
                { "prim": "CAR" },
                { "prim": "SWAP" },
                { "prim": "CDR" },
                {
                  "prim": "LAMBDA",
                  "args": [
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "int", "annots": [ "%amount" ] },
                            {
                              "prim": "pair",
                              "args": [
                                {
                                  "prim": "big_map",
                                  "args": [
                                    { "prim": "address" },
                                    {
                                      "prim": "pair",
                                      "args": [
                                        { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                        { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                      ]
                                    }
                                  ],
                                  "annots": [ "%balances" ]
                                },
                                { "prim": "big_map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ], "annots": [ "%data" ] }
                              ]
                            }
                          ]
                        },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "address", "annots": [ "%f" ] },
                            { "prim": "pair", "args": [ { "prim": "address", "annots": [ "%sender" ] }, { "prim": "address", "annots": [ "%t" ] } ] }
                          ]
                        }
                      ]
                    },
                    {
                      "prim": "big_map",
                      "args": [
                        { "prim": "address" },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                            { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                          ]
                        }
                      ]
                    },
                    [
                      [
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000d61646d696e6973747261746f72" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:12" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "UNPACK", "args": [ { "prim": "address" } ] },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "COMPARE" },
                        { "prim": "EQ" },
                        {
                          "prim": "IF",
                          "args": [
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "True" } ] } ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CAR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "050100000006706175736564" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:13" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "bool" } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                {
                                  "prim": "IF",
                                  "args": [
                                    [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "False" } ] } ],
                                    [
                                      [
                                        { "prim": "DUP" },
                                        { "prim": "CDR" },
                                        { "prim": "CDR" },
                                        { "prim": "CAR" },
                                        { "prim": "SWAP" },
                                        { "prim": "DUP" },
                                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                                        { "prim": "CDR" },
                                        { "prim": "CAR" },
                                        { "prim": "COMPARE" },
                                        { "prim": "EQ" }
                                      ]
                                    ]
                                  ]
                                }
                              ]
                            ]
                          ]
                        },
                        {
                          "prim": "IF",
                          "args": [
                            [ [] ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CAR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001373757065725472616e736665724167656e7473" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:15" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "MEM" },
                                {
                                  "prim": "IF",
                                  "args": [
                                    [ [] ],
                                    [
                                      [
                                        { "prim": "DUP" },
                                        { "prim": "CAR" },
                                        { "prim": "CDR" },
                                        { "prim": "CDR" },
                                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000e7472616e736665724167656e7473" } ] },
                                        { "prim": "GET" },
                                        {
                                          "prim": "IF_NONE",
                                          "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:17" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                        },
                                        { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                        {
                                          "prim": "IF_NONE",
                                          "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ]
                                        },
                                        { "prim": "SWAP" },
                                        { "prim": "DUP" },
                                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                                        { "prim": "CDR" },
                                        { "prim": "CDR" },
                                        { "prim": "CAR" },
                                        { "prim": "MEM" },
                                        {
                                          "prim": "IF",
                                          "args": [
                                            [
                                              [
                                                { "prim": "DUP" },
                                                { "prim": "CDR" },
                                                { "prim": "CDR" },
                                                { "prim": "CDR" },
                                                { "prim": "SWAP" },
                                                { "prim": "DUP" },
                                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                                { "prim": "CDR" },
                                                { "prim": "CDR" },
                                                { "prim": "CAR" },
                                                { "prim": "COMPARE" },
                                                { "prim": "EQ" },
                                                {
                                                  "prim": "IF",
                                                  "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "25" } ] }, { "prim": "FAILWITH" } ] ] ]
                                                }
                                              ]
                                            ],
                                            [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "04" } ] }, { "prim": "FAILWITH" } ] ]
                                          ]
                                        }
                                      ]
                                    ]
                                  ]
                                }
                              ]
                            ]
                          ]
                        },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "0" } ] },
                        { "prim": "COMPARE" },
                        { "prim": "LT" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "11" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:314" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "MEM" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "15" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "DIG", "args": [ { "int": "3" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "4" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:314" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "3" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "4" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:313" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "COMPARE" },
                        { "prim": "GT" },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "16" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "DUP" },
                        { "prim": "CDR" },
                        { "prim": "SWAP" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "DIG", "args": [ { "int": "6" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "7" } ] },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:314" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "6" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "7" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:313" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "SUB" },
                        { "prim": "SOME" },
                        { "prim": "DIG", "args": [ { "int": "5" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "UPDATE" },
                        { "prim": "PAIR" },
                        { "prim": "SOME" },
                        { "prim": "SWAP" },
                        { "prim": "UPDATE" }
                      ]
                    ]
                  ]
                },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DROP" },
                { "prim": "SOME" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                { "prim": "PACK" },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001563616c6c4465637265617365416c6c6f77616e6365" } ] },
                { "prim": "UPDATE" },
                { "prim": "SWAP" },
                { "prim": "PAIR" },
                { "prim": "SWAP" },
                { "prim": "SWAP" },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "2" } ] },
                { "prim": "DUP" },
                { "prim": "CAR" },
                { "prim": "SWAP" },
                { "prim": "CDR" },
                {
                  "prim": "LAMBDA",
                  "args": [
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "address", "annots": [ "%address" ] },
                            {
                              "prim": "big_map",
                              "args": [
                                { "prim": "address" },
                                {
                                  "prim": "pair",
                                  "args": [
                                    { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                    { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                  ]
                                }
                              ],
                              "annots": [ "%balances" ]
                            }
                          ]
                        },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "big_map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ], "annots": [ "%data" ] },
                            { "prim": "address", "annots": [ "%sender" ] }
                          ]
                        }
                      ]
                    },
                    {
                      "prim": "big_map",
                      "args": [
                        { "prim": "address" },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                            { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                          ]
                        }
                      ]
                    },
                    [
                      [
                        { "prim": "DUP" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000d61646d696e6973747261746f72" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:12" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "UNPACK", "args": [ { "prim": "address" } ] },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "COMPARE" },
                        { "prim": "EQ" },
                        {
                          "prim": "IF",
                          "args": [
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "True" } ] } ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CAR" },
                                { "prim": "CAR" },
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "COMPARE" },
                                { "prim": "EQ" }
                              ]
                            ]
                          ]
                        },
                        {
                          "prim": "IF",
                          "args": [
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "True" } ] } ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001373757065725472616e736665724167656e7473" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:15" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "MEM" }
                              ]
                            ]
                          ]
                        },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "05" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "PUSH", "args": [ { "prim": "nat" }, { "int": "0" } ] },
                        { "prim": "NIL", "args": [ { "prim": "address" } ] },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "DIG", "args": [ { "int": "4" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "5" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:359" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CAR" },
                        { "prim": "ITER", "args": [ [ { "prim": "CAR" }, { "prim": "CONS" } ] ] },
                        { "prim": "NIL", "args": [ { "prim": "address" } ] },
                        { "prim": "SWAP" },
                        { "prim": "ITER", "args": [ [ { "prim": "CONS" } ] ] },
                        { "prim": "SIZE" },
                        { "prim": "COMPARE" },
                        { "prim": "GT" },
                        {
                          "prim": "IF",
                          "args": [
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "DIG", "args": [ { "int": "2" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "3" } ] },
                                { "prim": "CAR" },
                                { "prim": "CAR" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "CDR" },
                                { "prim": "PUSH", "args": [ { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ] }, [] ] },
                                { "prim": "PAIR" },
                                { "prim": "SOME" },
                                { "prim": "SWAP" },
                                { "prim": "UPDATE" }
                              ]
                            ],
                            [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "26" } ] }, { "prim": "FAILWITH" } ] ]
                          ]
                        },
                        { "prim": "SWAP" },
                        { "prim": "DROP" }
                      ]
                    ]
                  ]
                },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DROP" },
                { "prim": "SOME" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                { "prim": "PACK" },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001663616c6c5265736574416c6c416c6c6f77616e636573" } ] },
                { "prim": "UPDATE" },
                { "prim": "SWAP" },
                { "prim": "PAIR" },
                { "prim": "SWAP" },
                { "prim": "SWAP" },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "2" } ] },
                { "prim": "DUP" },
                { "prim": "CAR" },
                { "prim": "SWAP" },
                { "prim": "CDR" },
                {
                  "prim": "LAMBDA",
                  "args": [
                    {
                      "prim": "pair",
                      "args": [
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "address", "annots": [ "%address" ] },
                            {
                              "prim": "big_map",
                              "args": [
                                { "prim": "address" },
                                {
                                  "prim": "pair",
                                  "args": [
                                    { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                                    { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                                  ]
                                }
                              ],
                              "annots": [ "%balances" ]
                            }
                          ]
                        },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "big_map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ], "annots": [ "%data" ] },
                            { "prim": "pair", "args": [ { "prim": "address", "annots": [ "%f" ] }, { "prim": "address", "annots": [ "%sender" ] } ] }
                          ]
                        }
                      ]
                    },
                    {
                      "prim": "big_map",
                      "args": [
                        { "prim": "address" },
                        {
                          "prim": "pair",
                          "args": [
                            { "prim": "map", "args": [ { "prim": "address" }, { "prim": "int" } ], "annots": [ "%approvals" ] },
                            { "prim": "pair", "args": [ { "prim": "int", "annots": [ "%balance" ] }, { "prim": "int", "annots": [ "%nonce" ] } ] }
                          ]
                        }
                      ]
                    },
                    [
                      [
                        { "prim": "DUP" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000d61646d696e6973747261746f72" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:12" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "UNPACK", "args": [ { "prim": "address" } ] },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                        { "prim": "SWAP" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "COMPARE" },
                        { "prim": "EQ" },
                        {
                          "prim": "IF",
                          "args": [
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "True" } ] } ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CAR" },
                                { "prim": "CAR" },
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "COMPARE" },
                                { "prim": "EQ" }
                              ]
                            ]
                          ]
                        },
                        {
                          "prim": "IF",
                          "args": [
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "True" } ] } ],
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001373757065725472616e736665724167656e7473" } ] },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:15" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "UNPACK", "args": [ { "prim": "set", "args": [ { "prim": "address" } ] } ] },
                                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                                { "prim": "SWAP" },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "2" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "MEM" }
                              ]
                            ]
                          ]
                        },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "05" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "CAR" },
                        { "prim": "CDR" },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:391" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "CAR" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "MEM" },
                        {
                          "prim": "IF",
                          "args": [
                            [
                              [
                                { "prim": "DUP" },
                                { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "0" } ] },
                                { "prim": "SWAP" },
                                { "prim": "DIG", "args": [ { "int": "3" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "4" } ] },
                                { "prim": "CAR" },
                                { "prim": "CAR" },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:391" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "CAR" },
                                { "prim": "DIG", "args": [ { "int": "3" } ] },
                                { "prim": "DUP" },
                                { "prim": "DUG", "args": [ { "int": "4" } ] },
                                { "prim": "CDR" },
                                { "prim": "CDR" },
                                { "prim": "CAR" },
                                { "prim": "GET" },
                                {
                                  "prim": "IF_NONE",
                                  "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:392" } ] }, { "prim": "FAILWITH" } ] ], [] ]
                                },
                                { "prim": "COMPARE" },
                                { "prim": "GT" }
                              ]
                            ],
                            [ { "prim": "PUSH", "args": [ { "prim": "bool" }, { "prim": "False" } ] } ]
                          ]
                        },
                        { "prim": "IF", "args": [ [ [] ], [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "17" } ] }, { "prim": "FAILWITH" } ] ] ] },
                        { "prim": "DUP" },
                        { "prim": "DIG", "args": [ { "int": "2" } ] },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "3" } ] },
                        { "prim": "CAR" },
                        { "prim": "CAR" },
                        { "prim": "DUP" },
                        { "prim": "DUG", "args": [ { "int": "2" } ] },
                        { "prim": "GET" },
                        { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "set_in_top-any" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                        { "prim": "DUP" },
                        { "prim": "CDR" },
                        { "prim": "SWAP" },
                        { "prim": "CAR" },
                        { "prim": "NONE", "args": [ { "prim": "int" } ] },
                        { "prim": "DIG", "args": [ { "int": "5" } ] },
                        { "prim": "CDR" },
                        { "prim": "CDR" },
                        { "prim": "CAR" },
                        { "prim": "UPDATE" },
                        { "prim": "PAIR" },
                        { "prim": "SOME" },
                        { "prim": "SWAP" },
                        { "prim": "UPDATE" }
                      ]
                    ]
                  ]
                },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DROP" },
                { "prim": "DIG", "args": [ { "int": "3" } ] },
                { "prim": "DROP" },
                { "prim": "SOME" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                { "prim": "PACK" },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001263616c6c5265736574416c6c6f77616e6365" } ] },
                { "prim": "UPDATE" },
                { "prim": "SWAP" },
                { "prim": "PAIR" },
                { "prim": "NIL", "args": [ { "prim": "operation" } ] }
              ]
            ],
            [
              [
                { "prim": "CONTRACT", "args": [ { "prim": "map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ] } ], "annots": [ "%receiveStrandardEntrypoints" ] },
                { "prim": "NIL", "args": [ { "prim": "operation" } ] },
                { "prim": "SWAP" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "unit" }, { "prim": "Unit" } ] }, { "prim": "FAILWITH" } ] ], [ [] ] ] },
                { "prim": "PUSH", "args": [ { "prim": "mutez" }, { "int": "0" } ] },
                { "prim": "PUSH", "args": [ { "prim": "map", "args": [ { "prim": "bytes" }, { "prim": "bytes" } ] }, [] ] },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "5" } ] },
                { "prim": "CDR" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001563616c6c4465637265617365416c6c6f77616e6365" } ] },
                { "prim": "GET" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:8" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001563616c6c4465637265617365416c6c6f77616e6365" } ] },
                { "prim": "UPDATE" },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "5" } ] },
                { "prim": "CDR" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000863616c6c4275726e" } ] },
                { "prim": "GET" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:4" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000863616c6c4275726e" } ] },
                { "prim": "UPDATE" },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "5" } ] },
                { "prim": "CDR" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001563616c6c496e637265617365416c6c6f77616e6365" } ] },
                { "prim": "GET" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:7" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001563616c6c496e637265617365416c6c6f77616e6365" } ] },
                { "prim": "UPDATE" },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "5" } ] },
                { "prim": "CDR" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001263616c6c5265736574416c6c6f77616e6365" } ] },
                { "prim": "GET" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:10" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001263616c6c5265736574416c6c6f77616e6365" } ] },
                { "prim": "UPDATE" },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "5" } ] },
                { "prim": "CDR" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001663616c6c5265736574416c6c416c6c6f77616e636573" } ] },
                { "prim": "GET" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:9" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000001663616c6c5265736574416c6c416c6c6f77616e636573" } ] },
                { "prim": "UPDATE" },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "5" } ] },
                { "prim": "CDR" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000863616c6c4d696e74" } ] },
                { "prim": "GET" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:3" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000863616c6c4d696e74" } ] },
                { "prim": "UPDATE" },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "5" } ] },
                { "prim": "CDR" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000c63616c6c5472616e73666572" } ] },
                { "prim": "GET" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:5" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000c63616c6c5472616e73666572" } ] },
                { "prim": "UPDATE" },
                { "prim": "DIG", "args": [ { "int": "4" } ] },
                { "prim": "DUP" },
                { "prim": "DUG", "args": [ { "int": "5" } ] },
                { "prim": "CDR" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000b63616c6c417070726f7665" } ] },
                { "prim": "GET" },
                { "prim": "IF_NONE", "args": [ [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "Get-item:6" } ] }, { "prim": "FAILWITH" } ] ], [] ] },
                { "prim": "SOME" },
                { "prim": "PUSH", "args": [ { "prim": "bytes" }, { "bytes": "05010000000b63616c6c417070726f7665" } ] },
                { "prim": "UPDATE" },
                { "prim": "TRANSFER_TOKENS" },
                { "prim": "CONS" }
              ]
            ]
          ]
        },
        { "prim": "PAIR" }
      ]
    ]
  }
]