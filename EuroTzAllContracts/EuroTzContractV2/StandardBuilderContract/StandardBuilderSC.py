import smartpy as sp

#################

callMintInBytes = sp.bytes("0x05010000000863616c6c4d696e74")
callBurnInBytes = sp.bytes("0x05010000000863616c6c4275726e")
callTransferInBytes = sp.bytes("0x05010000000c63616c6c5472616e73666572")
callApproveInBytes = sp.bytes("0x05010000000b63616c6c417070726f7665")
callIncreaseAllowanceInBytes = sp.bytes("0x05010000001563616c6c496e637265617365416c6c6f77616e6365")
callDecreaseAllowanceInBytes = sp.bytes("0x05010000001563616c6c4465637265617365416c6c6f77616e6365")
callResetAllAllowancesInBytes = sp.bytes("0x05010000001663616c6c5265736574416c6c416c6c6f77616e636573")
callResetAllowanceInBytes = sp.bytes("0x05010000001263616c6c5265736574416c6c6f77616e6365")
administratorInBytes = sp.bytes("0x05010000000d61646d696e6973747261746f72")
pausedInBytes = sp.bytes("0x050100000006706175736564")
superTransferAgentsInBytes = sp.bytes("0x05010000001373757065725472616e736665724167656e7473")
transferAgentsInBytes = sp.bytes("0x05010000000e7472616e736665724167656e7473")
eventSinkContractAddressInBytes = sp.bytes("0x0501000000186576656e7453696e6b436f6e747261637441646472657373")
tokenMetadataInBytes = sp.bytes("0x05010000000d746f6b656e4d65746164617461")

#################

class StandardBuilder(sp.Contract):
    def __init__(self, admin):
        self.init(
            entrypointsBytes = sp.big_map(
                tkey = sp.TBytes, 
                tvalue = sp.TBytes
            ),
            administrator = admin,
            )
    
    '''
    Modiifiers
    '''
    
    # self explanatory             
    def onlyAdmin(self, sender, administrator):
        # message = "Only admin allowed"  
        sp.verify((sender == administrator), message = "01") 
        
    # only account owner or approved account can call the transfer entrypoint
    def onlySelfOrApproved(self, params):
        # message = "Contract is paused."
        sp.verify(~params.paused, message = "06")
        
        sp.if ((params.sender == params.administrator) | (params.f == params.sender) | (params.superTransferAgents.contains(params.sender))): 
            pass
        sp.else:
            # message = "Caller not approved"
            sp.verify((params.fromDetails.approvals.contains(params.sender)), message = "07")
                
            # message = "Amount allowed smaller than amount to transfer."
            sp.verify((params.fromDetails.approvals[params.sender] >= params.amount), message = "08")
                
            pass
        
    # approvals function modifier
    def onlySelfOrWithRole(self, params):
        
        sp.if ((params.sender == params.administrator) | (~params.paused & (params.f == params.sender))):
            pass
        sp.else:
            sp.if params.superTransferAgents.contains(params.sender):
                pass
            sp.else:
                sp.if params.transferAgents.contains(params.sender):
                    sp.if (params.sender == params.t):
                        pass
                    sp.else:
                        # message = "TransferAgent is only allowed to approve to his address"
                        sp.failwith("25")
                sp.else:
                     # message = "Not allowed to approve from this account"
                     sp.failwith("04")
                     
    def onlySelfOrAdminOrSueprTransferAgent(self, params):
        # message = "Only account Owner, Admin or SuperTransferAgent are allowed"
        sp.verify(((params.sender == params.administrator) | (params.sender == params.address) | params.superTransferAgents.contains(params.sender)), message = "05") 
        
    '''
    Entrypoints
    '''
        
    def mint(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)
        sp.set_type(params.amount, sp.TInt)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        sp.set_type(params.totalSupply, sp.TInt)
            
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        # Verify sender is admin
        self.onlyAdmin(params.sender, administrator)
        
        # assign entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)

        # add user in entrypointsBytes if he doesn't exist
        sp.if ~ balances.value.contains(params.address):
            balances.value[params.address] = sp.record(balance = sp.int(0), approvals = sp.map(tkey = sp.TAddress, tvalue = sp.TInt), nonce = sp.int(0))
         
        # assign userDetails to local variable to modify it later.
        totalSupply = sp.local("totalSupply", params.totalSupply)
        
        # update totalSupply
        totalSupply.value += params.amount
        
        # update dataBigMap with new values
        balances.value[params.address].balance += params.amount
        
        
        return sp.record(
            balances = balances.value, 
            totalSupply = totalSupply.value 
            )
       
    def burn(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)
        sp.set_type(params.amount, sp.TInt)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        sp.set_type(params.totalSupply, sp.TInt)
        
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        # Verify sender is admin
        self.onlyAdmin(params.sender, administrator)
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)
        
        # check balance greate than amount
        sp.verify(balances.value[params.address].balance >= params.amount, message = "09")
        
        # assign userDetails to local variable to modify it later.
        totalSupply = sp.local("totalSupply", params.totalSupply)
        
        # update totalSupply
        totalSupply.value -= params.amount
        
        # update user's balance
        balances.value[params.address].balance -= params.amount
        
        return sp.record(
            balances = balances.value, 
            totalSupply = totalSupply.value
            )
    
    def transfer(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.t, sp.TAddress)
        sp.set_type(params.f, sp.TAddress)
        sp.set_type(params.amount, sp.TInt)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)
        
        paused = sp.unpack(params.data[pausedInBytes], t = sp.TBool).open_some()
        
        # To remove after SmartPy bug fix
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        checkRecord = sp.record(fromDetails = balances.value[params.f], f = params.f, amount = params.amount, sender = params.sender, paused = paused, superTransferAgents = superTransferAgents, administrator = administrator)
        # End to remove
        
        # checkRecord = sp.record(fromDetails = balances.value[params.f], f = params.f, amount = params.amount, sender = params.sender, paused = paused)
        
        self.onlySelfOrApproved(checkRecord)
        
        sp.verify(balances.value[params.f].balance >= params.amount, message = "10")
        
        # add user in entrypointsBytes if he doesn't exist
        sp.if ~ balances.value.contains(params.t):
            balances.value[params.t] = sp.record(balance = sp.int(0), approvals = sp.map(tkey = sp.TAddress, tvalue = sp.TInt), nonce = sp.int(0))    
        
        # update user's balance
        balances.value[params.f].balance -= params.amount
        balances.value[params.t].balance += params.amount
        
        sp.if ((params.f != params.sender) & (~ superTransferAgents.contains(params.sender)) & (administrator != params.sender)):
            balances.value[params.f].approvals[params.sender] -= params.amount
        
        #sp.if (params.f != params.sender):
        #    balances.value[params.f].approvals[params.sender] -= params.amount 

        return balances.value
        

    def approve(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.t, sp.TAddress)
        sp.set_type(params.f, sp.TAddress)
        sp.set_type(params.amount, sp.TInt)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        paused = sp.unpack(params.data[pausedInBytes], t = sp.TBool).open_some()
        
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        transferAgents = sp.unpack(params.data[transferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        checkRecord = sp.record(
            administrator = administrator, 
            transferAgents = transferAgents, 
            superTransferAgents = superTransferAgents, 
            paused = paused, 
            sender = params.sender,
            t = params.t, 
            f = params.f
            )
            
        self.onlySelfOrWithRole(checkRecord)
        
        # message =  "amount to approve must be greater than zero"
        sp.verify(params.amount > 0, message =  "11")
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)

        sp.if balances.value[params.f].approvals.contains(params.t):
            sp.if balances.value[params.f].approvals[params.t] > 0:
                # message = account already allowed to spend > 0 amount.
                sp.failwith("29")
        

        # update dataBigMap with new values
        balances.value[params.f].approvals[params.t] = params.amount
        
        return balances.value
    
    
    def increaseAllowance(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.t, sp.TAddress)
        sp.set_type(params.f, sp.TAddress)
        sp.set_type(params.amount, sp.TInt)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        paused = sp.unpack(params.data[pausedInBytes], t = sp.TBool).open_some()
        
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        transferAgents = sp.unpack(params.data[transferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        checkRecord = sp.record(
            administrator = administrator, 
            transferAgents = transferAgents, 
            superTransferAgents = superTransferAgents, 
            paused = paused, 
            sender = params.sender,
            t = params.t, 
            f = params.f
            )
            
        self.onlySelfOrWithRole(checkRecord)
        
        # message =  "amount to approve must be greater than zero"
        sp.verify(params.amount > 0, message =  "11")
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)
        
        # message = "impossible to increase allowance of unexistant account"
        sp.verify(balances.value[params.f].approvals.contains(params.t), message = "13")

        balances.value[params.f].approvals[params.t] += params.amount
        
        return balances.value
        
    
    def decreaseAllowance(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.t, sp.TAddress)
        sp.set_type(params.f, sp.TAddress)
        sp.set_type(params.amount, sp.TInt)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))
        
        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        paused = sp.unpack(params.data[pausedInBytes], t = sp.TBool).open_some()
        
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        transferAgents = sp.unpack(params.data[transferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        checkRecord = sp.record(
            administrator = administrator, 
            transferAgents = transferAgents, 
            superTransferAgents = superTransferAgents, 
            paused = paused, 
            sender = params.sender,
            t = params.t, 
            f = params.f
            )
            
        self.onlySelfOrWithRole(checkRecord)
        
        # message =  "amount to approve must be greater than zero"
        sp.verify(params.amount > 0, message =  "11")
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)
        
        # message = "impossible to decrease allowance of unexistant account"
        sp.verify(balances.value[params.f].approvals.contains(params.t), message = "15")
        
        # message = "amount to decrease must be smaller than the one already approved"
        sp.verify((balances.value[params.f].approvals[params.t] > params.amount), message = "16")       

        balances.value[params.f].approvals[params.t] -= params.amount
        
        return balances.value
    
    def resetAllAllowances(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        checkRecord = sp.record(
            administrator = administrator, 
            superTransferAgents = superTransferAgents, 
            address = params.address, 
            sender = params.sender
            )
            
        self.onlySelfOrAdminOrSueprTransferAgent(checkRecord)
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("data", params.balances)
        
        sp.if (sp.len(balances.value[params.address].approvals.keys()) > 0):
            balances.value[params.address].approvals = {}  
        sp.else:
            # message = "Approvals map is already empty"
            sp.failwith("26")
        
        return balances.value
        
    def resetAllowance(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)
        sp.set_type(params.f, sp.TAddress)
        sp.set_type(params.balances, sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, approvals = sp.TMap(sp.TAddress, sp.TInt), nonce = sp.TInt)))

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        checkRecord = sp.record(
            administrator = administrator, 
            superTransferAgents = superTransferAgents, 
            address = params.address, 
            sender = params.sender
            )
            
        self.onlySelfOrAdminOrSueprTransferAgent(checkRecord)
        
        # affect entrypointsBytes to local variable to modify it later.
        balances = sp.local("balances", params.balances)
        
        sp.verify((balances.value[params.address].approvals.contains(params.f) & (balances.value[params.address].approvals[params.f] > 0)), message = "17")
        
        del balances.value[params.address].approvals[params.f] 
        
        return balances.value
        
        
    @sp.entry_point
    def buildStandardEntrypoints(self):
        
        callMint = sp.some(sp.build_lambda(self.mint))
        self.data.entrypointsBytes[callMintInBytes] = sp.pack(callMint.open_some())
        
        callBurn = sp.some(sp.build_lambda(self.burn))
        self.data.entrypointsBytes[callBurnInBytes] = sp.pack(callBurn.open_some())
        
        callTransfer = sp.some(sp.build_lambda(self.transfer))
        self.data.entrypointsBytes[callTransferInBytes] = sp.pack(callTransfer.open_some())
        
        callApprove = sp.some(sp.build_lambda(self.approve))
        self.data.entrypointsBytes[callApproveInBytes] = sp.pack(callApprove.open_some())
        
        callIncreaseAllowance = sp.some(sp.build_lambda(self.increaseAllowance))
        self.data.entrypointsBytes[callIncreaseAllowanceInBytes] = sp.pack(callIncreaseAllowance.open_some())
        
        callDecreaseAllowance = sp.some(sp.build_lambda(self.decreaseAllowance))
        self.data.entrypointsBytes[callDecreaseAllowanceInBytes] = sp.pack(callDecreaseAllowance.open_some())
        
        callResetAllAllowances = sp.some(sp.build_lambda(self.resetAllAllowances))
        self.data.entrypointsBytes[callResetAllAllowancesInBytes] = sp.pack(callResetAllAllowances.open_some())
        
        callResetAllowance = sp.some(sp.build_lambda(self.resetAllowance))
        self.data.entrypointsBytes[callResetAllowanceInBytes] = sp.pack(callResetAllowance.open_some())
        
     
    @sp.entry_point
    def sendEntrypoints(self, params):

        # we can't iterate on bigMap...
        mapRecord = {
            callMintInBytes: self.data.entrypointsBytes[callMintInBytes],
            callBurnInBytes: self.data.entrypointsBytes[callBurnInBytes],
            callTransferInBytes: self.data.entrypointsBytes[callTransferInBytes],
            callApproveInBytes: self.data.entrypointsBytes[callApproveInBytes],
            callIncreaseAllowanceInBytes: self.data.entrypointsBytes[callIncreaseAllowanceInBytes],
            callDecreaseAllowanceInBytes: self.data.entrypointsBytes[callDecreaseAllowanceInBytes],
            callResetAllowanceInBytes: self.data.entrypointsBytes[callResetAllowanceInBytes],
            callResetAllAllowancesInBytes: self.data.entrypointsBytes[callResetAllAllowancesInBytes]
        }    
        
        EPType = sp.TMap(sp.TBytes, sp.TBytes)
        
        sp.transfer(
            mapRecord, 
            sp.tez(0), 
            sp.contract(
                t = EPType,
                address = params.target,
                entry_point = "receiveStrandardEntrypoints"
                ).open_some()
            )

@sp.add_test(name = "StandardBuilder")
def test():
    scenario = sp.test_scenario()
    scenario.h1("StandardBuilder Contract")

    admin = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")


    c1 = StandardBuilder(admin)

    scenario += c1
    
    