import { Tezos } from "@taquito/taquito";

import conf from "../../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

Tezos.importKey(conf.adminSecretKey);

const contractsAddresses = {
  euroTzAddress: "KT1QrMNdrFEwRdiT4ZeBNHoE6bN7VUMp7Qqb",
  proxyAddress: "KT1U9f17hSrBdRmybFT7cS3PNQSAD1pTDZ3W",
  standardAddress: "KT19KpFJWVHs2dHiegCKeMSTVbkMQCcs1rQT",
  feelessAddress: "KT1WdFWaBdDLCcnHXGBcJCiBRF88bGtoYMNs",
  managementAddress: "KT1UCwjwaGuMBY4dDxyrJmn9LBWSDHPHJnvH",
};

async function buildManagementData() {
  try {
    const managementContract = await Tezos.contract.at(
      contractsAddresses.managementAddress
    );

    const buildData = await managementContract.methods
      .buildData(
        "2", // decimals
        conf.eventSinkSCAddress,
        "EuroTz/tkNext", // name
        "euroTz" // symbol
      )
      .send();

    await buildData.confirmation();

    console.log("Success buildData ", buildData.hash);

    const sendManagementData = await managementContract.methods
      .sendEntrypoints(contractsAddresses.proxyAddress)
      .send();

    await sendManagementData.confirmation();

    console.log("Success sendManagementData ", sendManagementData.hash);
  } catch (e) {
    console.log(e);
  }
}

async function buildStandardEntrypoints() {
  try {
    const standardBuilderContract = await Tezos.contract.at(
      contractsAddresses.standardAddress
    );

    const buildStandardEntrypoints = await standardBuilderContract.methods
      .buildStandardEntrypoints("unit")
      .send();

    await buildStandardEntrypoints.confirmation();

    console.log(
      "Success buildStandardEntrypoints ",
      buildStandardEntrypoints.hash
    );

    const sendStandardEntrypoints = await standardBuilderContract.methods
      .sendEntrypoints(contractsAddresses.proxyAddress)
      .send();

    await sendStandardEntrypoints.confirmation();

    console.log(
      "Success sendStandardEntrypoints ",
      sendStandardEntrypoints.hash
    );
  } catch (e) {
    console.log(e);
  }
}

async function buildFeelessEntrypoints() {
  try {
    const feelessBuilderContract = await Tezos.contract.at(
      contractsAddresses.feelessAddress
    );

    const buildFeelessEntrypoints = await feelessBuilderContract.methods
      .buildFeelessEntrypoints("unit")
      .send();

    await buildFeelessEntrypoints.confirmation();

    console.log(
      "Success buildFeelessEntrypoints ",
      buildFeelessEntrypoints.hash
    );

    const sendFeelessEntrypoints = await feelessBuilderContract.methods
      .sendEntrypoints(contractsAddresses.proxyAddress)
      .send();

    await sendFeelessEntrypoints.confirmation();

    console.log("Success sendFeelessEntrypoints ", sendFeelessEntrypoints.hash);
  } catch (e) {
    console.log(e);
  }
}

async function upgradeProxy() {
  try {
    const proxyContract = await Tezos.contract.at(
      contractsAddresses.proxyAddress
    );

    const upgrade = await proxyContract.methods
      .upgrade(contractsAddresses.euroTzAddress)
      .send();

    await upgrade.confirmation();

    console.log("Success upgrade ", upgrade.hash);

    const upgradeFeeless = await proxyContract.methods
      .upgradeFeeless(contractsAddresses.euroTzAddress)
      .send();

    await upgradeFeeless.confirmation();

    console.log("Success upgradeFeeless ", upgradeFeeless.hash);
  } catch (e) {
    console.log(e);
  }
}

async function upgradeEuroTz() {
  try {
    console.log("Begin upgrade...");
    console.log("=======================================");

    await buildManagementData();
    await buildStandardEntrypoints();
    await buildFeelessEntrypoints();
    await upgradeProxy();

    console.log("=======================================");
    console.log("Upgrade done successfully...");
  } catch (e) {
    console.log(e);
  }
}

upgradeEuroTz();
