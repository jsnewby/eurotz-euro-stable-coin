import smartpy as sp

##################

callMintInBytes = sp.bytes("0x05010000000863616c6c4d696e74")
callBurnInBytes = sp.bytes("0x05010000000863616c6c4275726e")
callTransferInBytes = sp.bytes("0x05010000000c63616c6c5472616e73666572")
callApproveInBytes = sp.bytes("0x05010000000b63616c6c417070726f7665")
callIncreaseAllowanceInBytes = sp.bytes("0x05010000001563616c6c496e637265617365416c6c6f77616e6365")
callDecreaseAllowanceInBytes = sp.bytes("0x05010000001563616c6c4465637265617365416c6c6f77616e6365")
callResetAllAllowancesInBytes = sp.bytes("0x05010000001663616c6c5265736574416c6c416c6c6f77616e636573")
callResetAllowanceInBytes = sp.bytes("0x05010000001263616c6c5265736574416c6c6f77616e6365")
administratorInBytes = sp.bytes("0x05010000000d61646d696e6973747261746f72")
pausedInBytes = sp.bytes("0x050100000006706175736564")
superTransferAgentsInBytes = sp.bytes("0x05010000001373757065725472616e736665724167656e7473")
transferAgentsInBytes = sp.bytes("0x05010000000e7472616e736665724167656e7473")
eventSinkContractAddressInBytes = sp.bytes("0x0501000000186576656e7453696e6b436f6e747261637441646472657373")
tokenMetadataInBytes = sp.bytes("0x05010000000d746f6b656e4d65746164617461")

##################

class ManagementBuilder(sp.Contract):
    def __init__(self, admin):
        self.init(
            entrypointsBytes = sp.big_map(
                tkey = sp.TBytes, 
                tvalue = sp.TBytes
            ),
            administrator = admin,
            )
    
    '''
    Modiifiers
    '''
    
    # self explanatory             
    def onlyAdmin(self, sender, administrator):
        # message = "Only admin allowed"  
        sp.verify((sender == administrator), message = "01") 
        
    # contracts management modifier 
    def onlyAdminOrSuperTransferAgent(self, administrator, sender, superTransferAgents):
        # message = "Only admin or Super transfer agent are allowed"
        sp.verify(( (sender == administrator) | (superTransferAgents.contains(sender))), message = "02")    
        
        
    '''
    Entrypoints
    '''
    
    def changeEventSink(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
            
       # Verify sender is admin
        self.onlyAdmin(params.sender, administrator)
        
        # affect entrypointsBytes to local variable to modify it later.
        data = sp.local("data", params.data)
        
        data.value[eventSinkContractAddressInBytes] = sp.pack(params.address)
        
        return data.value
    
    def changeAdmin(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
            
       # Verify sender is admin
        self.onlyAdmin(params.sender, administrator)
        
        # affect entrypointsBytes to local variable to modify it later.
        data = sp.local("data", params.data)
        
        data.value[administratorInBytes] = sp.pack(params.address)
        
        return data.value
        
    def addSuperTransferAgent(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
            
        # Verify sender is admin
        self.onlyAdmin(params.sender, administrator)
        
        # To remove after SmartPy bug fix
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        sp.verify(~ superTransferAgents.contains(params.address), message = "23")
        
        superTransferAgentsData = sp.local("superTransferAgentsData", superTransferAgents)
        
        superTransferAgentsData.value.add(params.address)
        
        # assing entrypointsBytes to local variable to modify it later.
        data = sp.local("data", params.data)
        
        data.value[superTransferAgentsInBytes] = sp.pack(superTransferAgentsData.value)
        
        return data.value
    
    def deleteSuperTransferAgent(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
            
       # Verify sender is admin
        self.onlyAdmin(params.sender, administrator)
        
        # To remove after SmartPy bug fix
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        sp.verify(superTransferAgents.contains(params.address), message = "24")
        
        superTransferAgentsData = sp.local("superTransferAgentsData", superTransferAgents)
        
        superTransferAgentsData.value.remove(params.address)
        
        # assing entrypointsBytes to local variable to modify it later.
        data = sp.local("data", params.data)
        
        data.value[superTransferAgentsInBytes] = sp.pack(superTransferAgentsData.value)
        
        return data.value    
    
    def addTransferAgent(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        self.onlyAdminOrSuperTransferAgent(administrator, params.sender, superTransferAgents)

        # To remove after SmartPy bug fix
        transferAgents = sp.unpack(params.data[transferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        sp.verify(~ transferAgents.contains(params.address), message = "21")
        
        transferAgentsData = sp.local("transferAgentsData", transferAgents)
        
        transferAgentsData.value.add(params.address)
        
        # assing entrypointsBytes to local variable to modify it later.
        data = sp.local("data", params.data)
        
        data.value[transferAgentsInBytes] = sp.pack(transferAgentsData.value)
        
        return data.value 
    
    def deleteTransferAgent(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)
        sp.set_type(params.address, sp.TAddress)

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
        
        superTransferAgents = sp.unpack(params.data[superTransferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        self.onlyAdminOrSuperTransferAgent(administrator, params.sender, superTransferAgents)

        # To remove after SmartPy bug fix
        transferAgents = sp.unpack(params.data[transferAgentsInBytes], t = sp.TSet(sp.TAddress)).open_some()
        
        sp.verify(transferAgents.contains(params.address), message = "22")
        
        transferAgentsData = sp.local("transferAgentsData", transferAgents)
        
        transferAgentsData.value.remove(params.address)
        
        # assing entrypointsBytes to local variable to modify it later.
        data = sp.local("data", params.data)
        
        data.value[transferAgentsInBytes] = sp.pack(transferAgentsData.value)
        
        return data.value     
    
    def pause(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
            
       # Verify sender is admin
        self.onlyAdmin(params.sender, administrator)
        
        # affect entrypointsBytes to local variable to modify it later.
        data = sp.local("data", params.data)
        
        data.value[pausedInBytes] = sp.pack(sp.bool(True))
        
        return data.value  
    
    def unpause(self, params):
        # Set params types
        sp.set_type(params.data, sp.TBigMap(sp.TBytes, sp.TBytes))
        sp.set_type(params.sender, sp.TAddress)

        # Retrieve admin address
        administrator = sp.unpack(params.data[administratorInBytes], t = sp.TAddress).open_some()
            
       # Verify sender is admin
        self.onlyAdmin(params.sender, administrator)
        
        # affect entrypointsBytes to local variable to modify it later.
        data = sp.local("data", params.data)
        
        data.value[pausedInBytes] = sp.pack(sp.bool(False))
        
        return data.value      
        
        
    @sp.entry_point
    def buildData(self, params):
        sp.set_type(params.eventSinkContractAddress, sp.TAddress)
        sp.set_type(params.name, sp.TString)
        sp.set_type(params.symbol, sp.TString)
        sp.set_type(params.decimals, sp.TNat)
        
        # set admin
        self.data.entrypointsBytes[administratorInBytes] = sp.pack(self.data.administrator)
        
        # set Pause state
        self.data.entrypointsBytes[pausedInBytes] = sp.pack(sp.bool(False))
    
        # set TrasnferAgents
        self.data.entrypointsBytes[transferAgentsInBytes] = sp.pack(sp.set(t = sp.TAddress))
        
        # set SuperTransferAgents
        self.data.entrypointsBytes[superTransferAgentsInBytes] = sp.pack(sp.set(t = sp.TAddress))
        
        # setEventSink contract address
        self.data.entrypointsBytes[eventSinkContractAddressInBytes] = sp.pack(params.eventSinkContractAddress)
        
        # set tokenMetada
        tokenMetadata = sp.record(name = params.name, symbol = params.symbol, decimals = params.decimals)
        
        self.data.entrypointsBytes[tokenMetadataInBytes] = sp.pack(tokenMetadata)
        
        # build management entrypoints
        
        callChangeEventSink = sp.some(sp.build_lambda(self.changeEventSink))
        self.data.entrypointsBytes[sp.pack("callChangeEventSink")] = sp.pack(callChangeEventSink.open_some())
        
        callChangeAdmin = sp.some(sp.build_lambda(self.changeAdmin))
        self.data.entrypointsBytes[sp.pack("callChangeAdmin")] = sp.pack(callChangeAdmin.open_some())
        
        callPause = sp.some(sp.build_lambda(self.pause))
        self.data.entrypointsBytes[sp.pack("callPause")] = sp.pack(callPause.open_some())
        
        callUnpause = sp.some(sp.build_lambda(self.unpause))
        self.data.entrypointsBytes[sp.pack("callUnpause")] = sp.pack(callUnpause.open_some())
        
        callAddSuperTransferAgent = sp.some(sp.build_lambda(self.addSuperTransferAgent))
        self.data.entrypointsBytes[sp.pack("callAddSuperTransferAgent")] = sp.pack(callAddSuperTransferAgent.open_some())
        
        callDeleteSuperTransferAgent = sp.some(sp.build_lambda(self.deleteSuperTransferAgent))
        self.data.entrypointsBytes[sp.pack("callDeleteSuperTransferAgent")] = sp.pack(callDeleteSuperTransferAgent.open_some())
        
        callAddTransferAgent = sp.some(sp.build_lambda(self.addTransferAgent))
        self.data.entrypointsBytes[sp.pack("callAddTransferAgent")] = sp.pack(callAddTransferAgent.open_some())
        
        callDeleteTransferAgent = sp.some(sp.build_lambda(self.deleteTransferAgent))
        self.data.entrypointsBytes[sp.pack("callDeleteTransferAgent")] = sp.pack(callDeleteTransferAgent.open_some())
     
    @sp.entry_point
    def sendEntrypoints(self, params):
        

        # we can't iterate on bigMap...
        mapRecord = {

            sp.pack("callChangeEventSink"): self.data.entrypointsBytes[sp.pack("callChangeEventSink")],
            
            sp.pack("callChangeAdmin"): self.data.entrypointsBytes[sp.pack("callChangeAdmin")],
            
            sp.pack("callPause"): self.data.entrypointsBytes[sp.pack("callPause")],
            
            sp.pack("callUnpause"): self.data.entrypointsBytes[sp.pack("callUnpause")],
            
            sp.pack("callAddSuperTransferAgent"): self.data.entrypointsBytes[sp.pack("callAddSuperTransferAgent")],
            
            sp.pack("callDeleteSuperTransferAgent"): self.data.entrypointsBytes[sp.pack("callDeleteSuperTransferAgent")],
            
            sp.pack("callAddTransferAgent"): self.data.entrypointsBytes[sp.pack("callAddTransferAgent")],
            
            sp.pack("callDeleteTransferAgent"): self.data.entrypointsBytes[sp.pack("callDeleteTransferAgent")],

            
            administratorInBytes: self.data.entrypointsBytes[administratorInBytes],
            pausedInBytes: self.data.entrypointsBytes[pausedInBytes],
            eventSinkContractAddressInBytes: self.data.entrypointsBytes[eventSinkContractAddressInBytes],
            superTransferAgentsInBytes: self.data.entrypointsBytes[superTransferAgentsInBytes],
            transferAgentsInBytes: self.data.entrypointsBytes[transferAgentsInBytes],
            tokenMetadataInBytes: self.data.entrypointsBytes[tokenMetadataInBytes]
        }  
        
        EPType = sp.TMap(sp.TBytes, sp.TBytes)
        
        sp.transfer(
            mapRecord, 
            sp.tez(0), 
            sp.contract(
                t = EPType,
                address = params.target,
                entry_point = "receiveManagementEntrypoints"
                ).open_some()
            )

@sp.add_test(name = "Lambdas")
def test():
    scenario = sp.test_scenario()
    scenario.h1("ManagementBuilder Contract")

    admin = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")

    c1 = ManagementBuilder(admin)

    scenario += c1
    
    