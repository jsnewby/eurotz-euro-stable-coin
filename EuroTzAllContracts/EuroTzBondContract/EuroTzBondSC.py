import smartpy as sp


# set token_id type as NAT
token_id_type = sp.TNat

class Error_message:
    def token_undefined(): return "TOKEN_UNDEFINED"
    def insufficient_balance(): return "INSUFFICIENT_BALANCE"
    def not_operator(): return "NOT_OPERATOR"
    def not_owner(): return "NOT_OWNER"
    def insufficient_amount(): return "INSUFFICIENT_AMOUNT"
    def period_error(): return "PERIOD_ERROR"
    def not_admin(): return "NOT_ADMIN"
    def token_not_found(): return "TOKEN_NOT_FOUND"
    def user_not_found(): return "USER_NOT_FOUND"
    def not_redeemable(): return "NOT_REDEEMABLE"
    def not_allowed(): return "NOT_ALLOWED"
    def paused(): return "CONTRACT_IS_PAUSED"

class Transfer:
    def get_from(t): return t.from_
    def get_to(t): return t.to_
    def get_token_id(t): return t.token_id

    def get_type():
        return sp.TRecord(from_ = sp.TAddress,
                          to_ = sp.TAddress,
                          token_id = token_id_type
                          )
                          
    def set_type_and_layout(expr):
        sp.set_type(expr, Transfer.get_type())
        sp.set_record_layout(expr, ("from_", ("to_", "token_id")))
            
    def make(f,t,i):
        r = sp.record(from_ = f, to_ = t, token_id = i)
        Transfer.set_type_and_layout(r)
        return r

class Operator_param:

    def get_type():
        return sp.TRecord(
            owner = sp.TAddress,
            operator = sp.TAddress)
            
    def set_type_and_layout(expr):
        sp.set_type(expr, Operator_param.get_type())
        sp.set_record_layout(expr, ("owner", "operator"))
            
    def make(owner, operator):
        r = sp.record(owner = owner,
                      operator = operator)
        Operator_param.set_type_and_layout(r)
        return r
        
    def is_operator_response_type():
        return sp.TRecord(
            operator = Operator_param.get_type(),
            is_operator = sp.TBool
            )
            
    def make_is_operator_response(operator, is_operator):
        return sp.record(operator = operator, is_operator = is_operator)
        
    def is_operator_request_type():
        return sp.TRecord(
            operator = Operator_param.get_type(),
            callback = sp.TContract(Operator_param.is_operator_response_type())
            )

class Operator_set:
    def get_key_type():
        t = sp.TRecord(owner = sp.TAddress,
                       operator = sp.TAddress).layout(("owner", "operator")
                       )
        return t
    
    def get_value_type():
        return sp.TUnit

    def make():
        return sp.big_map(tkey = Operator_set.key_type(), tvalue = sp.TUnit)
    
    def make_key(owner, operator):
        metakey = sp.record(owner = owner, operator = operator)
        return metakey

    def add(set, owner, operator):
        set[Operator_set.make_key(owner, operator)] = sp.unit
        
    def remove(set, owner, operator):
        del set[Operator_set.make_key(owner, operator)]
        
    def is_member(set, owner, operator):
        return set.contains(Operator_set.make_key(owner, operator))

class Permissions_descriptor:
    def get_type():
        operator_transfer_policy = sp.TVariant(
            no_transfer = sp.TUnit,
            owner_transfer = sp.TUnit,
            owner_or_operator_transfer = sp.TUnit)

        sp.set_type_variant_layout(operator_transfer_policy,
                                   ("no_transfer",
                                    ("owner_transfer",
                                     "owner_or_operator_transfer")))
        owner_transfer_policy =  sp.TVariant(
            owner_no_op = sp.TUnit,
            optional_owner_hook = sp.TUnit,
            required_owner_hook = sp.TUnit)

        sp.set_type_variant_layout(owner_transfer_policy,
                                   ("owner_no_op",
                                    ("optional_owner_hook",
                                     "required_owner_hook")))
        custom_permission_policy = sp.TRecord(
            tag = sp.TString,
            config_api = sp.TOption(sp.TAddress))
        main = sp.TRecord(
            operator = operator_transfer_policy,
            receiver = owner_transfer_policy,
            sender   = owner_transfer_policy,
            custom   = sp.TOption(custom_permission_policy))
        sp.set_type_record_layout(main, ("operator",
                                         ("receiver",
                                          ("sender", "custom"))))
        return main
        
    def set_type_and_layout(expr):
        sp.set_type(expr, Permissions_descriptor.get_type())
        
    def make():
        def uv(s):
            return sp.variant(s, sp.unit)
            
        v = sp.record(
            operator = uv("owner_or_operator_transfer"),
            receiver = uv("owner_no_op"),
            sender = uv("owner_no_op"),
            custom = sp.none
            )
        Permissions_descriptor.set_type_and_layout(v)
        return v

class Token_meta_data:
    def get_type():
        return sp.TRecord(
             issueTime = sp.TTimestamp,
             period = sp.TNat, # in months
             interestRate = sp.TNat, # period + amount
             amount = sp.TNat,
             owner = sp.TAddress
        )
    def set_type_and_layout(expr):
        sp.set_type(expr, Token_meta_data.get_type())
        sp.set_record_layout(expr, ("issueTime",
                                    ("period",
                                     ("interestRate",
                                      ("amount", "owner")))))
    def request_type():
        return Total_supply.request_type()            

class Ledger_key:
    def get_type():
        return sp.TAddress
        
    def make(userAddress):
        sp.set_type(userAddress, sp.TAddress)
        return userAddress 

class Ledger_value:
    def get_type():
        return sp.TRecord(
            nonce = sp.TNat, 
            ownedTokens = sp.TSet(sp.TNat)
            )
            
class Balance_of:
    def request_type():
        return sp.TAddress
            
    def response_type():
        return sp.TRecord(
                owner = Balance_of.request_type(),
                balance = sp.TNat)

class Total_supply:
    
    def request_type():
        return unit
        
    def response_type():
        return sp.TInt            

class InterestRate:
    # to be calculated based on Period + Amount
    def getInterestRate(period):
        
        interestRate = sp.local('interestRate', 0)
        
        sp.if (period <= 12):
            interestRate.value = 100 # => 1% 
        sp.else:
            sp.if (period <= 24):
                interestRate.value = 200
            sp.else:
                sp.if (period <= 36):
                    interestRate.value = 300
                sp.else:
                    sp.if (period <= 48):
                        interestRate.value = 400
                    sp.else:
                        sp.if (period <= 60):
                            interestRate.value =500 
        
        return interestRate.value    

class FeelessOperation:
    def get_type():
        return sp.TRecord(k = sp.TKey,
                          s = sp.TSignature,
                          b = sp.TBytes
                          )
            

class NeofactoBond(sp.Contract):
    def __init__(self, admin, euroTzAddress, eventSinkContractAddress):
        self.init(
            # STOP the contract!
            paused = sp.bool(False),
            
            # Mapping between users and their nonces & tokens
            ledger = sp.big_map(
                tkey = Ledger_key.get_type(), 
                tvalue = Ledger_value.get_type() 
                ),
            
            # Mapping between tokenId and token details: period, amount, owner...
            tokens = sp.big_map(
                tkey = token_id_type, 
                tvalue = Token_meta_data.get_type()
                ),
            
            # Operator is a Tezos address that initiates token transfer operation on behalf of the owner.
            operators = sp.big_map(
                tkey = Operator_set.get_key_type(), 
                tvalue = Operator_set.get_value_type(), 
                ),
            
            # Admin of the contract: MinterRole
            administrator = admin,
            
            # Keep track of the current tokenId, we assume consecutive token ids
            currentTokenId = sp.nat(0),
            
            # Keep track of existant (non-burned) tokens count.
            ownedTokensCount = sp.int(0),
            
            # Bond Provider details
            providerMetadata = sp.map({
                "symbol": sp.string("NEO.BD"),
                "name": sp.string("Neofacto Bond"),
                "fullName": sp.string("Neofacto Neo'squad"),
                "shortName": sp.string("Neofacto"),
                "country": sp.string("Luxembourg"),
                "website": sp.string("https://www.neofacto.com")
            }),
            
            distributionCurrenyAddress = euroTzAddress,
            
            minPeriod = sp.nat(3), # in months
            maxPeriod = sp.nat(60), # in months
            minAmount = sp.nat(10000), # => 100 EuroTz
            maxAmount = sp.nat(10000000),  # => 100.000 EuroTz
            commissionBeneficiary = admin,
            
            eventSinkContractAddress = eventSinkContractAddress
        )
    
    # ===============================================================

    # ==================== Call EventSink contract ==================
    
    def callEventSinkContractTransfer(self, params):
        entryPointName = "transferEvent"
        transferParamsRecord = sp.record(amount = params.amount, fromAddress = params.fromAddress, toAddress = params.toAddress)
        c = sp.contract(
                        t = sp.TRecord(amount = sp.TIntOrNat, fromAddress = sp.TOption(sp.TAddress), toAddress = sp.TOption(sp.TAddress)), 
                        address = self.data.eventSinkContractAddress, 
                        entry_point = entryPointName
                        ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
        
        
    def callEventSinkContractApprove(self, params):
        approvalParamsRecord = sp.record(amount = params.amount, owner = params.owner, spender = params.spender)
        entryPointName = "approvalEvent"
        c = sp.contract(
                        t = sp.TRecord(amount = sp.TIntOrNat, owner = sp.TAddress, spender = sp.TAddress), 
                        address = self.data.eventSinkContractAddress, 
                        entry_point = entryPointName
                        ).open_some()
        sp.transfer(approvalParamsRecord, sp.mutez(0), c)
    
    # ==================== Calculations ==========================
    
    def isExpired (self, issueTime, period):  
        #  (365/12) * 24 * 60 * 60 = 2.628.000
        return sp.now > issueTime.add_seconds(sp.to_int(period * 2628000))
        
    def interest (self, amount, interestRate, period):
        return ((amount * interestRate) * (period / 12)) / 100
        
    
    # ==================== Checkers ==============================
    
    def checkAmount(self, amount):
        sp.verify(
            ((amount >= self.data.minAmount) & 
            (amount <= self.data.maxAmount)), 
            message = Error_message.insufficient_amount())    
    
    def checkPeriod(self, period):
        sp.verify(
            ((period >= self.data.minPeriod) & 
            (period <= self.data.maxPeriod)), 
            message = Error_message.period_error())
            
    def checkPaused(self):
        sp.verify(~self.data.paused, message = Error_message.paused())
    
    # ==================== Modifiers =============================
    
    def onlyAdmin(self):
        sp.verify(sp.sender == self.data.administrator, message = Error_message.not_admin()) 
    
    def onlyOwnerOrAdminOrOperator(self, from_user):
        sp.verify(
               ((sp.sender == self.data.administrator) |
               (from_user == sp.sender) |
               Operator_set.is_member(self.data.operators,
                                      from_user,
                                      sp.sender)),
               message = Error_message.not_operator()) 
    
    def onlyAdminOrOwner(self, owner):
        sp.verify(((owner == sp.sender) |
                          (sp.sender == self.data.administrator)), message = Error_message.not_allowed())
    
    # ==================== Unpack feeless data functions =======================
    
    def unpackTransferFeelessData(self, operationbytes):
        # Unpack data in order to get: from, to, amount, nonce
        dataRecordType = sp.TPair(sp.TNat, sp.TPair(sp.TNat, sp.TPair(sp.TAddress, sp.TPair(sp.TAddress, sp.TAddress))))
        
        dataRecord = sp.unpack(
            operationbytes, 
            t = dataRecordType
            ).open_some()
        
        token_id = sp.fst(dataRecord)
        nonce = sp.fst(sp.snd(dataRecord))
        from_ = sp.fst(sp.snd(sp.snd(dataRecord)))
        to_ = sp.fst(sp.snd(sp.snd(sp.snd(dataRecord))))
        contractAddress = sp.snd(sp.snd(sp.snd(sp.snd(dataRecord))))
        
        operationData = sp.record(token_id = token_id, nonce = nonce, from_ = from_, to_ = to_, contractAddress = contractAddress)
        
        return operationData
    
    def unpackApproveFeelessData(self, operationbytes):
        # Unpack data in order to get: from, to, amount, nonce
        dataRecordType = sp.TPair(sp.TOption(sp.TNat), sp.TPair(sp.TAddress, sp.TPair(sp.TAddress, sp.TAddress)))
        
        dataRecord = sp.unpack(
            operationbytes, 
            t = dataRecordType
            ).open_some()
        
        nonce = sp.fst(dataRecord)
        owner = sp.fst(sp.snd(dataRecord))
        operator = sp.fst(sp.snd(sp.snd(dataRecord)))
        contractAddress = sp.snd(sp.snd(sp.snd(dataRecord)))

        operationData = sp.record(nonce = nonce, owner = owner, operator = operator, contractAddress = contractAddress)
        
        return operationData 
    
    def unpackMintFeelessData(self, operationbytes):
        # Unpack data in order to get: from, to, amount, nonce
        dataRecordType = sp.TPair(sp.TNat, sp.TPair(sp.TOption(sp.TNat), sp.TPair(sp.TNat, sp.TPair(sp.TAddress, sp.TAddress))))
        
        dataRecord = sp.unpack(
            operationbytes, 
            t = dataRecordType
            ).open_some()
        
        period = sp.fst(dataRecord)
        nonce = sp.fst(sp.snd(dataRecord))
        amount = sp.fst(sp.snd(sp.snd(dataRecord)))
        to_ = sp.fst(sp.snd(sp.snd(sp.snd(dataRecord))))
        contractAddress = sp.snd(sp.snd(sp.snd(sp.snd(dataRecord))))
        
        operationData = sp.record(period = period, nonce = nonce, amount = amount, to_ = to_, contractAddress = contractAddress)
        
        return operationData
    
    def unpackBurnFeelessData(self, operationbytes):
        # Unpack data in order to get: from, to, amount, nonce
        dataRecordType = sp.TPair(sp.TNat, sp.TPair(sp.TNat, sp.TPair(sp.TAddress, sp.TAddress)))
        
        dataRecord = sp.unpack(
            operationbytes, 
            t = dataRecordType
            ).open_some()
        
        token_id = sp.fst(dataRecord)
        nonce = sp.fst(sp.snd(dataRecord))
        from_ = sp.fst(sp.snd(sp.snd(dataRecord)))
        contractAddress = sp.snd(sp.snd(sp.snd(dataRecord)))
        
        operationData = sp.record(token_id = token_id, from_ = from_, contractAddress = contractAddress, nonce = nonce)
        
        return operationData
    
    # ======================== Pay Investment function =====================
    # ======================== Redeem Investment function =====================
    
    def payAmount(self, from_, to_, amount):
        data = sp.record(
            amount = amount,
            f = from_,
            t = to_,
        )
        
        tokenContractInstance = sp.contract(
            t = sp.TRecord(amount = sp.TInt, f = sp.TAddress, t = sp.TAddress), 
            address = self.data.distributionCurrenyAddress, 
            entry_point = "transfer").open_some()
        
        sp.transfer(data, sp.mutez(0), tokenContractInstance) 
    
    # ====================== Perform Feeless fucntions ============================
    
    def checkSignature(self, b, s, k):
        
        # Check for signature
        #  message= "Wrong signature"
        sp.verify(sp.check_signature(k, s, b), message= "20")
    
    def checkNonce(self, storedNonce, nonce):
        
        # Nonce verification
        # message = "Invalid nonce"
        sp.verify((storedNonce == nonce), message = "19")
    
    def checkSameContract(self, contractAddress, thisAddress):
        # thisAddress = sp.to_address(sp.self)
        
        sp.verify((thisAddress == contractAddress), message = "35")
    
    def prePerformFeelessOperation(self, b, s, k, contractAddress, nonce, userAddress):
        
        self.checkSignature(b, s, k)
        
        thisAddress = sp.to_address(sp.self)
        
        self.checkSameContract(contractAddress, thisAddress)
        
        self.checkNonce(self.data.ledger[userAddress].nonce, nonce)
    
    def prePerformMintFeelessOperation(self, b, s, k, contractAddress, nonce, userAddress):
        
        self.checkSignature(b, s, k)
        
        thisAddress = sp.to_address(sp.self)
        
        self.checkSameContract(contractAddress, thisAddress)
        
        sp.if ((~ nonce.is_some()) & (~ self.data.ledger.contains(userAddress))):
            pass
        sp.else:
            self.checkNonce(self.data.ledger[userAddress].nonce, nonce.open_some())
    
    def prePerformApproveFeelessOperation(self, b, s, k, contractAddress, nonce, userAddress):
        
        self.checkSignature(b, s, k)
        
        thisAddress = sp.to_address(sp.self)
        
        self.checkSameContract(contractAddress, thisAddress)
        
        sp.if ((~ nonce.is_some()) & (~ self.data.ledger.contains(userAddress))):
            self.data.ledger[userAddress] = sp.record(nonce = 0, ownedTokens = sp.set([]))
        sp.else:
            self.checkNonce(self.data.ledger[userAddress].nonce, nonce.open_some())     
    
    # =========================== Main functions =========================
    
    def _mint(self, amount, period, to):
        self.checkAmount(amount)
        
        self.checkPeriod(period)
        
        self.payAmount(to, self.data.commissionBeneficiary, sp.to_int(amount))   
        
        # Second transfer from the provider (Neofacto) to us
        # self.payAmount(to, self.data.commissionBeneficiary, sp.to_int(amount))   
        
        # check if user exists already in the ledger before minting...
        sp.if self.data.ledger.contains(to):
                self.data.ledger[to].ownedTokens.add(self.data.currentTokenId)
        sp.else:
                self.data.ledger[to] = sp.record(nonce = 0, ownedTokens = sp.set([self.data.currentTokenId]))
        
        # set the new token in the tokens big_map
        self.data.tokens[self.data.currentTokenId] = sp.record(
             issueTime = sp.now,
             period = period,
             interestRate = InterestRate.getInterestRate(period),
             amount = amount,
             owner = to
         )
        
        # increment the currentTokenId in the storage 
        self.data.currentTokenId += 1
        self.data.ownedTokensCount += 1
        
        # Call EventSink
        self.callEventSinkContractTransfer(sp.record(amount = sp.to_int(self.data.currentTokenId), fromAddress = sp.none, toAddress = sp.some(to)))
    
    def _burn(self, token_id):
        
        issueTime = self.data.tokens[token_id].issueTime
        period = self.data.tokens[token_id].period
        interestRate = self.data.tokens[token_id].interestRate
        owner = self.data.tokens[token_id].owner
        amount = self.data.tokens[token_id].amount
        
        amountToRedeem = self.interest(amount, interestRate, period)
        
        sp.verify(self.data.tokens.contains(token_id), message = Error_message.token_undefined())
        
        sp.verify(self.data.ledger[owner].ownedTokens.contains(token_id), message = Error_message.not_owner())
        
        sp.verify(self.isExpired(issueTime, period), message = Error_message.not_redeemable())
        
        self.payAmount(self.data.commissionBeneficiary, owner, sp.to_int(amountToRedeem))    
        self.data.ledger[owner].ownedTokens.remove(token_id)
        
        del self.data.tokens[token_id]
        
        # decrement the currentTokenId in the storage 
        self.data.ownedTokensCount -= 1
        
        self.callEventSinkContractTransfer(sp.record(amount = sp.to_int(token_id), fromAddress = sp.some(owner), toAddress = sp.none))
    
    def _burnFeeless(self, token_id, from_):
        
        issueTime = self.data.tokens[token_id].issueTime
        period = self.data.tokens[token_id].period
        interestRate = self.data.tokens[token_id].interestRate
        # owner = self.data.tokens[token_id].owner
        amount = self.data.tokens[token_id].amount
        
        amountToRedeem = self.interest(amount, interestRate, period)
        
        sp.verify(self.data.tokens.contains(token_id), message = Error_message.token_undefined())
        
        sp.verify((self.data.tokens[token_id].owner == from_), message = Error_message.not_owner())
        
        sp.verify(self.data.ledger[from_].ownedTokens.contains(token_id), message = Error_message.not_owner())
        
        sp.verify(self.isExpired(issueTime, period), message = Error_message.not_redeemable())
        
        self.payAmount(self.data.commissionBeneficiary, from_, sp.to_int(amountToRedeem))    
        self.data.ledger[from_].ownedTokens.remove(token_id)
        
        del self.data.tokens[token_id]
        
        # decrement the currentTokenId in the storage 
        self.data.ownedTokensCount -= 1
        
        self.callEventSinkContractTransfer(sp.record(amount = sp.to_int(token_id), fromAddress = sp.some(from_), toAddress = sp.none))
        
    
    def _transfer(self, token_id, from_user, to_user):
        sp.verify((self.data.tokens.contains(token_id)), message = Error_message.token_undefined())
        
        sp.verify((self.data.tokens[token_id].owner == from_user), message = Error_message.not_owner())
              
        self.data.ledger[from_user].ownedTokens.remove(token_id)
        self.data.tokens[token_id].owner = to_user
          
        sp.if self.data.ledger.contains(to_user):
            self.data.ledger[to_user].ownedTokens.add(token_id)
        sp.else:
            self.data.ledger[to_user] = sp.record(nonce = 0, ownedTokens = sp.set([token_id]))
            
        self.callEventSinkContractTransfer(sp.record(amount = sp.to_int(token_id), fromAddress = sp.some(from_user), toAddress = sp.some(to_user)))    
    
    # =========================================================================
    # =========================x Entrypoints x=================================
    # =========================================================================
    
    # ========================= Management ====================================
    

    @sp.entry_point
    def pause(self, params):
        self.onlyAdmin()
        self.data.paused = sp.bool(True)
    
    @sp.entry_point
    def unpause(self, params):
        self.onlyAdmin()
        self.data.paused = sp.bool(False)

    @sp.entry_point
    def set_administrator(self, newAdministrator):
        self.onlyAdmin()
        self.data.administrator = newAdministrator
        
    @sp.entry_point
    def set_currency(self, newDistributionCurrenyAddress):
        self.onlyAdmin()
        self.data.distributionCurrenyAddress = newDistributionCurrenyAddress
    
    @sp.entry_point
    def set_eventSink(self, newEventSinkContractAddress):
        self.onlyAdmin()
        self.data.eventSinkContractAddress = newEventSinkContractAddress    
    
    # ========================= Main ===================================
    

    @sp.entry_point
    def mint(self, params):
        self.onlyAdmin()
        
        ###################################
        
        self._mint(params.amount, params.period, params.to)
        
        ###################################

    @sp.entry_point
    def burn(self, token_id):
        self.onlyAdmin()
        
        ###################################
        
        self._burn(token_id)
        
        ###################################

    @sp.entry_point
    def transfer(self, params):
        self.checkPaused()
        
        sp.set_type(params, sp.TList(Transfer.get_type()))
        
        sp.for transfer in params:
            
            Transfer.set_type_and_layout(transfer)
               
            from_user = Ledger_key.make(Transfer.get_from(transfer))
                                            
            to_user = Ledger_key.make(Transfer.get_to(transfer))
               
            token_id = Transfer.get_token_id(transfer)
              
            self.onlyOwnerOrAdminOrOperator(from_user) 
            
            ###################################
 
            self._transfer(token_id, from_user, to_user)
                
            ###################################    
                

    @sp.entry_point
    def update_operators(self, params):
        sp.set_type(params, sp.TList(
            sp.TVariant(
                add_operator = Operator_param.get_type(),
                remove_operator = Operator_param.get_type())))
        sp.for update in params:
            sp.if update.is_variant("add_operator"):
                upd = update.open_variant("add_operator")
                self.onlyAdminOrOwner(upd.owner)
                Operator_set.add(self.data.operators,
                                 upd.owner,
                                 upd.operator)
            sp.else:
                upd = update.open_variant("remove_operator")
                self.onlyAdminOrOwner(upd.owner)
                Operator_set.remove(self.data.operators,
                                    upd.owner,
                                    upd.operator)

    
    # ========================= Feeless Entrypoints ===================================
    
    @sp.entry_point
    def mintFeeless(self, params):
        # Set params types
        sp.set_type(params, FeelessOperation.get_type())
        
        self.onlyAdmin()  
        
        operationData = self.unpackMintFeelessData(params.b)
        
        self.prePerformMintFeelessOperation(params.b, params.s, params.k, operationData.contractAddress, operationData.nonce, operationData.to_)
        
        # ======================================================
        
        self._mint(operationData.amount, operationData.period, operationData.to_)
        
        # ======================================================
        
        # Increment user's nonce
        self.data.ledger[operationData.to_].nonce += 1 
        
    
    @sp.entry_point
    def burnFeeless(self, params):
        # Set params types
        sp.set_type(params, FeelessOperation.get_type())
        
        self.onlyAdmin() 
        
        operationData = self.unpackBurnFeelessData(params.b)
        
        self.prePerformFeelessOperation(params.b, params.s, params.k, operationData.contractAddress, operationData.nonce, operationData.from_)
        
        # ======================================================

        self._burnFeeless(operationData.token_id, operationData.from_)
        
        # ======================================================
        
        # Increment user's nonce
        self.data.ledger[operationData.from_].nonce += 1
    
    @sp.entry_point        
    def transferFeeless(self, params):
        # Set params types
        sp.set_type(params, FeelessOperation.get_type())
        
        self.onlyAdmin()
        
        operationData = self.unpackTransferFeelessData(params.b)
        
        self.prePerformFeelessOperation(params.b, params.s, params.k, operationData.contractAddress, operationData.nonce, operationData.from_)
        
        # ======================================================
       
        self._transfer(operationData.token_id, operationData.from_, operationData.to_)    
        # ======================================================
        
        # Increment user's nonce
        self.data.ledger[operationData.from_].nonce += 1    
    
    @sp.entry_point        
    def approveFeeless(self, params):
        # Set params types
        sp.set_type(params, FeelessOperation.get_type())
        
        self.onlyAdmin()
        
        operationData = self.unpackApproveFeelessData(params.b)
        
        self.prePerformApproveFeelessOperation(params.b, params.s, params.k, operationData.contractAddress, operationData.nonce, operationData.owner)
        
        # ======================================================
        
        Operator_set.add(self.data.operators,
                         operationData.owner,
                         operationData.operator)
        
        # ======================================================
        
        # Increment user's nonce
        self.data.ledger[operationData.owner].nonce += 1    

    
    # ========================= Views Entrypoints ===================================
    

    @sp.entry_point
    def balance_of(self, params):
        # paused may mean that balances are meaningless:
        self.checkPaused()
        
        user = params.owner

        sp.verify(self.data.ledger.contains(user), message = Error_message.user_not_found())
        
        balance = sp.len(self.data.ledger[user].ownedTokens.elements())
        
        destination = sp.set_type(params.callback,
                                  sp.TContract(Balance_of.response_type()))
                                  
        response = sp.record(owner = user, balance = balance)                          
        
        sp.transfer(response, sp.mutez(0), destination)
    

    @sp.entry_point
    def total_supply(self, params):
        self.checkPaused()
                    
        destination = sp.set_type(params.callback,
                                  sp.TContract(Total_supply.response_type()))
                                  
                                  
        sp.transfer(self.data.ownedTokensCount + 1, sp.mutez(0), destination)
    
    @sp.entry_point
    def permissions_descriptor(self, params):
        sp.set_type(params, sp.TContract(Permissions_descriptor.get_type()))
        v = Permissions_descriptor.make()
        sp.transfer(v, sp.mutez(0), params)

    @sp.entry_point
    def is_operator(self, params):
        sp.set_type(params, Operator_param.is_operator_request_type())

        res = Operator_set.is_member(self.data.operators,
                                     params.operator.owner,
                                     params.operator.operator)
        returned = sp.record(
            operator = params.operator,
            is_operator = res)
        sp.transfer(returned, sp.mutez(0), params.callback)
   
    
   
    @sp.entry_point
    def token_metadata(self, params):
        self.checkPaused()
  
        sp.verify(self.data.tokens.contains(params.token_id), message = Error_message.token_not_found())
        Token_meta_data.set_type_and_layout(self.data.tokens[params.token_id])
        
        res = self.data.tokens[params.token_id]
        
        destination = sp.set_type(params.callback,
                                  sp.TContract(
                                      Token_meta_data.get_type()))
                                      
        sp.transfer(res, sp.mutez(0), destination)


if "templates" not in __name__:
    @sp.add_test(name = "NeofactoBond")
    def test():
        scenario = sp.test_scenario()
        scenario.h1("NeofactoBond Contract")

        euroTzAddress = sp.address("KT1NWTYLM9gsq49D4ZqmiszwTxg3ePb59f7v")
        admin = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")
        eventSinkContractAddress = sp.address("KT1RzokejsxyURUBeaGoB9DMeqeWz89YJBjy")
        
        c1 = NeofactoBond(admin, euroTzAddress, eventSinkContractAddress)
        scenario += c1
        
        