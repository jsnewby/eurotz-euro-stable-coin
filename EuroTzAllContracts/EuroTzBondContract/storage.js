import conf from "../../conf/conf";

export default {
  prim: "Pair",
  args: [
    {
      prim: "Pair",
      args: [
        {
          prim: "Pair",
          args: [
            { string: conf.adminAddress },
            {
              prim: "Pair",
              args: [{ string: conf.adminAddress }, { int: "0" }],
            },
          ],
        },
        {
          prim: "Pair",
          args: [
            {
              prim: "Pair",
              args: [
                { string: conf.contractAddress },
                { string: conf.eventSinkSCAddress },
              ],
            },
            { prim: "Pair", args: [[], { int: "10000000" }] },
          ],
        },
      ],
    },
    {
      prim: "Pair",
      args: [
        {
          prim: "Pair",
          args: [
            { prim: "Pair", args: [{ int: "60" }, { int: "10000" }] },
            { prim: "Pair", args: [{ int: "3" }, []] },
          ],
        },
        {
          prim: "Pair",
          args: [
            { prim: "Pair", args: [{ int: "0" }, { prim: "False" }] },
            {
              prim: "Pair",
              args: [
                [
                  {
                    prim: "Elt",
                    args: [{ string: "country" }, { string: "Luxembourg" }],
                  },
                  {
                    prim: "Elt",
                    args: [
                      { string: "fullName" },
                      { string: "Neofacto Neo'squad" },
                    ],
                  },
                  {
                    prim: "Elt",
                    args: [{ string: "name" }, { string: "Neofacto Bond" }],
                  },
                  {
                    prim: "Elt",
                    args: [{ string: "shortName" }, { string: "Neofacto" }],
                  },
                  {
                    prim: "Elt",
                    args: [{ string: "symbol" }, { string: "NEO.BD" }],
                  },
                  {
                    prim: "Elt",
                    args: [
                      { string: "website" },
                      { string: "https://www.neofacto.com" },
                    ],
                  },
                ],
                [],
              ],
            },
          ],
        },
      ],
    },
  ],
};
