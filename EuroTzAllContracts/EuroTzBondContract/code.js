export default [
  {
    prim: "storage",
    args: [
      {
        prim: "pair",
        args: [
          {
            prim: "pair",
            args: [
              {
                prim: "pair",
                args: [
                  { prim: "address", annots: ["%administrator"] },
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%commissionBeneficiary"] },
                      { prim: "nat", annots: ["%currentTokenId"] },
                    ],
                  },
                ],
              },
              {
                prim: "pair",
                args: [
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "address",
                        annots: ["%distributionCurrenyAddress"],
                      },
                      {
                        prim: "address",
                        annots: ["%eventSinkContractAddress"],
                      },
                    ],
                  },
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "big_map",
                        args: [
                          { prim: "address" },
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat", annots: ["%nonce"] },
                              {
                                prim: "set",
                                args: [{ prim: "nat" }],
                                annots: ["%ownedTokens"],
                              },
                            ],
                          },
                        ],
                        annots: ["%ledger"],
                      },
                      { prim: "nat", annots: ["%maxAmount"] },
                    ],
                  },
                ],
              },
            ],
          },
          {
            prim: "pair",
            args: [
              {
                prim: "pair",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "nat", annots: ["%maxPeriod"] },
                      { prim: "nat", annots: ["%minAmount"] },
                    ],
                  },
                  {
                    prim: "pair",
                    args: [
                      { prim: "nat", annots: ["%minPeriod"] },
                      {
                        prim: "big_map",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "address", annots: ["%owner"] },
                              { prim: "address", annots: ["%operator"] },
                            ],
                          },
                          { prim: "unit" },
                        ],
                        annots: ["%operators"],
                      },
                    ],
                  },
                ],
              },
              {
                prim: "pair",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "int", annots: ["%ownedTokensCount"] },
                      { prim: "bool", annots: ["%paused"] },
                    ],
                  },
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "map",
                        args: [{ prim: "string" }, { prim: "string" }],
                        annots: ["%providerMetadata"],
                      },
                      {
                        prim: "big_map",
                        args: [
                          { prim: "nat" },
                          {
                            prim: "pair",
                            args: [
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat", annots: ["%amount"] },
                                  { prim: "nat", annots: ["%interestRate"] },
                                ],
                              },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "timestamp", annots: ["%issueTime"] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address", annots: ["%owner"] },
                                      { prim: "nat", annots: ["%period"] },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                        annots: ["%tokens"],
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "parameter",
    args: [
      {
        prim: "or",
        args: [
          {
            prim: "or",
            args: [
              {
                prim: "pair",
                args: [
                  { prim: "bytes", annots: ["%b"] },
                  {
                    prim: "pair",
                    args: [
                      { prim: "key", annots: ["%k"] },
                      { prim: "signature", annots: ["%s"] },
                    ],
                  },
                ],
                annots: ["%approveFeeless"],
              },
              {
                prim: "pair",
                args: [
                  { prim: "bytes", annots: ["%b"] },
                  {
                    prim: "pair",
                    args: [
                      { prim: "key", annots: ["%k"] },
                      { prim: "signature", annots: ["%s"] },
                    ],
                  },
                ],
                annots: ["%burnFeeless"],
              },
            ],
          },
          {
            prim: "or",
            args: [
              {
                prim: "pair",
                args: [
                  { prim: "bytes", annots: ["%b"] },
                  {
                    prim: "pair",
                    args: [
                      { prim: "key", annots: ["%k"] },
                      { prim: "signature", annots: ["%s"] },
                    ],
                  },
                ],
                annots: ["%mintFeeless"],
              },
              {
                prim: "or",
                args: [
                  {
                    prim: "list",
                    args: [
                      {
                        prim: "pair",
                        args: [
                          { prim: "address", annots: ["%from_"] },
                          {
                            prim: "pair",
                            args: [
                              { prim: "address", annots: ["%to_"] },
                              { prim: "nat", annots: ["%token_id"] },
                            ],
                          },
                        ],
                      },
                    ],
                    annots: ["%transfer"],
                  },
                  {
                    prim: "pair",
                    args: [
                      { prim: "bytes", annots: ["%b"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "key", annots: ["%k"] },
                          { prim: "signature", annots: ["%s"] },
                        ],
                      },
                    ],
                    annots: ["%transferFeeless"],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "code",
    args: [
      [
        { prim: "DUP" },
        { prim: "CDR" },
        { prim: "SWAP" },
        { prim: "CAR" },
        {
          prim: "IF_LEFT",
          args: [
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    [
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "SENDER" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "NOT_ADMIN" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CHECK_SIGNATURE" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "string" }, { string: "20" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "option", args: [{ prim: "nat" }] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "SELF" },
                      { prim: "ADDRESS" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "string" }, { string: "35" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "option", args: [{ prim: "nat" }] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            {
                              prim: "PUSH",
                              args: [{ prim: "bool" }, { prim: "False" }],
                            },
                          ],
                          [
                            [
                              { prim: "DROP" },
                              {
                                prim: "PUSH",
                                args: [{ prim: "bool" }, { prim: "True" }],
                              },
                            ],
                          ],
                        ],
                      },
                      {
                        prim: "IF",
                        args: [
                          [
                            {
                              prim: "PUSH",
                              args: [{ prim: "bool" }, { prim: "False" }],
                            },
                          ],
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      {
                                        prim: "option",
                                        args: [{ prim: "nat" }],
                                      },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "address" },
                                              { prim: "address" },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "MEM" },
                              { prim: "NOT" },
                            ],
                          ],
                        ],
                      },
                      {
                        prim: "IF",
                        args: [
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              {
                                prim: "PUSH",
                                args: [
                                  {
                                    prim: "option",
                                    args: [
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "nat", annots: ["%nonce"] },
                                          {
                                            prim: "set",
                                            args: [{ prim: "nat" }],
                                            annots: ["%ownedTokens"],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                  {
                                    prim: "Some",
                                    args: [
                                      {
                                        prim: "Pair",
                                        args: [{ int: "0" }, []],
                                      },
                                    ],
                                  },
                                ],
                              },
                              { prim: "DIG", args: [{ int: "6" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "7" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      {
                                        prim: "option",
                                        args: [{ prim: "nat" }],
                                      },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "address" },
                                              { prim: "address" },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "UPDATE" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                            ],
                          ],
                          [
                            [
                              { prim: "DUP" },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      {
                                        prim: "option",
                                        args: [{ prim: "nat" }],
                                      },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "address" },
                                              { prim: "address" },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CAR" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      {
                                        prim: "option",
                                        args: [{ prim: "nat" }],
                                      },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "address" },
                                              { prim: "address" },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:383" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "CAR" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "19" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                            ],
                          ],
                        ],
                      },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      {
                        prim: "PUSH",
                        args: [
                          { prim: "option", args: [{ prim: "unit" }] },
                          { prim: "Some", args: [{ prim: "Unit" }] },
                        ],
                      },
                      { prim: "DIG", args: [{ int: "6" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "7" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "option", args: [{ prim: "nat" }] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "8" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "option", args: [{ prim: "nat" }] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%owner", "%operator"] },
                      { prim: "UPDATE" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "DIG", args: [{ int: "6" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "7" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "option", args: [{ prim: "nat" }] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "set_in_top-any" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "1" }] },
                      { prim: "DIG", args: [{ int: "9" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "9" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "option", args: [{ prim: "nat" }] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:383" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "ADD" },
                      { prim: "PAIR" },
                      { prim: "SOME" },
                      { prim: "SWAP" },
                      { prim: "UPDATE" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                    ],
                  ],
                  [
                    [
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "SENDER" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "NOT_ADMIN" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CHECK_SIGNATURE" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "string" }, { string: "20" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "SELF" },
                      { prim: "ADDRESS" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "string" }, { string: "35" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:421" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "string" }, { string: "19" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "MEM" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "TOKEN_UNDEFINED" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:414" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "NOT_OWNER" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:421" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "MEM" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "NOT_OWNER" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:414" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      {
                        prim: "PUSH",
                        args: [{ prim: "nat" }, { int: "2628000" }],
                      },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:414" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "MUL" },
                      { prim: "INT" },
                      { prim: "ADD" },
                      { prim: "NOW" },
                      { prim: "COMPARE" },
                      { prim: "GT" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "NOT_REDEEMABLE" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                      { prim: "DUP" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      {
                        prim: "CONTRACT",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "int", annots: ["%amount"] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address", annots: ["%f"] },
                                  { prim: "address", annots: ["%t"] },
                                ],
                              },
                            ],
                          },
                        ],
                        annots: ["%transfer"],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "6" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "7" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%f", "%t"] },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "100" }] },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "12" }] },
                      { prim: "DIG", args: [{ int: "8" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "9" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "8" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "9" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:414" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "EDIV" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "division by zero" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [{ prim: "CAR" }],
                        ],
                      },
                      { prim: "DIG", args: [{ int: "8" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "9" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "8" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "9" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:414" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "9" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "10" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "9" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "10" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:414" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "MUL" },
                      { prim: "MUL" },
                      { prim: "EDIV" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "division by zero" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [{ prim: "CAR" }],
                        ],
                      },
                      { prim: "DIG", args: [{ int: "5" }] },
                      { prim: "DROP" },
                      { prim: "INT" },
                      { prim: "PAIR", annots: ["%amount"] },
                      { prim: "TRANSFER_TOKENS" },
                      { prim: "CONS" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "8" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "set_in_top-any" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      {
                        prim: "PUSH",
                        args: [{ prim: "bool" }, { prim: "False" }],
                      },
                      { prim: "DIG", args: [{ int: "10" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "11" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "UPDATE" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SOME" },
                      { prim: "SWAP" },
                      { prim: "UPDATE" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      {
                        prim: "NONE",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat", annots: ["%amount"] },
                                  { prim: "nat", annots: ["%interestRate"] },
                                ],
                              },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "timestamp", annots: ["%issueTime"] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address", annots: ["%owner"] },
                                      { prim: "nat", annots: ["%period"] },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "8" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "UPDATE" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "PUSH", args: [{ prim: "int" }, { int: "1" }] },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "SUB" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      {
                        prim: "CONTRACT",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "int", annots: ["%amount"] },
                              {
                                prim: "pair",
                                args: [
                                  {
                                    prim: "option",
                                    args: [{ prim: "address" }],
                                    annots: ["%fromAddress"],
                                  },
                                  {
                                    prim: "option",
                                    args: [{ prim: "address" }],
                                    annots: ["%toAddress"],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                        annots: ["%transferEvent"],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                      { prim: "NONE", args: [{ prim: "address" }] },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "SOME" },
                      { prim: "PAIR", annots: ["%fromAddress", "%toAddress"] },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "INT" },
                      { prim: "PAIR", annots: ["%amount"] },
                      { prim: "TRANSFER_TOKENS" },
                      { prim: "CONS" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "8" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "set_in_top-any" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "1" }] },
                      { prim: "DIG", args: [{ int: "10" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "10" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "nat" },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "address" },
                                      { prim: "address" },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:421" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "ADD" },
                      { prim: "PAIR" },
                      { prim: "SOME" },
                      { prim: "SWAP" },
                      { prim: "UPDATE" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                    ],
                  ],
                ],
              },
            ],
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    [
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "SENDER" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "NOT_ADMIN" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CHECK_SIGNATURE" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "string" }, { string: "20" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "SELF" },
                      { prim: "ADDRESS" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "string" }, { string: "35" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            {
                              prim: "PUSH",
                              args: [{ prim: "bool" }, { prim: "False" }],
                            },
                          ],
                          [
                            [
                              { prim: "DROP" },
                              {
                                prim: "PUSH",
                                args: [{ prim: "bool" }, { prim: "True" }],
                              },
                            ],
                          ],
                        ],
                      },
                      {
                        prim: "IF",
                        args: [
                          [
                            {
                              prim: "PUSH",
                              args: [{ prim: "bool" }, { prim: "False" }],
                            },
                          ],
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "option",
                                            args: [{ prim: "nat" }],
                                          },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "nat" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  { prim: "address" },
                                                  { prim: "address" },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "MEM" },
                              { prim: "NOT" },
                            ],
                          ],
                        ],
                      },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              { prim: "DUP" },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "option",
                                            args: [{ prim: "nat" }],
                                          },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "nat" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  { prim: "address" },
                                                  { prim: "address" },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "option",
                                            args: [{ prim: "nat" }],
                                          },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "nat" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  { prim: "address" },
                                                  { prim: "address" },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:403" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "CAR" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "19" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                            ],
                          ],
                        ],
                      },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "COMPARE" },
                      { prim: "GE" },
                      {
                        prim: "IF",
                        args: [
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "option",
                                            args: [{ prim: "nat" }],
                                          },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "nat" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  { prim: "address" },
                                                  { prim: "address" },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "COMPARE" },
                              { prim: "LE" },
                            ],
                          ],
                          [
                            {
                              prim: "PUSH",
                              args: [{ prim: "bool" }, { prim: "False" }],
                            },
                          ],
                        ],
                      },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "INSUFFICIENT_AMOUNT" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "COMPARE" },
                      { prim: "GE" },
                      {
                        prim: "IF",
                        args: [
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "option",
                                            args: [{ prim: "nat" }],
                                          },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "nat" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  { prim: "address" },
                                                  { prim: "address" },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CAR" },
                              { prim: "COMPARE" },
                              { prim: "LE" },
                            ],
                          ],
                          [
                            {
                              prim: "PUSH",
                              args: [{ prim: "bool" }, { prim: "False" }],
                            },
                          ],
                        ],
                      },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "PERIOD_ERROR" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      {
                        prim: "CONTRACT",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "int", annots: ["%amount"] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address", annots: ["%f"] },
                                  { prim: "address", annots: ["%t"] },
                                ],
                              },
                            ],
                          },
                        ],
                        annots: ["%transfer"],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%f", "%t"] },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "INT" },
                      { prim: "PAIR", annots: ["%amount"] },
                      { prim: "TRANSFER_TOKENS" },
                      { prim: "CONS" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "MEM" },
                      {
                        prim: "IF",
                        args: [
                          [
                            [
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "DIG", args: [{ int: "7" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "8" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "option",
                                            args: [{ prim: "nat" }],
                                          },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "nat" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  { prim: "address" },
                                                  { prim: "address" },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "set_in_top-any" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              {
                                prim: "PUSH",
                                args: [{ prim: "bool" }, { prim: "True" }],
                              },
                              { prim: "DIG", args: [{ int: "11" }] },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "UPDATE" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SOME" },
                              { prim: "SWAP" },
                              { prim: "UPDATE" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "DUG", args: [{ int: "2" }] },
                            ],
                          ],
                          [
                            [
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "set", args: [{ prim: "nat" }] },
                                  [],
                                ],
                              },
                              {
                                prim: "PUSH",
                                args: [{ prim: "bool" }, { prim: "True" }],
                              },
                              { prim: "DIG", args: [{ int: "9" }] },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "UPDATE" },
                              {
                                prim: "PUSH",
                                args: [{ prim: "nat" }, { int: "0" }],
                              },
                              {
                                prim: "PAIR",
                                annots: ["%nonce", "%ownedTokens"],
                              },
                              { prim: "SOME" },
                              { prim: "DIG", args: [{ int: "7" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "8" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "option",
                                            args: [{ prim: "nat" }],
                                          },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "nat" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  { prim: "address" },
                                                  { prim: "address" },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "UPDATE" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "DUG", args: [{ int: "2" }] },
                            ],
                          ],
                        ],
                      },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "0" }] },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "12" }] },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "COMPARE" },
                      { prim: "LE" },
                      {
                        prim: "IF",
                        args: [
                          [
                            [
                              { prim: "DROP" },
                              {
                                prim: "PUSH",
                                args: [{ prim: "nat" }, { int: "100" }],
                              },
                            ],
                          ],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "nat" }, { int: "24" }],
                              },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "4" }] },
                              { prim: "CAR" },
                              {
                                prim: "UNPACK",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "option",
                                            args: [{ prim: "nat" }],
                                          },
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "nat" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  { prim: "address" },
                                                  { prim: "address" },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "unit" },
                                          { prim: "Unit" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [[]],
                                ],
                              },
                              { prim: "CAR" },
                              { prim: "COMPARE" },
                              { prim: "LE" },
                              {
                                prim: "IF",
                                args: [
                                  [
                                    [
                                      { prim: "DROP" },
                                      {
                                        prim: "PUSH",
                                        args: [{ prim: "nat" }, { int: "200" }],
                                      },
                                    ],
                                  ],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [{ prim: "nat" }, { int: "36" }],
                                      },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "4" }] },
                                      { prim: "CAR" },
                                      {
                                        prim: "UNPACK",
                                        args: [
                                          {
                                            prim: "pair",
                                            args: [
                                              { prim: "nat" },
                                              {
                                                prim: "pair",
                                                args: [
                                                  {
                                                    prim: "option",
                                                    args: [{ prim: "nat" }],
                                                  },
                                                  {
                                                    prim: "pair",
                                                    args: [
                                                      { prim: "nat" },
                                                      {
                                                        prim: "pair",
                                                        args: [
                                                          { prim: "address" },
                                                          { prim: "address" },
                                                        ],
                                                      },
                                                    ],
                                                  },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                      {
                                        prim: "IF_NONE",
                                        args: [
                                          [
                                            [
                                              {
                                                prim: "PUSH",
                                                args: [
                                                  { prim: "unit" },
                                                  { prim: "Unit" },
                                                ],
                                              },
                                              { prim: "FAILWITH" },
                                            ],
                                          ],
                                          [[]],
                                        ],
                                      },
                                      { prim: "CAR" },
                                      { prim: "COMPARE" },
                                      { prim: "LE" },
                                      {
                                        prim: "IF",
                                        args: [
                                          [
                                            [
                                              { prim: "DROP" },
                                              {
                                                prim: "PUSH",
                                                args: [
                                                  { prim: "nat" },
                                                  { int: "300" },
                                                ],
                                              },
                                            ],
                                          ],
                                          [
                                            [
                                              {
                                                prim: "PUSH",
                                                args: [
                                                  { prim: "nat" },
                                                  { int: "48" },
                                                ],
                                              },
                                              {
                                                prim: "DIG",
                                                args: [{ int: "3" }],
                                              },
                                              { prim: "DUP" },
                                              {
                                                prim: "DUG",
                                                args: [{ int: "4" }],
                                              },
                                              { prim: "CAR" },
                                              {
                                                prim: "UNPACK",
                                                args: [
                                                  {
                                                    prim: "pair",
                                                    args: [
                                                      { prim: "nat" },
                                                      {
                                                        prim: "pair",
                                                        args: [
                                                          {
                                                            prim: "option",
                                                            args: [
                                                              { prim: "nat" },
                                                            ],
                                                          },
                                                          {
                                                            prim: "pair",
                                                            args: [
                                                              { prim: "nat" },
                                                              {
                                                                prim: "pair",
                                                                args: [
                                                                  {
                                                                    prim:
                                                                      "address",
                                                                  },
                                                                  {
                                                                    prim:
                                                                      "address",
                                                                  },
                                                                ],
                                                              },
                                                            ],
                                                          },
                                                        ],
                                                      },
                                                    ],
                                                  },
                                                ],
                                              },
                                              {
                                                prim: "IF_NONE",
                                                args: [
                                                  [
                                                    [
                                                      {
                                                        prim: "PUSH",
                                                        args: [
                                                          { prim: "unit" },
                                                          { prim: "Unit" },
                                                        ],
                                                      },
                                                      { prim: "FAILWITH" },
                                                    ],
                                                  ],
                                                  [[]],
                                                ],
                                              },
                                              { prim: "CAR" },
                                              { prim: "COMPARE" },
                                              { prim: "LE" },
                                              {
                                                prim: "IF",
                                                args: [
                                                  [
                                                    [
                                                      { prim: "DROP" },
                                                      {
                                                        prim: "PUSH",
                                                        args: [
                                                          { prim: "nat" },
                                                          { int: "400" },
                                                        ],
                                                      },
                                                    ],
                                                  ],
                                                  [
                                                    [
                                                      {
                                                        prim: "PUSH",
                                                        args: [
                                                          { prim: "nat" },
                                                          { int: "60" },
                                                        ],
                                                      },
                                                      {
                                                        prim: "DIG",
                                                        args: [{ int: "3" }],
                                                      },
                                                      { prim: "DUP" },
                                                      {
                                                        prim: "DUG",
                                                        args: [{ int: "4" }],
                                                      },
                                                      { prim: "CAR" },
                                                      {
                                                        prim: "UNPACK",
                                                        args: [
                                                          {
                                                            prim: "pair",
                                                            args: [
                                                              { prim: "nat" },
                                                              {
                                                                prim: "pair",
                                                                args: [
                                                                  {
                                                                    prim:
                                                                      "option",
                                                                    args: [
                                                                      {
                                                                        prim:
                                                                          "nat",
                                                                      },
                                                                    ],
                                                                  },
                                                                  {
                                                                    prim:
                                                                      "pair",
                                                                    args: [
                                                                      {
                                                                        prim:
                                                                          "nat",
                                                                      },
                                                                      {
                                                                        prim:
                                                                          "pair",
                                                                        args: [
                                                                          {
                                                                            prim:
                                                                              "address",
                                                                          },
                                                                          {
                                                                            prim:
                                                                              "address",
                                                                          },
                                                                        ],
                                                                      },
                                                                    ],
                                                                  },
                                                                ],
                                                              },
                                                            ],
                                                          },
                                                        ],
                                                      },
                                                      {
                                                        prim: "IF_NONE",
                                                        args: [
                                                          [
                                                            [
                                                              {
                                                                prim: "PUSH",
                                                                args: [
                                                                  {
                                                                    prim:
                                                                      "unit",
                                                                  },
                                                                  {
                                                                    prim:
                                                                      "Unit",
                                                                  },
                                                                ],
                                                              },
                                                              {
                                                                prim:
                                                                  "FAILWITH",
                                                              },
                                                            ],
                                                          ],
                                                          [[]],
                                                        ],
                                                      },
                                                      { prim: "CAR" },
                                                      { prim: "COMPARE" },
                                                      { prim: "LE" },
                                                      {
                                                        prim: "IF",
                                                        args: [
                                                          [
                                                            [
                                                              { prim: "DROP" },
                                                              {
                                                                prim: "PUSH",
                                                                args: [
                                                                  {
                                                                    prim: "nat",
                                                                  },
                                                                  {
                                                                    int: "500",
                                                                  },
                                                                ],
                                                              },
                                                            ],
                                                          ],
                                                          [[]],
                                                        ],
                                                      },
                                                    ],
                                                  ],
                                                ],
                                              },
                                            ],
                                          ],
                                        ],
                                      },
                                    ],
                                  ],
                                ],
                              },
                            ],
                          ],
                        ],
                      },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "8" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "8" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "9" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%owner", "%period"] },
                      { prim: "NOW" },
                      { prim: "PAIR", annots: ["%issueTime"] },
                      { prim: "DIG", args: [{ int: "6" }] },
                      { prim: "DIG", args: [{ int: "8" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "9" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%amount", "%interestRate"] },
                      { prim: "PAIR" },
                      { prim: "SOME" },
                      { prim: "DIG", args: [{ int: "8" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "UPDATE" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "1" }] },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "ADD" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "PUSH", args: [{ prim: "int" }, { int: "1" }] },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "ADD" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      {
                        prim: "CONTRACT",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "int", annots: ["%amount"] },
                              {
                                prim: "pair",
                                args: [
                                  {
                                    prim: "option",
                                    args: [{ prim: "address" }],
                                    annots: ["%fromAddress"],
                                  },
                                  {
                                    prim: "option",
                                    args: [{ prim: "address" }],
                                    annots: ["%toAddress"],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                        annots: ["%transferEvent"],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "SOME" },
                      { prim: "NONE", args: [{ prim: "address" }] },
                      { prim: "PAIR", annots: ["%fromAddress", "%toAddress"] },
                      { prim: "DIG", args: [{ int: "5" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "6" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "INT" },
                      { prim: "PAIR", annots: ["%amount"] },
                      { prim: "TRANSFER_TOKENS" },
                      { prim: "CONS" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "8" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "set_in_top-any" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "PUSH", args: [{ prim: "nat" }, { int: "1" }] },
                      { prim: "DIG", args: [{ int: "10" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "10" }] },
                      { prim: "CAR" },
                      {
                        prim: "UNPACK",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat" },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "option", args: [{ prim: "nat" }] },
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "nat" },
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "address" },
                                          { prim: "address" },
                                        ],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                        ],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "Get-item:403" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      { prim: "CAR" },
                      { prim: "ADD" },
                      { prim: "PAIR" },
                      { prim: "SOME" },
                      { prim: "SWAP" },
                      { prim: "UPDATE" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                    ],
                  ],
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            {
                              prim: "IF",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "CONTRACT_IS_PAUSED" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "DUP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "SWAP" },
                            {
                              prim: "ITER",
                              args: [
                                [
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CAR" },
                                  { prim: "CAR" },
                                  { prim: "CAR" },
                                  { prim: "SENDER" },
                                  { prim: "COMPARE" },
                                  { prim: "EQ" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "bool" },
                                            { prim: "True" },
                                          ],
                                        },
                                      ],
                                      [
                                        [
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "SENDER" },
                                          { prim: "COMPARE" },
                                          { prim: "EQ" },
                                        ],
                                      ],
                                    ],
                                  },
                                  {
                                    prim: "IF",
                                    args: [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "bool" },
                                            { prim: "True" },
                                          ],
                                        },
                                      ],
                                      [
                                        [
                                          { prim: "DIG", args: [{ int: "3" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "4" }] },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "SENDER" },
                                          { prim: "DIG", args: [{ int: "2" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "3" }] },
                                          { prim: "CAR" },
                                          {
                                            prim: "PAIR",
                                            annots: ["%owner", "%operator"],
                                          },
                                          { prim: "MEM" },
                                        ],
                                      ],
                                    ],
                                  },
                                  {
                                    prim: "IF",
                                    args: [
                                      [[]],
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "NOT_OPERATOR" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                    ],
                                  },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "MEM" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [[]],
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "TOKEN_UNDEFINED" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                    ],
                                  },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "Get-item:23" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "COMPARE" },
                                  { prim: "EQ" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [[]],
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "NOT_OWNER" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                    ],
                                  },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "CDR" },
                                  { prim: "SWAP" },
                                  { prim: "CAR" },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "CDR" },
                                  { prim: "SWAP" },
                                  { prim: "CAR" },
                                  { prim: "DUP" },
                                  { prim: "DIG", args: [{ int: "6" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "7" }] },
                                  { prim: "CAR" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "set_in_top-any" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  {
                                    prim: "PUSH",
                                    args: [{ prim: "bool" }, { prim: "False" }],
                                  },
                                  { prim: "DIG", args: [{ int: "9" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "10" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "UPDATE" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SOME" },
                                  { prim: "SWAP" },
                                  { prim: "UPDATE" },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "PAIR" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "DIG", args: [{ int: "6" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "7" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "set_in_top-any" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "9" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "10" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SOME" },
                                  { prim: "SWAP" },
                                  { prim: "UPDATE" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "MEM" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [
                                        [
                                          { prim: "DIG", args: [{ int: "3" }] },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DUP" },
                                          { prim: "DIG", args: [{ int: "6" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "7" }] },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "2" }] },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                [
                                                  {
                                                    prim: "PUSH",
                                                    args: [
                                                      { prim: "string" },
                                                      {
                                                        string:
                                                          "set_in_top-any",
                                                      },
                                                    ],
                                                  },
                                                  { prim: "FAILWITH" },
                                                ],
                                              ],
                                              [],
                                            ],
                                          },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "bool" },
                                              { prim: "True" },
                                            ],
                                          },
                                          { prim: "DIG", args: [{ int: "9" }] },
                                          { prim: "DUP" },
                                          {
                                            prim: "DUG",
                                            args: [{ int: "10" }],
                                          },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "UPDATE" },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "SOME" },
                                          { prim: "SWAP" },
                                          { prim: "UPDATE" },
                                          { prim: "PAIR" },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "PAIR" },
                                          { prim: "DUG", args: [{ int: "3" }] },
                                        ],
                                      ],
                                      [
                                        [
                                          { prim: "DIG", args: [{ int: "3" }] },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          {
                                            prim: "PUSH",
                                            args: [
                                              {
                                                prim: "set",
                                                args: [{ prim: "nat" }],
                                              },
                                              [],
                                            ],
                                          },
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "bool" },
                                              { prim: "True" },
                                            ],
                                          },
                                          { prim: "DIG", args: [{ int: "7" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "8" }] },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "UPDATE" },
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "nat" },
                                              { int: "0" },
                                            ],
                                          },
                                          {
                                            prim: "PAIR",
                                            annots: ["%nonce", "%ownedTokens"],
                                          },
                                          { prim: "SOME" },
                                          { prim: "DIG", args: [{ int: "6" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "7" }] },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "UPDATE" },
                                          { prim: "PAIR" },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "PAIR" },
                                          { prim: "DUG", args: [{ int: "3" }] },
                                        ],
                                      ],
                                    ],
                                  },
                                  { prim: "SWAP" },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  {
                                    prim: "CONTRACT",
                                    args: [
                                      {
                                        prim: "pair",
                                        args: [
                                          { prim: "int", annots: ["%amount"] },
                                          {
                                            prim: "pair",
                                            args: [
                                              {
                                                prim: "option",
                                                args: [{ prim: "address" }],
                                                annots: ["%fromAddress"],
                                              },
                                              {
                                                prim: "option",
                                                args: [{ prim: "address" }],
                                                annots: ["%toAddress"],
                                              },
                                            ],
                                          },
                                        ],
                                      },
                                    ],
                                    annots: ["%transferEvent"],
                                  },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "unit" },
                                              { prim: "Unit" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  {
                                    prim: "PUSH",
                                    args: [{ prim: "mutez" }, { int: "0" }],
                                  },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "SOME" },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CAR" },
                                  { prim: "SOME" },
                                  {
                                    prim: "PAIR",
                                    annots: ["%fromAddress", "%toAddress"],
                                  },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "INT" },
                                  { prim: "PAIR", annots: ["%amount"] },
                                  { prim: "TRANSFER_TOKENS" },
                                  { prim: "CONS" },
                                ],
                              ],
                            },
                            { prim: "SWAP" },
                            { prim: "DROP" },
                          ],
                        ],
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "SENDER" },
                            { prim: "COMPARE" },
                            { prim: "EQ" },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "NOT_ADMIN" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "CHECK_SIGNATURE" },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "20" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "SELF" },
                            { prim: "ADDRESS" },
                            { prim: "COMPARE" },
                            { prim: "EQ" },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "35" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:365" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "COMPARE" },
                            { prim: "EQ" },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "19" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "MEM" },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "TOKEN_UNDEFINED" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:358" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "COMPARE" },
                            { prim: "EQ" },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "NOT_OWNER" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "DUP" },
                            { prim: "DIG", args: [{ int: "6" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "7" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "set_in_top-any" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            {
                              prim: "PUSH",
                              args: [{ prim: "bool" }, { prim: "False" }],
                            },
                            { prim: "DIG", args: [{ int: "9" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "10" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "UPDATE" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SOME" },
                            { prim: "SWAP" },
                            { prim: "UPDATE" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "DIG", args: [{ int: "6" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "7" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "set_in_top-any" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "9" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "10" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SOME" },
                            { prim: "SWAP" },
                            { prim: "UPDATE" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "MEM" },
                            {
                              prim: "IF",
                              args: [
                                [
                                  [
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DUP" },
                                    { prim: "DIG", args: [{ int: "6" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "7" }] },
                                    { prim: "CAR" },
                                    {
                                      prim: "UNPACK",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "nat" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "nat" },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        { prim: "address" },
                                                        { prim: "address" },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "unit" },
                                                { prim: "Unit" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [[]],
                                      ],
                                    },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "set_in_top-any" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "bool" },
                                        { prim: "True" },
                                      ],
                                    },
                                    { prim: "DIG", args: [{ int: "9" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "10" }] },
                                    { prim: "CAR" },
                                    {
                                      prim: "UNPACK",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "nat" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "nat" },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        { prim: "address" },
                                                        { prim: "address" },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "unit" },
                                                { prim: "Unit" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [[]],
                                      ],
                                    },
                                    { prim: "CAR" },
                                    { prim: "UPDATE" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "SOME" },
                                    { prim: "SWAP" },
                                    { prim: "UPDATE" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                  ],
                                ],
                                [
                                  [
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        {
                                          prim: "set",
                                          args: [{ prim: "nat" }],
                                        },
                                        [],
                                      ],
                                    },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "bool" },
                                        { prim: "True" },
                                      ],
                                    },
                                    { prim: "DIG", args: [{ int: "7" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "8" }] },
                                    { prim: "CAR" },
                                    {
                                      prim: "UNPACK",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "nat" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "nat" },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        { prim: "address" },
                                                        { prim: "address" },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "unit" },
                                                { prim: "Unit" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [[]],
                                      ],
                                    },
                                    { prim: "CAR" },
                                    { prim: "UPDATE" },
                                    {
                                      prim: "PUSH",
                                      args: [{ prim: "nat" }, { int: "0" }],
                                    },
                                    {
                                      prim: "PAIR",
                                      annots: ["%nonce", "%ownedTokens"],
                                    },
                                    { prim: "SOME" },
                                    { prim: "DIG", args: [{ int: "6" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "7" }] },
                                    { prim: "CAR" },
                                    {
                                      prim: "UNPACK",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "nat" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "nat" },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    { prim: "address" },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        { prim: "address" },
                                                        { prim: "address" },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "unit" },
                                                { prim: "Unit" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [[]],
                                      ],
                                    },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "UPDATE" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            {
                              prim: "CONTRACT",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "int", annots: ["%amount"] },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%fromAddress"],
                                        },
                                        {
                                          prim: "option",
                                          args: [{ prim: "address" }],
                                          annots: ["%toAddress"],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                              annots: ["%transferEvent"],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "SOME" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "SOME" },
                            {
                              prim: "PAIR",
                              annots: ["%fromAddress", "%toAddress"],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "INT" },
                            { prim: "PAIR", annots: ["%amount"] },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "DUP" },
                            { prim: "DIG", args: [{ int: "7" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "8" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "set_in_top-any" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "CDR" },
                            {
                              prim: "PUSH",
                              args: [{ prim: "nat" }, { int: "1" }],
                            },
                            { prim: "DIG", args: [{ int: "10" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "10" }] },
                            { prim: "CAR" },
                            {
                              prim: "UNPACK",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    { prim: "nat" },
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "nat" },
                                        {
                                          prim: "pair",
                                          args: [
                                            { prim: "address" },
                                            {
                                              prim: "pair",
                                              args: [
                                                { prim: "address" },
                                                { prim: "address" },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:365" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "ADD" },
                            { prim: "PAIR" },
                            { prim: "SOME" },
                            { prim: "SWAP" },
                            { prim: "UPDATE" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                          ],
                        ],
                      ],
                    },
                  ],
                ],
              },
            ],
          ],
        },
        { prim: "NIL", args: [{ prim: "operation" }] },
        { prim: "SWAP" },
        { prim: "ITER", args: [[{ prim: "CONS" }]] },
        { prim: "PAIR" },
      ],
    ],
  },
];
