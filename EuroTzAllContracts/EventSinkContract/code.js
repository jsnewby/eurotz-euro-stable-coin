export default [
  { prim: "storage", args: [{ prim: "unit" }] },
  {
    prim: "parameter",
    args: [
      {
        prim: "or",
        args: [
          {
            prim: "pair",
            args: [
              { prim: "int", annots: ["%amount"] },
              {
                prim: "pair",
                args: [
                  { prim: "address", annots: ["%owner"] },
                  { prim: "address", annots: ["%spender"] },
                ],
              },
            ],
            annots: ["%approvalEvent"],
          },
          {
            prim: "or",
            args: [
              {
                prim: "pair",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%DVPaddress"] },
                      { prim: "int", annots: ["%amount_A"] },
                    ],
                  },
                  {
                    prim: "pair",
                    args: [
                      { prim: "int", annots: ["%amount_B"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "address", annots: ["%party_A"] },
                          { prim: "address", annots: ["%party_B"] },
                        ],
                      },
                    ],
                  },
                ],
                annots: ["%exchangeEvent"],
              },
              {
                prim: "pair",
                args: [
                  { prim: "int", annots: ["%amount"] },
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "option",
                        args: [{ prim: "address" }],
                        annots: ["%fromAddress"],
                      },
                      {
                        prim: "option",
                        args: [{ prim: "address" }],
                        annots: ["%toAddress"],
                      },
                    ],
                  },
                ],
                annots: ["%transferEvent"],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "code",
    args: [
      [
        { prim: "DUP" },
        { prim: "CDR" },
        { prim: "SWAP" },
        { prim: "CAR" },
        {
          prim: "IF_LEFT",
          args: [
            [[{ prim: "DROP" }]],
            [
              {
                prim: "IF_LEFT",
                args: [[[{ prim: "DROP" }]], [[{ prim: "DROP" }]]],
              },
            ],
          ],
        },
        { prim: "NIL", args: [{ prim: "operation" }] },
        { prim: "PAIR" },
      ],
    ],
  },
];
