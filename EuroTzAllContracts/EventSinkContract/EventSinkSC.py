import smartpy as sp

class EventSink(sp.Contract):
    def __init__(self):
        self.init()

    @sp.entry_point
    def transferEvent(self, params):
        sp.set_type(params, sp.TRecord(amount = sp.TIntOrNat, fromAddress = sp.TOption(sp.TAddress), toAddress = sp.TOption(sp.TAddress)))
        
    @sp.entry_point
    def approvalEvent(self, params):
        sp.set_type(params, sp.TRecord(amount = sp.TIntOrNat, owner = sp.TAddress, spender = sp.TAddress))
    
    @sp.entry_point
    def exchangeEvent(self, params):
        sp.set_type(params, sp.TRecord(amount_A = sp.TIntOrNat, amount_B = sp.TIntOrNat, party_A = sp.TAddress, party_B = sp.TAddress, DVPaddress = sp.TAddress))    


if "templates" not in __name__:
    @sp.add_test(name = "EventSink")
    def test():
        
        c1 = EventSink()
        
        scenario = sp.test_scenario()
        scenario += c1
       
        scenario.h1("EventSink Mint Event")
        scenario += c1.transferEvent(amount = 40, fromAddress = sp.none, toAddress = sp.some(sp.address("tz1-toAddress-12345")))
        scenario.h1("EventSink Burn Event")
        scenario += c1.transferEvent(amount = 40, fromAddress = sp.some(sp.address("tz1-toAddress-12345")), toAddress = sp.none)
        scenario.h1("EventSink Transfer Event")
        scenario += c1.transferEvent(amount = 40, fromAddress = sp.some(sp.address("tz1-fromAddress-12345")), toAddress = sp.some(sp.address("tz1-toAddress-12345")))
        scenario.h1("EventSink Approval Event")
        scenario += c1.approvalEvent(amount = 40, owner = sp.address("tz1-owner-12345"), spender = sp.address("tz1-spender-12345"))
        scenario.h1("Exchange Event")
        scenario += c1.exchangeEvent(amount_A = 40, amount_B = 12, party_A = sp.address("tz1-party-A-12345"), party_B = sp.address("tz1-party-B-12345"), DVPaddress = sp.address("KT1-dvp-address"))
