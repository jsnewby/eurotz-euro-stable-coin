import smartpy as sp

class Escrow(sp.Contract):
    def __init__(self):
        self.init_type(sp.TRecord(
                  admin = sp.TAddress,
                  eventSinkContractAddress = sp.TAddress,
                  creationTime = sp.TTimestamp,
                  expiryTime = sp.TTimestamp,
                  exchanged = sp.TBool,
                  partyDetailsMap = sp.TMap(sp.TString, sp.TRecord(publicAddress = sp.TAddress, tokenAddress = sp.TAddress, amount = sp.TInt, contractType = sp.TNat))
                  )) 
        admin = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")
        publicAddress_A = sp.address("tz1XvMBRHwmXtXS2K6XYZdmcc5kdwB9STFJu")
        publicAddress_B = sp.address("tz1RmD9igqvhQ4FkWw7GMQxxoenvHj6N478g")
        tokenAddress_A = sp.address("KT1QrZze3XztWqZJWWBcfL7WkAkx6nArtZja")
        tokenAddress_B = sp.address("KT1MBdsmrMz2cdUjXvQjbGi6QJVxSARThpSy")
        eventSinkContractAddress = sp.address("KT1RzokejsxyURUBeaGoB9DMeqeWz89YJBjy")
        creationTime = sp.timestamp(1587560389)
        expiryTime = sp.timestamp(1601251200)
        amount_A = 50
        amount_B = 29
        contractType_A = sp.nat(0)
        contractType_B = sp.nat(1)
        
        self.init(admin = admin,
                  eventSinkContractAddress = eventSinkContractAddress,
                  creationTime = creationTime,
                  expiryTime = expiryTime,
                  exchanged = sp.bool(False),
                  partyDetailsMap = sp.map({
                      "party_A": sp.record(publicAddress = publicAddress_A, tokenAddress = tokenAddress_A, amount = amount_A, contractType = contractType_A),
                      "party_B": sp.record(publicAddress = publicAddress_B, tokenAddress = tokenAddress_B, amount = amount_B, contractType = contractType_B)
                  })
                  )


    '''
    Exchange Section 
    '''
    
    def transferFungible(self, from_, to_, amount, tokenAddress):
        
        data = sp.record(
            amount = amount,
            f = from_,
            t = to_,
        )
        
        tokenContractInstance = sp.contract(
            t = sp.TRecord(amount = sp.TInt, f = sp.TAddress, t = sp.TAddress), 
            address = tokenAddress, 
            entry_point = "transfer").open_some()
        
        sp.transfer(data, sp.mutez(0), tokenContractInstance)
    
    def transferNonFungible(self, from_, to_, token_id, tokenAddress):
        
        data = sp.list([sp.record(token_id = token_id, from_ = from_, to_ = to_)])
        
        tokenContractInstance = sp.contract(
            t = sp.TList(sp.TRecord(
                        from_ = sp.TAddress,
                        to_ = sp.TAddress,
                        token_id = sp.TNat
                        )), 
            address = tokenAddress, 
            entry_point = "transfer").open_some()
        
        sp.transfer(data, sp.mutez(0), tokenContractInstance)
    
    def callEventSink(self):
        _amount_A = self.data.partyDetailsMap["party_A"].amount
        _amount_B = self.data.partyDetailsMap["party_B"].amount
    
        _DVPaddress = sp.to_address(sp.self)

        
        exchangeEventRecord = sp.record(amount_A = _amount_A, amount_B = _amount_B, party_A = self.data.partyDetailsMap["party_A"].publicAddress, party_B = self.data.partyDetailsMap["party_B"].publicAddress, DVPaddress = _DVPaddress)
        
        entryPointName = "exchangeEvent"
        
        eventSinkContract = sp.contract(
                        t = sp.TRecord(amount_A = sp.TIntOrNat, amount_B = sp.TIntOrNat, party_A = sp.TAddress, party_B = sp.TAddress, DVPaddress = sp.TAddress), 
                        address = self.data.eventSinkContractAddress, 
                        entry_point = entryPointName
                        ).open_some()
                        
        sp.transfer(exchangeEventRecord, sp.mutez(0), eventSinkContract)
    
    def setExchanged(self):
        self.data.exchanged = sp.bool(True)
    
    def onlyAdmin(self):
        # message = "Only admin allowed"  
        sp.verify((sp.sender == self.data.admin), message = "01")  
    
    def preExchangeChecks(self):
        sp.verify(~ self.data.exchanged, message = "Escrow already exchanged")
        sp.verify(sp.now <= self.data.expiryTime, message = "Escrow already expired")    
        
    
    @sp.entry_point
    def exchange(self, params):
        sp.set_type(params, sp.TUnit)
        
        self.onlyAdmin()
        
        # Checks
        self.preExchangeChecks()
        
        # PartyA
        from_A = self.data.partyDetailsMap["party_A"].publicAddress
        to_A = self.data.partyDetailsMap["party_B"].publicAddress
        amount_A = self.data.partyDetailsMap["party_A"].amount
        tokenAddress_A = self.data.partyDetailsMap["party_A"].tokenAddress
        
        sp.if (self.data.partyDetailsMap["party_A"].contractType == sp.nat(0)):
            self.transferFungible(from_A, to_A, amount_A, tokenAddress_A)
        sp.else sp.if (self.data.partyDetailsMap["party_A"].contractType == sp.nat(1)):
            self.transferNonFungible(from_A, to_A, sp.as_nat(amount_A), tokenAddress_A)
        
        # PartyB
        from_B = self.data.partyDetailsMap["party_B"].publicAddress
        to_B = self.data.partyDetailsMap["party_A"].publicAddress
        amount_B = self.data.partyDetailsMap["party_B"].amount
        tokenAddress_B = self.data.partyDetailsMap["party_B"].tokenAddress
        
        sp.if (self.data.partyDetailsMap["party_B"].contractType == sp.nat(0)):
            self.transferFungible(from_B, to_B, amount_B, tokenAddress_B)
        sp.else sp.if (self.data.partyDetailsMap["party_A"].contractType == sp.nat(1)):
            self.transferNonFungible(from_B, to_B, sp.as_nat(amount_B), tokenAddress_B)
        
        # Set DVP exchanged
        self.setExchanged()
        
        # Fire an eventSinkSC call
        self.callEventSink()
                

class EscrowFactory(sp.Contract):
    def __init__(self, commissionToken, commissionBeneficiary, owner, eventSinkContractAddress):
        
        self.escrow = Escrow()
        
        self.init(
            owner = owner,
            _commissionToken = commissionToken,
            _commissionBeneficiary = commissionBeneficiary,
            eventSinkContractAddress = eventSinkContractAddress,
            _escrowAddressByParty = sp.big_map(tkey = sp.TAddress, tvalue = sp.TSet(t = sp.TAddress)),
            _allEscrowAddresses = sp.set(t = sp.TAddress),
            _creationCost = sp.int(250)
            )
    
    def payCommission(self, creatorAddress):
        data = sp.record(
            amount = self.data._creationCost,
            f = creatorAddress,
            t = self.data._commissionBeneficiary,
        )
        
        tokenContractInstance = sp.contract(
            t = sp.TRecord(amount = sp.TInt, f = sp.TAddress, t = sp.TAddress), 
            address = self.data._commissionToken, 
            entry_point = "transfer").open_some()
        
        sp.transfer(data, sp.mutez(0), tokenContractInstance)        
    
    
    def addInMapIfNecessary(self, publicAddress):
        
        sp.if ~ self.data._escrowAddressByParty.contains(publicAddress):
            self.data._escrowAddressByParty[publicAddress] = sp.set()
    
            
    def addEscrowParties(self, publicAddress, DVPaddress):
        
        self.addInMapIfNecessary(publicAddress)
        
        self.data._escrowAddressByParty[publicAddress].add(DVPaddress)
        
    def addEscrowAddress(self, DVPaddress):    
        
        self.data._allEscrowAddresses.add(DVPaddress)
        
    def createDVPContract(self, params):
        
        # Create Escrow contract
        createdDVPAddress = sp.create_contract(
                storage = sp.record(
                    admin = params.admin, 
                    eventSinkContractAddress = params.eventSinkContractAddress,
                    creationTime = params.creationTime, 
                    expiryTime = params.expiryTime, 
                    exchanged = sp.bool(False),
                    partyDetailsMap = params.partyDetailsMap
                    ), 
                contract = self.escrow
                )
                
        sp.set_type(createdDVPAddress, sp.TAddress)
        
        return createdDVPAddress
        
    @sp.entry_point
    def createDVP(self, params):
        sp.set_type(params.expiryTime, sp.TTimestamp)
        
        # Get current timestamp
        _creationTime = sp.now
        
        DVPFactoryaddress = sp.to_address(sp.self)
        
        # As the DVPFactory is a superTransferAgent, we can transfer from the creator address to our address the commission of DvP creation.
        self.payCommission(params.creatorAddress)
        
        # Create Escrow contract
        createdDVPAddress = self.createDVPContract(sp.record(
                    admin = self.data.owner, 
                    eventSinkContractAddress = self.data.eventSinkContractAddress,
                    creationTime = _creationTime, 
                    expiryTime = params.expiryTime, 
                    partyDetailsMap = sp.map({
                      "party_A": sp.record(publicAddress = params.publicAddress_A, tokenAddress = params.tokenAddress_A, amount = params.amount_A, contractType = params.contractType_A),
                      "party_B": sp.record(publicAddress = params.publicAddress_B, tokenAddress = params.tokenAddress_B, amount = params.amount_B, contractType = params.contractType_B)
                        })
                    ))
        
        # Update _escrowAddressByParty
        self.addEscrowParties(params.publicAddress_A, createdDVPAddress)
        self.addEscrowParties(params.publicAddress_B, createdDVPAddress)
        
        # Update _allEscrowAddresses
        self.addEscrowAddress(createdDVPAddress)
    
    @sp.entry_point
    def setCommissionBeneficiary(self, newCommissionBeneficiary):
        sp.verify((sp.sender == self.data.owner), message = "Only owner can change the commissionBeneficiary.")
        self.data._commissionBeneficiary = newCommissionBeneficiary
    
    @sp.entry_point
    def setCommissionToken(self, newCommissionToken):
        sp.verify((sp.sender == self.data.owner), message = "Only owner can change the commissionToken.")    
        self.data._commissionToken = newCommissionToken
    
    @sp.entry_point
    def setCreationCost(self, newCreationCost):
        sp.verify((sp.sender == self.data.owner), message = "Only owner can change the creation cost.")    
        self.data._creationCost = newCreationCost        
        


@sp.add_test(name = "EscrowFactory")
def test():
    scenario = sp.test_scenario()
    
    commissionToken = sp.address("KT19aZ8zegakCsiHmLZcp1M8gt5jE6Y617HL")
    commissionBeneficiary = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")
    owner = sp.address("tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW")
    eventSinkContractAddress = sp.address("KT1RzokejsxyURUBeaGoB9DMeqeWz89YJBjy")
    
    escrowFactory = EscrowFactory(commissionToken, commissionBeneficiary, owner, eventSinkContractAddress)
    scenario += Escrow()
    scenario += escrowFactory
