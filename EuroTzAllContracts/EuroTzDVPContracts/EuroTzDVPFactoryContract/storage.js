import conf from "../../../conf/conf";

export default {
  prim: "Pair",
  args: [
    {
      prim: "Pair",
      args: [
        [],
        {
          prim: "Pair",
          args: [
            { string: conf.adminAddress },
            { string: conf.contractAddress },
          ],
        },
      ],
    },
    {
      prim: "Pair",
      args: [
        { prim: "Pair", args: [{ int: "250" }, []] },
        {
          prim: "Pair",
          args: [
            { string: conf.eventSinkSCAddress },
            { string: conf.adminAddress },
          ],
        },
      ],
    },
  ],
};
