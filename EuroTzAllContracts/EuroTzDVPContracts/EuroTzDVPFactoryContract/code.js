export default [
  {
    prim: "storage",
    args: [
      {
        prim: "pair",
        args: [
          {
            prim: "pair",
            args: [
              {
                prim: "set",
                args: [{ prim: "address" }],
                annots: ["%_allEscrowAddresses"],
              },
              {
                prim: "pair",
                args: [
                  { prim: "address", annots: ["%_commissionBeneficiary"] },
                  { prim: "address", annots: ["%_commissionToken"] },
                ],
              },
            ],
          },
          {
            prim: "pair",
            args: [
              {
                prim: "pair",
                args: [
                  { prim: "int", annots: ["%_creationCost"] },
                  {
                    prim: "big_map",
                    args: [
                      { prim: "address" },
                      { prim: "set", args: [{ prim: "address" }] },
                    ],
                    annots: ["%_escrowAddressByParty"],
                  },
                ],
              },
              {
                prim: "pair",
                args: [
                  { prim: "address", annots: ["%eventSinkContractAddress"] },
                  { prim: "address", annots: ["%owner"] },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "parameter",
    args: [
      {
        prim: "or",
        args: [
          {
            prim: "or",
            args: [
              {
                prim: "pair",
                args: [
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "pair",
                        args: [
                          { prim: "int", annots: ["%amount_A"] },
                          { prim: "int", annots: ["%amount_B"] },
                        ],
                      },
                      {
                        prim: "pair",
                        args: [
                          { prim: "nat", annots: ["%contractType_A"] },
                          {
                            prim: "pair",
                            args: [
                              { prim: "nat", annots: ["%contractType_B"] },
                              { prim: "address", annots: ["%creatorAddress"] },
                            ],
                          },
                        ],
                      },
                    ],
                  },
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "pair",
                        args: [
                          { prim: "timestamp", annots: ["%expiryTime"] },
                          { prim: "address", annots: ["%publicAddress_A"] },
                        ],
                      },
                      {
                        prim: "pair",
                        args: [
                          { prim: "address", annots: ["%publicAddress_B"] },
                          {
                            prim: "pair",
                            args: [
                              { prim: "address", annots: ["%tokenAddress_A"] },
                              { prim: "address", annots: ["%tokenAddress_B"] },
                            ],
                          },
                        ],
                      },
                    ],
                  },
                ],
                annots: ["%createDVP"],
              },
              { prim: "address", annots: ["%setCommissionBeneficiary"] },
            ],
          },
          {
            prim: "or",
            args: [
              { prim: "address", annots: ["%setCommissionToken"] },
              { prim: "int", annots: ["%setCreationCost"] },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "code",
    args: [
      [
        { prim: "DUP" },
        { prim: "CDR" },
        { prim: "SWAP" },
        { prim: "CAR" },
        {
          prim: "IF_LEFT",
          args: [
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    [
                      { prim: "NIL", args: [{ prim: "operation" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      {
                        prim: "CONTRACT",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "int", annots: ["%amount"] },
                              {
                                prim: "pair",
                                args: [
                                  { prim: "address", annots: ["%f"] },
                                  { prim: "address", annots: ["%t"] },
                                ],
                              },
                            ],
                          },
                        ],
                        annots: ["%transfer"],
                      },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [{ prim: "unit" }, { prim: "Unit" }],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [[]],
                        ],
                      },
                      { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "PAIR", annots: ["%f", "%t"] },
                      { prim: "DIG", args: [{ int: "5" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "6" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%amount"] },
                      { prim: "TRANSFER_TOKENS" },
                      { prim: "CONS" },
                      {
                        prim: "PUSH",
                        args: [
                          {
                            prim: "map",
                            args: [
                              { prim: "string" },
                              {
                                prim: "pair",
                                args: [
                                  {
                                    prim: "pair",
                                    args: [
                                      { prim: "int", annots: ["%amount"] },
                                      {
                                        prim: "nat",
                                        annots: ["%contractType"],
                                      },
                                    ],
                                  },
                                  {
                                    prim: "pair",
                                    args: [
                                      {
                                        prim: "address",
                                        annots: ["%publicAddress"],
                                      },
                                      {
                                        prim: "address",
                                        annots: ["%tokenAddress"],
                                      },
                                    ],
                                  },
                                ],
                              },
                            ],
                          },
                          [],
                        ],
                      },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      {
                        prim: "PAIR",
                        annots: ["%publicAddress", "%tokenAddress"],
                      },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "PAIR", annots: ["%amount", "%contractType"] },
                      { prim: "PAIR" },
                      { prim: "SOME" },
                      {
                        prim: "PUSH",
                        args: [{ prim: "string" }, { string: "party_A" }],
                      },
                      { prim: "UPDATE" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      {
                        prim: "PAIR",
                        annots: ["%publicAddress", "%tokenAddress"],
                      },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "PAIR", annots: ["%amount", "%contractType"] },
                      { prim: "PAIR" },
                      { prim: "SOME" },
                      {
                        prim: "PUSH",
                        args: [{ prim: "string" }, { string: "party_B" }],
                      },
                      { prim: "UPDATE" },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CAR" },
                      {
                        prim: "PAIR",
                        annots: ["%expiryTime", "%partyDetailsMap"],
                      },
                      {
                        prim: "PUSH",
                        args: [{ prim: "bool" }, { prim: "False" }],
                      },
                      { prim: "PAIR", annots: ["%exchanged"] },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "NOW" },
                      {
                        prim: "PAIR",
                        annots: ["%creationTime", "%eventSinkContractAddress"],
                      },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "5" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "PAIR", annots: ["%admin"] },
                      { prim: "PAIR" },
                      { prim: "PUSH", args: [{ prim: "mutez" }, { int: "0" }] },
                      { prim: "NONE", args: [{ prim: "key_hash" }] },
                      {
                        prim: "CREATE_CONTRACT",
                        args: [
                          [
                            { prim: "parameter", args: [{ prim: "unit" }] },
                            {
                              prim: "storage",
                              args: [
                                {
                                  prim: "pair",
                                  args: [
                                    {
                                      prim: "pair",
                                      args: [
                                        { prim: "address", annots: ["%admin"] },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "timestamp",
                                              annots: ["%creationTime"],
                                            },
                                            {
                                              prim: "address",
                                              annots: [
                                                "%eventSinkContractAddress",
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    {
                                      prim: "pair",
                                      args: [
                                        {
                                          prim: "bool",
                                          annots: ["%exchanged"],
                                        },
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "timestamp",
                                              annots: ["%expiryTime"],
                                            },
                                            {
                                              prim: "map",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "int",
                                                          annots: ["%amount"],
                                                        },
                                                        {
                                                          prim: "nat",
                                                          annots: [
                                                            "%contractType",
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "address",
                                                          annots: [
                                                            "%publicAddress",
                                                          ],
                                                        },
                                                        {
                                                          prim: "address",
                                                          annots: [
                                                            "%tokenAddress",
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                              annots: ["%partyDetailsMap"],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                  ],
                                },
                              ],
                            },
                            {
                              prim: "code",
                              args: [
                                [
                                  [
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CAR" },
                                    { prim: "CAR" },
                                    { prim: "SENDER" },
                                    { prim: "COMPARE" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "01" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "Escrow already exchanged",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [[]],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "NOW" },
                                    { prim: "COMPARE" },
                                    { prim: "LE" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "Escrow already expired",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    {
                                      prim: "PUSH",
                                      args: [{ prim: "nat" }, { int: "0" }],
                                    },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "party_A" },
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:150" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CAR" },
                                    { prim: "CDR" },
                                    { prim: "COMPARE" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "NIL",
                                              args: [{ prim: "operation" }],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "3" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_A" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:148",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "CONTRACT",
                                              args: [
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "int",
                                                      annots: ["%amount"],
                                                    },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "address",
                                                          annots: ["%f"],
                                                        },
                                                        {
                                                          prim: "address",
                                                          annots: ["%t"],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                              annots: ["%transfer"],
                                            },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "unit" },
                                                        { prim: "Unit" },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [[]],
                                              ],
                                            },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "mutez" },
                                                { int: "0" },
                                              ],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "4" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_B" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:146",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_A" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:145",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "PAIR",
                                              annots: ["%f", "%t"],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_A" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:147",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CAR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "PAIR",
                                              annots: ["%amount"],
                                            },
                                            { prim: "TRANSFER_TOKENS" },
                                            { prim: "CONS" },
                                          ],
                                        ],
                                        [
                                          [
                                            {
                                              prim: "NIL",
                                              args: [{ prim: "operation" }],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "3" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_A" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:148",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "CONTRACT",
                                              args: [
                                                {
                                                  prim: "list",
                                                  args: [
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "address",
                                                          annots: ["%from_"],
                                                        },
                                                        {
                                                          prim: "pair",
                                                          args: [
                                                            {
                                                              prim: "address",
                                                              annots: ["%to_"],
                                                            },
                                                            {
                                                              prim: "nat",
                                                              annots: [
                                                                "%token_id",
                                                              ],
                                                            },
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                              annots: ["%transfer"],
                                            },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "unit" },
                                                        { prim: "Unit" },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [[]],
                                              ],
                                            },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "mutez" },
                                                { int: "0" },
                                              ],
                                            },
                                            {
                                              prim: "NIL",
                                              args: [
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "address",
                                                      annots: ["%from_"],
                                                    },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "address",
                                                          annots: ["%to_"],
                                                        },
                                                        {
                                                          prim: "nat",
                                                          annots: ["%token_id"],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_A" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:147",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CAR" },
                                            { prim: "CAR" },
                                            { prim: "ISNAT" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "unit" },
                                                        { prim: "Unit" },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [[]],
                                              ],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "7" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_B" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:146",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "PAIR",
                                              annots: ["%to_", "%token_id"],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "7" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_A" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:145",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "PAIR",
                                              annots: ["%from_"],
                                            },
                                            { prim: "CONS" },
                                            { prim: "TRANSFER_TOKENS" },
                                            { prim: "CONS" },
                                          ],
                                        ],
                                      ],
                                    },
                                    {
                                      prim: "PUSH",
                                      args: [{ prim: "nat" }, { int: "0" }],
                                    },
                                    { prim: "DIG", args: [{ int: "3" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "4" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "party_B" },
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:163" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CAR" },
                                    { prim: "CDR" },
                                    { prim: "COMPARE" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "DIG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "3" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_B" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:161",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "CONTRACT",
                                              args: [
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "int",
                                                      annots: ["%amount"],
                                                    },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "address",
                                                          annots: ["%f"],
                                                        },
                                                        {
                                                          prim: "address",
                                                          annots: ["%t"],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                              annots: ["%transfer"],
                                            },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "unit" },
                                                        { prim: "Unit" },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [[]],
                                              ],
                                            },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "mutez" },
                                                { int: "0" },
                                              ],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "4" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_A" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:159",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_B" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:158",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "PAIR",
                                              annots: ["%f", "%t"],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_B" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:160",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CAR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "PAIR",
                                              annots: ["%amount"],
                                            },
                                            { prim: "TRANSFER_TOKENS" },
                                            { prim: "CONS" },
                                          ],
                                        ],
                                        [
                                          [
                                            {
                                              prim: "DIG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "3" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_B" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:161",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "CONTRACT",
                                              args: [
                                                {
                                                  prim: "list",
                                                  args: [
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "address",
                                                          annots: ["%from_"],
                                                        },
                                                        {
                                                          prim: "pair",
                                                          args: [
                                                            {
                                                              prim: "address",
                                                              annots: ["%to_"],
                                                            },
                                                            {
                                                              prim: "nat",
                                                              annots: [
                                                                "%token_id",
                                                              ],
                                                            },
                                                          ],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                              annots: ["%transfer"],
                                            },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "unit" },
                                                        { prim: "Unit" },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [[]],
                                              ],
                                            },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "mutez" },
                                                { int: "0" },
                                              ],
                                            },
                                            {
                                              prim: "NIL",
                                              args: [
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "address",
                                                      annots: ["%from_"],
                                                    },
                                                    {
                                                      prim: "pair",
                                                      args: [
                                                        {
                                                          prim: "address",
                                                          annots: ["%to_"],
                                                        },
                                                        {
                                                          prim: "nat",
                                                          annots: ["%token_id"],
                                                        },
                                                      ],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_B" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:160",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CAR" },
                                            { prim: "CAR" },
                                            { prim: "ISNAT" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "unit" },
                                                        { prim: "Unit" },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [[]],
                                              ],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "7" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_A" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:159",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "PAIR",
                                              annots: ["%to_", "%token_id"],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "6" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "7" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "party_B" },
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:158",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "PAIR",
                                              annots: ["%from_"],
                                            },
                                            { prim: "CONS" },
                                            { prim: "TRANSFER_TOKENS" },
                                            { prim: "CONS" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DROP" },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "bool" },
                                        { prim: "True" },
                                      ],
                                    },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "SWAP" },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CAR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    {
                                      prim: "CONTRACT",
                                      args: [
                                        {
                                          prim: "pair",
                                          args: [
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "address",
                                                  annots: ["%DVPaddress"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%amount_A"],
                                                },
                                              ],
                                            },
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "int",
                                                  annots: ["%amount_B"],
                                                },
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "address",
                                                      annots: ["%party_A"],
                                                    },
                                                    {
                                                      prim: "address",
                                                      annots: ["%party_B"],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                      ],
                                      annots: ["%exchangeEvent"],
                                    },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "unit" },
                                                { prim: "Unit" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [[]],
                                      ],
                                    },
                                    {
                                      prim: "PUSH",
                                      args: [{ prim: "mutez" }, { int: "0" }],
                                    },
                                    { prim: "DIG", args: [{ int: "3" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "4" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "party_B" },
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:109" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "4" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "5" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "party_A" },
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:109" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    {
                                      prim: "PAIR",
                                      annots: ["%party_A", "%party_B"],
                                    },
                                    { prim: "DIG", args: [{ int: "4" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "5" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "party_B" },
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:104" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CAR" },
                                    { prim: "CAR" },
                                    { prim: "PAIR", annots: ["%amount_B"] },
                                    { prim: "DIG", args: [{ int: "4" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "5" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "party_A" },
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:103" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CAR" },
                                    { prim: "CAR" },
                                    { prim: "SELF" },
                                    { prim: "ADDRESS" },
                                    {
                                      prim: "PAIR",
                                      annots: ["%DVPaddress", "%amount_A"],
                                    },
                                    { prim: "PAIR" },
                                    { prim: "TRANSFER_TOKENS" },
                                    { prim: "CONS" },
                                    {
                                      prim: "NIL",
                                      args: [{ prim: "operation" }],
                                    },
                                    { prim: "SWAP" },
                                    {
                                      prim: "ITER",
                                      args: [[{ prim: "CONS" }]],
                                    },
                                    { prim: "PAIR" },
                                  ],
                                ],
                              ],
                            },
                          ],
                        ],
                      },
                      { prim: "PAIR" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CAR" },
                      { prim: "CONS" },
                      { prim: "SWAP" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "MEM" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              {
                                prim: "PUSH",
                                args: [
                                  {
                                    prim: "option",
                                    args: [
                                      {
                                        prim: "set",
                                        args: [{ prim: "address" }],
                                      },
                                    ],
                                  },
                                  { prim: "Some", args: [[]] },
                                ],
                              },
                              { prim: "DIG", args: [{ int: "7" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "8" }] },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "UPDATE" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "DUG", args: [{ int: "3" }] },
                            ],
                          ],
                        ],
                      },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "8" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "set_in_top-nil-some" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      {
                        prim: "PUSH",
                        args: [{ prim: "bool" }, { prim: "True" }],
                      },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "8" }] },
                      { prim: "CDR" },
                      { prim: "UPDATE" },
                      { prim: "SOME" },
                      { prim: "SWAP" },
                      { prim: "UPDATE" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "DUG", args: [{ int: "3" }] },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "MEM" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              {
                                prim: "PUSH",
                                args: [
                                  {
                                    prim: "option",
                                    args: [
                                      {
                                        prim: "set",
                                        args: [{ prim: "address" }],
                                      },
                                    ],
                                  },
                                  { prim: "Some", args: [[]] },
                                ],
                              },
                              { prim: "DIG", args: [{ int: "7" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "8" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "UPDATE" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "DUG", args: [{ int: "3" }] },
                            ],
                          ],
                        ],
                      },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "DUP" },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "GET" },
                      {
                        prim: "IF_NONE",
                        args: [
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  { string: "set_in_top-nil-some" },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                          [],
                        ],
                      },
                      {
                        prim: "PUSH",
                        args: [{ prim: "bool" }, { prim: "True" }],
                      },
                      { prim: "DIG", args: [{ int: "7" }] },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "8" }] },
                      { prim: "CDR" },
                      { prim: "UPDATE" },
                      { prim: "SOME" },
                      { prim: "SWAP" },
                      { prim: "UPDATE" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "DIG", args: [{ int: "2" }] },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      {
                        prim: "PUSH",
                        args: [{ prim: "bool" }, { prim: "True" }],
                      },
                      { prim: "DIG", args: [{ int: "4" }] },
                      { prim: "CDR" },
                      { prim: "UPDATE" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                    ],
                  ],
                  [
                    [
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "DUG", args: [{ int: "2" }] },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "SENDER" },
                      { prim: "COMPARE" },
                      { prim: "EQ" },
                      {
                        prim: "IF",
                        args: [
                          [[]],
                          [
                            [
                              {
                                prim: "PUSH",
                                args: [
                                  { prim: "string" },
                                  {
                                    string:
                                      "Only owner can change the commissionBeneficiary.",
                                  },
                                ],
                              },
                              { prim: "FAILWITH" },
                            ],
                          ],
                        ],
                      },
                      { prim: "SWAP" },
                      { prim: "DUP" },
                      { prim: "CDR" },
                      { prim: "SWAP" },
                      { prim: "CAR" },
                      { prim: "DUP" },
                      { prim: "CAR" },
                      { prim: "SWAP" },
                      { prim: "CDR" },
                      { prim: "CDR" },
                      { prim: "DIG", args: [{ int: "3" }] },
                      { prim: "PAIR" },
                      { prim: "SWAP" },
                      { prim: "PAIR" },
                      { prim: "PAIR" },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                    ],
                  ],
                ],
              },
            ],
            [
              [
                {
                  prim: "IF_LEFT",
                  args: [
                    [
                      [
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "SENDER" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    {
                                      string:
                                        "Only owner can change the commissionToken.",
                                    },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "CDR" },
                        { prim: "SWAP" },
                        { prim: "CAR" },
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "CDR" },
                        { prim: "CAR" },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                        { prim: "PAIR" },
                      ],
                    ],
                    [
                      [
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "DUG", args: [{ int: "2" }] },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "CDR" },
                        { prim: "SENDER" },
                        { prim: "COMPARE" },
                        { prim: "EQ" },
                        {
                          prim: "IF",
                          args: [
                            [[]],
                            [
                              [
                                {
                                  prim: "PUSH",
                                  args: [
                                    { prim: "string" },
                                    {
                                      string:
                                        "Only owner can change the creation cost.",
                                    },
                                  ],
                                },
                                { prim: "FAILWITH" },
                              ],
                            ],
                          ],
                        },
                        { prim: "SWAP" },
                        { prim: "DUP" },
                        { prim: "CAR" },
                        { prim: "SWAP" },
                        { prim: "CDR" },
                        { prim: "DUP" },
                        { prim: "CDR" },
                        { prim: "SWAP" },
                        { prim: "CAR" },
                        { prim: "CDR" },
                        { prim: "DIG", args: [{ int: "3" }] },
                        { prim: "PAIR" },
                        { prim: "PAIR" },
                        { prim: "SWAP" },
                        { prim: "PAIR" },
                      ],
                    ],
                  ],
                },
                { prim: "NIL", args: [{ prim: "operation" }] },
              ],
            ],
          ],
        },
        { prim: "NIL", args: [{ prim: "operation" }] },
        { prim: "SWAP" },
        { prim: "ITER", args: [[{ prim: "CONS" }]] },
        { prim: "PAIR" },
      ],
    ],
  },
];
