export default {
  // Admin credentials
  adminAddress: "tz1djN1zPWUYpanMS1YhKJ2EmFSYs6qjf4bW",
  adminPublicKey: "edpkuMKbhGnBNmk3MyACiVi8SqsaP1N1H6NoeB8md2vgRuhXFxqALT",
  adminSecretKeyUnencrypted:
    "edsk4UEK9MNEXE3DGz2tqYAFFZg1JuYnWkFefdnUDdh1k3kZWxaSjR",
  adminSecretKey:
    "edskS3eyP77mJHL9tpLYRDCVfbkutRyc24Ja5Xaw2UffqqeGRBg5oEfYS9kpHWNGgarGJb2wbunCrZismFhvJe9ceEMeVNCZu6",

  // Alice credentials:
  aliceAddress: "tz1SVqTz7entj982jDSKcTQNgT7f2cg7C8dk",
  alicePublicKey: "edpkudkbM1JPwxchDKZt5J7FjVP2K8G2q8fEforMZX8osvbkJeGRD1",
  aliceSecretKeyUnencrypted:
    "edsk34kAz18pFQBtDLiU41v6Sm4rGvZ3bnKsG5koj4TWjC59TQ4svj",
  aliceSecretKey:
    "edskRioMPrRYbibwR7EqrokJtq3f8a66LeUvm1Sxya3pTgvWz7pTdghfNAvahAHNf3iox3j7P9nkYrKeCiaHVtiMu7yZraNYRe",

  // Make Alice as Admin
  // adminAddress: "tz1SVqTz7entj982jDSKcTQNgT7f2cg7C8dk",
  // adminPublicKey: "edpkudkbM1JPwxchDKZt5J7FjVP2K8G2q8fEforMZX8osvbkJeGRD1",
  // adminSecretKeyUnencrypted:
  //   "edsk34kAz18pFQBtDLiU41v6Sm4rGvZ3bnKsG5koj4TWjC59TQ4svj",
  // adminSecretKey:
  //   "edskRioMPrRYbibwR7EqrokJtq3f8a66LeUvm1Sxya3pTgvWz7pTdghfNAvahAHNf3iox3j7P9nkYrKeCiaHVtiMu7yZraNYRe",

  // Fred credentials
  fredAddress: "tz1XrCvviH8CqoHMSKpKuznLArEa1yR9U7ep",
  fredSecretKey: "edsk3bpNdqNfX5aPiFZJvszyAKSYrj1cuWzX5VLUaTfpYx39h8aVmq",
  fredPublicKey: "edpkvNYwBeVWcrn3SS55mUyi8qBiTZdQbzDgkRpW65xLG2W2c7aA6u",
  fredPrivateKey:
    "edskRt2ibNbv4BNrn7FRXuYTwGF54oVAe8JuUuE8YVsvnc78GwpRvYk9ftmQKxQjeczGZwacGSdHWWXrLDWcYoq3EhiY5TuUtU",

  // Ouss credentials
  oussAddress: "tz1XvMBRHwmXtXS2K6XYZdmcc5kdwB9STFJu",
  oussSecretKey: "edsk2wW5nyKq7VEwXJntkiGkUb52nP92ukYDWNTRNWXZzJrsZheqHD",
  oussPrivateKey:
    "edskRgeZRQ7YtDGzz9qesBeRBhQUXmeNDzmXDGyfzFNBHbvnKEx6ofQUrg3aVeuMS2afNh3zNjZpVT2HBYTXfZD7zr5ye6ZB5Z",
  oussPublicKey: "edpkud9ThGW8zBxucFnssPusUQZmijuSpAdL2xekQS6wRuTi1TdvYq",

  // Khaled credentials:
  khaledAddress: "tz1RmD9igqvhQ4FkWw7GMQxxoenvHj6N478g",
  khaledSecretKey: "edsk3SuUN2GstegvesSimN5NzkxoCtUyFr3yfdBkqpXg4FyNpckfXd",
  khaledPrivateKey:
    "edskRqPAbJTG4fpHqgWdJHLVSTGaTcmYDwj1XYC9KWcqZbuc6VvtBGvvRQ76ApEmLYD53ixHopM1kw4PufJoreMcpui5QBjJnq",
  khaledPublicKey: "edpku16FzRvRSdqMHFLmR1SimEaLUrvishG8Yuu9sFgLdCEPiJdPvP",

  // Carthagenet

  /** ===================== EuroTz Prod addresses ================ */
  euroTzProdAddress: "KT1GVGz2YwuscuN1MEtocf45Su4xomQj1K8z",
  proxyProdAddress: "KT1DzuutCr2Mqt2KXhyBkcZJBN2ZJsA7AST6",
  DVPFactoryProdAddress: "KT1CCiZeqm9UrSyGf2JC7Jg164Q72Bc8d6qm",
  bondProdAddress: "KT1MAJqEd1y1EbHrt1wpcf1n8k6SjMCFR3dZ",
  /** ===================== EuroTz Prod addresses ================ */

  /** ===================== EuroTz Dev addresses ================ */
  contractAddress: "KT1QrMNdrFEwRdiT4ZeBNHoE6bN7VUMp7Qqb", // Version 2
  proxyContractAddress: "KT1U9f17hSrBdRmybFT7cS3PNQSAD1pTDZ3W", // Version 2

  contractAddressV1: "KT1Dti319yejwnYHbquKgDZuMZ7WhnExzgcu", // Version 1

  // contractWithChain verification in feeless entryPoints.
  // contractAddressV1: "KT1M3nce5dWTej6GtEUrVfcRAzeBauLJZhGx",

  /** DVP ISSUES */

  euroTzContractA: "KT1QrMNdrFEwRdiT4ZeBNHoE6bN7VUMp7Qqb",
  euroTzContractB: "KT1VZpcPw1aZJdmjDennMewGUW6jYZGHAiFm",

  DVPFactoryAddress: "KT1Mb2WdsPv6JC2hjcgUpVALE2eFtgZPPAiX",

  bondContract: "KT1ViynBLXYcrZe1JEjAc4xANhJbx1GUKfDN",

  /** ===================== EuroTz Dev addresses ================ */

  eventSinkSCAddress: "KT1RzokejsxyURUBeaGoB9DMeqeWz89YJBjy",

  /** */

  superTransferAgent: {
    sk:
      "edskRqJQxWz6RZ9iMmC7DyJYKGmYPAcBP5jbA3mD8u8rv7wAC2x5k38ALrWjqUscjMtYkWegu2d1Bimdyo6bLsHPWe5jAjixw5",
    pk: "edpkvApWgC1r1x51cYUMGkjx9xjh24acqhx1bEwWS4HDyVYPEaFTZx",
    pkh: "tz1Nf7CWDwhRRBdN5p5z9wEAUuSM1afCNdqb",
    skh: "edsk3SdUViBQWRGXY9XmFdxsiXPt4K8K9iH379D52b2fDFBKXDs84r",
  },

  transferAgent: {
    sk:
      "edskSB4GYahaSKcYSVAC7K2w7Ju9hzAP1TeeRZSTTHmLwV16UwsiwB5xTRR8BzBn2Vste9jEDGo4n8hFhLoKce1Rqj8gdMKWda",
    pk: "edpkuui9zUPL5WjTcup9fz6MvfmBx9xTVT3oVw3VEWyxxPVfw9Bc38",
    pkh: "tz1e2VkaXdHXX7RrJUBNHztGHWnrAWMjsUp1",
    skh: "edsk4b8mQi5iQQND49L9b7pb7XA6tptbzeBRbFtdLBYxzViNtB4zD9",
  },

  unknownAccount: {
    sk:
      "edskRj834H7r8xqHcYFimxReFdSfhsb2eGxJAUuvvb8jnfpP2j7bKTDRaYVyfGRsmkW8RjChhQSYx1EMSg4CiAzhtgHwF6HdHE",
    pk: "edpkuzjbodMJ3tSKZGdhAgDrtiPurVUg4XfdPhxFKc45oZDHu8bpCi",
    pkh: "tz1SaUEpoM8ME55RdMbNifypEqETodYKyNBD",
    skh: "edsk35q4v8RMwD4cfpqx3AvNKRi4dipmBEiv3rYJNabYxuycEYxLdK",
  },

  safwenKeys: {
    privateKey:
      "edskRgzvgkj2V3Gw2yX6AwUJq4neLtBPyShAFatWvJeQq2q5Q7PuVcDZMxTLnVvsZKzXL1DYFcSFuqByvWcsrKFgRD2UBZvZW4",
    publicKey: "edpktrXQj4LvucYe7Z3i8zBbC9vvnodoF32N6dcB2wsoyz4tAiE3Ji",
    publicAddress: "tz1LTLvbWJ85Lsq6zkgcSgisNFfNu7mRNEvg",
  },

  /** */

  // RPC config

  localNodeRPC: "http://localhost:8732",
  remoteNodeRPC: "https://carthagenet.SmartPy.io",
  // Alternatives...
  // remoteNodeRPC: "http://carthagenet.tezos.cryptium.ch:8732",
  // remoteNodeRPC: "https://testnet.tezster.tech",
  // remoteNodeRPC: "https://testnet-tezos.giganode.io"
  // remoteNodeRPC: "http://carthagenet.mohamedali.uno",
  // remoteNodeRPC: "https://tezos-dev.cryptonomic-infra.tech"
  // remoteNodeRPC: "https://tezos-dev.cryptonomic-infra.tech:443"
};
