import { Tezos, TezosOperationError } from "@taquito/taquito";

import conf from "../conf/conf";


import euroTzStorageJSON from "../EuroTzAllContracts/EuroTzContractV1ChainID/euroTzStorage"
import euroTzCodeJSON from "../EuroTzAllContracts/EuroTzContractV1ChainID/euroTzCode"



Tezos.setProvider({ rpc: conf.remoteNodeRPC });

Tezos.importKey(conf.adminSecretKey);



export async function originateTkZosContract() {
    try {
        console.log("Begin euroTz origination operation...")

        const originationOp = await Tezos.contract.originate({
            code: euroTzCodeJSON,
            init: euroTzStorageJSON
        })

        const contractOriginationOp = await originationOp.contract()

        console.log("Your new EuroTz (with chainID verif) contractAddress :", contractOriginationOp.address)

        console.log("End euroTz origination operation...")
        return contractOriginationOp.address
    } catch (e) {
        if (e instanceof TezosOperationError) {
            console.log("MESSAGE: ", e.errors);
        } else {
            console.log("PublicNode Error ", e)
        }
    }
}

originateTkZosContract()

