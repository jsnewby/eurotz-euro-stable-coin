import { Tezos, TezosOperationError } from "@taquito/taquito";

import conf from "../conf/conf";

import eventSinkStorageJSON from "../EuroTzAllContracts/EventSinkContract/storage"
import eventSinkCodeJSON from "../EuroTzAllContracts/EventSinkContract/code"


Tezos.setProvider({ rpc: conf.remoteNodeRPC });

Tezos.importKey(conf.adminSecretKey);

export async function originateEventSinkContract() {
    try {
        console.log("Begin eventSink origination operation...")

        const originationOp = await Tezos.contract.originate({
            code: eventSinkCodeJSON,
            init: eventSinkStorageJSON
        })

        const contractOriginationOp = await originationOp.contract()

        console.log("Your new EventSink contractAddress :", contractOriginationOp.address)

        console.log("End eventSink origination operation...")
        return contractOriginationOp.address
    } catch (e) {
        if (e instanceof TezosOperationError) {
            console.log("MESSAGE: ", e.errors);
        } else {
            console.log("PublicNode Error ", e)
        }
    }
}

originateEventSinkContract()

