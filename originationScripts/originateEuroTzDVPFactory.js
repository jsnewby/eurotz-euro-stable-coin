import { Tezos, TezosOperationError } from "@taquito/taquito";

import conf from "../conf/conf";

import euroTzCodeJSON from "../EuroTzAllContracts/EuroTzDVPContracts/EuroTzDVPFactoryContract/code";
import euroTzStorageJSON from "../EuroTzAllContracts/EuroTzDVPContracts/EuroTzDVPFactoryContract/storage";

const util = require("util");

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

Tezos.importKey(conf.adminSecretKey);

export async function originateDVPContract() {
  try {
    console.log("Begin EuroTzDVP Factory origination operation...");

    const originationOp = await Tezos.contract.originate({
      code: euroTzCodeJSON,
      init: euroTzStorageJSON,
    });

    const contractOriginationOp = await originationOp.contract();

    console.log(
      "Your new EuroTzDVP Factory contractAddress :",
      contractOriginationOp.address
    );

    console.log("End EuroTzDVP Factory origination operation...");
    return contractOriginationOp.address;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      // console.log("MESSAGE: ", e.errors);
      console.log(
        util.inspect(e.errors, false, null, true /* enable colors */)
      );
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

originateDVPContract();
