import { Tezos, TezosOperationError } from "@taquito/taquito";

import conf from "../conf/conf";

import euroTzCodeJSON from "../EuroTzAllContracts/EuroTzBondContract/code";
import euroTzStorageJSON from "../EuroTzAllContracts/EuroTzBondContract/storage";

const util = require("util");

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

Tezos.importKey(conf.adminSecretKey);

export async function originateBondContract() {
  try {
    console.log("Begin EuroTzBond origination operation...");

    const originationOp = await Tezos.contract.originate({
      code: euroTzCodeJSON,
      init: euroTzStorageJSON,
    });

    const contractOriginationOp = await originationOp.contract();

    console.log(
      "Your new EuroTzBond contractAddress :",
      contractOriginationOp.address
    );

    console.log("End EuroTzBond Factory origination operation...");
    return contractOriginationOp.address;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      // console.log("MESSAGE: ", e.errors);
      console.log(
        util.inspect(e.errors, false, null, true /* enable colors */)
      );
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

originateBondContract();
