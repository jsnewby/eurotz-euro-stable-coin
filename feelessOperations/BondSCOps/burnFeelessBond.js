import { Tezos } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";

import { signOperation } from "../../Utils/signOperation";

import { packDataBurnFeeless } from "../../Utils/packDataBond";

import conf from "../../conf/conf";

Tezos.setProvider({
  rpc: conf.remoteNodeRPC,
});

Tezos.importKey(conf.adminSecretKey);

export async function burnFeelessBond(
  token_id,
  from_,
  userPublicKey,
  userPrivateKey,
  contractAddress
) {
  try {
    const euroTzContract = await Tezos.contract.at(contractAddress);

    const euroTzContractStorage = await euroTzContract.storage();
    let userDetails;
    let userNonce;

    userDetails = await euroTzContractStorage.ledger.get(from_);
    userNonce = new BigNumber(userDetails.nonce).toNumber();

    // Pack transfer operation
    const opBytes = await packDataBurnFeeless(
      `${token_id}`,
      `${userNonce}`,
      from_,
      contractAddress
    );

    // Sign operation bytes
    const opSign = await signOperation(opBytes, userPrivateKey);

    const tezosErc20Approve = await euroTzContract.methods
      .burnFeeless(opBytes, userPublicKey, opSign.edsig)
      .send();

    console.log(
      "Waiting for the transaction to be included..."
    );

    await tezosErc20Approve.confirmation();

    console.log("tezosErc20Approve.hash: ", tezosErc20Approve.hash);
    return tezosErc20Approve.hash;
  } catch (e) {
    console.log(e.errors);
    // throw e;
  }
}

burnFeelessBond(
  0,
  conf.oussAddress,
  conf.oussPublicKey,
  conf.oussPrivateKey,
  conf.bondContract
); 
