import { Tezos } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";

import { signOperation } from "../../Utils/signOperation";

import { packDataApproveFeeless } from "../../Utils/packDataBond";

import conf from "../../conf/conf";

Tezos.setProvider({
  rpc: conf.remoteNodeRPC,
});

// Alice is the admin in the ProdBond contract.
Tezos.importKey(conf.aliceSecretKey);

export async function approveFeelessBond(
  ownerAccount,
  ownerPublicKey,
  ownerSecretKey,
  spenderAccount,
  contractAddress
) {
  try {
    const euroTzContract = await Tezos.contract.at(contractAddress);

    const euroTzContractStorage = await euroTzContract.storage();
    let ownerDetails;
    let ownerNonce;

    try {
      ownerDetails = await euroTzContractStorage.ledger.get(ownerAccount);
      ownerNonce = new BigNumber(ownerDetails.nonce).toNumber();
    } catch (e) {
      ownerNonce = null;
    }

    // Pack transfer operation
    const opBytes = await packDataApproveFeeless(
      ownerNonce === null ? null : `${ownerNonce}`,
      ownerAccount,
      spenderAccount,
      contractAddress
    );

    // Sign operation bytes
    const opSign = await signOperation(opBytes, ownerSecretKey);

    const tezosErc20Approve = await euroTzContract.methods
      .approveFeeless(opBytes, ownerPublicKey, opSign.edsig)
      .send();

    console.log("Waiting for the transaction to be included...");

    await tezosErc20Approve.confirmation();

    console.log("Trasnaction hash: ", tezosErc20Approve.hash);

    return tezosErc20Approve.hash;
  } catch (e) {
    throw e;
  }
}

approveFeelessBond(
  conf.safwenKeys.publicAddress, // ownerAccount tz1..,
  conf.safwenKeys.publicKey, // ownerPublicKey edpk...,
  conf.safwenKeys.privateKey, // ownerSecretKey edsk...,
  conf.fredAddress, // spenderAccount KT1 or tz1,
  conf.bondProdAddress //   contractAddress
);
