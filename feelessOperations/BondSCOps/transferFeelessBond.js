import { Tezos } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";

import { signOperation } from "../../Utils/signOperation";

import { packDataTrasnferFeeless } from "../../Utils/packDataBond";

import conf from "../../conf/conf";

Tezos.setProvider({
  rpc: conf.remoteNodeRPC,
});

Tezos.importKey(conf.adminSecretKey);

export async function transferFeelessBond(
  token_id,
  from_,
  to_,
  userPublicKey,
  userPrivateKey,
  contractAddress
) {
  try {
    const euroTzContract = await Tezos.contract.at(contractAddress);

    const euroTzContractStorage = await euroTzContract.storage();
    let userDetails;
    let userNonce;

    try {
      userDetails = await euroTzContractStorage.ledger.get(from_);
      userNonce = new BigNumber(userDetails.nonce).toNumber();
    } catch (e) {
      userNonce = null;
    }

    // Pack transfer operation
    const opBytes = await packDataTrasnferFeeless(
      `${token_id}`,
      `${userNonce}`,
      from_,
      to_,
      contractAddress
    );

    // Sign operation bytes
    const opSign = await signOperation(opBytes, userPrivateKey);

    const tezosErc20Approve = await euroTzContract.methods
      .transferFeeless(opBytes, userPublicKey, opSign.edsig)
      .send();

    console.log("Waiting for the transaction to be included...");

    await tezosErc20Approve.confirmation();

    return tezosErc20Approve.hash;
  } catch (e) {
    console.log(e.errors);
    // throw e;
  }
}

transferFeelessBond(
  3, // token_id,
  conf.fredAddress, // from_,
  conf.khaledAddress, // to_
  conf.fredPublicKey, //   userPublicKey,
  conf.fredPrivateKey, //   userPrivateKey,
  conf.bondContract //   contractAddress
);
