import { Tezos } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";

import { signOperation } from "../../Utils/signOperation";

import { packDataMintFeeless } from "../../Utils/packDataBond";

import conf from "../../conf/conf";

Tezos.setProvider({
  rpc: conf.remoteNodeRPC,
});

Tezos.importKey(conf.adminSecretKey);

export async function mintFeelessBond(
  period,
  amount,
  to_,
  userPublicKey,
  userPrivateKey,
  contractAddress
) {
  try {
    const euroTzContract = await Tezos.contract.at(contractAddress);

    const euroTzContractStorage = await euroTzContract.storage();
    let userDetails;
    let userNonce;

    try {
      userDetails = await euroTzContractStorage.ledger.get(to_);
      userNonce = new BigNumber(userDetails.nonce).toNumber();
    } catch (e) {
      userNonce = null;
    }

    // Pack transfer operation
    const opBytes = await packDataMintFeeless(
      `${period}`,
      userNonce === null ? null : `${userNonce}`,
      `${amount}`,
      to_,
      contractAddress
    );

    // Sign operation bytes
    const opSign = await signOperation(opBytes, userPrivateKey);

    const tezosErc20Approve = await euroTzContract.methods
      .mintFeeless(opBytes, userPublicKey, opSign.edsig)
      .send();

    console.log("Waiting for the transaction to be included...");

    await tezosErc20Approve.confirmation();

    return tezosErc20Approve.hash;
  } catch (e) {
    console.log(e.errors);
    throw e;
  }
}

async function performMintFeelessBond() {
  try {
    const args = process.argv.slice(2);

    if (args.length != 6) {
      console.log(
        "You must pass 6 parameters as following: period, amount, creditorAddress, creditorPublicKey, creditorPrivateKey, bondContractAddress"
      );
      return;
    } else {
      const trxHash = await mintFeelessBond(
        args[0], // 24 period,
        args[1], // 14450 amount,
        args[2], // tz1... to_,
        args[3], // edpk... userPublicKey,
        args[4], // edsk...   userPrivateKey,
        args[5] // KT1... contractAddress
      );

      console.log("Transaction hash: ", trxHash);
    }
  } catch (e) {
    console.log(e);
  }
}

performMintFeelessBond();
