import { Tezos } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";

import { signOperation } from "../Utils/signOperation";

import { packData } from "../Utils/packData";

import conf from "../conf/conf";

Tezos.setProvider({
  rpc: conf.remoteNodeRPC,
});

Tezos.importKey(conf.adminSecretKey);

export async function increaseAllowanceFeeless(
  ownerAccount,
  ownerPublicKey,
  ownerSecretKey,
  spenderAccount,
  amount,
  contractAddress
) {
  try {
    const euroTzContract = await Tezos.contract.at(contractAddress);

    const euroTzContractStorage = await euroTzContract.storage();
    let ownerDetails;
    let ownerNonce;

    try {
      ownerDetails = await euroTzContractStorage.nonces.get(ownerAccount);
      ownerNonce = new BigNumber(ownerDetails).toNumber();
    } catch (e) {
      ownerNonce = 0;
    }

    // Pack transfer operation
    const opBytes = await packData(
      `${amount}`,
      `${ownerNonce}`,
      ownerAccount,
      spenderAccount,
      contractAddress
    );

    // Sign operation bytes
    const opSign = await signOperation(opBytes, ownerSecretKey);

    const tezosErc20Approve = await euroTzContract.methods
      .increaseAllowanceFeeless(opBytes, ownerPublicKey, opSign.edsig)
      .send();

    console.log(
      "Waiting for the transaction to be included..."
    );

    await tezosErc20Approve.confirmation();

    return tezosErc20Approve.hash;
  } catch (e) {
    throw e;
  }
}
