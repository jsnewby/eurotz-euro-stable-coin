import { Tezos } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";

import { signOperation } from "../Utils/signOperation";

import { packData } from "../Utils/packData";

import conf from "../conf/conf";

Tezos.setProvider({
  rpc: conf.remoteNodeRPC,
});

Tezos.importKey(conf.adminSecretKey);

export async function transferFeeless(
  senderAccount,
  senderPublicKey,
  secretKey,
  receiverAccount,
  amount,
  contractAddress,
  // for test purpose: in case we want to introduce an invalid nonce
  nonce
) {
  try {
    const euroTzContract = await Tezos.contract.at(contractAddress);

    const euroTzContractStorage = await euroTzContract.storage();
    let senderDetails;
    let senderNonce;

    if (nonce) {
      senderNonce = nonce;
    } else {
      try {
        senderDetails = await euroTzContractStorage.nonces.get(senderAccount);
        senderNonce = new BigNumber(senderDetails).toNumber();
      } catch (e) {
        senderNonce = 0;
      }
    }

    // Pack transfer operation
    const opBytes = await packData(
      `${amount}`,
      `${senderNonce}`,
      senderAccount,
      receiverAccount,
      contractAddress
    );

    // Sign operation bytes
    const opSign = await signOperation(opBytes, secretKey);

    const tezosErc20Transfer = await euroTzContract.methods
      .transferFeeless(opBytes, senderPublicKey, opSign.edsig)
      .send();

    console.log(
      "Waiting for the transaction to be included..."
    );

    await tezosErc20Transfer.confirmation();

    return tezosErc20Transfer.hash;
  } catch (e) {
    throw e;
  }
}
