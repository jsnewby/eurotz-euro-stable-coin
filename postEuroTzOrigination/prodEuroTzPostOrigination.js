import { Tezos, TezosOperationError } from "@taquito/taquito";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

// Alice is the admin of the Prod contract...
Tezos.importKey(conf.aliceSecretKey);

async function mintEuroTz(creditorAddress, amount) {
  try {
    const euroTzContract = await Tezos.contract.at(conf.euroTzProdAddress);

    const tezosMint = await euroTzContract.methods
      .mint(creditorAddress, amount)
      .send();

    await tezosMint.confirmation();

    const tezosMintHash = tezosMint.hash;

    console.log("tezosMintHash : ", tezosMintHash);
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log("MESSAGE: ", e.errors[1].with.string);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

async function addSuperTransferAgents() {
  try {
    const proxyContract = await Tezos.contract.at(conf.proxyProdAddress);

    const addTransferAgentOperation = await proxyContract.methods
      .run(conf.DVPFactoryProdAddress, "callAddSuperTransferAgent")
      .send();

    await addTransferAgentOperation.confirmation();

    const addTransferAgentOperationHash = addTransferAgentOperation.hash;

    console.log(
      "addTransferAgentOperationHash : ",
      addTransferAgentOperationHash
    );

    // ============================================================

    const addBondAsAgentOperation = await proxyContract.methods
      .run(conf.bondProdAddress, "callAddSuperTransferAgent")
      .send();

    await addBondAsAgentOperation.confirmation();

    const addBondAsAgentOperationHash = addBondAsAgentOperation.hash;

    console.log("addBondAsAgentOperationHash : ", addBondAsAgentOperationHash);

    // ============================================================

    const upgrade = await proxyContract.methods
      .upgrade(conf.euroTzProdAddress)
      .send();

    await upgrade.confirmation();

    console.log("Success upgrade ", upgrade.hash);
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log("MESSAGE: ", e.errors[1].with.string);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

async function postOriginationEuroTz() {
  console.log("============== BEGIN ==============");
  await mintEuroTz("tz1LTLvbWJ85Lsq6zkgcSgisNFfNu7mRNEvg", 50000000);
  await mintEuroTz("tz1MwD9RhFGD8DN5PmBg3aKJPcVr5AoF8LmX", 50000000);
  await mintEuroTz(conf.aliceAddress, 50000000);
  await addSuperTransferAgents();
  console.log("============== END ================");
}

postOriginationEuroTz();
