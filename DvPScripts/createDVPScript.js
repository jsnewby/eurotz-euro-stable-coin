import { Tezos } from "@taquito/taquito";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

Tezos.importKey(conf.adminSecretKey);

async function DVPCreationScript() {
  try {
    const euroTzContract = await Tezos.contract.at(conf.DVPFactoryAddress);
    // const euroTzContract = await Tezos.contract.at(conf.DVPFactoryProdAddress);

    /**
    amount_A int
    amount_B int
    contractType_A nat
    contractType_B nat
    creatorAddress address
    expiryTime timestamp
    publicAddress_A address
    publicAddress_B address
    tokenAddress_A address
    tokenAddress_B address
     *  */

    const setContract = await euroTzContract.methods
      .createDVP(
        "13500", // amount in EuroTz
        "0", // token_id
        "0", // contractType for Ouss
        "1", // contractType for Fred
        conf.oussAddress,
        "1601251200",
        conf.oussAddress,
        conf.fredAddress,
        conf.euroTzContractA,
        conf.bondContract
        // conf.euroTzProdAddress,
        // conf.bondProdAddress
      )
      .send();

    await setContract.confirmation();

    const setContractHash = setContract.hash;

    console.log("Done successfully: ", setContractHash);
  } catch (e) {
    console.log(e);
  }
}

DVPCreationScript();
