import { Tezos, TezosOperationError } from "@taquito/taquito";

import { approveFeeless } from "../feelessOperations/approveFeeless";

import { approveFeelessBond } from "../feelessOperations/BondSCOps/approveFeelessBond";

import conf from "../conf/conf";
import errors from "../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

async function exchange() {
  try {
    /** ================================================================ */
    const DVPAddress = "Put the DvP address here...";
    const tokenA = conf.contractAddress;
    const tokenB = conf.bondContract;
    // const tokenA = conf.euroTzProdAddress;
    // const tokenB = conf.bondProdAddress;
    /** ================================================================ */

    /** ================================================================ */
    Tezos.importKey(conf.adminSecretKey);
    const tezosApproveHashA = await approveFeeless(
      conf.oussAddress,
      conf.oussPublicKey,
      conf.oussPrivateKey,
      DVPAddress,
      13500,
      tokenA
    );

    console.log("tezosApproveHashA : ", tezosApproveHashA);
    console.log(
      "/** ================================================================ */"
    );
    /** ================================================================ */

    /** ================================================================ */

    // call approve feeless in BondContract
    Tezos.importKey(conf.adminSecretKey);
    const tezosApproveHashB = await approveFeelessBond(
      conf.fredAddress,
      conf.fredPublicKey,
      conf.fredPrivateKey,
      DVPAddress,
      tokenB
    );

    console.log("tezosApproveHashB : ", tezosApproveHashB);
    console.log(
      "/** ================================================================ */"
    );
    /** ================================================================ */

    /** ================================================================ */
    Tezos.importKey(conf.adminSecretKey);
    const euroTzDVPContract = await Tezos.contract.at(DVPAddress);
    const tezosExchange = await euroTzDVPContract.methods.main("unit").send();

    await tezosExchange.confirmation();

    const tezosExchangeHash = tezosExchange.hash;

    console.log("tezosExchangeHash : ", tezosExchangeHash);
    console.log(
      "/** ================================================================ */"
    );
    /** ================================================================ */
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log("MESSAGE CODE: ", e.errors[1].with.string);
      console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

exchange();
