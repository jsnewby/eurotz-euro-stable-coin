# EuroTz Smart Contracts

##### NOTE: Doc not finished yet...

#### This repository contains EUR.TZ Smart Contracts: Fungible token SC and the eventSink SC (details later) written in [SmartPy](https://smartpy.io/) which is a Python library for constructing Tezos SC and compiled to Michelson code. The contract was inspired by the [FA1.2 standard](https://assets.tqtezos.com/token-contracts/1-fa12-lorentz).

#### Get the Project:

1. Clone the project
2. `cd eurotz-euro-stable-coin`
3. Install the packages with `yarn install`

#### Compile SmartPy to Michelson:

1. Install the SmartPy command line interface:

for the official stable version

    sh <(curl -s https://SmartPy.io/SmartPyBasic/SmartPy.sh) local-install ~

for the dev version (not as stable as the stable version but reasonably stable, new incremental features are here)

    sh <(curl -s https://SmartPy.io/SmartPyBasicDev/SmartPy.sh) local-install-dev ~

2. Executing a SmartPy Script
```
~/SmartPyBasic/SmartPy.sh run <SmartContractCode.py>
```
#### EntryPoints  (SC methods):

|EntryPoint| Params  | Permission
|--|--|--|
| addSuperTransferAgent | address | Admin
| deleteSuperTransferAgent | address | Admin
| addTransferAgent | address | Admin / SuperTransferAgent
| deleteTransferAgent | address | Admin / SuperTransferAgent
| approve | amount: int / f: address / t: address    | Owner / Admin / SuperTransferAgent
| approveFeeless | amount: int / b: bytes  / f: address  / k: key  nonce: nat / s: signature / t: address | Admin / TransferAgent
| burn | address: address / amount: int | Admin
| changeEventSinkContractAddress | address | Admin
| decreaseAllowance | amount: int / f: address / t: address | Owner / Admin / SuperTransferAgent
| decreaseAllowanceFeeless | amount: int / b: bytes  / f: address  / k: key  nonce: nat / s: signature / t: address | Admin / TransferAgent
| increaseAllowance | amount: int / f: address / t: address    | Owner / Admin / SuperTransferAgent
| increaseAllowanceFeeless | amount: int / b: bytes  / f: address  / k: key  nonce: nat / s: signature / t: address | Admin / TransferAgent
| mint | address: address / amount: int | Admin
| resetAllAllowances | address | Owner / Admin / SuperTransferAgent
| resetAllowance | address: address /  f: address | Owner / Admin / SuperTransferAgent
| setAdministrator | address | Admin
| setPause | bool | Admin
| transfer | amount: int / f: address / t: address    | Owner / Admin / SuperTransferAgent
| transferFeeless | amount: int / b: bytes  / f: address  / k: key  nonce: nat / s: signature / t: address | Admin / TransferAgent



#### TEST: The smartContract tests are written in smartPy and in JS (with Taquito, Mocha and Chai)
>  To Run SC *entry points* SmartPy tests: Executing a SmartPy Script with its tests:

    ~/SmartPyBasic/SmartPy.sh test <SmartContractCode.py> <output-directory>

>  Run SC *entry points* JS tests:

1. Test separately each entry point:
```
    yarn test-mint # Test Mint entryPoint

    yarn test-burn # Test Burn entryPoint

    yarn test-transfer # Test Transfer entryPoint

    yarn test-manage-super-transfer-agent # Test AddSuperTransferAgent / DeleteSuperTransferAgent entryPoint

    yarn test-manage-transfer-agent # Test AddTransferAgent / DeleteTransferAgent entryPoint

    yarn test-approve # Test Approve entryPoint

    yarn test-increase-allowance # Test IncreaseAllowance entryPoint

    yarn test-decrease-allowance # Test DecreaseAllowance entryPoint

    yarn test-reset-allowance # Test ResetAllowance entryPoint

    yarn test-reset-all-allowances # Test ResetAllAllowances entryPoint

    yarn test-feeless-transfer # Test FeelessTransfer entryPoint

    yarn test-feeless-approve # Test FeelessApprove entryPoint

    yarn test-feeless-increase-allowance # Test FeelessIncreaseAllowance entryPoint

    yarn test-feeless-decrease-allowance # Test FeelessDecreaseAllowance entryPoint

    yarn test-change-event-sink-address # Test ChangeEventSink entryPoint

    yarn test-set-admin # Test SetAdmin entryPoint

    yarn test-set-pause # Test SetPause entryPoint
```

#### OR

2. Launch batch of tests:

	a. in a terminal, type: `chmod a+x ./scripts/runBatchOfTests.sh`

	b. in a terminal, type: `./scripts/runBatchOfTests.sh`


#### Feeless Operations:

> Make someone else pay the GAS!

Feeless entrypoints are implemented to pay gas for new users that have no XTZ on their accounts, so the process is the following:

The user signs the operation locally with their privateKey and the **admin**  or **transferAgent** will inject that operation in the chain after the signature verification in the smart contract with:

> *sp.check_signature(k, s, b)* where k: user public key, s: operation signature and b: operation bytes.

#### ChainID & Replay attacks for *Feeless* Entry Points

The chain identifier is designed to prevent replay attacks between the main chain and the test chain forked during amendement exploration, where contract addresses and storages are identical. The value is derived from the genesis block hash and will thus be different on the main chains of different networks and on the test chains on which the value depends on the first block of the test chain.

Due to the fork explained, we can have *the same forge operation result* (same branchID, same contractAddress) on two or more chains and that will automatically give *the same operation signature*, so the *sp.check_signature* will return a *TRUE* for all those chains, so to prevent that:

*Solution 1*:

>  The smart contract code is under:
[tkZosSmartContracts/EuroTzAllContracts/EuroTzContractV2withChainID/euroTzSC.py](https://gitlab.com/tezos-paris-hub/eurotz-euro-stable-coin/-/blob/master/EuroTzAllContracts/EuroTzContractV2withChainID/euroTzSC.py)

> **sp.chain_id**: The id of the network currently evaluating the transaction. (dynamically)

> In the smart contract initial storage, we set a constant value with **sp.chain_id_cst("chainIDHere")** where chainID is the ID of the chain where we will deploy our contract.

In each *feeless* entry point we add the following command to verify that we are executing an operation on the chain where the smart contract was deployed.

`sp.verify_equal(self.data.chainID, sp.chain_id, message = "Chain Mismatch")`

*Solution 2*:

>  The smart contract code is under:
     [tkZosSmartContracts/EuroTzAllContracts/EuroTzContractV2/euroTzSC.py](https://gitlab.com/tezos-paris-hub/eurotz-euro-stable-coin/-/blob/master/EuroTzAllContracts/EuroTzContractV2/EuroTzSC.py)

> *Signature of operation must be different for each chain*
> Add the *chainID* bytes to the *forge* result before *signing* the operation, this way we guarantee that the operation signature is different for each chain.

**Demo:**
```
 // Forge transfer operation with hardCoded smartContractAddress and branchID => same opBytes on all chains.
 let opBytes = await forgeTransferOperation(
 senderAccount,
 receiverAccount,
 amount,
 contractAddress,
 "transfer"
 );
 // -------------------------------Carthagenet-------------------------------
 // Retrieved value from RPC endpoint for carthageNet chainID: "NetXjD3HPJJjmcd", to Hex (bytes) give:
 const carthageNetChaniIdBytes = "4e6574586a443348504a4a6a6d6364";
 // Concat bytes
 opBytes = opBytes + carthageNetChaniIdBytes;
 // Sign operation bytes with carthagenetID
 const opSignCarthage = await signOperation(opBytes, secretKey);
 console.log("Carthage Signature: ", opSignCarthage.edsig);
 ==> edsigtsfcYXEU56brEhDPPze53mmCEZRtvF6xUxbZhpLxfbx9MdKt1TcHgQPwRxpMBH5bY1p8exBtXxq8NRCvLz7kAaLZpQxxSj
 // -------------------------------Babylonnet-------------------------------
 // Retrieved value from RPC endpoint for carthageNet chainID: "NetXUdfLh6Gm88t", to Hex (bytes) give:
 const babylonnetNetChaniIdBytes = "4e6574585564664c6836476d383874";
 // Concat bytes
 opBytes = opBytes + babylonnetNetChaniIdBytes;
 // Sign operation bytes with babylonnetID
 const opSignBabylon = await signOperation(opBytes, secretKey);
 console.log("Babylon Signature: ", opSignBabylon.edsig);
 ==> edsigtxRJb9QqU88GZH7UkgRPdoRJPuKtYbpDuGwT1opcB9wrfbmgWfCfqhH6edSGhuLq14Y9FPgrzQGf1SdVdxq1ddoSFdQjJt
```
#### Roles:

1. **Admin**: the contract owner, generally the originator of the SC (origination operation in Tezos = SC Deployment in Ethereum), this account can:
    • Mint and Burn EUR.TZ
    • Manage other roles (add and delete transferAgents and superTransfer agent)
    • Change the contract's admin
    • Approve any address to transfer EUR.TZ from an address to an other
    • Change the eventSinkContract address
	• Pay gas in Feeless operations

2. **Super Transfer Agent**:

    • Approves any address to transfer EUR.TZ from one address to an other
    • Manages the transferAgents (add & delete)

3. **TransferAgent**:

    • Approves only their address to transfer EUR.TZ from an address to an other
	• Pay gas in Feeless operations

#### EventSinkContract:

> The eventSink contract is under:
     [tkZosSmartContracts/EuroTzAllContracts/EventSinkContract/eventSink.py](https://gitlab.com/tezos-paris-hub/eurotz-euro-stable-coin/-/blob/master/EuroTzAllContracts/EventSinkContract/eventSink.py)

> This contract is designed as an alternative to the *event* system in *Ethereum*, after any mint, transfer or approval invocation in the EUR.TZ SC, an internal call to this contract is fired as an event.

Here is a schema that explains the process more fully:

![Event Sink](assets/tkZosTokenDoc.png "EventSink Process")


#### EntryPoints Errors Code / Statement:

In order to reduce the EuroTz smart contract size, we replaced statements by codes in error messages:

```
 "01": "Only admin allowed.",
 "02": "Only admin or Super transfer agent are allowed.",
 "03": "Only admin or Transfer agent are allowed.",
 "04": "Not allowed to approve from this account.",
 "05": "Only account Owner, Admin or SuperTransferAgent are allowed.",
 "06": "Contract is paused.",
 "07": "Caller not approved.",
 "08": "Amount allowed smaller than amount to transfer.",
 "09": "Amount to burn must be smaller than user balance.",
 "10": "Amount to transfer must be smaller than user balance.",
 "11": "Amount to approve must be greater than zero.",
 "12": "Amount to increase must be greater than zero.",
 "13": "Impossible to increase allowance of unexistant account.",
 "14": "Amount to decrease must be greater than zero.",
 "15": "Impossible to decrease allowance of unexistant account.",
 "16": "Amount to decrease must be smaller than the one already approved.",
 "17": "Not approved or already reseted.",
 "18": "Insufficient balance.",
 "19": "Invalid nonce.",
 "20": "Wrong signature.",
 "21": "Transfer agent exists already.",
 "22": "Transfer agent doesn't exist.",
 "23": "Super transfer agent exists already.",
 "24": "Super transfer agent doesn't exist.",
 "25": "TransferAgent is only allowed to approve to his address.",
 "26": "Approvals map is already empty.",
 "27": "Chains Mismatch",
 "28": "Trying to retrive undefined element in the Map/BigMap"
```
