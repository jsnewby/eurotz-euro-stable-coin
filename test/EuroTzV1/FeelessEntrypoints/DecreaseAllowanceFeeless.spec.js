import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";

import { decreaseAllowanceFeeless } from "../../../feelessOperations/EuroTzV1/decreaseAllowanceFeeless"



Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Decrease Allowance Operation Testing", function () {
    let euroTzContractStorage;
    let euroTzContract;
    let khaledBalance;
    let khaledDetails;

    before(async function () {
        euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

        euroTzContractStorage = await euroTzContract.storage();

        khaledDetails = await euroTzContractStorage.balances.get(conf.khaledAddress);
        khaledBalance = new BigNumber(khaledDetails.balance).toNumber();

        for (let [key, value] of Object.entries(khaledDetails.approvals)) {
            console.log(
                `Initial khaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
            );
        }

        console.log("Khaled initial balance: ", khaledBalance, "euroTz");
    });

    it("Khaled decreases allowance of random account by 7 euroTz from his account's balance", async function () {
        Tezos.importKey(conf.adminSecretKey);

        try {
            const tezosApproveHash = await decreaseAllowanceFeeless(
                conf.khaledAddress,
                conf.khaledPublicKey,
                conf.khaledPrivateKey,
                conf.unknownAccount.pkh,
                7,
                conf.contractAddress
            );

            console.log("tezosApproveHash : ", tezosApproveHash);

            euroTzContractStorage = await euroTzContract.storage();

            khaledDetails = await euroTzContractStorage.balances.get(conf.khaledAddress);

            for (let [key, value] of Object.entries(khaledDetails.approvals)) {
                console.log(
                    `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
                );
            }

            expect(tezosApproveHash).to.be.a("string");
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Khaled tries to decrease allowance of a random account by 12 euroTz with wrong signature", async function () {
        Tezos.importKey(conf.adminSecretKey);

        try {
            await decreaseAllowanceFeeless(
                conf.khaledAddress,
                conf.khaledPublicKey,
                // wrong secretKey => wrong signature
                conf.fredPrivateKey,
                conf.unknownAccount.pkh,
                12,
                conf.contractAddress
            );
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

});
