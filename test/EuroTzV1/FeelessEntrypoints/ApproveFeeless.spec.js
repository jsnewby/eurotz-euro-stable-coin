import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";

import { approveFeeless } from "../../../feelessOperations/EuroTzV1/approveFeeless";
import { getXTZBalance } from "../../../Utils/getXTZBalance";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Approve Feeless Operation Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;
  let khaledBalance;
  let khaledDetails;
  let khaledNonce;
  let khaledNonceBN;
  let khaledXTZBalance;
  let adminXTZBalance;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

    euroTzContractStorage = await euroTzContract.storage();

    khaledDetails = await euroTzContractStorage.balances.get(
      conf.khaledAddress
    );
    khaledBalance = new BigNumber(khaledDetails.balance).toNumber();

    try {
      khaledNonceBN = await euroTzContractStorage.nonces.get(
        conf.khaledAddress
      );
      khaledNonce = new BigNumber(khaledNonceBN).toNumber();
    } catch (e) {
      if ((e.status === 404) & (e.statusText === "Not Found")) {
        khaledNonce = 0;
      } else {
        console.log("PublicNode Error ", e);
      }
    }

    for (let [key, value] of Object.entries(khaledDetails.approvals)) {
      console.log(
        `Initial khaledApprovals: ${key}: ${new BigNumber(
          value
        ).toNumber()} euroTz`
      );
    }

    console.log("Khaled initial balance: ", khaledBalance, "euroTz");

    khaledXTZBalance = await getXTZBalance(conf.khaledAddress);
    adminXTZBalance = await getXTZBalance(conf.adminAddress);

    console.log("Initial Khaled XTZ balance: ", khaledXTZBalance, "ꜩ");
    console.log("Initial Admin XTZ initial balance: ", adminXTZBalance, "ꜩ");
    console.log("-------------------------------------------------");
  });

  it("Khaled tries to approve a random account to spend 17 euroTz by introducing a wrong secretKey (=> wrong signature)", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await approveFeeless(
        conf.khaledAddress,
        conf.khaledPublicKey,
        // wrong secretKey => wrong signature
        conf.fredPrivateKey,
        conf.unknownAccount.pkh,
        17,
        conf.contractAddress
      );
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Khaled tries to approve a random account to spend 0 euroTz", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await approveFeeless(
        conf.khaledAddress,
        conf.khaledPublicKey,
        conf.khaledPrivateKey,
        conf.unknownAccount.pkh,
        0,
        conf.contractAddress
      );
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Khaled tries to approve a random account to spend 44 euroTz with an invalid nonce", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await approveFeeless(
        conf.khaledAddress,
        conf.khaledPublicKey,
        conf.khaledPrivateKey,
        conf.unknownAccount.pkh,
        44,
        conf.contractAddress,
        khaledNonce + 1
      );
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Khaled tries to approve a random account to spend 24 euroTz where Alice (Not admin & Not TransferAgent) is the gas payer", async function () {
    Tezos.importKey(conf.aliceSecretKey);

    try {
      await approveFeeless(
        conf.khaledAddress,
        conf.khaledPublicKey,
        conf.khaledPrivateKey,
        conf.unknownAccount.pkh,
        44,
        conf.contractAddress,
        khaledNonce + 1
      );
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Khaled approves a random account to spend 20 euroTz from his account's balance where Admin is the gaz payer", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const tezosApproveHash = await approveFeeless(
        conf.khaledAddress,
        conf.khaledPublicKey,
        conf.khaledPrivateKey,
        conf.unknownAccount.pkh,
        20,
        conf.contractAddress
      );

      console.log("tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        // console.log("MESSAGE CODE: ", e.errors[1].with.string);
        // console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
        console.log("MESSAGE CODE: ", e.errors);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Khaled's XTZ balance must be the same before the Trasnfer Operation and the Admin XTZ balance decreased by the Operation fees", async function () {
    const newKhaledXTZBalance = await getXTZBalance(conf.khaledAddress);
    const newAdminXTZBalance = await getXTZBalance(conf.adminAddress);

    console.log("-------------------------------------------------");
    console.log("Final Khaled XTZ balance: ", newKhaledXTZBalance, "ꜩ");
    console.log("Final Admin XTZ initial balance: ", newAdminXTZBalance, "ꜩ");
    console.log(
      "Gas value of the Operation: ",
      adminXTZBalance - newAdminXTZBalance,
      "ꜩ"
    );
    console.log("-------------------------------------------------");

    expect(khaledXTZBalance).to.equal(newKhaledXTZBalance);
    expect(newAdminXTZBalance).to.be.below(adminXTZBalance);
  });

  it("Khaled tries to approve an already approved account to spend 31 euroTz", async function () {
    Tezos.importKey(conf.oussSecretKey);

    try {
      const tezosApproveHash = await approveFeeless(
        conf.khaledAddress,
        conf.khaledPublicKey,
        conf.khaledPrivateKey,
        conf.unknownAccount.pkh,
        31,
        conf.contractAddress
      );

      console.log("tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
