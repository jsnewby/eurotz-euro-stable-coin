import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Super Transfer Agents Management Operations Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

    euroTzContractStorage = await euroTzContract.storage();

  });

  it("Admin add a SuperTransferAgent", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const addSuperTransferAgentOperation = await euroTzContract.methods
        .addSuperTransferAgent(conf.superTransferAgent.pkh)
        .send();

      await addSuperTransferAgentOperation.confirmation();

      const addSuperTransferAgentOperationHash = addSuperTransferAgentOperation.hash;

      console.log("1. addSuperTransferAgentOperationHash : ", addSuperTransferAgentOperationHash);

      euroTzContractStorage = await euroTzContract.storage();

      console.log("newStorage: ", euroTzContractStorage.superTransferAgents);

      expect(addSuperTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Admin tries to add an existant SuperTransferAgent", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      await euroTzContract.methods
        .addSuperTransferAgent(conf.superTransferAgent.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Admin add fred as a SuperTransferAgent", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const addSuperTransferAgentOperation = await euroTzContract.methods
        .addSuperTransferAgent(conf.fredAddress)
        .send();

      await addSuperTransferAgentOperation.confirmation();

      const addSuperTransferAgentOperationHash = addSuperTransferAgentOperation.hash;

      console.log("2. addSuperTransferAgentOperationHash : ", addSuperTransferAgentOperationHash);

      euroTzContractStorage = await euroTzContract.storage();

      console.log("newStorage: ", euroTzContractStorage.superTransferAgents);

      expect(addSuperTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Admin deletes a superTransferAgent from the SuperTransferAgents set", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const deleteSuperTransferAgentOperation = await euroTzContract.methods
        .deleteSuperTransferAgent(conf.superTransferAgent.pkh)
        .send();

      await deleteSuperTransferAgentOperation.confirmation();

      const deleteSuperTransferAgentOperationHash = deleteSuperTransferAgentOperation.hash;

      console.log("3. deleteSuperTransferAgentOperationHash : ", deleteSuperTransferAgentOperationHash);

      euroTzContractStorage = await euroTzContract.storage();

      console.log("newStorage: ", euroTzContractStorage.superTransferAgents);

      expect(deleteSuperTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Admin tries to delete an unexistant superTransferAgent from the SuperTransferAgents set", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      await euroTzContract.methods
        .deleteSuperTransferAgent(conf.superTransferAgent.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Non Admin tries to add a SuperTransferAgent", async function () {
    Tezos.importKey(conf.oussSecretKey);
    try {
      await euroTzContract.methods
        .addSuperTransferAgent(conf.khaledAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Non Admin tries to delete a SuperTransferAgent", async function () {
    Tezos.importKey(conf.oussSecretKey);
    try {
      await euroTzContract.methods
        .deleteSuperTransferAgent(conf.fredAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

});
