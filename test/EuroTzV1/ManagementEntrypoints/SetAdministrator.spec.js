import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";


Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: SetAdmin Operation Testing", function () {
    let euroTzContractStorage;
    let euroTzContract;

    before(async function () {

        euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

        euroTzContractStorage = await euroTzContract.storage();

        console.log("Initial Admin: ", euroTzContractStorage.administrator)
    });

    it("Admin set Alice as new Admin", async function () {
        Tezos.importKey(conf.adminSecretKey);

        try {
            const tezosSetAdmin = await euroTzContract.methods
                .setAdministrator(conf.aliceAddress)
                .send();

            await tezosSetAdmin.confirmation();

            const tezosSetAdminHash = tezosSetAdmin.hash;

            console.log("tezosSetAdminHash : ", tezosSetAdminHash);

            euroTzContractStorage = await euroTzContract.storage();

            console.log("New Admin: ", euroTzContractStorage.administrator)

            expect(tezosSetAdminHash).to.be.a("string");
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Non admin tries to set a new admin", async function () {
        Tezos.importKey(conf.oussSecretKey);
        try {
            await euroTzContract.methods
                .setAdministrator(conf.khaledAddress)
                .send();
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Alice reset the first Admin", async function () {
        Tezos.importKey(conf.aliceSecretKey);

        try {
            const tezosSetAdmin = await euroTzContract.methods
                .setAdministrator(conf.adminAddress)
                .send();

            await tezosSetAdmin.confirmation();

            const tezosSetAdminHash = tezosSetAdmin.hash;

            console.log("tezosSetAdminHash : ", tezosSetAdminHash);

            euroTzContractStorage = await euroTzContract.storage();

            console.log("New Admin: ", euroTzContractStorage.administrator)

            expect(tezosSetAdminHash).to.be.a("string");
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });
});
