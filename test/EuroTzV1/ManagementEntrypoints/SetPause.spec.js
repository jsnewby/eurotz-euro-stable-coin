import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: SetPause Operation Testing", function () {
    let euroTzContractStorage;
    let euroTzContract;

    before(async function () {

        euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

        euroTzContractStorage = await euroTzContract.storage();

        console.log("Initial Contract State: Paused ", euroTzContractStorage.paused)
    });

    it("Admin set Contract state as Paused", async function () {
        Tezos.importKey(conf.adminSecretKey);

        try {
            const tezosSetPause = await euroTzContract.methods
                .setPause(true)
                .send();

            await tezosSetPause.confirmation();

            const tezosSetPauseHash = tezosSetPause.hash;

            console.log("tezosSetPauseHash : ", tezosSetPauseHash);

            euroTzContractStorage = await euroTzContract.storage();

            console.log("New Contract State: Paused ", euroTzContractStorage.paused)

            expect(tezosSetPauseHash).to.be.a("string");
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Fred tries to transfer 15 euroTz to Ouss while Contract is Paused", async function () {
        Tezos.importKey(conf.fredSecretKey);
        try {
            await euroTzContract.methods
                .transfer(15, conf.fredAddress, conf.oussAddress)
                .send();

        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Admin reactivate the euroTz Contract", async function () {
        Tezos.importKey(conf.adminSecretKey);

        try {
            const tezosSetPause = await euroTzContract.methods
                .setPause(false)
                .send();

            await tezosSetPause.confirmation();

            const tezosSetPauseHash = tezosSetPause.hash;

            console.log("tezosSetPauseHash : ", tezosSetPauseHash);

            euroTzContractStorage = await euroTzContract.storage();

            console.log("New Contract State after reactivation: Paused ", euroTzContractStorage.paused)

            expect(tezosSetPauseHash).to.be.a("string");
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Fred transfers 32 euroTz to Ouss after contract reactivation", async function () {
        Tezos.importKey(conf.fredSecretKey);
        try {
            const tezosTransfer = await euroTzContract.methods
                .transfer(32, conf.fredAddress, conf.oussAddress)
                .send();

            await tezosTransfer.confirmation();

            const tezosTransferHash = tezosTransfer.hash;

            console.log("tezosTransferHash : ", tezosTransferHash);

            expect(tezosTransferHash).to.be.a("string").to.have.length(51);

        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Non admin tries to pause the contract", async function () {
        Tezos.importKey(conf.oussSecretKey);
        try {
            await euroTzContract.methods
                .setPause("true")
                .send();
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });
});
