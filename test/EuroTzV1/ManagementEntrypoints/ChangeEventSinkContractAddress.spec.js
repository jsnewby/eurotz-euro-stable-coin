import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Change EventSink contract address Operation Testing", function () {
    let euroTzContractStorage;
    let euroTzContract;
    let newEventSinkContractAddress;

    before(async function () {
        newEventSinkContractAddress = "KT1D4ji2fv9uG9jq4aAKMK3kz1oCHxQGkWWX";

        euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

        euroTzContractStorage = await euroTzContract.storage();
    });

    it("Admin Changes the eventSink contract address", async function () {
        Tezos.importKey(conf.adminSecretKey);

        try {
            console.log("Old event sink contract address: ", euroTzContractStorage.eventSinkContractAddress);

            const tezosChangeEventSinkContractAddress = await euroTzContract.methods
                .changeEventSinkContractAddress(newEventSinkContractAddress)
                .send();

            await tezosChangeEventSinkContractAddress.confirmation();

            const tezosChangeEventSinkContractAddressHash = tezosChangeEventSinkContractAddress.hash;

            console.log("tezosChangeEventSinkContractAddressHash : ", tezosChangeEventSinkContractAddressHash);

            euroTzContractStorage = await euroTzContract.storage();

            console.log("New event sink contract address: ", euroTzContractStorage.eventSinkContractAddress)

            expect(tezosChangeEventSinkContractAddressHash).to.be.a("string");

        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error:", e)
            }
        }

    });

    it("Non admin tries to change eventSinkContractAddress", async function () {
        Tezos.importKey(conf.oussSecretKey);
        try {

            const tezosChangeEventSinkContractAddress = await euroTzContract.methods
                .changeEventSinkContractAddress(newEventSinkContractAddress)
                .send();

            await tezosChangeEventSinkContractAddress.confirmation();

        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error:", e)
            }
        }
    });
});
