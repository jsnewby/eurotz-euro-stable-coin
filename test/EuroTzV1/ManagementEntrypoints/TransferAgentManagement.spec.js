import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Transfer Agents Management Operations Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

    euroTzContractStorage = await euroTzContract.storage();

  });

  it("Admin add a TransferAgent", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const addTransferAgentOperation = await euroTzContract.methods
        .addTransferAgent(conf.transferAgent.pkh)
        .send();

      await addTransferAgentOperation.confirmation();

      const addTransferAgentOperationHash = addTransferAgentOperation.hash;

      console.log("1. addTransferAgentOperationHash : ", addTransferAgentOperationHash);

      euroTzContractStorage = await euroTzContract.storage();

      console.log("newStorage: ", euroTzContractStorage.transferAgents);

      expect(addTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Admin deletes a TransferAgent", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const deleteTransferAgentOperation = await euroTzContract.methods
        .deleteTransferAgent(conf.transferAgent.pkh)
        .send();

      await deleteTransferAgentOperation.confirmation();

      const deleteTransferAgentOperationHash = deleteTransferAgentOperation.hash;

      console.log("2. deleteTransferAgentOperationHash : ", deleteTransferAgentOperationHash);

      euroTzContractStorage = await euroTzContract.storage();

      console.log("newStorage: ", euroTzContractStorage.transferAgents);

      expect(deleteTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("SuperTransferAgent add a TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      const addTransferAgentOperation = await euroTzContract.methods
        .addTransferAgent(conf.unknownAccount.pkh)
        .send();

      await addTransferAgentOperation.confirmation();

      const addTransferAgentOperationHash = addTransferAgentOperation.hash;

      console.log("3. addTransferAgentOperationHash : ", addTransferAgentOperationHash);

      euroTzContractStorage = await euroTzContract.storage();

      console.log("newStorage: ", euroTzContractStorage.transferAgents);

      expect(addTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("SuperTransferAgent deletes a TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      const deleteTransferAgentOperation = await euroTzContract.methods
        .deleteTransferAgent(conf.unknownAccount.pkh)
        .send();

      await deleteTransferAgentOperation.confirmation();

      const deleteTransferAgentOperationHash = deleteTransferAgentOperation.hash;

      console.log("4. deleteTransferAgentOperationHash : ", deleteTransferAgentOperationHash);

      euroTzContractStorage = await euroTzContract.storage();

      console.log("newStorage: ", euroTzContractStorage.transferAgents);

      expect(deleteTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("SuperTransferAgent tries to delete an unexistant TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      await euroTzContract.methods
        .deleteTransferAgent(conf.unknownAccount.pkh)
        .send();

    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Fred (SuperTransferAgent) add ouss as a TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      const addTransferAgentOperation = await euroTzContract.methods
        .addTransferAgent(conf.oussAddress)
        .send();

      await addTransferAgentOperation.confirmation();

      const addTransferAgentOperationHash = addTransferAgentOperation.hash;

      console.log("5. addTransferAgentOperationHash : ", addTransferAgentOperationHash);

      euroTzContractStorage = await euroTzContract.storage();

      console.log("newStorage: ", euroTzContractStorage.transferAgents);

      expect(addTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Fred (SuperTransferAgent) tries to add an existant TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      await euroTzContract.methods
        .addTransferAgent(conf.oussAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Non SuperTransferAgent & non admin account tries to add a TransferAgent", async function () {
    Tezos.importKey(conf.khaledSecretKey);
    try {
      await euroTzContract.methods
        .addTransferAgent(conf.unknownAccount.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

  it("Non SuperTransferAgent & non admin account tries to delete a TransferAgent", async function () {
    Tezos.importKey(conf.khaledSecretKey);
    try {
      await euroTzContract.methods
        .deleteTransferAgent(conf.oussAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e)
      }
    }
  });

});
