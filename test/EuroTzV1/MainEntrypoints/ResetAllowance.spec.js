import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: ResetAllowance Operation Testing", function () {
    let euroTzContractStorage;
    let euroTzContract;
    let khaledBalance;
    let khaledDetails;

    before(async function () {
        euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

        euroTzContractStorage = await euroTzContract.storage();

        khaledDetails = await euroTzContractStorage.balances.get(conf.khaledAddress);
        khaledBalance = new BigNumber(khaledDetails.balance).toNumber();

        for (let [key, value] of Object.entries(khaledDetails.approvals)) {
            console.log(
                `Initial fredApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
            );
        }

        console.log("Khaled initial balance: ", khaledBalance, "tkZos");
        console.log("-------------------------------------------------");
    });

    it("Khaled reset allowance of random account", async function () {
        Tezos.importKey(conf.khaledSecretKey);

        try {
            const tezosApprove = await euroTzContract.methods
                .resetAllowance(conf.khaledAddress, conf.unknownAccount.pkh)
                .send();

            await tezosApprove.confirmation();

            const tezosApproveHash = tezosApprove.hash;

            console.log("1. tezosApproveHash : ", tezosApproveHash);

            euroTzContractStorage = await euroTzContract.storage();

            khaledDetails = await euroTzContractStorage.balances.get(conf.khaledAddress);

            for (let [key, value] of Object.entries(khaledDetails.approvals)) {
                console.log(
                    `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
                );
            }

            expect(tezosApproveHash).to.be.a("string").to.have.length(51);
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Khaled tries to reset allowance of random account", async function () {
        Tezos.importKey(conf.khaledSecretKey);

        try {
            await euroTzContract.methods
                .resetAllowance(conf.khaledAddress, conf.unknownAccount.pkh)
                .send();
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Alice tries to reset allowance of Ouss account in Khaled approvals", async function () {
        Tezos.importKey(conf.aliceSecretKey);

        try {
            await euroTzContract.methods
                .resetAllowance(conf.khaledAddress, conf.oussAddress)
                .send();
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });
});
