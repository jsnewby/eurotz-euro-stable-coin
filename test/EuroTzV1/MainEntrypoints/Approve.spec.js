import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Approve Operation Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;
  let khaledBalance;
  let khaledDetails;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

    euroTzContractStorage = await euroTzContract.storage();

    khaledDetails = await euroTzContractStorage.balances.get(
      conf.khaledAddress
    );
    khaledBalance = new BigNumber(khaledDetails.balance).toNumber();

    for (let [key, value] of Object.entries(khaledDetails.approvals)) {
      console.log(
        `Initial KhaledApprovals: ${key}: ${new BigNumber(
          value
        ).toNumber()} euroTz`
      );
    }

    console.log("Initial Khaled balance: ", khaledBalance, "euroTz");
    console.log("-------------------------------------------------");
  });

  it("Khaled approves an account to spend 20 euroTz from his balance", async function () {
    Tezos.importKey(conf.khaledSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .approve(20, conf.khaledAddress, conf.unknownAccount.pkh)
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("1. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }
      console.log("-------------------------------------------------");
      expect(tezosApproveHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin tries to approve an account (same above) to spend 45 euroTz from khaled account balance", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await euroTzContract.methods
        .approve(45, conf.khaledAddress, conf.unknownAccount.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Super Transfer Agent approves Alice to spend 52 euroTz from Khaled account balance", async function () {
    // Fred is a SuperTransferAgent added by the Admin
    Tezos.importKey(conf.fredSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .approve(52, conf.khaledAddress, conf.aliceAddress)
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("3. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }
      console.log("-------------------------------------------------");
      expect(tezosApproveHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Transfer Agent approves hisslef to spend 89 euroTz from khaled account balance", async function () {
    // Ouss is a TransferAgent added by Fred, the SuperTrasnferAgent
    Tezos.importKey(conf.oussSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .approve(89, conf.khaledAddress, conf.oussAddress)
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("4. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }
      console.log("-------------------------------------------------");
      expect(tezosApproveHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Transfer Agent tries to approve an account to spend 90 euroTz from khaled account balance", async function () {
    Tezos.importKey(conf.oussSecretKey);

    try {
      await euroTzContract.methods
        .approve(90, conf.khaledAddress, conf.unknownAccount.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Alice tries to approve herself to spend 12 euroTz from khaled account balance", async function () {
    Tezos.importKey(conf.aliceSecretKey);

    try {
      await euroTzContract.methods
        .approve(12, conf.khaledAddress, conf.aliceAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Ouss transfers 13 euroTz from khaled account's to fred account after been approved", async function () {
    Tezos.importKey(conf.oussSecretKey);

    try {
      const tezosTransfer = await euroTzContract.methods
        .transfer(13, conf.khaledAddress, conf.fredAddress)
        .send();

      await tezosTransfer.confirmation();

      const tezosTransferHash = tezosTransfer.hash;

      console.log("5. tezosTransferHash : ", tezosTransferHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );
      khaledBalance = new BigNumber(khaledDetails.balance).toNumber();

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `khaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      console.log("Khaled final balance: ", khaledBalance, "euroTz");

      expect(tezosTransferHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });
});
