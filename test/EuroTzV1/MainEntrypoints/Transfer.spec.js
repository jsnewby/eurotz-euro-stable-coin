import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../../conf/conf";
import errors from "../../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Transfer Operation Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;
  let fredBalance;
  let fredDetails;
  let oussBalance;
  let oussDetails;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddressV1);

    euroTzContractStorage = await euroTzContract.storage();

    fredDetails = await euroTzContractStorage.balances.get(conf.fredAddress);
    fredBalance = new BigNumber(fredDetails.balance).toNumber();

    oussDetails = await euroTzContractStorage.balances.get(conf.oussAddress);
    oussBalance = new BigNumber(oussDetails.balance).toNumber();

    console.log("Initial ouss balance: ", oussBalance, "euroTz");
    console.log("Initial fred balance: ", fredBalance, "euroTz");

    for (let [key, value] of Object.entries(fredDetails.approvals)) {
      console.log(
        `Fredpprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
      );
    }
    console.log("-------------------------------------------------");
  });

  it("Non allowed user (doesn't appear in the approved accounts set), Khaled in this example, tries to transfer from Fred to Ouss 29 euroTz", async function () {
    Tezos.importKey(conf.khaledPrivateKey);
    try {
      await euroTzContract.methods
        .transfer(29, conf.fredAddress, conf.oussAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  // it("Non allowed user (having an allowance < amount to transfer), Admin in this example, tries to transfer from Fred to Ouss 35 euroTz", async function () {
  //   Tezos.importKey(conf.adminSecretKey);
  //   try {
  //     await euroTzContract.methods
  //       .transfer(35, conf.fredAddress, conf.oussAddress)
  //       .send();

  //   } catch (e) {
  //     if (e instanceof TezosOperationError) {
  //       console.log("MESSAGE CODE: ", e.errors[1].with.string);
  //       console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
  //     } else {
  //       console.log("PublicNode Error ", e)
  //     }
  //   }
  // });

  // We granted the admin to transfer from any user account for management issues...

  it("Admin transfer from Fred to Ouss 35 euroTz", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const tezosTransfer = await euroTzContract.methods
        .transfer(35, conf.fredAddress, conf.oussAddress)
        .send();

      await tezosTransfer.confirmation();

      const tezosTransferHash = tezosTransfer.hash;

      console.log("tezosTransferHash : ", tezosTransferHash);

      expect(tezosTransferHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Fred tries to transfer a greater amount than his balance", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      await euroTzContract.methods
        .transfer(fredBalance + 1, conf.fredAddress, conf.oussAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Fred transfers 15 euroTz to Ouss", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      const tezosTransfer = await euroTzContract.methods
        .transfer(15, conf.fredAddress, conf.oussAddress)
        .send();

      await tezosTransfer.confirmation();

      const tezosTransferHash = tezosTransfer.hash;

      console.log("tezosTransferHash : ", tezosTransferHash);

      expect(tezosTransferHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Ouss's balance must increase by 15 euroTz", async function () {
    try {
      euroTzContractStorage = await euroTzContract.storage();

      oussDetails = await euroTzContractStorage.balances.get(conf.oussAddress);
      const newOussBalance = new BigNumber(oussDetails.balance).toNumber();

      console.log("Ouss's final balance: ", newOussBalance);

      // expect(newOussBalance).to.equal(oussBalance + 15);
      expect(newOussBalance).to.equal(oussBalance + 50);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Fred's balance must decrease by 15 euroTz", async function () {
    try {
      euroTzContractStorage = await euroTzContract.storage();

      fredDetails = await euroTzContractStorage.balances.get(conf.fredAddress);
      const newFredBalance = new BigNumber(fredDetails.balance).toNumber();

      console.log("Fred's final balance: ", newFredBalance);

      // expect(newFredBalance).to.equal(fredBalance - 15);
      expect(newFredBalance).to.equal(fredBalance - 50);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
