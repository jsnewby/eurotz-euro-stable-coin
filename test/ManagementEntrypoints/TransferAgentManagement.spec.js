import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../../conf/conf";
import errors from "../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Transfer Agents Management Operations Testing", function () {
  let proxyContract;

  before(async function () {
    proxyContract = await Tezos.contract.at(conf.proxyContractAddress);
  });

  it("Admin add a TransferAgent", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const addTransferAgentOperation = await proxyContract.methods
        .run(conf.transferAgent.pkh, "callAddTransferAgent")
        .send();

      await addTransferAgentOperation.confirmation();

      const addTransferAgentOperationHash = addTransferAgentOperation.hash;

      console.log(
        "1. addTransferAgentOperationHash : ",
        addTransferAgentOperationHash
      );

      const upgrade = await proxyContract.methods
        .upgrade(conf.contractAddress)
        .send();

      await upgrade.confirmation();

      console.log("Success upgrade ", upgrade.hash);

      expect(addTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Admin deletes a TransferAgent", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const deleteTransferAgentOperation = await proxyContract.methods
        .run(conf.transferAgent.pkh, "callDeleteTransferAgent")
        .send();

      await deleteTransferAgentOperation.confirmation();

      const deleteTransferAgentOperationHash =
        deleteTransferAgentOperation.hash;

      console.log(
        "2. deleteTransferAgentOperationHash : ",
        deleteTransferAgentOperationHash
      );

      const upgrade = await proxyContract.methods
        .upgrade(conf.contractAddress)
        .send();

      await upgrade.confirmation();

      console.log("Success upgrade ", upgrade.hash);

      expect(deleteTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("SuperTransferAgent add a TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      const addTransferAgentOperation = await proxyContract.methods
        .run(conf.unknownAccount.pkh, "callAddTransferAgent")
        .send();

      await addTransferAgentOperation.confirmation();

      const addTransferAgentOperationHash = addTransferAgentOperation.hash;

      console.log(
        "3. addTransferAgentOperationHash : ",
        addTransferAgentOperationHash
      );

      const upgrade = await proxyContract.methods
        .upgrade(conf.contractAddress)
        .send();

      await upgrade.confirmation();

      console.log("Success upgrade ", upgrade.hash);

      expect(addTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("SuperTransferAgent deletes a TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      const deleteTransferAgentOperation = await proxyContract.methods
        .run(conf.unknownAccount.pkh, "callDeleteTransferAgent")
        .send();

      await deleteTransferAgentOperation.confirmation();

      const deleteTransferAgentOperationHash =
        deleteTransferAgentOperation.hash;

      console.log(
        "4. deleteTransferAgentOperationHash : ",
        deleteTransferAgentOperationHash
      );

      const upgrade = await proxyContract.methods
        .upgrade(conf.contractAddress)
        .send();

      await upgrade.confirmation();

      console.log("Success upgrade ", upgrade.hash);

      expect(deleteTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("SuperTransferAgent tries to delete an unexistant TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      await proxyContract.methods
        .run(conf.unknownAccount.pkh, "callDeleteTransferAgent")
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Fred (SuperTransferAgent) add Alice as a TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      const addTransferAgentOperation = await proxyContract.methods
        .run(conf.aliceAddress, "callAddTransferAgent")
        .send();

      await addTransferAgentOperation.confirmation();

      const addTransferAgentOperationHash = addTransferAgentOperation.hash;

      console.log(
        "5. addTransferAgentOperationHash : ",
        addTransferAgentOperationHash
      );

      const upgrade = await proxyContract.methods
        .upgrade(conf.contractAddress)
        .send();

      await upgrade.confirmation();

      console.log("Success upgrade ", upgrade.hash);

      expect(addTransferAgentOperationHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Fred (SuperTransferAgent) tries to add an existant TransferAgent", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      await proxyContract.methods
        .run(conf.aliceAddress, "callAddTransferAgent")
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Non SuperTransferAgent & non admin account tries to add a TransferAgent", async function () {
    Tezos.importKey(conf.khaledSecretKey);
    try {
      await proxyContract.methods
        .run(conf.unknownAccount.pkh, "callAddTransferAgent")
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Non SuperTransferAgent & non admin account tries to delete a TransferAgent", async function () {
    Tezos.importKey(conf.khaledSecretKey);
    try {
      await proxyContract.methods
        .run(conf.oussAddress, "callDeleteTransferAgent")
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });
});
