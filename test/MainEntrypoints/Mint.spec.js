import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../conf/conf";
import errors from "../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz SmartContract Test: Mint Operation Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;
  let fredBalance;
  let fredDetails;
  let oussBalance;
  let oussDetails;
  let khaledDetails;
  let khaledBalance;
  let totalSupply;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddress);

    euroTzContractStorage = await euroTzContract.storage();

    totalSupply = new BigNumber(euroTzContractStorage.totalSupply).toNumber();

    // Waiting to fix the taquito bug when map key in unknown to throw undefined and not an error...
    // I opened an issue: https://github.com/ecadlabs/taquito/issues/277
    try {
      fredDetails = await euroTzContractStorage.balances.get(conf.fredAddress);
      fredBalance = new BigNumber(fredDetails.balance).toNumber();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );
      khaledBalance = new BigNumber(khaledDetails.balance).toNumber();

      oussDetails = await euroTzContractStorage.balances.get(conf.oussAddress);
      oussBalance = new BigNumber(oussDetails.balance).toNumber();
    } catch (e) {
      fredBalance = 0;
      khaledBalance = 0;
      oussBalance = 0;
    }

    // let testAccountBalance;

    // try {
    //   // Test Taquito bugFix

    //   var testAccount = await euroTzContractStorage.balances.get("tz1e2VkaXdHXX7RrJUBNHztGHWnrAWMjsUp1");

    //   testAccountBalance = new BigNumber(testAccount.balance).toNumber();

    // } catch (e) {
    //   if (e.status === 404) {
    //     console.log("error: ", e)
    //     testAccountBalance = 0;
    //   } else {
    //     throw e;
    //   }
    // }

    console.log("Initial totalSupply: ", totalSupply, "euroTz");
    console.log("Initial fred balance: ", fredBalance, "euroTz");
    console.log("Initial ouss balance: ", oussBalance, "euroTz");
    console.log("Initial khaled balance: ", khaledBalance, "euroTz");
    console.log("-------------------------------------------------");
  });

  it("Non Admin (Fred in this test) tries to Mint 4000 euroTz to Ouss's account", async function () {
    // Fred signs this Mint Operation
    Tezos.importKey(conf.fredPrivateKey);

    try {
      await euroTzContract.methods.mint(conf.oussAddress, 4000).send();
    } catch (e) {
      // To be changed to error code...
      // expect(e.errors).to.equal("Only admin allowed");
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin Mint 600000 euroTz to Fred's account", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const tezosMint = await euroTzContract.methods
        .mint(conf.fredAddress, 600000)
        .send();

      await tezosMint.confirmation();

      const tezosMintHash = tezosMint.hash;

      console.log("1. tezosMintHash : ", tezosMintHash);

      expect(tezosMintHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin Mint 850000 euroTz to Ouss's account", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const tezosMint = await euroTzContract.methods
        .mint(conf.oussAddress, 850000)
        .send();

      await tezosMint.confirmation();

      const tezosMintHash = tezosMint.hash;

      console.log("2. tezosMintHash : ", tezosMintHash);

      expect(tezosMintHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin Mint 780000 euroTz to Khaled's account", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const tezosMint = await euroTzContract.methods
        .mint(conf.khaledAddress, 780000)
        .send();

      await tezosMint.confirmation();

      const tezosMintHash = tezosMint.hash;

      console.log("3. tezosMintHash : ", tezosMintHash);

      euroTzContractStorage = await euroTzContract.storage();

      expect(tezosMintHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Fred's balance must be increased by 600000 euroTz", async function () {
    try {
      euroTzContractStorage = await euroTzContract.storage();

      const newFredDetails = await euroTzContractStorage.balances.get(
        conf.fredAddress
      );
      const newFredBalance = new BigNumber(newFredDetails.balance).toNumber();

      console.log("Final Fred's balance: ", newFredBalance, "euroTz");

      expect(newFredBalance).to.equal(fredBalance + 600000);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Ouss's balance must be increased by 850000 euroTz", async function () {
    try {
      euroTzContractStorage = await euroTzContract.storage();

      const newOussDetails = await euroTzContractStorage.balances.get(
        conf.oussAddress
      );
      const newOussBalance = new BigNumber(newOussDetails.balance).toNumber();

      console.log("Final Ouss's balance: ", newOussBalance, "euroTz");

      expect(newOussBalance).to.equal(oussBalance + 850000);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Khaled's balance must be increased by 780000 euroTz", async function () {
    try {
      euroTzContractStorage = await euroTzContract.storage();

      const newKhaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );
      const newKhaledBalance = new BigNumber(
        newKhaledDetails.balance
      ).toNumber();

      console.log("Final Khaled's balance: ", newKhaledBalance, "euroTz");

      expect(newKhaledBalance).to.equal(khaledBalance + 780000);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("TotalSupply increase by 22300 euroTz", async function () {
    try {
      euroTzContractStorage = await euroTzContract.storage();

      const newTotalSupply = new BigNumber(
        euroTzContractStorage.totalSupply
      ).toNumber();

      console.log(
        "Final TotalSupply after minting operations: ",
        newTotalSupply,
        "euroTz"
      );

      expect(newTotalSupply).to.equal(totalSupply + 2230000);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
