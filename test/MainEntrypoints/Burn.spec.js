import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../conf/conf";
import errors from "../../errors/errors"

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Burn Operation Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;
  let totalSupply;
  let fredBalance;
  let fredDetails;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddress);

    euroTzContractStorage = await euroTzContract.storage();

    totalSupply = new BigNumber(euroTzContractStorage.totalSupply).toNumber();

    fredDetails = await euroTzContractStorage.balances.get(conf.fredAddress);
    fredBalance = new BigNumber(fredDetails.balance).toNumber();

    console.log("Total Supply before burn operation: ", totalSupply, "euroTz");
    console.log("Initial Fred balance: ", fredBalance, "euroTz");
    console.log("-------------------------------------------------");
  });

  it("Non Admin tries to burn 25 euroTz from Fred's account", async function () {
    Tezos.importKey(conf.khaledPrivateKey);
    try {
      await euroTzContract.methods
        .burn(conf.fredAddress, 25)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e)
      }
    }
  });

  it("Admin burn 25 euroTz from Fred's account", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const tezosBurn = await euroTzContract.methods
        .burn(conf.fredAddress, 25)
        .send();

      await tezosBurn.confirmation();

      const tezosBurnHash = tezosBurn.hash;

      console.log("tezosBurnHash : ", tezosBurnHash);

      expect(tezosBurnHash).to.be.a("string").to.have.length(51);

    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
        
      } else {
        console.log("PublicNode Error ", e)
      }
    }
  });

  it("Fred's balance must be decreased by 25 euroTz", async function () {
    try {
      euroTzContractStorage = await euroTzContract.storage();

      fredDetails = await euroTzContractStorage.balances.get(conf.fredAddress);
      const newFredBalance = new BigNumber(fredDetails.balance).toNumber();

      console.log("Fred final balance: ", newFredBalance);

      expect(newFredBalance).to.equal(fredBalance - 25);

    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e)
      }
    }
  });

  it("Total supply decrease by 25 euroTz", async function () {
    try {
      euroTzContractStorage = await euroTzContract.storage();

      const newTotalSupply = new BigNumber(euroTzContractStorage.totalSupply).toNumber();

      console.log("Final TotalSupply: ", newTotalSupply, "euroTz");

      expect(newTotalSupply).to.equal(totalSupply - 25);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e)
      }
    }
  });
});
