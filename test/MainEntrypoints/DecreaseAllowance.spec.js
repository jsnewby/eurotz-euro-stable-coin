import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../conf/conf";
import errors from "../../errors/errors"

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: DecreaseAllowance Operation Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;
  let khaledBalance;
  let khaledDetails;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddress);

    euroTzContractStorage = await euroTzContract.storage();

    khaledDetails = await euroTzContractStorage.balances.get(conf.khaledAddress);
    khaledBalance = new BigNumber(khaledDetails.balance).toNumber();

    for (let [key, value] of Object.entries(khaledDetails.approvals)) {
      console.log(
        `Initial khaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
      );
    }

    console.log("Khaled initial balance: ", khaledBalance, "euroTz");
    console.log("-------------------------------------------------");
  });

  it("Khaled decrease allowance of random account by 13 euroTz", async function () {
    Tezos.importKey(conf.khaledSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .decreaseAllowance(13, conf.khaledAddress, conf.unknownAccount.pkh)
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("1. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(conf.khaledAddress);

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e)
      }
    }
  });

  it("Admin decrease random account allowance by 18 euroTz in khaled account's", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .decreaseAllowance(18, conf.khaledAddress, conf.unknownAccount.pkh)
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("2. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(conf.khaledAddress);

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e)
      }
    }
  });

  it("Super Transfer Agent tries to decrease unknown account allowance by 10000 euroTz which is greater thant amount already approved", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await euroTzContract.methods
        .decreaseAllowance(10000, conf.khaledAddress, conf.unknownAccount.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e)
      }
    }
  });

  it("Super Transfer Agent tries to decrease allowance of an unexistant account in approvals map", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await euroTzContract.methods
        .decreaseAllowance(90, conf.khaledAddress, conf.aliceAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e)
      }
    }
  });

});
