import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../conf/conf";
import errors from "../../errors/errors";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: ChangeAllowance Operation Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;
  let khaledBalance;
  let khaledDetails;
  const callIncreaseAllowance = "callIncreaseAllowance";
  const callDecreaseAllowance = "callDecreaseAllowance";

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddress);

    euroTzContractStorage = await euroTzContract.storage();

    khaledDetails = await euroTzContractStorage.balances.get(
      conf.khaledAddress
    );
    khaledBalance = new BigNumber(khaledDetails.balance).toNumber();

    for (let [key, value] of Object.entries(khaledDetails.approvals)) {
      console.log(
        `Initial khaledApprovals: ${key}: ${new BigNumber(
          value
        ).toNumber()} euroTz`
      );
    }

    console.log("Khaled initial balance: ", khaledBalance, "euroTz");
    console.log("-------------------------------------------------");
  });

  it("Khaled increase allowance of random account by 20 euroTz", async function () {
    Tezos.importKey(conf.khaledSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .changeAllowance(
          20,
          callIncreaseAllowance,
          conf.khaledAddress,
          conf.unknownAccount.pkh
        )
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("1. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin increase random account allowance by 45 euroTz in khaled account's", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .changeAllowance(
          45,
          callIncreaseAllowance,
          conf.khaledAddress,
          conf.unknownAccount.pkh
        )
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("2. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Super Transfer Agent increase random account allowance by 52 euroTz from khaled account's", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .changeAllowance(
          52,
          callIncreaseAllowance,
          conf.khaledAddress,
          conf.unknownAccount.pkh
        )
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("3. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Transfer Agent increase his allowance by 12 euroTz from khaled account", async function () {
    Tezos.importKey(conf.oussSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .changeAllowance(
          89,
          callIncreaseAllowance,
          conf.khaledAddress,
          conf.oussAddress
        )
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("4. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error:", e);
      }
    }
  });

  it("Transfer Agent tries to increase allowance of the random account by 90 euroTz from khaled account", async function () {
    Tezos.importKey(conf.oussSecretKey);

    try {
      await euroTzContract.methods
        .changeAllowance(
          90,
          callIncreaseAllowance,
          conf.khaledAddress,
          conf.unknownAccount.pkh
        )
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Super Transfer Agent tries to increase allowance of an unexistant account in approvals map", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await euroTzContract.methods
        .changeAllowance(
          90,
          callIncreaseAllowance,
          conf.khaledAddress,
          conf.superTransferAgent.pkh
        )
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Super Transfer Agent tries to increase random account allowance in khaled account by 0 euroTz", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await euroTzContract.methods
        .changeAllowance(
          0,
          callIncreaseAllowance,
          conf.khaledAddress,
          conf.unknownAccount.pkh
        )
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  /** ******** */

  it("Khaled decrease allowance of random account by 13 euroTz", async function () {
    Tezos.importKey(conf.khaledSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .changeAllowance(
          13,
          callDecreaseAllowance,
          conf.khaledAddress,
          conf.unknownAccount.pkh
        )
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("1. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin decrease random account allowance by 18 euroTz in khaled account's", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const tezosApprove = await euroTzContract.methods
        .changeAllowance(
          18,
          callDecreaseAllowance,
          conf.khaledAddress,
          conf.unknownAccount.pkh
        )
        .send();

      await tezosApprove.confirmation();

      const tezosApproveHash = tezosApprove.hash;

      console.log("2. tezosApproveHash : ", tezosApproveHash);

      euroTzContractStorage = await euroTzContract.storage();

      khaledDetails = await euroTzContractStorage.balances.get(
        conf.khaledAddress
      );

      for (let [key, value] of Object.entries(khaledDetails.approvals)) {
        console.log(
          `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
        );
      }

      expect(tezosApproveHash).to.be.a("string").to.have.length(51);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Super Transfer Agent tries to decrease unknown account allowance by 10000 euroTz which is greater thant amount already approved", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await euroTzContract.methods
        .changeAllowance(
          10000,
          callDecreaseAllowance,
          conf.khaledAddress,
          conf.unknownAccount.pkh
        )
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Super Transfer Agent tries to decrease allowance of an unexistant account in approvals map", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await euroTzContract.methods
        .changeAllowance(
          90,
          callDecreaseAllowance,
          conf.khaledAddress,
          conf.aliceAddress
        )
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
