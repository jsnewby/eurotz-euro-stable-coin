import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../conf/conf";
import errors from "../../errors/errors";

import { transferFeeless } from "../../feelessOperations/transferFeeless";
import { getXTZBalance } from "../../Utils/getXTZBalance";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Transfer Feeless Operation Testing", function () {
  let euroTzContractStorage;
  let euroTzContract;
  let fredBalance;
  let fredDetails;
  let fredNonce;
  let fredNonceBN;
  let oussBalance;
  let oussDetails;
  let fredXTZBalance;
  let adminXTZBalance;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.contractAddress);

    euroTzContractStorage = await euroTzContract.storage();

    fredDetails = await euroTzContractStorage.balances.get(conf.fredAddress);
    fredBalance = new BigNumber(fredDetails.balance).toNumber();

    // try {
    //   fredNonceBN = await euroTzContractStorage.nonces.get(conf.fredAddress);
    //   fredNonce = new BigNumber(fredNonceBN).toNumber();
    // } catch (e) {
    //   if ((e.status === 404) & (e.statusText === "Not Found")) {
    //     fredNonce = 0;
    //   } else {
    //     console.log("PublicNode Error ", e);
    //   }
    // }

    fredNonce = new BigNumber(fredDetails.nonce).toNumber();

    oussDetails = await euroTzContractStorage.balances.get(conf.oussAddress);
    oussBalance = new BigNumber(oussDetails.balance).toNumber();

    console.log("Initial ouss balance: ", oussBalance, "euroTz");
    console.log("Initial fred initial balance: ", fredBalance, "euroTz");

    fredXTZBalance = await getXTZBalance(conf.fredAddress);
    adminXTZBalance = await getXTZBalance(conf.adminAddress);

    console.log("Initial Fred XTZ balance: ", fredXTZBalance, "ꜩ");
    console.log("Initial Admin XTZ initial balance: ", adminXTZBalance, "ꜩ");
    console.log("-------------------------------------------------");
  });

  it("Fred tries to transfer a greater value than his balance", async function () {
    try {
      await transferFeeless(
        conf.fredAddress,
        conf.fredPublicKey,
        conf.fredPrivateKey,
        conf.oussAddress,
        fredBalance + 1000,
        conf.contractAddress
      );
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Fred tries to transfer 15 euroTz to Ouss by calling transferFeeless entrypoint", async function () {
    Tezos.importKey(conf.fredSecretKey);
    try {
      await transferFeeless(
        conf.fredAddress,
        conf.fredPublicKey,
        conf.fredPrivateKey,
        conf.oussAddress,
        15,
        conf.contractAddress
      );
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Fred tries to transfer 150 euroTz to Ouss with a wrong signature", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      await transferFeeless(
        conf.fredAddress,
        conf.fredPublicKey,
        // wrong secretKey
        conf.unknownAccount.sk,
        conf.oussAddress,
        150,
        conf.contractAddress
      );
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Fred tries to transfer 150 euroTz to Ouss with an invalid nonce", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      await transferFeeless(
        conf.fredAddress,
        conf.fredPublicKey,
        conf.fredPrivateKey,
        conf.oussAddress,
        150,
        conf.contractAddress,
        fredNonce + 1
      );
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Fred transfers 15 euroTz to Ouss, Admin will be the gas payer", async function () {
    Tezos.importKey(conf.adminSecretKey);
    try {
      const tezosTransferHash = await transferFeeless(
        conf.fredAddress,
        conf.fredPublicKey,
        conf.fredPrivateKey,
        conf.oussAddress,
        15,
        conf.contractAddress
      );

      console.log("tezosTransferHash : ", tezosTransferHash);

      expect(tezosTransferHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Ouss's balance must increase by 15 euroTz ", async function () {
    euroTzContractStorage = await euroTzContract.storage();

    oussDetails = await euroTzContractStorage.balances.get(conf.oussAddress);
    const newOussBalance = new BigNumber(oussDetails.balance).toNumber();

    console.log("Ouss's final balance: ", newOussBalance);

    expect(newOussBalance).to.equal(oussBalance + 15);
  });

  it("Fred's balance must decrease by 15 euroTz ", async function () {
    euroTzContractStorage = await euroTzContract.storage();

    fredDetails = await euroTzContractStorage.balances.get(conf.fredAddress);
    const newFredBalance = new BigNumber(fredDetails.balance).toNumber();

    console.log("Fred's final balance: ", newFredBalance);

    expect(newFredBalance).to.equal(fredBalance - 15);
  });

  it("Fred's XTZ balance must be the same before the Trasnfer Operation and the Admin XTZ balance decreased by the Operation fees", async function () {
    const newFredXTZBalance = await getXTZBalance(conf.fredAddress);
    const newAdminXTZBalance = await getXTZBalance(conf.adminAddress);

    console.log("-------------------------------------------------");
    console.log("Final Fred XTZ balance: ", newFredXTZBalance, "ꜩ");
    console.log("Final Admin XTZ initial balance: ", newAdminXTZBalance, "ꜩ");
    console.log(
      "Gas value of the Operation: ",
      adminXTZBalance - newAdminXTZBalance,
      "ꜩ"
    );
    console.log("-------------------------------------------------");

    expect(fredXTZBalance).to.equal(newFredXTZBalance);
    expect(newAdminXTZBalance).to.be.below(adminXTZBalance);
  });

  it("Fred transfers 12 euroTz to Ouss, TrasnferAgent will be the gas payer", async function () {
    Tezos.importKey(conf.oussSecretKey);
    try {
      const tezosTransferHash = await transferFeeless(
        conf.fredAddress,
        conf.fredPublicKey,
        conf.fredPrivateKey,
        conf.oussAddress,
        12,
        conf.contractAddress
      );

      console.log("tezosTransferHash : ", tezosTransferHash);

      expect(tezosTransferHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE CODE: ", e.errors[1].with.string);
        console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
