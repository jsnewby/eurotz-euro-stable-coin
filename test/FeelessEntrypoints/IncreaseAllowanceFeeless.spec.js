import { Tezos, TezosOperationError } from "@taquito/taquito";
import { BigNumber } from "bignumber.js";
import { expect } from "chai";

import conf from "../../conf/conf";
import errors from "../../errors/errors";

import { increaseAllowanceFeeless } from "../../feelessOperations/increaseAllowanceFeeless"


Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("EuroTz Smart Contract: Increase Allowance Operation Testing", function () {
    let euroTzContractStorage;
    let euroTzContract;
    let khaledBalance;
    let khaledDetails;

    before(async function () {
        euroTzContract = await Tezos.contract.at(conf.contractAddress);

        euroTzContractStorage = await euroTzContract.storage();

        khaledDetails = await euroTzContractStorage.balances.get(conf.khaledAddress);
        khaledBalance = new BigNumber(khaledDetails.balance).toNumber();

        for (let [key, value] of Object.entries(khaledDetails.approvals)) {
            console.log(
                `Initial khaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
            );
        }

        console.log("Khaled initial balance: ", khaledBalance, "euroTz");
    });

    it("Khaled increases allowance of random account by 13 euroTz from his account's balance", async function () {
        Tezos.importKey(conf.adminSecretKey);

        try {
            const tezosApproveHash = await increaseAllowanceFeeless(
                conf.khaledAddress,
                conf.khaledPublicKey,
                conf.khaledPrivateKey,
                conf.unknownAccount.pkh,
                13,
                conf.contractAddress
            );

            console.log("tezosApproveHash : ", tezosApproveHash);

            euroTzContractStorage = await euroTzContract.storage();

            khaledDetails = await euroTzContractStorage.balances.get(conf.khaledAddress);

            for (let [key, value] of Object.entries(khaledDetails.approvals)) {
                console.log(
                    `KhaledApprovals: ${key}: ${new BigNumber(value).toNumber()} euroTz`
                );
            }

            expect(tezosApproveHash).to.be.a("string");
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

    it("Khaled tries to increase allowance of a random account by 17 euroTz with wrong signature", async function () {
        Tezos.importKey(conf.adminSecretKey);

        try {
            await increaseAllowanceFeeless(
                conf.khaledAddress,
                conf.khaledPublicKey,
                // wrong secretKey => wrong signature
                conf.fredPrivateKey,
                conf.unknownAccount.pkh,
                17,
                conf.contractAddress
            );
        } catch (e) {
            if (e instanceof TezosOperationError) {
                console.log("MESSAGE CODE: ", e.errors[1].with.string);
                console.log("MESSAGE STATEMENT: ", errors[e.errors[1].with.string]);
            } else {
                console.log("PublicNode Error ", e)
            }
        }
    });

});
