const signalR = require("@aspnet/signalr");

async function getNewEvents() {
  try {
    const hubConnection = new signalR.HubConnectionBuilder()
      .withUrl("http://localhost:8080/tezosHub")
      .configureLogging(signalR.LogLevel.Information)
      .build();

    await hubConnection.start().catch((err) => console.error(err.toString()));

    await hubConnection.send("subscribe", {
      transactionAddresses: ["KT1Dti319yejwnYHbquKgDZuMZ7WhnExzgcu"],
      // delegationAddresses: ["all"],
      // originationAddresses: ["all"],
      // fromBlockLevel: 340076,
      blockHeaders: true,
    });

    hubConnection.on("transactions", (data) => {
      console.log("-----------------------------------");
      console.log("NEW TRANSACTION: ", data.transaction);
      console.log("-----------------------------------");
    });

    hubConnection.on("block_headers", (data) => {
      // console.log(data.block_header);
    });
  } catch (e) {
    console.log(`Error in request: ${e}`);
  }
}

getNewEvents();

// import axios from "axios";
// import { v4 as uuidv4 } from "uuid";
// import * as signalR from '@aspnet/signalr';

// async function getNewEvents() {
//   try {
//     const uuid = uuidv4();

//     const negotiate = await axios.get(
//       `https://taas-DalyIng.tezoslive.io/api/negotiate`,
//       {
//         headers: {
//           "x-tezos-live-userid": uuid,
//         },
//       }
//     );

//     console.log("negotiate: ", negotiate.status, " ", negotiate.statusText);

//     const subscribe = await axios.post(
//       `https://taas-DalyIng.tezoslive.io/api/subscribe`,
//       {
//         transactionAddresses: ["KT1ChsjhH4s9fYP3WBVR5wr7a9tZ33jHC5fE"],
//         userId: uuid,
//       },
//       {
//         headers: {
//           "Content-Type": "application/json",
//         },
//       }
//     );

//     console.log("subscribe: ", subscribe.status, " ", subscribe.statusText);

//     const hubConnection = new signalR.HubConnectionBuilder()
//       .withUrl(negotiate.data.url, {
//         accessTokenFactory: () => negotiate.data.accessToken,
//       })
//       .configureLogging(signalR.LogLevel.Information)
//       .build();

//     await hubConnection.start().catch((err) => console.error(err.toString()));

//     await hubConnection.send("subscribe", {
//       transactionAddresses: ["KT1ChsjhH4s9fYP3WBVR5wr7a9tZ33jHC5fE"],
//     });

//     hubConnection.on("transactions", (data) => {
//       console.log(data.transaction);
//     });

//     hubConnection.on("block_headers", (data) => {
//       console.log(data.block_header);
//     });
//   } catch (e) {
//     console.log(`Error in request: ${e}`);
//   }
// }

// getNewEvents();
