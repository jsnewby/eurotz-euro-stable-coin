import axios from "axios";

import conf from "../conf/conf";
import { getMintFeelessBondData } from "./packBondFeelessData/packBondMintFeelessData";
import { getTransferFeelessBondData } from "./packBondFeelessData/packBondTransferFeelessData";
import { getApproveFeelessBondData } from "./packBondFeelessData/packBondApproveFeelessData";
import { getBurnFeelessBondData } from "./packBondFeelessData/packBondBurnFeelessData";

export async function packDataApproveFeeless(
  nonce,
  owner,
  operator,
  contractAddress
) {
  try {
    const result = getApproveFeelessBondData(
      // nonce,
      nonce === null ? "None" : nonce, // null in case of unexistant user.
      owner,
      operator,
      contractAddress
    );

    const packedData = await axios.post(
      `${conf.remoteNodeRPC}/chains/main/blocks/head/helpers/scripts/pack_data`,
      {
        data: result.data,
        type: result.type,
        gas: "800000",
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return packedData.data.packed;
  } catch (e) {
    console.log(e);
  }
}

export async function packDataMintFeeless(
  period,
  nonce, // null in case of unexistant user.
  amount,
  to_,
  contractAddress
) {
  try {
    const result = getMintFeelessBondData(
      period,
      nonce === null ? "None" : nonce, // null in case of unexistant user.
      amount,
      to_,
      contractAddress
    );

    const packedData = await axios.post(
      `${conf.remoteNodeRPC}/chains/main/blocks/head/helpers/scripts/pack_data`,
      {
        data: result.data,
        type: result.type,
        gas: "800000",
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return packedData.data.packed;
  } catch (e) {
    console.log(e);
  }
}

export async function packDataTrasnferFeeless(
  token_id,
  nonce,
  from_,
  to_,
  contractAddress
) {
  try {
    const result = getTransferFeelessBondData(
      token_id,
      nonce,
      from_,
      to_,
      contractAddress
    );

    const packedData = await axios.post(
      `${conf.remoteNodeRPC}/chains/main/blocks/head/helpers/scripts/pack_data`,
      {
        data: result.data,
        type: result.type,
        gas: "800000",
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return packedData.data.packed;
  } catch (e) {
    console.log(e);
  }
}

export async function packDataBurnFeeless(
  token_id,
  nonce,
  from_,
  contractAddress
) {
  try {
    const result = getBurnFeelessBondData(
      token_id,
      nonce,
      from_,
      contractAddress
    );

    const packedData = await axios.post(
      `${conf.remoteNodeRPC}/chains/main/blocks/head/helpers/scripts/pack_data`,
      {
        data: result.data,
        type: result.type,
        gas: "800000",
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    return packedData.data.packed;
  } catch (e) {
    console.log(e);
  }
}
