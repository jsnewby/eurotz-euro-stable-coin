export function getBurnFeelessBondData(
  token_id,
  nonce,
  from_,
  contractAddress
) {
  const data = {
    prim: "Pair",
    args: [
      { int: token_id },
      {
        prim: "Pair",
        args: [
          { int: nonce },
          {
            prim: "Pair",
            args: [{ string: from_ }, { string: contractAddress }],
          },
        ],
      },
    ],
  };

  const type = {
    prim: "pair",
    args: [
      { prim: "nat" },
      {
        prim: "pair",
        args: [
          { prim: "nat" },
          { prim: "pair", args: [{ prim: "address" }, { prim: "address" }] },
        ],
      },
    ],
  };

  return {
    data,
    type,
  };
}
