export function getApproveFeelessBondData(
  nonce,
  owner,
  operator,
  contractAddress
) {
  const dataSome = {
    prim: "Pair",
    args: [
      { prim: "Some", args: [{ int: nonce }] },
      {
        prim: "Pair",
        args: [
          { string: owner },
          {
            prim: "Pair",
            args: [{ string: operator }, { string: contractAddress }],
          },
        ],
      },
    ],
  };

  const dataNone = {
    prim: "Pair",
    args: [
      { prim: "None" },
      {
        prim: "Pair",
        args: [
          { string: owner },
          {
            prim: "Pair",
            args: [{ string: operator }, { string: contractAddress }],
          },
        ],
      },
    ],
  };

  const type = {
    prim: "pair",
    args: [
      { prim: "option", args: [{ prim: "nat" }] },
      {
        prim: "pair",
        args: [
          { prim: "address" },
          { prim: "pair", args: [{ prim: "address" }, { prim: "address" }] },
        ],
      },
    ],
  };

  return {
    data: nonce === "None" ? dataNone : dataSome,
    type,
  };
}