export function getMintFeelessBondData(
  period,
  nonce, // null in case of unexistant user.
  amount,
  to_,
  contractAddress
) {
  const dataSome = {
    prim: "Pair",
    args: [
      { int: period },
      {
        prim: "Pair",
        args: [
          { prim: "Some", args: [{ int: nonce }] },
          {
            prim: "Pair",
            args: [
              { int: amount },
              {
                prim: "Pair",
                args: [{ string: to_ }, { string: contractAddress }],
              },
            ],
          },
        ],
      },
    ],
  };

  const dataNone = {
    prim: "Pair",
    args: [
      { int: period },
      {
        prim: "Pair",
        args: [
          { prim: "None" },
          {
            prim: "Pair",
            args: [
              { int: amount },
              {
                prim: "Pair",
                args: [{ string: to_ }, { string: contractAddress }],
              },
            ],
          },
        ],
      },
    ],
  };

  const type = {
    prim: "pair",
    args: [
      { prim: "nat" },
      {
        prim: "pair",
        args: [
          { prim: "option", args: [{ prim: "nat" }] },
          {
            prim: "pair",
            args: [
              { prim: "nat" },
              {
                prim: "pair",
                args: [{ prim: "address" }, { prim: "address" }],
              },
            ],
          },
        ],
      },
    ],
  };

  return {
    data: nonce === "None" ? dataNone : dataSome,
    type,
  };
}
