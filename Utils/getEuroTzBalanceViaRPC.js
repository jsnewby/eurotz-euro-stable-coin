import axios from "axios";

const blake = require("blakejs");
const bs58check = require("bs58check");

const prefix = {
  expr: new Uint8Array([13, 44, 64, 27]),
};

const remoteNodeRPC = "https://carthagenet.SmartPy.io";

function hex2buf(hex) {
  return new Uint8Array(
    hex.match(/[\da-f]{2}/gi).map(function (h) {
      return parseInt(h, 16);
    })
  );
}

function b58encode(payload, prefix) {
  const n = new Uint8Array(prefix.length + payload.length);
  n.set(prefix);
  n.set(payload, prefix.length);
  return bs58check.encode(Buffer.from(n, "hex"));
}

function encodeExpr(value) {
  const blakeHash = blake.blake2b(hex2buf(value), null, 32);
  const encodedKey = b58encode(blakeHash, prefix.expr);
  return encodedKey;
}

export async function getEuroTzBalance(bigMapID, address) {
  try {
    const packedData = await axios.post(
      `${remoteNodeRPC}/chains/main/blocks/head/helpers/scripts/pack_data`,
      {
        data: { string: address },
        type: { prim: "address" },
        gas: "800000",
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    const expr = encodeExpr(packedData.data.packed);

    let userBalance;

    try {
      const value = await axios.get(
        `${remoteNodeRPC}/chains/main/blocks/head/context/big_maps/${bigMapID}/${expr}`
      );
      userBalance = parseInt(value.data.args[1].args[0].int, 10);
    } catch (e) {
      if (e.response.status === 404 && e.response.statusText === "Not Found") {
        userBalance = 0;
      } else {
        throw e;
      }
    }

    console.log("Balance: ", userBalance);
    return userBalance;
  } catch (e) {
    throw e;
  }
}
