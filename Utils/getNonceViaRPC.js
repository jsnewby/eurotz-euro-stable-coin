const axios = require("axios");
const blake = require("blakejs");
const bs58check = require("bs58check");

const prefix = {
  expr: new Uint8Array([13, 44, 64, 27]),
};

const remoteNodeRPC = "https://carthagenet.SmartPy.io";

function hex2buf(hex) {
  return new Uint8Array(
    hex.match(/[\da-f]{2}/gi).map(function (h) {
      return parseInt(h, 16);
    })
  );
}

function b58encode(payload, prefix) {
  const n = new Uint8Array(prefix.length + payload.length);
  n.set(prefix);
  n.set(payload, prefix.length);
  return bs58check.encode(Buffer.from(n, "hex"));
}

function encodeExpr(value) {
  const blakeHash = blake.blake2b(hex2buf(value), null, 32);
  const encodedKey = b58encode(blakeHash, prefix.expr);
  return encodedKey;
}

export async function getNonce(bigMapID, address) {
  try {
    const packedData = await axios.post(
      `${remoteNodeRPC}/chains/main/blocks/head/helpers/scripts/pack_data`,
      {
        data: { string: address },
        type: { prim: "address" },
        gas: "800000",
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    const expr = encodeExpr(packedData.data.packed);

    let userNonce;

    try {
      const value = await axios.get(
        `${remoteNodeRPC}/chains/main/blocks/head/context/big_maps/${bigMapID}/${expr}`
      );
      userNonce = parseInt(value.data.args[1].args[1].int, 10);
    } catch (e) {
      if (e.response.status === 404 && e.response.statusText === "Not Found") {
        userNonce = 0;
      } else {
        throw e;
      }
    }

    return userNonce;
  } catch (e) {
    throw e;
  }
}
