import { Tezos } from "@taquito/taquito";

import conf from "../conf/conf";

const euroTz = require("eurotz-sign");

Tezos.setProvider({
  rpc: conf.remoteNodeRPC,
});

Tezos.importKey(conf.aliceSecretKey);

export async function transferFeeless(
  senderAccount,
  senderPublicKey,
  senderPrivateKey,
  receiverAccount,
  amount,
  contractAddress
) {
  try {
    const euroTzContract = await Tezos.contract.at(contractAddress);

    const senderNonce = await euroTz.getNonce(senderAccount);

    // Pack transfer operation
    const opBytes = await euroTz.pack(
      `${amount}`,
      `${senderNonce}`,
      senderAccount,
      receiverAccount,
      contractAddress
    );

    // Sign operation bytes
    const opSign = await euroTz.sign(opBytes, senderPrivateKey);

    const tezosErc20Transfer = await euroTzContract.methods
      .transferFeeless(opBytes, senderPublicKey, opSign)
      .send();

    console.log("Waiting for the transaction to be included...");

    await tezosErc20Transfer.confirmation();

    console.log("tezosErc20Transfer.hash: ", tezosErc20Transfer.hash);

    return tezosErc20Transfer.hash;
  } catch (e) {
    console.log(e);
    throw e;
  }
}

transferFeeless(
  conf.khaledAddress,
  conf.khaledPublicKey,
  conf.khaledPrivateKey,
  conf.aliceAddress,
  "455",
  conf.euroTzProdAddress
);
