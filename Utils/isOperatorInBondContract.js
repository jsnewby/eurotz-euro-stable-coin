import { Tezos, TezosOperationError } from "@taquito/taquito";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

/**
 * Check whether an address is an operator granted to transfer tokens on behalf of the owner.
 */

async function isOperator(contractAddress, owner, operator) {
  try {
    const bondContract = await Tezos.contract.at(contractAddress);
    const bondContractStorage = await bondContract.storage();
    let isOperator;
    let result;

    try {
      result = await bondContractStorage.operators.get({
        owner, // owenr tz1 address
        operator, // operator tz1 address to be checked
      });
      // if result is of type Symbol => isOperator = true
      // else isOperator = false
      isOperator = result ? true : false;
    } catch (e) {
      // if e.status === 404 && e.statusText === "Not Found" => the key doesn't exist in the operators bigMap
      // => The address passed as operator is not an operator.
      if (e.status === 404 && e.statusText === "Not Found") {
        isOperator = false;
      }
    }

    console.log("isOperator : ", isOperator);

    return isOperator;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log("MESSAGE: ", e.errors);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

isOperator(
  "KT1CNAei6a65eR1EcWwf5iCReKRBzPMxLoXf",
  "tz1RmD9igqvhQ4FkWw7GMQxxoenvHj6N478g",
  "KT1QKX4cNuv3VnJmxxSrHsmSv73RDbHDg9Wo"
);
