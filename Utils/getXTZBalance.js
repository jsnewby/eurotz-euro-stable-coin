import { Tezos } from "@taquito/taquito";

import conf from "../conf/conf";

Tezos.setProvider({
  rpc: conf.remoteNodeRPC
});

export async function getXTZBalance(address) {
  let addressBalance = await Tezos.tz.getBalance(address);

  // balance returned in MuTez, so we have to divide that by 10^6 to get the balance in Tez.
  addressBalance = addressBalance / 1000000;
  
  return addressBalance;
}