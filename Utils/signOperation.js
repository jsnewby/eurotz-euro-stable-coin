const sodium = require("libsodium-wrappers");
const bs58check = require("bs58check");

function hex2buf(hex) {
  return new Uint8Array(
    hex.match(/[\da-f]{2}/gi).map(function (h) {
      return parseInt(h, 16);
    })
  );
}

function mergebuf(b1, b2) {
  var r = new Uint8Array(b1.length + b2.length);
  r.set(b1);
  r.set(b2, b1.length);
  return r;
}

function b58encode(payload, prefix) {
  const n = new Uint8Array(prefix.length + payload.length);
  n.set(prefix);
  n.set(payload, prefix.length);
  return bs58check.encode(Buffer.from(n, "hex"));
}

function b58decode(enc, prefix) {
  return bs58check.decode(enc).slice(prefix.length);
}

const prefix = {
  edsk: new Uint8Array([43, 246, 78, 7]),
  edsig: new Uint8Array([9, 245, 205, 134, 18]),
};

function buf2hex(buffer) {
  const byteArray = new Uint8Array(buffer),
    hexParts = [];
  for (let i = 0; i < byteArray.length; i++) {
    let hex = byteArray[i].toString(16);
    let paddedHex = ("00" + hex).slice(-2);
    hexParts.push(paddedHex);
  }
  return hexParts.join("");
}

export async function signOperation(bytes, sk, wm) {
  await sodium.ready;
  var bb = hex2buf(bytes);

  if (typeof wm != "undefined") bb = mergebuf(wm, bb);

  const sig = sodium.crypto_sign_detached(
    sodium.crypto_generichash(32, bb), // can be replaced with blake.blake2b(bb, null, 32);
    b58decode(sk, prefix.edsk),
    "uint8array"
  );
  const edsig = b58encode(sig, prefix.edsig);
  const sbytes = bytes + buf2hex(sig);

  console.log("edsig: ", edsig);

  return {
    bytes: bytes,
    sig: sig,
    edsig: edsig,
    sbytes: sbytes,
  };
}
