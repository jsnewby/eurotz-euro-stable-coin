import axios from "axios";

import conf from "../conf/conf";

export async function packData(amount, nonce, from_, to_, contractAddress) {
  try {
    const data = {
      prim: "Pair",
      args: [
        { int: amount },
        {
          prim: "Pair",
          args: [
            { int: nonce },
            {
              prim: "Pair",
              args: [
                { string: from_ },
                {
                  prim: "Pair",
                  args: [{ string: to_ }, { string: contractAddress }],
                },
              ],
            },
          ],
        },
      ],
    };

    const type = {
      prim: "pair",
      args: [
        { prim: "int" },
        {
          prim: "pair",
          args: [
            { prim: "int" },
            {
              prim: "pair",
              args: [
                { prim: "address" },
                {
                  prim: "pair",
                  args: [{ prim: "address" }, { prim: "address" }],
                },
              ],
            },
          ],
        },
      ],
    };

    const packedData = await axios.post(
      `${conf.remoteNodeRPC}/chains/main/blocks/head/helpers/scripts/pack_data`,
      {
        data,
        type,
        gas: "800000",
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return packedData.data.packed;
  } catch (e) {
    console.log(e);
  }
}
