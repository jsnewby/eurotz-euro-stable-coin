const { Tezos } = require("@taquito/taquito");

const { BigNumber } = require("bignumber.js");

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

export async function getEuroTzBalance(userAddress) {
  try {
    var userBalance;

    const euroTzContract = await Tezos.contract.at(conf.contractAddress);

    const euroTzContractStorage = await euroTzContract.storage();
    console.log("euroTzContractStorage: ", euroTzContractStorage);

    const userDetails = await euroTzContractStorage.balances.get(userAddress);

    userBalance = new BigNumber(userDetails.balance).toNumber();

    console.log("userBalance: ", userBalance);
  } catch (e) {
    if (e.statusText === "Not Found" && e.status === 404) {
      userBalance = 0;
      console.log("User Not Found, so balance: ", userBalance);
    } else {
      console.log(e);
      throw e;
    }
  }
  return userBalance;
}
